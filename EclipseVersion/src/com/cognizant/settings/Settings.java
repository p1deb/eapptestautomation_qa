/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.settings;

import com.cognizant.support.KeyMap;
import java.util.Properties;

public class Settings<PropType> implements SettingsInf<PropType> {

    private Properties properties;

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public String getProperty(PropType prop) {
        return getProperties().getProperty(prop.toString());
    }

    @Override
    public String getProperty(PropType prop, String defaultValue) {
        return getProperties().getProperty(prop.toString(), defaultValue);
    }

    @Override
    public void setProperty(PropType prop, String val) {
        getProperties().setProperty(prop.toString(), val);
    }

    public void update() {
        for (Object key : properties.keySet()) {
            properties.put(key, KeyMap.replaceKeys(properties.getProperty(key.toString())));
        }

    }
}

/**
 *
 * @author 394173
 * @param <PropType>
 */
interface SettingsInf<PropType> {

    public String getProperty(PropType prop);

    public String getProperty(PropType prop, String defaultValue);

    public void setProperty(PropType prop, String val);

}
