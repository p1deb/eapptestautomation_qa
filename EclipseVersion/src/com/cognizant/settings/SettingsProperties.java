/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.settings;

/**
 *
 * @author 394173
 */
public class SettingsProperties {

    public enum ExecProperties {

        ProjectPath,
        ProjectName,
        TestRun,
        TestCase,
        Scenario,
        Browser,
        Release,
        TestSet,

    }

    public enum RunProperties {

        ExecutionMode,
        IterationMode,
        ThreadCount,
        RemoteGridURL,
        ScreenShotFor,
        TakeFullPageScreenShot,
        RerunTimes,
        ExecutionTimeOut,
        ExistingDriver,
        reportPerformanceLog;

        public enum QcDetails {

            qcTestsetLocation,
            qcTestsetName;
        }

    };

    public enum TMProperties {

        UpdateResultsToTM;

        public enum qcSettings {

            QCUrl,
            QCUserName,
            QCPassword,
            QCDomain,
            QCProject
        };

        public enum jiraZephyrSettings {

            JIRAZephyrUrl,
            JIRAZephyrUserName,
            JIRAZephyrPassword,
            JIRAZephyrProject
        };
    };

    public enum DriverProperties {

        GeckoDriverPath,
        ChromeDriverPath,
        IEDriverPath,
        EdgeDriverPath,
        FirefoxBinaryPath,
        UseMarionetteDriver;

    };

    public enum EmulatorProperties {

        Name,
        Driver,
        Size,
        Unit,
        UserAgent,
        AppiumURL
    };

}
