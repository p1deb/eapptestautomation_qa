/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.settings;

import com.cognizant.settings.SettingsProperties.RunProperties;
import com.cognizant.settings.SettingsProperties.RunProperties.QcDetails;

/**
 *
 * @author 394173
 */
public class RunSettings extends Settings<RunProperties> implements SettingsInf<RunProperties> {

    public String getProperty(QcDetails prop) {
        return getProperties().getProperty(prop.toString());
    }

}
