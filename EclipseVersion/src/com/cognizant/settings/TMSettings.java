/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.settings;

import com.cognizant.settings.SettingsProperties.TMProperties;

/**
 *
 * @author 394173
 */
public class TMSettings extends Settings<TMProperties> implements SettingsInf<TMProperties> {

    public String getProperty(TMProperties.jiraZephyrSettings prop) {
        return getProperties().getProperty(prop.toString());
    }

    public String getProperty(TMProperties.qcSettings prop) {
        return getProperties().getProperty(prop.toString());
    }

}
