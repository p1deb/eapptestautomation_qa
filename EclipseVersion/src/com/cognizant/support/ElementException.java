/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support;

/**
 *
 * @author 394173
 */
public class ElementException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public enum ExceptionType {

        Element_Not_Found,
        Element_Not_Visible,
        Element_Not_Enabled,
        Element_Not_Selected,
        Not_Found_on_Screen,
        Empty_Group;

        @Override
        public String toString() {
            switch (this) {
                case Element_Not_Found:
                    return "Seems Like the Element is Not Present/Found in the page Try Adding wait or heal it";
                case Element_Not_Visible:
                    return "Seems Like the Element is Not Visible or hidden at the moment";
                case Element_Not_Enabled:
                    return "Seems Like the Element is Not Enabled";
                case Element_Not_Selected:
                    return "Seems Like the Element is Not Selected";
                case Not_Found_on_Screen:
                    return " not Found on the Screen. ";
                case Empty_Group:
                    return " -- Object Group is Empty. ";
            }
            return "";
        }
    };

    public ElementException(ExceptionType type) {
        super(type.toString());
    }
}
