/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.support;

import com.cognizant.constants.FilePath;
import com.cognizant.spritz.soapui.SoapUtils;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 394173
 */
public class SoapConnector {

    static Map<String, String> soapProjects = new HashMap<>();

    public static Boolean loadProject(String key, String location) {
        if (key == null || key.trim().isEmpty()) {
            key = "*";
        }
        for (Map.Entry<String, String> entrySet : soapProjects.entrySet()) {
            String key1 = entrySet.getKey();
            String value = entrySet.getValue();
            if (key1.equals(key)) {
                if (value.equals(location)) {
                    return true;
                } else {
                    Logger.getLogger(SoapConnector.class.getName()).log(Level.SEVERE, "Key {0} already exists", key);
                    return false;
                }
            }
            if (value.equals(location)) {
                return true;
            }
        }
        soapProjects.put(key, location);
        return true;
    }

    public synchronized static Boolean executeTestSuite(String reportName, String projectKey, String testsuite, String outputLocation) {
        return SoapUtils.executeTestSuite(reportName, getProject(projectKey), testsuite, outputLocation);
    }

    public synchronized static Boolean executeAllTestSuite(String reportName, String projectKey, String outputLocation) {
        return SoapUtils.executeAllTestSuites(reportName, getProject(projectKey), outputLocation);
    }

    public synchronized static Boolean executeTestCase(String reportName, String projectKey, String testsuite, String testcase, String outputLocation) {
        return SoapUtils.executeTestCase(reportName, getProject(projectKey), testsuite, testcase, outputLocation);
    }

    private static String getProject(String key) {
        if (key == null || key.trim().isEmpty()) {
            key = "*";
        }
        String location = soapProjects.get(key);
        if (location == null) {
            throw new RuntimeException("Project Not Available for key " + key);
        }
        return location;
    }

    public static void killIt() {
        if (!soapProjects.isEmpty()) {
            SoapUtils.shutDownGracefully();
        }
        soapProjects.clear();
    }

    public static void init() {
        com.cognizant.spritz.soapui.utils.Config.toolLocation = FilePath.getSoapUIUtilPath() + File.separator;
    }
}
