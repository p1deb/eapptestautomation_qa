/**
 *  Cognizant Technology Solutions 2016. All rights reserved. *
 *
 * Except for open source or proprietary third party software components
 * embedded in this Cognizant proprietary software program ("Program"), this
 * Program is protected by copyright laws, international treaties and other
 * pending or existing intellectual property rights and statutes in India, the
 * United States and other countries. Except as expressly permitted by Cognizant
 * and its third party licensors, the Program may neither be used, reproduced,
 * transmitted, distributed or modified, either in whole nor in part, in any
 * manner or form whatsoever (including without limitation electronic,
 * mechanical, printing, photocopying, recording or otherwise), without the
 * prior, express, written consent and acknowledgment of Cognizant Technology
 * Solutions. Any violation may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law
 *
 */
package com.cognizant.support;

/**
 *
 * @author 389747
 */
/**
 *
 * @author 389747
 */
import com.cognizant.core.CommandControl;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FParser {

    CommandControl cc;
    static List<String> methods = new ArrayList<>();

    static {

        for (Method m : Functions.class.getDeclaredMethods()) {
            methods.add("="+m.getName());
        }
    }

    public static List<String> getFuncList() {
        return methods;
    }

    public FParser(CommandControl cc) {
        this.cc = cc;
    }

    public Object OP(String s) {

        String func = FN(s);
        s = s.replaceFirst(func, "");
        String[] params = {};
        if (s.length() >= 2) {
            String param = s.substring(1, s.lastIndexOf(")"));
            params = CSVMgr.Split(param);
        }

        return String.valueOf(EXE(func, params));
    }

    private Object EXE(String func, String[] params) {
        try {
            List<String> p = new ArrayList<>();
            for (String param : params) {
                p.add(RES(param));
            }
            Method actn = Functions.class.getDeclaredMethod(func, String[].class);
            return actn.invoke(Functions.class.newInstance(),
                    new Object[]{p.toArray(new String[params.length])});

        } catch (Exception ex) {
            Logger.getLogger(FParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private String RES(String arg) {
        arg = remQ(arg);
        if (arg.startsWith("=")) {
            if (isF(FN(arg.substring(1)))) {
                return String.valueOf(OP(arg.substring(1)));
            }
        } else if (isRV(arg)) {
            return RV(arg);
        }
        return arg;
    }

    private static String FN(String s) {
        return s.substring(0, s.indexOf("("));
    }

    private static boolean isF(String arg) {
        try {
            Method m = Functions.class.getDeclaredMethod(arg, String[].class);
            return m != null;
        } catch (Exception ex) {
            Logger.getLogger(FParser.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private static String remQ(String arg) {
        if (arg.startsWith("\"") && arg.endsWith("\"")) {
            return arg.substring(1, arg.lastIndexOf("\""));
        } else {
            return arg;
        }
    }

    public void main(String[] args) {
        String func = "Round(=Random(=Round(=Random(2))))";
        System.out.println(OP(func));
    }

    private static boolean isRV(String arg) {
        return arg.startsWith("%") && arg.endsWith("%");
    }

    private String RV(String arg) {
        return cc.getVar(arg);
    }
}
