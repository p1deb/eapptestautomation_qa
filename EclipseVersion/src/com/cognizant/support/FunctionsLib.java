/**
 *  Cognizant Technology Solutions 2016. All rights reserved. *
 *
 * Except for open source or proprietary third party software components
 * embedded in this Cognizant proprietary software program ("Program"), this
 * Program is protected by copyright laws, international treaties and other
 * pending or existing intellectual property rights and statutes in India, the
 * United States and other countries. Except as expressly permitted by Cognizant
 * and its third party licensors, the Program may neither be used, reproduced,
 * transmitted, distributed or modified, either in whole nor in part, in any
 * manner or form whatsoever (including without limitation electronic,
 * mechanical, printing, photocopying, recording or otherwise), without the
 * prior, express, written consent and acknowledgment of Cognizant Technology
 * Solutions. Any violation may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law
 *
 */
package com.cognizant.support;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author 389747
 */
public class FunctionsLib {

    public Object getDate(int dx) {
        return getDate(dx, "dd/MM/yyyy");
    }

    public Object getDate(int dx, String format) {
        SimpleDateFormat date = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, dx);
        return date.format(cal.getTime());
    }

    public Object getRound(Double val) {
        return Math.round(val);
    }

    public Object getRandom(Double from, Double to) {
        Random rn = new Random();
        return (from + rn.nextDouble() * (to - from));
    }

    public Object getRandom(Double len) {
        Random rn = new Random();
        Double mul = (Double) getPow(10d, len);
        return (mul * rn.nextDouble());
    }

    public Object getPow(Double a, Double b) {
        return Math.pow(a, b);
    }

    public Object getMin(Double a, Double b) {
        return Math.min(a, b);
    }

    public Object getMax(Double a, Double b) {
        return Math.max(a, b);
    }

}
