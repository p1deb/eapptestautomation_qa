/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support;

import java.util.LinkedList;
import java.util.Queue;

public class DataSource {

    public String Scenario;
    public String TestCase;
    public String IterationType;
    public int CurrentTestCaseIteration = 1;
    public Queue<Integer> Iterations = new LinkedList<>();

    private int StartIteration = 0;
    private int EndIteration = 0;

    boolean doneOnce = false;

    public Boolean IterationSetFlag = true;

    public DataSource(String scenario, String testcase, String iterationType) {
        this.Scenario = scenario;
        this.TestCase = testcase;
        this.IterationType = iterationType;

        this.parseIteration();
        Iterations.add(getStartIteration());
        CurrentTestCaseIteration = getStartIteration() - 1;
    }

    public boolean nextTestCaseItertation() {
        if (IterationType.equalsIgnoreCase("All")) {
            if (Iterations.isEmpty()) {
                return false;
            } else {
                CurrentTestCaseIteration = Iterations.poll();
                return true;
            }
        } else if (IterationType.equalsIgnoreCase("Bounded")) {

            CurrentTestCaseIteration++;
            while (CurrentTestCaseIteration <= getEndIteration()) {
                if (Iterations.contains(CurrentTestCaseIteration)) {
                    return true;
                } else {
                    System.out.println("Iteration Not Available in Data Sheet:" + CurrentTestCaseIteration);
                }
                CurrentTestCaseIteration++;
            }
            return false;

        } else {
            if (doneOnce) {
                return false;
            } else {
                CurrentTestCaseIteration++;
                doneOnce = true;
                return true;
            }

        }

    }

    public void parseIteration() {
        if (IterationType.equalsIgnoreCase("All")) {
            setStartIteration(1);
            setEndIteration(-1);
        } else if (IterationType.equalsIgnoreCase("Single")) {
            IterationSetFlag = false;
            setStartIteration(1);
            setEndIteration(1);
        } else {
            if (IterationType.matches("[1-9]\\d*:[1-9]\\d*")) {
                String splitVals[] = IterationType.split("\\:", 2);
                setStartIteration(Integer.parseInt(splitVals[0].trim()));
                setEndIteration(Integer.parseInt(splitVals[1].trim()));
                IterationType = "Bounded";
            } else if (IterationType.matches("[1-9]\\d*")) {
                int iter = Integer.parseInt(IterationType);
                setStartIteration(iter);
                setEndIteration(iter);
                IterationType = "Bounded";
            } else {
                System.out.println("Defaulting to Single Iteration");
                setStartIteration(1);
                setEndIteration(1);
                IterationType = "Single";
            }
        }
        if (getStartIteration() > getEndIteration() && getEndIteration() != -1) {
            setEndIteration(getStartIteration());
            IterationType = "Single";
        }

    }

    public int getEndIteration() {
        return EndIteration;
    }

    public void setEndIteration(int endIteration) {
        EndIteration = endIteration;
    }

    public int getStartIteration() {
        return StartIteration;
    }

    public void setStartIteration(int startIteration) {
        StartIteration = startIteration;
    }

}
