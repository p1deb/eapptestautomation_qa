/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support.OR;

import java.util.HashMap;
import java.util.Map;

public class ObjectRepo {
	String pageName;
	Map<String, ORObject> objectMap = new HashMap<>();

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public Map<String, ORObject> getObjectMap() {
		return objectMap;
	}

	public void setObjectMap(Map<String, ORObject> objectMap) {
		this.objectMap = objectMap;
	}

	public void addObject(String objectName, ORObject object) {
		objectMap.put(objectName, object);
	}

	public ORObject get(String objectName) {
		return objectMap.get(objectName);
	}
}
