/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support.OR;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ORObject {
	String objectName;
	String frameData;
	ArrayList<Property> properties=new ArrayList<>();

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getFrameData() {
		return frameData;
	}

	public void setFrameData(String frameData) {
		this.frameData = frameData;
	}

	public ArrayList<Property> getProperties() {
		return properties;
	}

	public void setProperties(ArrayList<Property> properties) {
		this.properties = properties;
	}

	public void addObjectProperty(String propertyName, String propertyValue,
			int preferenceIndex) {
		Property p = new Property();
		p.setPropertyIndex(preferenceIndex);
		p.setPropertyName(propertyName);
		p.setPropertyValue(propertyValue);
		properties.add(p);
		Collections.sort(properties, new preferenceCompare());
	}

}

class preferenceCompare implements Comparator<Property> {
	@Override
	public int compare(Property p1, Property p2) {
		if (p1.getPropertyIndex() > p2.getPropertyIndex()) {
			return 1;
		} else {
			return -1;
		}
	}
}