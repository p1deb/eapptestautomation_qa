/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support;

import com.cognizant.constants.CommonConstants;
import com.cognizant.constants.FilePath;
import com.cognizant.constants.SystemDefaults;
import com.cognizant.core.CommandControl;
import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Discovery {

    public static ArrayList<Class<?>> clazzInPackage = null;

    public static boolean Done = false;

    public static ArrayList<Class<?>> getClassesForPackage() {
        ArrayList<Class<?>> clazz = new ArrayList<>();
        clazz.addAll(getClassesFromPackageList());
        clazz.addAll(getClassesFromUserDefinedPackage());
        return clazz;
    }

    public static ArrayList<Class<?>> getClassesFromPackageList() {
        ArrayList<Class<?>> clazz = new ArrayList<>();
        for (String packageName : CommonConstants.packageList) {
            try {
                clazz.addAll(ClassFinder.getClasses(packageName));
            } catch (ClassNotFoundException | IOException ex) {
                Logger.getLogger(Discovery.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return clazz;
    }

    public static ArrayList<Class<?>> getClassesFromUserDefinedPackage() {
        ArrayList<Class<?>> classes = new ArrayList<>();
        try {

            File directory = new File(FilePath.getSpritzPath() + File.separatorChar + "userdefined");
            URL url = directory.toURI().toURL();
            URL[] urls = new URL[]{url};
            if (directory.exists()) {
                String[] files = directory.list();
                for (String file : files) {
                    if (file.endsWith(".class")) {
                        String className = file.substring(0, file.length() - 6);
                        try {
                            classes.add(new URLClassLoader(urls).loadClass(className));
                        } catch (ClassNotFoundException e) {
                            Logger.getLogger(Discovery.class.getName()).log(Level.SEVERE, null, e);
                            throw new RuntimeException("ClassNotFoundException loading " + className);
                        }
                    }
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Discovery.class.getName()).log(Level.SEVERE, null, ex);
        }
        return classes;
    }

    public static void discoverCommands() {
        if (!Done) {
            // [Discovering Classes for Commands]
            clazzInPackage = getClassesForPackage();
            Done = true;
        }
    }

    public static boolean runAMethodInClass(String mName, CommandControl inst) throws Throwable {
        if (!Done) {
            // [Discovering Classes for Commands]
            clazzInPackage = getClassesForPackage();
            Done = true;
        }

        for (Class<?> c : clazzInPackage) {
            MethodHandle handle = makeMh(c, mName);
            if (handle != null) {
                handle.invoke(c.getConstructor(CommandControl.class).newInstance(inst));
                return true;
            }
        }
        return false;

    }

    public static MethodHandle makeMh(Class<?> c, String mName) {
        MethodHandle mh = null;
        MethodType desc = MethodType.methodType(void.class);
        try {
            mh = MethodHandles.lookup().findVirtual(c, mName, desc);
        } catch (NoSuchMethodException | IllegalAccessException ex) {
            //method not in the class .,so what try another 
        }
        return mh;
    }

    public static HashMap<String, ArrayList<String>> getMethodMap() {
        SystemDefaults.getClassesFromJar.set(true);
        HashMap<String, ArrayList<String>> methodMap = new HashMap<>();
        ArrayList<Class<?>> clazzes = getClassesFromPackageList();
        ArrayList<String> webMethods = new ArrayList<>();
        ArrayList<String> imageMethods = new ArrayList<>();
        ArrayList<String> mobileMethods = new ArrayList<>();
        ArrayList<String> webserviceMethods = new ArrayList<>();
        for (Class<?> classs : clazzes) {
            Method[] method = classs.getMethods();
            for (Method m : method) {
                if (m.getParameterTypes().length == 0
                        && m.getReturnType() == void.class && !Modifier.isFinal(m.getModifiers())) {
                    if (isMobileAction(classs)) {
                        mobileMethods.add(m.getName());
                    } else if (isImageAction(classs)) {
                        imageMethods.add(m.getName());
                    } else if (isWebServiceAction(classs)) {
                        webserviceMethods.add(m.getName());
                    } else {
                        webMethods.add(m.getName());
                    }
                }
            }

        }

        methodMap.put("webMethods", webMethods);
        methodMap.put("imageMethods", imageMethods);
        methodMap.put("mobileMethods", mobileMethods);
        methodMap.put("webServiceMethods", webserviceMethods);
        return methodMap;
    }

    public static Map<String, Map<String, List<String>>> getAllMethodMap() {
        SystemDefaults.getClassesFromJar.set(true);
        Map<String, Map<String, List<String>>> methodMap = new LinkedHashMap<>();
        ArrayList<Class<?>> clazzes = getClassesFromPackageList();
        for (Class<?> classs : clazzes) {
            Method[] method = classs.getMethods();
            if (method.length > 0) {
                String packageName = classs.getPackage().getName();
                if (packageName.equals("com.cognizant.commands")) {
                    packageName = "Web";
                } else {
                    packageName = packageName.replace("com.cognizant.commands.", "");
                }
                if (!methodMap.containsKey(packageName)) {
                    methodMap.put(packageName, new LinkedHashMap<String, List<String>>());
                }
                List<String> methods = new ArrayList<>();
                for (Method m : method) {
                    if (m.getParameterTypes().length == 0
                            && m.getReturnType() == void.class && !Modifier.isFinal(m.getModifiers())) {
                        methods.add(m.getName());
                    }
                }
                if (!methods.isEmpty()) {
                    methodMap.get(packageName).put(classs.getSimpleName(), methods);
                }
            }

        }
        return methodMap;
    }

    public static ArrayList<String> getUserMethods() {
        ArrayList<String> userMethods = new ArrayList<>();
        ArrayList<Class<?>> clazzes = getClassesFromUserDefinedPackage();
        for (Class<?> classs : clazzes) {
            Method[] method = classs.getMethods();
            for (Method m : method) {
                if (m.getParameterTypes().length == 0
                        && m.getReturnType() == void.class && !Modifier.isFinal(m.getModifiers())) {
                    userMethods.add(m.getName());

                }
            }

        }
        return userMethods;
    }

    private static boolean isImageAction(Class<?> className) {
        return className.getName().contains("com.cognizant.commands.image");
    }

    private static boolean isMobileAction(Class<?> className) {
        return className.getName().contains("com.cognizant.commands.mobile");
    }

    private static boolean isWebServiceAction(Class<?> className) {
        return className.getName().contains("com.cognizant.commands.soapui");
    }

    public static void main(String[] args) {
        System.out.println(getAllMethodMap());
    }
}
