/**
 *  Cognizant Technology Solutions 2016. All rights reserved. *
 *
 * Except for open source or proprietary third party software components
 * embedded in this Cognizant proprietary software program ("Program"), this
 * Program is protected by copyright laws, international treaties and other
 * pending or existing intellectual property rights and statutes in India, the
 * United States and other countries. Except as expressly permitted by Cognizant
 * and its third party licensors, the Program may neither be used, reproduced,
 * transmitted, distributed or modified, either in whole nor in part, in any
 * manner or form whatsoever (including without limitation electronic,
 * mechanical, printing, photocopying, recording or otherwise), without the
 * prior, express, written consent and acknowledgment of Cognizant Technology
 * Solutions. Any violation may result in severe civil and criminal penalties,
 * and will be prosecuted to the maximum extent possible under the law
 *
 */
package com.cognizant.support;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 389747
 */
public class Functions extends FunctionsLib {

    public Object Date(String... args) {
        try {
            int paramLength = args.length,dx;
            switch (paramLength) {
                case 1:
          dx = Integer.parseInt((args[0]).split("\\.")[0]);
            return this.getDate(dx);
                case 2:
                   dx = Integer.parseInt((args[0]).split("\\.")[0]);
                    String format=args[1];
                    return this.getDate(dx,format);
            }
        } catch (Exception ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this.getDate(0);
    }

    public Object Round(String... args) {
        try {
            Double val = Double.parseDouble((args[0]));
            return this.getRound(val);
        } catch (Exception ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return args[0];
    }

    public Object Random(String... args) {
        try {
            int paramLength = args.length;
            switch (paramLength) {
                case 1:
                    Double len = Double.parseDouble(args[0]);
                    return this.getRandom(len);
                case 2:
                    Double from = Double.parseDouble(args[0]);
                    Double to = Double.parseDouble(args[1]);
                    return this.getRandom(from, to);
            }
        } catch (Exception ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public Object Pow(String... args) {
        try {
            Double a = Double.parseDouble(args[0]);
            Double b = Double.parseDouble(args[1]);
            return this.getPow(a, b);
        } catch (Exception ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public Object Min(String... args) {
        try {
            Double a = Double.parseDouble(args[0]);
            Double b = Double.parseDouble(args[1]);
            return this.getMin(a, b);
        } catch (Exception ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public Object Max(String... args) {
        try {
            Double a = Double.parseDouble(args[0]);
            Double b = Double.parseDouble(args[1]);
            return this.getMax(a, b);
        } catch (Exception ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public Object Concat(String... args) {
        try {
            return args[0] + args[1];
        } catch (Exception ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

}
