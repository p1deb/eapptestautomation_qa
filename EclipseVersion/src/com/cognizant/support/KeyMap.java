/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.support;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author 389747
 */
public class KeyMap {

    public static String replaceKeys(String in, Properties vMap) {
        String out = in;
        Pattern p = Pattern.compile("\\{(.+?)\\}");

        Matcher m = p.matcher(in);
        String s, key;
        while (m.find()) {
            s = m.group();
            key = m.group(1);
            if (vMap.containsKey(key)) {
                out = out.replace(s, vMap.getProperty(key));
            }
        }
        return out;
    }

    public static String replaceKeys(String in) {
        String out = in;
        Pattern p = Pattern.compile("\\$\\{(.+?)\\}");

        Matcher m = p.matcher(in);
        String s, key;
        while (m.find()) {
            s = m.group();
            key = m.group(1);

            if (System.getProperties().containsKey(key)) {
                out = out.replace(s, System.getProperty(key));
            } else if (System.getenv().containsKey(key)) {
                out = out.replace(s, System.getenv(key));
            } else {
                out = out.replace(s, key);
            }
        }
        return out;
    }
}

