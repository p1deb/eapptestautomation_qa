/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support;

import com.cognizant.constants.CommonConstants.Browser;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author 394173
 */
public class ThreadPool extends ThreadPoolExecutor {

    public Boolean doSelectiveThreading = false;

    public ThreadPool(int threadCount, long keepAliveTime, boolean isGridMode) {
        super(threadCount, threadCount, keepAliveTime, TimeUnit.MINUTES, new LinkedBlockingQueue<Runnable>());
        doSelectiveThreading = threadCount > 1 && !isGridMode;
    }

    Map<Runnable, Browser> browserPool = new HashMap<>();
    Queue<Runnable> IEList = new LinkedList<>();

    @Override
    protected synchronized void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);
        if (doSelectiveThreading) {
            if (browserPool.containsKey(r)) {
                browserPool.remove(r);
            }
            if (IEList.isEmpty()) {
                shutdown();
                System.out.println("Shutting Down Thread as there is no IE Browser to do SelectiveThreading");
            } else if (!browserPool.containsValue(Browser.IE)) {
                if (getActiveCount() < getCorePoolSize()) {
                    Runnable ieRun = IEList.remove();
                    execute(ieRun);
                    browserPool.put(ieRun, Browser.IE);
                }
            }
        }
    }

    public synchronized void execute(Runnable command, Browser browserName) {
        if (doSelectiveThreading) {
            if (browserPool.containsValue(browserName)
                    && browserName.equals(Browser.IE)) {
                IEList.add(command);
            } else {
                browserPool.put(command, browserName);
                execute(command);
            }
        } else {
            execute(command);
        }
    }

    public void shutdownExecution() {
        if (!doSelectiveThreading) {
            System.out.println("Shutting Down Thread as there is no need for SelectiveThreading");
            shutdown();
        }
    }

}
