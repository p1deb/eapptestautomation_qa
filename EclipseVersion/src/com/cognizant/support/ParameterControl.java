/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support;

import com.cognizant.data.DataAccess;
import java.util.ArrayList;
import java.util.HashMap;

public class ParameterControl {

    String Scenario;
    String TestCase;

    DataSource Source;

    HashMap<String, FieldData> paramMap = new HashMap<>();
    HashMap<String, String> countMap = new HashMap<>();

    ArrayList<FieldData> currentSet = new ArrayList<>();

    public ParameterControl(String scn, String flow, DataSource src) {
        this.Scenario = scn;
        this.TestCase = flow;
        this.Source = src;
    }

    public String get(String field, int time) {
        if (field.trim().isEmpty()) {
            return field;
        }
        if (field.startsWith("@")) {
            return field.substring(1, field.length());
        } else if (field.startsWith("%") || field.startsWith("=")) {
            return field;
        } else {
            if (paramMap.containsKey(field)) {
                if (paramMap.get(field).values.size() > time) {
                    return paramMap.get(field).values.get(time);
                }
                return paramMap.get(field).values.get(0);
            } else {
                System.out.println("No Input available for " + field);
                throw new InputException(InputException.ExceptionType.Input_Not_Found);
            }
        }
    }

    public void initData() throws UnCaughtException {
        DataAccess da = new DataAccess(Scenario, TestCase);
        ArrayList<String> fName = da.getFieldNames();
        ArrayList<String> CName = da.getLoopVals();

        if (!(fName == null) && !fName.isEmpty()) {
            paramMap = da.getParamData(fName, Source);
        }
        if (!(CName == null) && !CName.isEmpty()) {
            countMap = da.setLoop(CName, Source.Scenario, Source.TestCase, Source.CurrentTestCaseIteration);
        }
    }

    public void getReadyToIterate() {
        if (!currentSet.isEmpty()) {
            for (FieldData f : currentSet) {
                f.reset();
            }
        }
        currentSet = new ArrayList<FieldData>();
    }

    public void beReadyWith(String input) {
        FieldData fd = paramMap.get(input);
        if (fd != null) {
            if (!currentSet.contains(fd)) {
                currentSet.add(fd);
            }
        }
    }

    public int maxIteration() {
        int max = 1;
        if (!paramMap.isEmpty()) {
            for (FieldData f : paramMap.values()) {
                if (f != null && !f.values.isEmpty()) {
                    if (f.values.size() > max) {
                        max = f.values.size();
                    }
                }
            }
        }
        System.out.println("MAX:" + max);
        return max;

    }

    public void iterate() {
        if (!currentSet.isEmpty()) {
            for (FieldData f : currentSet) {
                f.next();
            }
        }
    }

    public DataSource getDataSource() {
        return Source;

    }

    public String getc(String field) {
        return countMap.get(field);
    }

    public void putData(String sheet, String column, String value, int subIteration) {
        String field = sheet + ":" + column;
        if (!paramMap.containsKey(field)) {
            paramMap.put(field, new FieldData());
        }
        if (paramMap.get(field).values.size() >= subIteration) {
            paramMap.get(field).values.set(subIteration - 1, value);
        } else {
            paramMap.get(field).values.add(value);
        }
    }

}
