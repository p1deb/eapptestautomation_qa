/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support;

/**
 * Enumeration to represent the status of the current test step
 *
 * @author Cognizant
 */
public enum Status {

    /**
     * Indicates that the outcome of a verification was not successful
     */
    FAIL,
    /**
     * Indicates a warning message
     */
    WARNING,
    /**
     * Indicates that the outcome of a verification was successful
     */
    PASS,
    /**
     * Indicates a step that is logged into the results for informational
     * purposes, along with an attached screen shot for reference
     */
    SCREENSHOT,
    /**
     * Indicates a message that is logged into the results for informational
     * purposes
     */
    DONE,
    /**
     * Indicates a debug-level message, typically used by automation developers
     * to troubleshoot any errors that may occur
     */
    DEBUG;

    @Override
    public String toString() {
        switch (this) {
            case DONE:
                return "DONE";
            case PASS:
                return "PASS";
            case FAIL:
                return "FAIL";
            case SCREENSHOT:
                return "SCREENSHOT";
            case DEBUG:
                return "DEBUG";
            case WARNING:
                return "WARNING";
        }
        return null;
    }

    public static Status getValue(Boolean value) {
        return value ? PASS : FAIL;
    }
}
