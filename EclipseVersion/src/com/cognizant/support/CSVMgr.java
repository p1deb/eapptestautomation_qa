/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.support;

import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CSVMgr {

    private final static String rx = "(?x),(?=([^\"] * \" [^\"] * \" )* [^\"] * $ )";

    public static HashMap<String, ArrayList<String[]>> getHashCSV(File csvFile, String where) {
        return getHashCSV(csvFile, new String[]{where});
    }

    public static HashMap<String, ArrayList<String[]>> getHashCSV(File csvFile, String[] where) {
        if (!csvFile.exists()) {
            return null;
        }
        HashMap<String, ArrayList<String[]>> map = new HashMap<>();
        try (CSVReader<String[]> csvParser = CSVReaderBuilder.newDefaultReader(new FileReader(csvFile))) {
            List<String[]> data = csvParser.readAll();
            String[] header = data.get(0);
            ArrayList<Integer> index = new ArrayList<>();
            map.put("#Headers", new ArrayList<String[]>());
            map.get("#Headers").add(header);
            data.remove(0);
            for (String _where : where) {
                index.add(Arrays.asList(header).indexOf(_where));
            }
            for (String[] values : data) {
                String Key = "";
                for (int i : index) {
                    Key += values[i];
                }
                if (map.get(Key) == null) {
                    map.put(Key, new ArrayList<String[]>());
                }
                map.get(Key).add(values);
            }

        } catch (IOException ex) {
            Logger.getLogger(CSVMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    public static List<String[]> getData(String File) {
        try (CSVReader<String[]> csvParser = CSVReaderBuilder.newDefaultReader(new FileReader(File))) {
            return csvParser.readAll();
        } catch (IOException ex) {
            Logger.getLogger(CSVMgr.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static void putData(String File, List<String[]> data) {
        try (CSVWriter<String[]> csvParser = CSVWriterBuilder.newDefaultWriter(new FileWriter(File))) {
            csvParser.writeAll(data);
        } catch (IOException ex) {
            Logger.getLogger(CSVMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static String[] Split(String line) {
        String[] out = line.split(rx, -1);
        return out;
    }
}
