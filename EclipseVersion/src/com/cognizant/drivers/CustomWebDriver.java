/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.drivers;

import com.cognizant.constants.CommonConstants.Browser;
import com.cognizant.constants.FilePath;
import com.cognizant.core.RunContext;
import com.cognizant.data.LoadSettings;
import com.cognizant.settings.SettingsProperties;
import com.cognizant.settings.SettingsProperties.EmulatorProperties;
import com.cognizant.support.UnCaughtException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

/**
 *
 * @author 394173
 */
public class CustomWebDriver {

    public static ArrayList<Map<String, String>> emulatorList = new ArrayList<>();
    final static int minWidth = 100, minHeight = 100;
    final static Float inchincm = 0.393701f;
    final static int resolution = Toolkit.getDefaultToolkit().getScreenResolution();
    final static Boolean Normal = true, Remote = false;

    public static void Load() {
        emulatorList = CustomWebDriver.getemulatorList();
    }

    @SuppressWarnings("unchecked")
    private static ArrayList<Map<String, String>> getemulatorList() {
        ArrayList<Map<String, String>> list = new ArrayList<>();
        try {
            File file = new File(FilePath.getEmulatorFilelocation());
            if (file.exists()) {
                list = new ObjectMapper().readValue(file, ArrayList.class);
            }
        } catch (IOException ex) {
            Logger.getLogger(CustomWebDriver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static Map<String, String> getEmulatorMap(String emulatorName) {
        for (Map<String, String> emulatorMap : emulatorList) {
            if (emulatorMap.get(EmulatorProperties.Name.toString()).equals(emulatorName)) {
                return emulatorMap;
            }
        }
        return null;
    }

    public static WebDriver getDriver(RunContext context)
            throws UnCaughtException {
        WebDriver driver = null;
        Boolean maximize = true;
        switch (context.Browser) {
            case FireFox:
                System.setProperty("webdriver.gecko.driver",
                        LoadSettings.getSettings().getGekcoDriverPath());
                if (LoadSettings.getSettings().useMarionette()) {
                    driver = new MarionetteDriver(Capabilities.getFirefoxCapability());
                } else {
                    driver = new FirefoxDriver(Capabilities.getFirefoxCapability());
                }
                break;
            case Chrome:
                System.setProperty("webdriver.chrome.driver",
                        LoadSettings.getSettings().getChromeDriverPath());
                driver = new ChromeDriver(Capabilities.getChromeCapability());
                break;
            case IE:
                System.setProperty("webdriver.ie.driver",
                        LoadSettings.getSettings().getInternetExplorerDriverPath());

                driver = new InternetExplorerDriver(Capabilities.getCapability(DesiredCapabilities.internetExplorer(), context.Browser.toString()));
                break;
            case Edge:
                System.setProperty("webdriver.edge.driver",
                        LoadSettings.getSettings().getEdgeDriverPath());
                driver = new EdgeDriver(Capabilities.getCapability(DesiredCapabilities.edge(), context.Browser.toString()));
                maximize = false;
                break;
            case HtmlUnit:
                driver = new ExtendedHtmlUnitDriver(BrowserVersion.CHROME);
                maximize = false;
                break;
            case PhantomJS:
                driver = new PhantomJSDriver();
                break;
            case Opera:
                driver = new OperaDriver(Capabilities.getCapability(DesiredCapabilities.operaBlink(), context.Browser.toString()));
                break;
            case Safari:
                driver = new SafariDriver(Capabilities.getCapability(DesiredCapabilities.safari(), context.Browser.toString()));
                break;
            case Emulator:
                maximize = false;
                driver = getEmulatorDriver(context, Normal);
                break;
            default:
                break;
        }
        if (driver != null && maximize) {
            driver.manage().window().maximize();
        }
        return driver;
    }

    private static WebDriver getAppiumDriver(String emulatorName, String url) {
        try {
            DesiredCapabilities cp = Capabilities.getCapability(new DesiredCapabilities(), emulatorName);

            return getResolvedAppiumDriver(new URL(url), cp);

        } catch (MalformedURLException ex) {
            Logger.getLogger(CustomWebDriver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static WebDriver getEmulatorDriver(RunContext context, Boolean type) throws UnCaughtException {
        WebDriver driver = null;
        Map<String, String> emulator = getEmulatorMap(context.BrowserName);
        if (emulator != null) {
            String driverName = emulator.get(SettingsProperties.EmulatorProperties.Driver.toString());
            Boolean isAppium = Browser.fromString(driverName).equals(Browser.Appium);
            String userAgentrUrl;
            if (isAppium) {
                userAgentrUrl = emulator.get(SettingsProperties.EmulatorProperties.AppiumURL.toString());
            } else {
                userAgentrUrl = emulator.get(SettingsProperties.EmulatorProperties.UserAgent.toString());
            }
            if (type.equals(Normal)) {
                driver = getDriver(Browser.fromString(driverName), context.BrowserName, userAgentrUrl);
            } else {
                driver = getRemoteDriver(Browser.fromString(driverName), context.BrowserName, userAgentrUrl);
            }
            if (!isAppium) {
                String sizeUnit = emulator.get(SettingsProperties.EmulatorProperties.Unit.toString());
                String size = emulator.get(SettingsProperties.EmulatorProperties.Size.toString());
                if (sizeUnit.equals("Inches")) {
                    setBrowserSizeByInches(driver, size);
                } else {
                    setBrowserSizeByPixel(driver, size);
                }
            }
        }
        return driver;
    }

    public static String getBrowserName(RunContext context) {
        if (Browser.fromString(context.BrowserName).equals(Browser.Emulator)) {
            Map<String, String> emulator = getEmulatorMap(context.BrowserName);
            if (emulator != null) {
                String driverName = emulator.get(SettingsProperties.EmulatorProperties.Driver.toString());
                return context.BrowserName + "-" + driverName;
            }
        }
        return context.BrowserName;
    }

    public static void setBrowserSizeByInches(WebDriver driver, String size) {
        if (driver != null) {
            if (size.matches("\\d*(.\\d*)(x|,| )\\d*(.\\d*)+")) {
                size = size.replaceFirst("(x|,| )", " ");
                String[] sizes = size.split(" ", 2);
                String Width = sizes[0];
                String Height = sizes[1];
                int width = 0, height = 0;
                if (Width != null && Width.trim().length() > 0) {
                    width = (int) (resolution * checkValue(Width) * inchincm);
                }
                if (Height != null && Height.trim().length() > 0) {
                    height = (int) (resolution * checkValue(Height) * inchincm);
                }
                if (width >= minWidth && height >= minHeight) {
                    driver.manage().window().setSize(new Dimension(width, height));
                }
            }
        }
    }

    public static void setBrowserSizeByPixel(WebDriver driver, String size) {
        if (driver != null) {
            if (size.matches("\\d*(x|,| )\\d*")) {
                size = size.replaceFirst("(x|,| )", " ");
                String[] sizes = size.split(" ", 2);
                driver.manage().window().setSize(new Dimension(Integer.parseInt(sizes[0]), Integer.parseInt(sizes[1])));
            }
        }
    }

    private static Float checkValue(String value) {
        try {
            return Float.valueOf(value);
        } catch (NumberFormatException ex) {
            Logger.getLogger(CustomWebDriver.class.getName()).log(Level.OFF, null, ex);
            return 0f;
        }

    }

    private static WebDriver getDriver(Browser browser, String emulator, String userAgentRUrl)
            throws UnCaughtException {
        WebDriver driver = null;
        switch (browser) {
            case FireFox:
                if (LoadSettings.getSettings().useMarionette()) {
                    driver = new MarionetteDriver(Capabilities.getFirefoxCapability(emulator, userAgentRUrl));
                } else {
                    driver = new FirefoxDriver(Capabilities.getFirefoxCapability(emulator, userAgentRUrl));
                }
                break;
            case Chrome:
                System.setProperty("webdriver.chrome.driver",
                        LoadSettings.getSettings().getChromeDriverPath());
                driver = new ChromeDriver(Capabilities.getChromeCapability(emulator, userAgentRUrl));
                break;
            case Appium:
                driver = getAppiumDriver(emulator, userAgentRUrl);
                break;
            default:
                throw new UnCaughtException(
                        "[Selenium Driver] [Unknown UserAgent/Browser requested]");
        }
        return driver;
    }

    private static WebDriver getRemoteDriver(Browser browser, String emulator, String profileString)
            throws UnCaughtException {
        DesiredCapabilities capabilities;
        WebDriver driver = null;
        try {
            switch (browser) {
                case FireFox:
                    capabilities = Capabilities.getFirefoxCapability(emulator, profileString);
                    capabilities.setCapability("marionette", LoadSettings.getSettings().useMarionette());
                    break;
                case Chrome:
                    capabilities = Capabilities.getChromeCapability(emulator, profileString);
                    break;
                default:
                    throw new UnCaughtException(
                            "[Selenium Driver] [Unknown UserAgent/Browser requested]");
            }
            URL url = new URL(LoadSettings.getSettings().getRunSettings()
                    .getProperty(SettingsProperties.RunProperties.RemoteGridURL));
            driver = new RemoteWebDriver(url, capabilities);

        } catch (MalformedURLException ex) {
            Logger.getLogger(CustomWebDriver.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        return driver;
    }

    public static WebDriver getRemoteDriver(RunContext context) throws UnCaughtException {
        DesiredCapabilities capabilities;
        WebDriver driver;
        try {
            switch (context.Browser) {
                case FireFox:
                    capabilities = Capabilities.getFirefoxCapability();
                    capabilities.setCapability("marionette", LoadSettings.getSettings().useMarionette());
                    break;
                case Chrome:
                    capabilities = Capabilities.getChromeCapability();
                    break;
                case IE:
                    capabilities = DesiredCapabilities.internetExplorer();
                    break;
                case HtmlUnit:
                    capabilities = DesiredCapabilities.htmlUnit();
                    break;
                case Opera:
                    capabilities = DesiredCapabilities.operaBlink();
                    break;
                case Safari:
                    capabilities = DesiredCapabilities.safari();
                    break;
                default:
                    return getEmulatorDriver(context, Remote);
            }
            URL url = new URL(LoadSettings.getSettings().getRunSettings()
                    .getProperty(SettingsProperties.RunProperties.RemoteGridURL));
            if (!"default".equalsIgnoreCase(context.BrowserVersion)) {
                capabilities.setVersion(context.BrowserVersion);
            }
            capabilities.setPlatform(context.Platform);
            capabilities = Capabilities.getCapability(capabilities, context.Browser.toString());
            driver = new RemoteWebDriver(url, capabilities);
            driver.manage().window().maximize();
        } catch (UnCaughtException | MalformedURLException ex) {
            Logger.getLogger(CustomWebDriver.class.getName()).log(Level.OFF, null, ex);
            throw new UnCaughtException("[Selenium Driver Exception] " + ex.getMessage());
        }
        return driver;
    }

    public static WebDriver getResolvedAppiumDriver(URL url, DesiredCapabilities cp) {

        if (!cp.getBrowserName().isEmpty()) {
            return new RemoteWebDriver(url, cp);
        } else if (System.getProperty("os.name").contains("Mac")) {
            if ("ANDROID".equalsIgnoreCase(Objects.toString(cp.getCapability("platformName"), "ANDROID"))) {
                return new io.appium.java_client.android.AndroidDriver((url), cp);
            } else {
                return new io.appium.java_client.ios.IOSDriver((url), cp);
            }
        } else {
            return new io.appium.java_client.android.AndroidDriver((url), cp);
        }

    }
}
