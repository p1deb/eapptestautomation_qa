/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.drivers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.openqa.selenium.remote.SessionId;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * extract selenium grid node information source:
 *
 */
public class GridInfoExtractor {

    private static final ObjectMapper mapper = new ObjectMapper();

    public static String[] getHostNameAndPort(String hostName, int port, SessionId session) {
        String[] hostAndPort = new String[2];
        String errorMsg = "Failed to acquire remote webdriver node and port info. Root cause: \n";

        try {
            HttpHost host = new HttpHost(hostName, port);
            CloseableHttpClient client = HttpClients.createSystem();
            URL sessionURL = new URL("http://" + hostName + ":" + port + "/grid/api/testsession?session=" + session);
            BasicHttpEntityEnclosingRequest r = new BasicHttpEntityEnclosingRequest("POST", sessionURL.toExternalForm());
            HttpResponse response = client.execute(host, r);
            String url = extractUrlFromResponse(response);
            if (url != null) {
                URL myURL = new URL(url);
                if ((myURL.getHost() != null) && (myURL.getPort() != -1)) {
                    hostAndPort[0] = myURL.getHost();
                    hostAndPort[1] = Integer.toString(myURL.getPort());
                }
            }
        } catch (Exception e) {
            Logger.getLogger(GridInfoExtractor.class.getName()).log(Level.SEVERE, null, errorMsg + e);
        }
        return hostAndPort;
    }

    private static String extractUrlFromResponse(HttpResponse resp) {
        StringBuilder s;
        try (BufferedReader rd = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()))) {
            s = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                s.append(line);
            }
            if (s.length() > 0) {
                Map<Object, Object> val = mapper.readValue(s.toString(), Map.class);
                if (!val.isEmpty()) {
                    return val.get("proxyId").toString();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(GridInfoExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
