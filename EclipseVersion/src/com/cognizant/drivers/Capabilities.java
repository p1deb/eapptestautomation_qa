/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.drivers;

import com.cognizant.constants.CommonConstants.Browser;
import com.cognizant.constants.FilePath;
import com.cognizant.data.LoadSettings;
import com.cognizant.settings.SettingsProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author 394173
 */
public class Capabilities {

    private static Object getPropertyValueAsDesiredType(String key, String value) {
        if (value != null && !value.isEmpty()) {
            if (value.toLowerCase().matches("(true|false)")) {
                return Boolean.valueOf(value);
            }
            if (value.matches("\\d+")) {
                return Integer.valueOf(value);
            }
            if (key.contains("loggingPrefs")) {
                return getLogPrefs(value);
            }
        }
        return value;
    }

    private static LoggingPreferences getLogPrefs(String value) {
        LoggingPreferences logs = new LoggingPreferences();
        try {
            ObjectMapper mapper = new ObjectMapper();
            HashMap<String, String> prefs = mapper.readValue(value, HashMap.class);
            for (String logType : prefs.keySet()) {
                logs.enable(logType, Level.parse(prefs.get(logType)));
            }
        } catch (IOException ex) {
            Logger.getLogger(Capabilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return logs;
    }

    public static DesiredCapabilities getCapability(DesiredCapabilities capabilities, String browserName) {
        Properties prop = LoadSettings.getSettings().loadSettingsFor("Capabilities" + File.separator + browserName + ".properties");
        if (prop != null) {
            for (Object key : prop.keySet()) {
                capabilities.setCapability(key.toString(), getPropertyValueAsDesiredType(key.toString(), prop.getProperty(key.toString())));
            }
        }
        return capabilities;
    }

    public static DesiredCapabilities getFirefoxCapability() {
        return getFirefoxCapability(Browser.FireFox.toString(), null);
    }

    public static DesiredCapabilities getFirefoxCapability(String browser, String userAgent) {
        DesiredCapabilities cap = Capabilities.getCapability(DesiredCapabilities.firefox(), browser);
        cap.setCapability(FirefoxDriver.PROFILE, Capabilities.getFireFoxProfile(userAgent));
        String binayPATH = LoadSettings.getSettings().getDriverSettings().getProperty(SettingsProperties.DriverProperties.FirefoxBinaryPath);
        if (binayPATH != null && !binayPATH.trim().isEmpty()) {
            cap.setCapability(FirefoxDriver.BINARY, binayPATH);
        }
        return cap;
    }

    public static DesiredCapabilities getChromeCapability() {
        return getChromeCapability(Browser.Chrome.toString(), null);
    }

    public static DesiredCapabilities getChromeCapability(String browser, String userAgent) {
        DesiredCapabilities cap = getCapability(DesiredCapabilities.chrome(), browser);
        cap.setCapability(ChromeOptions.CAPABILITY, Capabilities.getChromeOptions(userAgent));
        return cap;
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        File path = FilePath.getFireFoxAddOnPath();
        if (path.exists()) {
            try {
                profile.addExtension(FilePath.getFireFoxAddOnPath());
            } catch (Exception ex) {
                Logger.getLogger(Capabilities.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("Spritz Firefox Addon not present in the path");
        }
        return profile;
    }

    private static FirefoxProfile getFireFoxProfile(String userAgent) {
        FirefoxProfile profile = getFireFoxProfile();
        if (userAgent != null && !userAgent.trim().isEmpty()) {
            profile.setPreference("general.useragent.override", userAgent);
        }
        return profile;
    }

    private static ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        File path = FilePath.getChromeAddOnPath();
        if (path.exists()) {
            try {
                options.addExtensions(FilePath.getChromeAddOnPath());
            } catch (Exception ex) {
                Logger.getLogger(Capabilities.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("Spritz Chrome Addon not present in the path");
        }
        return options;
    }

    private static ChromeOptions getChromeOptions(String userAgent) {
        ChromeOptions cOption = getChromeOptions();
        if (userAgent != null && !userAgent.trim().isEmpty()) {
            cOption.addArguments("--user-agent=" + userAgent);
        }
        return cOption;
    }

}
