/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.drivers;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 *
 * @author 394173
 */
class ExtendedHtmlUnitDriver extends HtmlUnitDriver implements TakesScreenshot {

    public ExtendedHtmlUnitDriver(BrowserVersion version) {
        super(version);
        setJavascriptEnabled(true);
    }

    public ExtendedHtmlUnitDriver() {
    }

    public ExtendedHtmlUnitDriver(boolean enableJavascript) {
        super(enableJavascript);
    }

    public ExtendedHtmlUnitDriver(Capabilities capabilities) {
        super(capabilities);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        Rectangle rectangle = new Rectangle(0, 0, screenSize.width, screenSize.height);
        try {
            File ss = new File("image");
            ImageIO.write(new Robot().createScreenCapture(rectangle), "png", ss);
            return ((X) ss);
        } catch (AWTException ex) {
            Logger.getLogger(ExtendedHtmlUnitDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExtendedHtmlUnitDriver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
