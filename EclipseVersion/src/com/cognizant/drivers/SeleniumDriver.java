/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.drivers;

import com.cognizant.constants.CommonConstants;
import com.cognizant.constants.CommonConstants.Browser;
import com.cognizant.core.RunContext;
import com.cognizant.data.LoadSettings;
import static com.cognizant.drivers.CustomWebDriver.getEmulatorMap;
import com.cognizant.settings.SettingsProperties;
import com.cognizant.support.UnCaughtException;
import com.galenframework.browser.SeleniumBrowser;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import java.io.File;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Class to handle driver related operation
 *
 */
public class SeleniumDriver {

    public WebDriver driver;
    protected RunContext runContext;

    public void launchDriver(RunContext context) throws UnCaughtException {
        runContext = context;
        System.out.println("Launching " + runContext.BrowserName);
        try {
            if (LoadSettings.getSettings().isGridExecution()) {
                driver = CustomWebDriver.getRemoteDriver(context);
            } else {
                driver = CustomWebDriver.getDriver(context);
            }
            if (driver == null) {
                throw new UnCaughtException("Unknown Browser Requested -[ " + runContext.BrowserName + " ]");
            } else {
                System.out.println(runContext.BrowserName + " Launched");
                openDefaultUrl();
            }
        } catch (UnCaughtException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
            throw new UnCaughtException("[Selenium Driver Exception] Cannot Initiate Browser " + ex.getMessage());
        }
    }

    public void launchDriver(String browser) throws UnCaughtException {
        RunContext context = new RunContext();
        context.BrowserName = browser;
        context.Browser = Browser.fromString(browser);
        context.Platform = Platform.getCurrent();
        context.BrowserVersion = "default";
        launchDriver(context);
    }

    private void openDefaultUrl() {
    }

    public void RestartBrowser() throws UnCaughtException {
        StopBrowser();
        StartBrowser(runContext.BrowserName);
    }

    public void StopBrowser() {
        try {
            driver.quit();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
        }
        driver = null;
    }

    public void StartBrowser(String b) throws UnCaughtException {
        StopBrowser();
        launchDriver(b);
    }

    public void closeBrowser() {
        if (driver != null) {
            try {
//            printLogs();
                driver.quit();
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, "Couldn't Kill the Driver");
            }
            driver = null;
        }
    }

    private void printLogs() {
        Logs logs = driver.manage().logs();
        for (String logtype : logs.getAvailableLogTypes()) {
            System.out.println("LogType : " + logtype);
            LogEntries logEntries = logs.get(logtype);
            for (LogEntry logEntry : logEntries) {
                System.out.println(logEntry.getMessage());
            }
        }
    }

    public String getCurrentBrowser() {
        return runContext.BrowserName;
    }

    public String getDriverName(String browserName) {
        try {
            Map<String, String> emulator = getEmulatorMap(browserName);
            if (emulator != null) {
                String driverName = emulator.get(SettingsProperties.EmulatorProperties.Driver.toString());
                if (driverName != null && driverName.equalsIgnoreCase(CommonConstants.Browser.Appium.toString())) {
                    return com.cognizant.drivers.Capabilities.getCapability(
                            new DesiredCapabilities(), runContext.BrowserName).getBrowserName();
                }
                return driverName;
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
        }
        return browserName;
    }

    public Boolean isAlive() {
        try {
            driver.manage();
            return true;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, " Driver is Dead");
            return false;
        }
    }

    public File createScreenShot() {
        if (driver == null) {
            System.err.println("Report Driver[" + runContext.BrowserName + "]  is not initialized");
        } else if (isAlive()) {
            if (alertPresent()) {
                System.err.println("Couldn't take ScreenShot Alert Present in the page");
            } else if (driver instanceof MobileDriver || driver instanceof ExtendedHtmlUnitDriver) {
                return ((TakesScreenshot) (driver)).getScreenshotAs(OutputType.FILE);
            } else {
                return new SeleniumBrowser(driver).createScreenshot();
            }
        } else {
            System.err.println("Couldn't take ScreenShot Driver is closed or connection is lost wih driver");
        }
        return null;
    }

    private boolean alertPresent() {
        try {
            driver.switchTo().alert();
            driver.switchTo().defaultContent();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String getBrowserVersion() {
        if (runContext.BrowserVersion.equalsIgnoreCase("default")) {
            if (!runContext.Browser.equals((Browser.HtmlUnit))) {
                String browser_version;
                Capabilities cap;
                if (driver instanceof ExtendedHtmlUnitDriver) {
                    cap = ((ExtendedHtmlUnitDriver) driver).getCapabilities();
                } else if (driver instanceof MobileDriver) {
                    cap = ((RemoteWebDriver) driver).getCapabilities();
                    Object pV = cap.getCapability("platformVersion");
                    return pV == null ? "" : pV.toString();
                } else {
                    cap = ((RemoteWebDriver) driver).getCapabilities();
                }
                String browsername = cap.getBrowserName();
                // This block to find out IE Version number
                if ("internet explorer".equalsIgnoreCase(browsername)) {
                    String uAgent = (String) ((JavascriptExecutor) driver).executeScript("return navigator.userAgent;");
                    //uAgent return as "MSIE 8.0 Windows" for IE8
                    if (uAgent.contains("MSIE") && uAgent.contains("Windows")) {
                        browser_version = uAgent.substring(uAgent.indexOf("MSIE") + 5, uAgent.indexOf("Windows") - 2);
                    } else if (uAgent.contains("Trident/7.0")) {
                        browser_version = "11.0";
                    } else {
                        browser_version = "0.0";
                    }
                } else {
                    //Browser version for Firefox and Chrome
                    browser_version = cap.getVersion();
                }
                if (browser_version.contains(".") && browser_version.length() > 5) {
                    return browser_version.substring(0, browser_version.indexOf("."));
                } else {
                    return browser_version;
                }
            }
        }
        return runContext.BrowserVersion;
    }

    public String getNodeIP() {
//        String ip = LoadSettings.getSettings().getRunSettings()
//                .getProperty(SettingsProperties.RunProperties.RemoteGridURL);
//        String[] details = GridInfoExtractor.getHostNameAndPort(null, port, ((RemoteWebDriver) driver).getSessionId());
        //Work in Progress
        return null;
    }

    public String getPlatformName() {

        if (runContext.Platform.equals(Platform.ANY) || runContext.Platform.equals(Platform.getCurrent())) {
            Capabilities cap;
            if (driver instanceof ExtendedHtmlUnitDriver) {
                cap = ((ExtendedHtmlUnitDriver) driver).getCapabilities();
            } else if (driver instanceof MobileDriver) {
                cap = ((RemoteWebDriver) driver).getCapabilities();
                Object platf = cap.getCapability("platformName");
                if (platf != null && !platf.toString().isEmpty()) {
                    return platf.toString();
                } else {
                    return (driver instanceof AndroidDriver) ? "Android" : "IOS";
                }
            } else {
                cap = ((RemoteWebDriver) driver).getCapabilities();
            }
            Platform p = cap.getPlatform();
            if (p.name().equals(Platform.VISTA.name()) || p.name().equals(Platform.XP.name())
                    || p.name().equals(Platform.WINDOWS.name()) || p.name().equals(Platform.WIN8.name())) {
                switch (p.getMajorVersion() + "." + p.getMinorVersion()) {
                    case "5.1":
                        return "XP";
                    case "6.0":
                        return "VISTA";
                    case "6.1":
                        return "WIN7";
                    case "6.2":
                        return "WIN8";
                    case "6.3":
                        return "WIN8.1";
                    default:
                        return p.name();
                }
            } else {
                return p.toString();
            }
        } else if (runContext.PlatformValue.equals("WINDOWS")) {
            return "WIN";
        } else {
            return runContext.PlatformValue;
        }
    }

}
