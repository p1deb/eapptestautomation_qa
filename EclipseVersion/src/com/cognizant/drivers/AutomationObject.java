/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.drivers;

import com.cognizant.constants.ObjectProperty;
import com.cognizant.constants.SystemDefaults;
import com.cognizant.data.DataAccess;
import com.cognizant.data.ObjectRepository;
import com.cognizant.support.OR.ORObject;
import com.cognizant.support.OR.ObjectRepo;
import com.cognizant.support.OR.Property;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author 394173
 */
public class AutomationObject {

    WebDriver Driver;

    String PageKey;
    String ObjectKey;
    Boolean Condition;
    Boolean waitAndFind = false;
    private Integer waitTime;

    public static ConcurrentHashMap<String, ObjectRepo> objectMap = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<String, ObjectRepo> mobileObjectMap = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<String, Map<String, List<Map<String, String>>>> iobjectMap = new ConcurrentHashMap<>();

    public static HashMap<String, Map<String, Map<String, String>>> dynamicValue = new HashMap<>();
    public static HashMap<String, String> globalDynamicValue = new HashMap<>();

    public AutomationObject() {
    }

    public AutomationObject(WebDriver Driver) {
        this.Driver = Driver;
    }

    public static void updateObjectMap() {
        objectMap = ObjectRepository.loadOR();
        mobileObjectMap = ObjectRepository.loadMOR();
        iobjectMap = DataAccess.getIObjects();
    }

    public static List<Map<String, String>> getIObject(String pageKey, String objectKey) {
        return iobjectMap.get(pageKey).get(objectKey);
    }

    public ObjectRepo getORObject(String pageName) {
        return objectMap.containsKey(pageName) ? objectMap.get(pageName) : mobileObjectMap.get(pageName);
    }

    /**
     *
     * @param objectKey ObjectName in pageKey in OR
     * @param pageKey PageName in OR
     * @return
     */
    public WebElement findElement(String objectKey, String pageKey) {
        return findElement(objectKey, pageKey, false);
    }

    /**
     *
     * @param element Driver or WebElement
     * @param objectKey ObjectName in pageKey in OR
     * @param pageKey PageName in OR
     * @return
     */
    public WebElement findElement(SearchContext element, String objectKey, String pageKey) {
        return findElement(element, objectKey, pageKey, false);
    }

    public WebElement findElement(String objectKey, String pageKey, String Attribute) {
        return findElement(objectKey, pageKey, Attribute, false);
    }

    public WebElement findElement(SearchContext element, String objectKey, String pageKey, String Attribute) {
        return findElement(element, objectKey, pageKey, Attribute, false);
    }

    public WebElement findElement(String objectKey, String pageKey, Boolean condition) {
        return findElement(Driver, objectKey, pageKey, condition);
    }

    public WebElement findElement(SearchContext element, String objectKey, String pageKey, Boolean condition) {
        PageKey = pageKey;
        ObjectKey = objectKey;
        Condition = condition;
        return getElementFromList(findElement(element, getORObject(pageKey).get(
                objectKey)));
    }

    public WebElement findElement(String objectKey, String pageKey, String Attribute, Boolean condition) {
        return findElement(Driver, objectKey, pageKey, Attribute, condition);
    }

    public WebElement findElement(SearchContext element, String objectKey, String pageKey, String Attribute, Boolean condition) {
        PageKey = pageKey;
        ObjectKey = objectKey;
        Condition = condition;
        return getElementFromList(findElements(element,
                getORObject(pageKey).get(objectKey), Attribute));
    }

    public List<WebElement> findElements(String objectKey, String pageKey) {
        return findElements(objectKey, pageKey, false);
    }

    public List<WebElement> findElements(String objectKey, String pageKey, String Attribute) {
        return findElements(objectKey, pageKey, Attribute, false);
    }

    public List<WebElement> findElements(String objectKey, String pageKey, Boolean condition) {
        return findElements(Driver, objectKey, pageKey, condition);
    }

    public List<WebElement> findElements(String objectKey, String pageKey, String Attribute, Boolean condition) {
        return findElements(Driver, objectKey, pageKey, Attribute, condition);
    }

    public List<WebElement> findElements(SearchContext element, String objectKey, String pageKey) {
        return findElements(element, objectKey, pageKey, false);
    }

    public List<WebElement> findElements(SearchContext element, String objectKey, String pageKey, String Attribute) {
        return findElements(element, objectKey, pageKey, Attribute, false);
    }

    public List<WebElement> findElements(SearchContext element, String objectKey, String pageKey, Boolean condition) {
        PageKey = pageKey;
        ObjectKey = objectKey;
        Condition = condition;
        return findElement(element, getORObject(pageKey).get(objectKey));
    }

    public List<WebElement> findElements(SearchContext element, String objectKey, String pageKey, String Attribute, Boolean condition) {
        PageKey = pageKey;
        ObjectKey = objectKey;
        Condition = condition;
        return findElements(element, getORObject(pageKey).get(objectKey), Attribute);
    }

    private synchronized List<WebElement> findElement(SearchContext element, ORObject orObject) {
        return findElements(element, orObject, null);
    }

    private WebElement getElementFromList(List<WebElement> elements) {
        return elements != null && !elements.isEmpty() ? elements.get(0) : null;
    }

    private synchronized List<WebElement> findElements(SearchContext element, ORObject orObject, String attribute) {
        if (orObject != null) {
            long startTime = System.nanoTime();
            switchFrame(orObject.getFrameData());
            List<WebElement> elements = getElements(element, orObject.getProperties(), attribute);
            long stopTime = System.nanoTime();
            if (elements != null) {
                System.out.println("Time taken for finding Object '" + orObject.getObjectName() + "' " + (stopTime - startTime) / 1000000 + " ms");
            } else {
                System.out.println("Couldn't find Object '" + orObject.getObjectName() + "' in stipulated Time '" + getWaitTime() + "'");
            }
            return elements;
        }
        return null;
    }

    private void switchFrame(String frameData) {
        try {
            if (frameData != null && !frameData.trim().isEmpty()) {
                Driver.switchTo().defaultContent();
                if (frameData.trim().matches("[0-9]+")) {
                    Driver.switchTo().frame(Integer.parseInt(frameData.trim()));
                } else {
                    WebDriverWait wait = new WebDriverWait(Driver,
                            SystemDefaults.waitTime.get());
                    wait.until(ExpectedConditions
                            .frameToBeAvailableAndSwitchToIt(frameData));
                }

            }
        } catch (Exception ex) {
            //Error while switching to frame
        }
    }

    private synchronized List<WebElement> getElements(final SearchContext context, final List<Property> properties, final String attribute) {
        try {
            WebDriverWait wait = new WebDriverWait(Driver, getWaitTime());
            return wait.until(new ExpectedCondition<List<WebElement>>() {
                @Override
                public List<WebElement> apply(WebDriver driver) {
                    List<WebElement> elements = null;
                    for (Property property : properties) {
                        if (elements != null && !elements.isEmpty()) {
                            break;
                        }
                        if (!property.getPropertyValue().trim().isEmpty() && (attribute == null || property.getPropertyName().equals(attribute))) {
                            String tag = property.getPropertyName();
                            String value = getRuntimeValue(property.getPropertyValue());
                            switch (tag) {
                                case ObjectProperty.Name:
                                    elements = context.findElements(By.name(value));
                                    break;
                                case ObjectProperty.Id:
                                    elements = context.findElements(By.id(value));
                                    break;
                                case ObjectProperty.ClassName:
                                    if (value.contains(" ")) {
                                        elements = context.findElements(By.xpath("//*[@class='" + value + "']"));
                                    } else {
                                        elements = context.findElements(By.className(value));
                                    }
                                    break;
                                case ObjectProperty.LinkText:
                                    elements = context.findElements(By.linkText(value));
                                    break;
                                case ObjectProperty.RXpath:
                                case ObjectProperty.Xpath:
                                    elements = context.findElements(By.xpath(value));
                                    break;
                                case ObjectProperty.Css:
                                    elements = context.findElements(By.cssSelector(value));
                                    break;
                                //For Mobile Android & IOS
                                case ObjectProperty.UiAutomator:
                                    if (context instanceof AndroidDriver) {
                                        elements = ((AndroidDriver) context).findElementsByAndroidUIAutomator(value);
                                    } else {
                                        elements = ((IOSDriver) context).findElementsByIosUIAutomation(value);
                                    }
                                    break;
                                case ObjectProperty.Accessibility:
                                    if (context instanceof AndroidDriver) {
                                        elements = ((AndroidDriver) context).findElementsByAccessibilityId(value);
                                    } else {
                                        elements = ((IOSDriver) context).findElementsByAccessibilityId(value);
                                    }
                                    break;
                            }

                        }
                    }
                    if (elements != null && !elements.isEmpty()) {
                        return elements;
                    }
                    return null;
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
        }
        return null;
    }

    private String getRuntimeValue(String value) {
        if (Condition) {
            for (String Key : globalDynamicValue.keySet()) {
                value = value.replaceAll(Key, globalDynamicValue.get(Key));
            }
        }
        if (dynamicValue.containsKey(PageKey)
                && dynamicValue.get(PageKey).containsKey(ObjectKey)) {
            for (String Key : dynamicValue.get(PageKey).get(ObjectKey).keySet()) {
                value = value.replaceAll(Key,
                        dynamicValue.get(PageKey).get(ObjectKey).get(Key));
            }
        }

        return value;
    }

    public void setWaitAndFind(Boolean waitAndFind) {
        this.waitAndFind = waitAndFind;
    }

    public void setDriver(WebDriver driver) {
        Driver = driver;

    }

    /**
     * Get Object Details
     *
     * @param page
     * @return
     */
    public Map<String, WebElement> findAllElementsFromPage(String page) {
        return findElementsByRegex(".*", page);
    }

    public Map<String, WebElement> findElementsByRegex(String regexObject, String page) {
        if (page == null || page.trim().isEmpty()) {
            throw new RuntimeException("Page Name is empty please give a valid pageName");
        }
        if (objectMap.get(page) == null && mobileObjectMap.get(page) == null) {
            throw new RuntimeException("Page [" + page + "] is not available in ObjectRepository");
        }
        Map<String, WebElement> elementList = new HashMap<>();
        ArrayList<String> objects = getObjectList(page, regexObject);
        if (objects != null) {
            for (String objectName : objects) {
                WebElement element = findElement(objectName, page);
                if (element != null) {
                    elementList.put(objectName, element);
                }
            }
        }
        return elementList;
    }

    public ArrayList<String> getObjectList(String page, String regexObject) {
        ArrayList<String> results = new ArrayList<>();
        Set<String> keys;
        if (objectMap.get(page) != null) {
            keys = objectMap.get(page).getObjectMap().keySet();
        } else {
            keys = mobileObjectMap.get(page).getObjectMap().keySet();
        }
        Iterator<String> ite = keys.iterator();
        while (ite.hasNext()) {
            String candidate = ite.next();
            if (candidate.matches(regexObject)) {
                results.add(candidate);
            }
        }
        if (results.isEmpty()) {
            return null;
        } else {
            return results;
        }
    }

    public String getObjectProperty(String pageName, String objectName, String propertyName) {
        if (objectMap.containsKey(pageName)) {
            if (objectMap.get(pageName).getObjectMap().containsKey(objectName)) {
                for (Property property : objectMap.get(pageName).get(objectName).getProperties()) {
                    if (property.getPropertyName().equals(propertyName)) {
                        return property.getPropertyValue();
                    }
                }
            }
        }
        return null;
    }

    public String getMobileObjectProperty(String pageName, String objectName, String propertyName) {
        if (mobileObjectMap.containsKey(pageName)) {
            if (mobileObjectMap.get(pageName).getObjectMap().containsKey(objectName)) {
                for (Property property : mobileObjectMap.get(pageName).get(objectName).getProperties()) {
                    if (property.getPropertyName().equals(propertyName)) {
                        return property.getPropertyValue();
                    }
                }
            }
        }
        return null;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }

    private int getWaitTime() {
        return this.waitTime != null ? this.waitTime : SystemDefaults.elementWaitTime.get();
    }

}
