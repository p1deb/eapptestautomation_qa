/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.drivers.AutomationObject;
import com.cognizant.support.Status;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 394173
 */
public class DynamicObject extends Command {
    // create your own function

    public DynamicObject(CommandControl cc) {
        super(cc);
    }

    public void setglobalObjectProperty() {
        AutomationObject.globalDynamicValue.put(Condition, Data);
        Report.updateTestLog(Action, "Set Global Object property for key '" + Condition + "' with value '" + Data + "'", Status.DONE);
    }

    public void setObjectProperty() {
        if (!AutomationObject.dynamicValue.containsKey(Reference)) {
            Map<String, Map<String, String>> Object = new HashMap<>();
            Map<String, String> property = new HashMap<>();
            property.put(Condition, Data);
            Object.put(ObjectName, property);
            AutomationObject.dynamicValue.put(Reference, Object);
        } else if (!AutomationObject.dynamicValue.get(Reference).containsKey(ObjectName)) {
            Map<String, String> property = new HashMap<>();
            property.put(Condition, Data);
            AutomationObject.dynamicValue.get(Reference).put(ObjectName, property);
        } else {
            AutomationObject.dynamicValue.get(Reference).get(ObjectName).put(Condition, Data);
        }
        Report.updateTestLog(Action, "Set Object property for key '" + Condition + "' with value '" + Data + "'", Status.DONE);
    }
}
