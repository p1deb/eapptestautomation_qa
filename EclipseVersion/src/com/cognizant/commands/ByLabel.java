/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.commands.galenCommands.Text;
import com.cognizant.core.CommandControl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author 394173
 */
public class ByLabel extends Command {

    CommandControl cc;//Commander

    public ByLabel(CommandControl cc) {
        super(cc);
        this.cc = cc;//Commander
    }

    public void setInputByLabel() {
        cc.Element = findInputElementByLabelTextByXpath();//Find the Element by your logic and then assign it to the Commander
        new Basic(cc).Set();//Create object for the necessary Class[Basic as it has the Set,Click etc and call you desired method[Set]
    }

    public void clickInputByLabel() {
        cc.Element = findInputElementByLabelTextByXpath();
        new Basic(cc).Click();
    }

    public void clickInputByText() {
        cc.Element = findInputElementByLabelTextByXpath(Data);//Another variant
        new Basic(cc).Click();
    }

    public void submitInputByLabel() {
        cc.Element = findInputElementByLabelTextByXpath();
        new Basic(cc).Submit();
    }

    public void assertElementTextByLabel() {
        cc.Element = findInputElementByLabelTextByXpath();
        new Text(cc).assertElementTextEquals();//Create object for the necessary Class[Text as it has the assertElementTextEquals etc and call you desired method[assertElementTextEquals]
    }

    public void assertElementTextContainsByLabel() {
        cc.Element = findInputElementByLabelTextByXpath();
        new Text(cc).assertElementTextContains();
    }

    private WebElement findInputElementByLabelTextByXpath() {
        return findInputElementByLabelTextByXpath(Element.getText());
    }

    private WebElement findInputElementByLabelTextByXpath(String text) {
        return Driver.findElement(By.xpath("//*[text()='" + text + "']/following::input[1]"));
    }
}
