/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.commands.mobile.nativ;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 389747
 */
public class Basic extends MobileNativeCommand {

    public Basic(CommandControl cc) {
        super(cc);
    }
    /**
     * method for clearing the element value on the screen
     *
     */

    public void clear() {
        try {
            if (Element != null) {
                (Element).clear();
                Report.updateTestLog(Action, "Cleared'" + ObjectName + "'", Status.PASS);
            } else {
                Report.updateTestLog(Action, "Unable clear '" + ObjectName + "'", Status.FAIL);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method for tapping the center of an element on the screen
     *
     * @see AppiumDriver#tap(int, org.openqa.selenium.WebElement, int)
     */
    public void tap() {
        try {
            if (Element != null) {
                int nof = this.getInt(Data, 0, 1);
                int time = this.getInt(Data, 1, 0);
                ((AppiumDriver) Driver).tap(nof, Element, time);
                Report.updateTestLog(Action, "Tapped on '" + ObjectName + "'", Status.PASS);
            } else {
                Report.updateTestLog(Action, "Unable tap on'" + ObjectName + "'", Status.FAIL);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method for tapping a position on the screen
     *
     * @see AppiumDriver#tap(int, int, int, int)
     */
    public void tapAt() {
        try {
            int nof = this.getInt(Data, 0, 1);
            int x = this.getInt(Data, 1, 10);
            int y = this.getInt(Data, 2, 10);
            int time = this.getInt(Data, 3, 1);
            ((AppiumDriver) Driver).tap(nof, x, y, time);
            Report.updateTestLog(Action, "Tapped at co-ordinates '" + x + "','" + y + "'", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method for "zooming in" on an element on the screen.
     *
     * @see AppiumDriver#zoom(org.openqa.selenium.WebElement)
     */
    public void zoom() {
        try {
            if (Element != null) {
                ((AppiumDriver) Driver).zoom(Element);
                Report.updateTestLog(Action, "Zoomed in '" + ObjectName + "'", Status.PASS);
            } else {
                Report.updateTestLog(Action, "Unable to Zoom '" + ObjectName + "'", Status.FAIL);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method for "zooming in" on an element on the screen.
     *
     * @see AppiumDriver#zoom(int, int)
     */
    public void zoomAt() {
        try {
            int x = this.getInt(Data, 0, 10);
            int y = this.getInt(Data, 1, 10);
            ((AppiumDriver) Driver).zoom(x, y);
            Report.updateTestLog(Action, "Zoomed at '" + x + "','" + y + "'", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method for pinching an element on the screen.
     *
     * @see AppiumDriver#pinch(org.openqa.selenium.WebElement)
     */
    public void pinch() {
        try {
            if (Element != null) {
                ((AppiumDriver) Driver).pinch(Element);
                Report.updateTestLog(Action, "Pinched '" + ObjectName + "'", Status.PASS);
            } else {
                Report.updateTestLog(Action, "Unable pinch'" + ObjectName + "'", Status.FAIL);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method for pinching an element on the screen.
     *
     * @see AppiumDriver#pinch(int, int)
     */
    public void pinchAt() {
        try {
            int x = this.getInt(Data, 0, 10);
            int y = this.getInt(Data, 1, 10);
            ((AppiumDriver) Driver).pinch(x, y);
            Report.updateTestLog(Action, "Pinched at'" + x + "','" + y + "'", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Scroll forward to the element which has a description or name which
     * contains the input text. The scrolling is performed on the first
     * scrollView present on the UI
     *
     * @see AppiumDriver#scrollTo(java.lang.String)
     */
    public void mScrollTo() {
        try {
            ((AppiumDriver) Driver).scrollTo(Data);
            Report.updateTestLog(Action, "Scrolled to object cointaining text'" + Data + "'", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Scroll forward to the element which has a description or name which
     * exactly matches the input text. The scrolling is performed on the first
     * scrollView present on the UI
     *
     * @see AppiumDriver#scrollToExact(java.lang.String)
     */
    public void mScrollToExact() {
        try {
            ((AppiumDriver) Driver).scrollToExact(Data);
            Report.updateTestLog(Action, "Scrolled to object with exact text'" + Data + "'", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Lock the device (bring it to the lock screen) for a given number of
     * seconds
     *
     * @see AppiumDriver#lockScreen(int)
     */
    public void lockScreen() {
        try {
            int time = this.getInt(Data, 5);
            ((AppiumDriver) Driver).lockScreen(time);
            Report.updateTestLog(Action, "Screen locked", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Switch context of the driver
     *
     * @see AppiumDriver#lockScreen(int)
     */
    public void switchContext() {
        try {
            ((MobileDriver) Driver).context(Data);
            if (Data.equals(((MobileDriver) Driver).getContext())) {
                Report.updateTestLog(Action, "Context switched to " + Data, Status.DONE);
            } else {
                Report.updateTestLog(Action, "Unable to swtich to context " + Data
                        + " , in " + ((MobileDriver) Driver).getContextHandles(), Status.FAIL);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            System.out.println(((MobileDriver) Driver).getContextHandles());
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    /**
     * Launch the app which was provided in the capabilities at session creation
     *
     * @see AppiumDriver#launchApp()
     */
    public void launchApp() {
        try {
            ((AppiumDriver) Driver).launchApp();
            Report.updateTestLog(Action, "Application launched", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Install an app on the mobile device
     *
     * @see AppiumDriver#launchApp()
     */
    public void installApp() {
        try {
            ((AppiumDriver) Driver).installApp(Data);
            Report.updateTestLog(Action, "Application Installed", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Toggle location services state
     *
     * @see AppiumDriver#toggleLocationServices()
     */
    public void toggleLocationServices() {
        try {
            ((AndroidDriver) Driver).toggleLocationServices();
            Report.updateTestLog(Action, "Location Service toggled", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Hide the software keyboard
     *
     * @see AppiumDriver#hideKeyboard()
     */
    public void hideKeyboard() {
        try {
            ((AppiumDriver) Driver).hideKeyboard();
            Report.updateTestLog(Action, "Keyboard hidden", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Reset the currently running app(given in capabilities) for this session
     *
     * @see AppiumDriver#hideKeyboard()
     */
    public void resetApp() {
        try {
            ((AppiumDriver) Driver).resetApp();
            Report.updateTestLog(Action, "Application reset successful", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Opens the openNotifications
     *
     * @see AppiumDriver#hideKeyboard()
     */
    public void openNotifications() {
        try {
            ((AndroidDriver) Driver).openNotifications();
            Report.updateTestLog(Action, "Notification Opened", Status.PASS);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Basic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
