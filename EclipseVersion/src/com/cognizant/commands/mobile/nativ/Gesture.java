/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.commands.mobile.nativ;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Dimension;

/**
 *
 * @author 389747
 */
public class Gesture extends MobileNativeCommand {

    public Gesture(CommandControl cc) {
        super(cc);
    }

    /**
     * swipe right(from left)
     */
    public void swipeRight() {
        try {
            if (Element != null) {
                ((MobileElement) Element).swipe(SwipeElementDirection.RIGHT, getInt(Data, 1000));
                Report.updateTestLog(Action, "Sucessfully swiped towards right", Status.DONE);
            } else {
                Dimension size = ((MobileDriver) Driver).manage().window().getSize();
                int startX, endX;
                if (Data != null && Data.contains(",")) {
                    startX = (int) (size.width * (getInt(Data, 0, 10) / 100d));
                    endX = (int) (size.width * (getInt(Data, 1, 80) / 100d));
                } else {
                    startX = (int) (size.width * 0.10);
                    endX = (int) (size.width * 0.80);
                }
                int Y = size.height / 2;
                ((MobileDriver) Driver).swipe(startX, Y, endX, Y, getInt(Data, 1000));
                Report.updateTestLog(Action, "Sucessfully swiped towards right", Status.DONE);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
            Logger.getLogger(Gesture.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * swipe left (from right)
     */
    public void swipeLeft() {
        try {
            if (Element != null) {
                ((MobileElement) Element).swipe(SwipeElementDirection.LEFT, getInt(Data, 1000));
                Report.updateTestLog(Action, "Sucessfully swiped towards left", Status.DONE);
            } else {
                Dimension size = ((MobileDriver) Driver).manage().window().getSize();
                int startX, endX;
                if (Data != null && Data.contains(",")) {
                    startX = (int) (size.width * (getInt(Data, 0, 80) / 100d));
                    endX = (int) (size.width * (getInt(Data, 1, 10) / 100d));
                } else {
                    startX = (int) (size.width * 0.80);
                    endX = (int) (size.width * 0.10);
                }
                int Y = size.height / 2;
                ((MobileDriver) Driver).swipe(startX, Y, endX, Y, getInt(Data, 1000));
                Report.updateTestLog(Action, "Sucessfully swiped towards left", Status.DONE);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
            Logger.getLogger(Gesture.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * swipe Down (from up)
     */
    public void swipeDown() {
        try {
            if (Element != null) {
                ((MobileElement) Element).swipe(SwipeElementDirection.DOWN, getInt(Data, 1000));
                Report.updateTestLog(Action, "Sucessfully swiped towards down", Status.DONE);
            } else {
                Dimension size = ((MobileDriver) Driver).manage().window().getSize();
                int startY, endY;
                if (Data != null && Data.contains(",")) {
                    startY = (int) (size.width * (getInt(Data, 0, 10) / 100d));
                    endY = (int) (size.width * (getInt(Data, 1, 80) / 100d));
                } else {
                    startY = (int) (size.width * 0.10);
                    endY = (int) (size.width * 0.80);
                }
                int X = size.width / 2;
                ((MobileDriver) Driver).swipe(X, startY, X, endY, getInt(Data, 1000));
                Report.updateTestLog(Action, "Sucessfully swiped towards down", Status.DONE);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
            Logger.getLogger(Gesture.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * swipe Up (from Down)
     */
    public void swipeUp() {
        try {
            if (Element != null) {
                ((MobileElement) Element).swipe(SwipeElementDirection.UP, getInt(Data, 1000));
                Report.updateTestLog(Action, "Sucessfully swiped towards down", Status.DONE);
            } else {
                Dimension size = ((MobileDriver) Driver).manage().window().getSize();
                int startY, endY;
                if (Data != null && Data.contains(",")) {
                    startY = (int) (size.width * (getInt(Data, 0, 80) / 100d));
                    endY = (int) (size.width * (getInt(Data, 1, 10) / 100d));
                } else {
                    startY = (int) (size.width * 0.80);
                    endY = (int) (size.width * 0.10);
                }
                int X = size.width / 2;
                ((MobileDriver) Driver).swipe(X, startY, X, endY, getInt(Data, 1000));
                Report.updateTestLog(Action, "Sucessfully swiped towards down", Status.DONE);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
            Logger.getLogger(Gesture.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
