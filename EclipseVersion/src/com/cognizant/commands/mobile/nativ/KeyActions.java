/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.commands.mobile.nativ;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Keys;

/**
 *
 * @author 389747
 */
public class KeyActions extends MobileNativeCommand {

    public KeyActions(CommandControl cc) {
        super(cc);
    }

    /**
     * press enter key
     */
    public void enter() {
        try {
            if (Element != null) {
                (Element).sendKeys(Keys.ENTER);
                Report.updateTestLog(Action, "Enter pressed", Status.PASS);
            } else {
                ((AndroidDriver) Driver).sendKeyEvent(AndroidKeyCode.KEYCODE_ENTER);
                Report.updateTestLog(Action, "Enter pressed", Status.PASS);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(KeyActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * press search key
     */
    public void search() {
        try {
            ((AndroidDriver) Driver).sendKeyEvent(AndroidKeyCode.KEYCODE_SEARCH);
            Report.updateTestLog(Action, "Search pressed", Status.PASS);

        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(KeyActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * press back key
     */
    public void back() {
        try {
            ((AndroidDriver) Driver).sendKeyEvent(AndroidKeyCode.BACK);
            Report.updateTestLog(Action, "Back pressed", Status.PASS);

        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(KeyActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * press home key
     */
    public void home() {
        try {
            ((AndroidDriver) Driver).sendKeyEvent(AndroidKeyCode.HOME);
            Report.updateTestLog(Action, "home pressed", Status.PASS);

        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(KeyActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * press home key
     */
    public void menu() {
        try {
            ((AndroidDriver) Driver).sendKeyEvent(AndroidKeyCode.MENU);
            Report.updateTestLog(Action, "Menu pressed", Status.PASS);

        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(KeyActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * press settings key
     */
    public void settings() {
        try {
            ((AndroidDriver) Driver).sendKeyEvent(AndroidKeyCode.SETTINGS);
            Report.updateTestLog(Action, "settings pressed", Status.PASS);

        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(KeyActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * press the given key in input
     */
    public void setKey() {
        try {
            Field f = AndroidKeyCode.class.getDeclaredField(Data);
            f.setAccessible(true);
            if (f.isAccessible()) {
                ((AndroidDriver) Driver).sendKeyEvent((f.getInt(null)));
                Report.updateTestLog(Action, "Key '" + Data + "' pressed", Status.DONE);
            } else {
                Report.updateTestLog(Action, "Key '" + Data + "'not accessible/available", Status.DEBUG);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(KeyActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
