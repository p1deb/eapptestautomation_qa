/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.commands.mobile.nativ;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 389747
 */
public class MobileNativeCommand extends Command {

    public MobileNativeCommand(CommandControl cc) {
        super(cc);
    }

    public int getInt(String Data, int i, int def) {
        try {
            return Integer.parseInt(Data.split(",")[i]);
        } catch (Exception ex) {
            Logger.getLogger(MobileNativeCommand.class.getName()).log(Level.WARNING, null, ex);
            return def;
        }
    }

    public int getInt(String Data, int def) {
        try {
            return Integer.parseInt(Data);
        } catch (Exception ex) {
            Logger.getLogger(MobileNativeCommand.class.getName()).log(Level.WARNING, null, ex);
            return def;
        }
    }

    public void waitfor(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException ex) {
            Logger.getLogger(MobileNativeCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
