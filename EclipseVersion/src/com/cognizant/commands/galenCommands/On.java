/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.Location;
import com.galenframework.specs.Side;
import com.galenframework.specs.SpecOn;
import galenWrapper.SpecValidation.SpecReader;
import java.util.List;

/**
 *
 * @author 394173
 */
public class On extends General {

    public On(CommandControl cc) {
        super(cc);
    }

    private void asssertElementOn(Side horizontal, Side vertical) {
        SpecOn spec = SpecReader.reader().getSpecOn(Condition, horizontal, vertical, Data);
        spec.setOriginalText(getMessage(horizontal, vertical, spec.getLocations()));
        validate(spec);
    }

    public void assertElementOnTopLeft() {
        asssertElementOn(Side.TOP, Side.LEFT);
    }

    public void assertElementOnTopRight() {
        asssertElementOn(Side.TOP, Side.RIGHT);
    }

    public void assertElementOnBottomLeft() {
        asssertElementOn(Side.BOTTOM, Side.LEFT);
    }

    public void assertElementOnBottomRight() {
        asssertElementOn(Side.BOTTOM, Side.RIGHT);
    }

    private String getMessage(Side horizontal, Side vertical, List<Location> locations) {
        String message = String.format("%s is On %s-%s of %s", ObjectName, horizontal.toString(), vertical.toString(), Condition);
        if (!locations.isEmpty()) {
            message += " over location" + Data;
        }
        return message;
    }
}
