/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.SpecText;
import galenWrapper.SpecValidation.SpecReader;
import galenWrapper.SpecValidation.SpecTitle;
import java.util.Arrays;

/**
 *
 * @author 394173
 */
public class Title extends General {

    public Title(CommandControl cc) {
        super(cc);
    }

    private void assertTitle(SpecText.Type type) {
        SpecTitle spec = SpecReader.reader().getSpecTitle(type, Data);
        spec.setOriginalText(getMessage(type));
        validate(spec, RelativeElement.None);
    }

    private void assertTitleI(SpecText.Type type) {
        SpecTitle spec = SpecReader.reader().getSpecTitle(type, Data.toLowerCase());
        spec.setOperations(Arrays.asList(new String[]{"lowercase"}));
        spec.setOriginalText(getMessage(type));
        validate(spec, RelativeElement.None);
    }

    public void assertTitleEquals() {
        assertTitle(SpecText.Type.IS);
    }

    public void assertTitleContains() {
        assertTitle(SpecText.Type.CONTAINS);
    }

    public void assertTitleStartsWith() {
        assertTitle(SpecText.Type.STARTS);
    }

    public void assertTitleEndsWith() {
        assertTitle(SpecText.Type.ENDS);
    }

    public void assertTitleMatches() {
        assertTitle(SpecText.Type.MATCHES);
    }

    public void assertTitleIEquals() {
        assertTitleI(SpecText.Type.IS);
    }

    public void assertTitleIContains() {
        assertTitleI(SpecText.Type.CONTAINS);
    }

    public void assertTitleIStartsWith() {
        assertTitleI(SpecText.Type.STARTS);
    }

    public void assertTitleIEndsWith() {
        assertTitleI(SpecText.Type.ENDS);
    }

    @Deprecated
    public void assertTitle() {
        assertTitleEquals();
    }

    @Deprecated
    public void verifyTitle() {
        assertTitleEquals();
    }

    private String getMessage(SpecText.Type type) {
        return String.format("%s's Title %s %s ", ObjectName, type.toString(), Data);
    }
}
