/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.SpecText;
import galenWrapper.SpecValidation.SpecReader;
import galenWrapper.SpecValidation.SpecUrl;
import java.util.Arrays;

/**
 *
 * @author 394173
 */
public class Url extends General {

    public Url(CommandControl cc) {
        super(cc);
    }

    private void assertUrl(SpecText.Type type) {
        SpecUrl spec = SpecReader.reader().getSpecUrl(type, Data);
        spec.setOriginalText(getMessage(type));
        validate(spec, RelativeElement.None);
    }

    private void assertUrlI(SpecText.Type type) {
        SpecUrl spec = SpecReader.reader().getSpecUrl(type, Data.toLowerCase());
        spec.setOperations(Arrays.asList(new String[]{"lowercase"}));
        spec.setOriginalText(getMessage(type));
        validate(spec, RelativeElement.None);
    }

    public void assertUrlEquals() {
        assertUrl(SpecText.Type.IS);
    }

    public void assertUrlContains() {
        assertUrl(SpecText.Type.CONTAINS);
    }

    public void assertUrlStartsWith() {
        assertUrl(SpecText.Type.STARTS);
    }

    public void assertUrlEndsWith() {
        assertUrl(SpecText.Type.ENDS);
    }

    public void assertUrlMatches() {
        assertUrl(SpecText.Type.MATCHES);
    }

    public void assertUrlIEquals() {
        assertUrlI(SpecText.Type.IS);
    }

    public void assertUrlIContains() {
        assertUrlI(SpecText.Type.CONTAINS);
    }

    public void assertUrlIStartsWith() {
        assertUrlI(SpecText.Type.STARTS);
    }

    public void assertUrlIEndsWith() {
        assertUrlI(SpecText.Type.ENDS);
    }

    @Deprecated
    public void assertCurrentURL() {
        assertUrlEquals();
    }

    @Deprecated
    public void verifyCurrentURL() {
        assertUrlEquals();
    }

    private String getMessage(SpecText.Type type) {
        return String.format("%s's Url %s %s ", ObjectName, type.toString(), Data);
    }

}
