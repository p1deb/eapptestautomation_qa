/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.Range;
import com.galenframework.specs.SpecAbove;
import com.galenframework.specs.SpecBelow;
import com.galenframework.specs.SpecLeftOf;
import com.galenframework.specs.SpecRightOf;
import galenWrapper.SpecValidation.SpecReader;

/**
 *
 * @author 394173
 */
public class Direction extends General {

    public Direction(CommandControl cc) {
        super(cc);
    }

    public void assertElementAbove() {
        SpecAbove spec = SpecReader.reader().getSpecAbove(Condition, Data);
        spec.setOriginalText(getMessage("above", spec.getRange()));
        validate(spec);
    }

    public void assertElementBelow() {
        SpecBelow spec = SpecReader.reader().getSpecBelow(Condition, Data);
        spec.setOriginalText(getMessage("below", spec.getRange()));
        validate(spec);
    }

    public void assertElementLeftOf() {
        SpecLeftOf spec = SpecReader.reader().getSpecLeftOf(Condition, Data);
        spec.setOriginalText(getMessage("left of", spec.getRange()));
        validate(spec);
    }

    public void assertElementRightOf() {
        SpecRightOf spec = SpecReader.reader().getSpecRightOf(Condition, Data);
        spec.setOriginalText(getMessage("right of", spec.getRange()));
        validate(spec);
    }

    private String getMessage(String direction, Range errorRate) {
        String message = String.format("%s is %s %s ", ObjectName, direction, Condition);
        if (errorRate != null && !errorRate.holds(0)) {
            message += " With Error rate " + errorRate.toString();
        }
        return message;
    }
}
