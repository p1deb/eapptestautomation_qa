/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.SpecContains;
import galenWrapper.SpecValidation.SpecReader;

/**
 *
 * @author 394173
 */
public class Contains extends General {

    public Contains(CommandControl cc) {
        super(cc);
    }

    public void assertElementContains(Boolean isPartly) {
        SpecContains spec = SpecReader.reader().getSpecContains(getElementsList(), isPartly);
        spec.setOriginalText(getMessage(isPartly));
        validate(spec, RelativeElement.WebElementList);
    }

    public void assertElementContains() {
        assertElementContains(false);
    }

    public void assertElementContainsPartly() {
        assertElementContains(true);
    }

    private String getMessage(Boolean isPartly) {
        String partly = isPartly ? " partly " : "";
        return String.format("%s %scontains %s", ObjectName, partly, Condition);
    }

}
