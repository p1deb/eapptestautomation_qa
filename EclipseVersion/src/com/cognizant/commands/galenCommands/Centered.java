/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.SpecCentered;
import galenWrapper.SpecValidation.SpecReader;

/**
 *
 * @author 394173
 */
public class Centered extends General {

    public Centered(CommandControl cc) {
        super(cc);
    }

    private void assertElementCentered(SpecCentered.Alignment alignment, SpecCentered.Location location) {
        SpecCentered spec = SpecReader.reader().getSpecCentered(Condition, Data, location, alignment);
        spec.setOriginalText(getMessage(alignment, location, spec.getErrorRate()));
        validate(spec);
    }

    public void assertElementCenteredAOn() {
        assertElementCentered(SpecCentered.Alignment.ALL, SpecCentered.Location.ON);
    }

    public void assertElementCenteredAInside() {
        assertElementCentered(SpecCentered.Alignment.ALL, SpecCentered.Location.INSIDE);
    }

    public void assertElementCenteredHOn() {
        assertElementCentered(SpecCentered.Alignment.HORIZONTALLY, SpecCentered.Location.ON);
    }

    public void assertElementCenteredHInside() {
        assertElementCentered(SpecCentered.Alignment.HORIZONTALLY, SpecCentered.Location.INSIDE);
    }

    public void assertElementCenteredVOn() {
        assertElementCentered(SpecCentered.Alignment.VERTICALLY, SpecCentered.Location.ON);
    }

    public void assertElementCenteredVInside() {
        assertElementCentered(SpecCentered.Alignment.VERTICALLY, SpecCentered.Location.INSIDE);
    }

    private String getMessage(SpecCentered.Alignment alignment, SpecCentered.Location location, int errorRate) {
        String message = String.format("%s is centered %s %s %s", ObjectName, alignment.toString(), location.toString(), Condition);
        if (Data != null && !Data.trim().isEmpty()) {
            message += " With Error rate " + errorRate;
        }
        return message;
    }

}
