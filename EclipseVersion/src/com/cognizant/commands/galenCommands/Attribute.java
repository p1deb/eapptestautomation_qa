/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.SpecText;
import galenWrapper.SpecValidation.SpecAttribute;
import galenWrapper.SpecValidation.SpecReader;
import java.util.Arrays;

/**
 *
 * @author 394173
 */
public class Attribute extends General {

    public Attribute(CommandControl cc) {
        super(cc);
    }

    private void assertElementAttr(SpecText.Type type) {
        SpecAttribute spec = SpecReader.reader().getSpecAttribute(type, Data);
        spec.setOriginalText(getMessage(spec));
        validate(spec, RelativeElement.None);
    }

    private void assertElementAttrI(SpecText.Type type) {
        SpecAttribute spec = SpecReader.reader().getSpecAttribute(type, Data.toLowerCase());
        spec.setOperations(Arrays.asList(new String[]{"lowercase"}));
        spec.setOriginalText(getMessage(spec));
        validate(spec, RelativeElement.None);
    }

    public void assertElementAttrEquals() {
        assertElementAttr(SpecText.Type.IS);
    }

    public void assertElementAttrContains() {
        assertElementAttr(SpecText.Type.CONTAINS);
    }

    public void assertElementAttrStartsWith() {
        assertElementAttr(SpecText.Type.STARTS);
    }

    public void assertElementAttrEndsWith() {
        assertElementAttr(SpecText.Type.ENDS);
    }

    public void assertElementAttrMatches() {
        assertElementAttr(SpecText.Type.MATCHES);
    }

    public void assertElementAttrIEquals() {
        assertElementAttrI(SpecText.Type.IS);
    }

    public void assertElementAttrIContains() {
        assertElementAttrI(SpecText.Type.CONTAINS);
    }

    public void assertElementAttrIStartsWith() {
        assertElementAttrI(SpecText.Type.STARTS);
    }

    public void assertElementAttrIEndsWith() {
        assertElementAttrI(SpecText.Type.ENDS);
    }

    @Deprecated //as of 3.5
    public void verifyElementAttribute() {
        assertElementAttrEquals();
    }

    private String getMessage(SpecAttribute spec) {
        return String.format("%s's Attribute %s %s %s ", ObjectName, spec.getAtributeName(), spec.getType().toString(), spec.getText());
    }

}
