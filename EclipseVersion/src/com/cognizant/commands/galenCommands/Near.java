/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.Location;
import com.galenframework.specs.SpecNear;
import galenWrapper.SpecValidation.SpecReader;
import java.util.List;

/**
 *
 * @author 394173
 */
public class Near extends General {

    public Near(CommandControl cc) {
        super(cc);
    }

    public void assertElementNear() {
        SpecNear spec = SpecReader.reader().getSpecNear(Condition, Data);
        spec.setOriginalText(getMessage(spec.getLocations()));
        validate(spec);
    }

    private String getMessage(List<Location> locations) {
        return String.format("%s is Near %s over location %s", ObjectName, Condition, Data);
    }
}
