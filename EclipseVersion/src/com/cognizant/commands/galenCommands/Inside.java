/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.Location;
import com.galenframework.specs.SpecInside;
import galenWrapper.SpecValidation.SpecReader;
import java.util.List;

/**
 *
 * @author 394173
 */
public class Inside extends General {

    public Inside(CommandControl cc) {
        super(cc);
    }

    private void assertElement(Boolean isPartly) {
        SpecInside spec = SpecReader.reader().getSpecInside(Condition, Data, isPartly);
        spec.setOriginalText(getMessage(isPartly, spec.getLocations()));
        validate(spec);
    }

    public void assertElementInside() {
        assertElement(false);
    }

    public void assertElementInsidePartly() {
        assertElement(true);
    }

    private String getMessage(Boolean isPartly, List<Location> locations) {
        String partly = isPartly ? " partly " : "";
        String message = String.format("%s is %sinside %s", ObjectName, partly, Condition);
        if (!locations.isEmpty()) {
            message += " over location" + Data;
        }
        return message;
    }

}
