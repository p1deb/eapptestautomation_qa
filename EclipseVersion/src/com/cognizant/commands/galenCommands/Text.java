/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.SpecText;
import com.galenframework.specs.SpecText.Type;
import galenWrapper.SpecValidation.SpecReader;
import java.util.Arrays;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author 394173
 */
public class Text extends General {

    public Text(CommandControl cc) {
        super(cc);
    }

    private void assertElementText(Type type) {
        checkElementTypeBeforeProcessing();
        SpecText spec = SpecReader.reader().getSpecText(type, Data);
        spec.setOriginalText(getMessage(type));
        validate(spec);
    }

    private void assertElementTextI(Type type) {
        checkElementTypeBeforeProcessing();
        SpecText spec = SpecReader.reader().getSpecText(type, Data.toLowerCase());
        spec.setOperations(Arrays.asList(new String[]{"lowercase"}));
        spec.setOriginalText(getMessage(type));
        validate(spec);
    }

    private void checkElementTypeBeforeProcessing() {
        if (Element != null) {
            if (Element.getTagName().equalsIgnoreCase("select")) {
                Select select = new Select(Element);
                Element = select.getFirstSelectedOption();
                System.out.println("As it is Select Element assserting the text of first selected Element");
            }
        }
    }

    public void assertElementTextEquals() {
        assertElementText(Type.IS);
    }

    public void assertElementTextContains() {
        assertElementText(Type.CONTAINS);
    }

    public void assertElementTextStartsWith() {
        assertElementText(Type.STARTS);
    }

    public void assertElementTextEndsWith() {
        assertElementText(Type.ENDS);
    }

    public void assertElementTextMatches() {
        assertElementText(Type.MATCHES);
    }

    public void assertElementTextIEquals() {
        assertElementTextI(Type.IS);
    }

    public void assertElementTextIContains() {
        assertElementTextI(Type.CONTAINS);
    }

    public void assertElementTextIStartsWith() {
        assertElementTextI(Type.STARTS);
    }

    public void assertElementTextIEndsWith() {
        assertElementTextI(Type.ENDS);
    }

    @Deprecated
    public void assertText() {
        assertElementTextEquals();
    }

    @Deprecated
    public void assertContainsText() {
        assertElementTextContains();
    }

    @Deprecated
    public void assertValue() {
        assertElementTextEquals();
    }

    @Deprecated
    public void assertContainsValue() {
        assertElementTextContains();
    }

    @Deprecated
    public void verifyText() {
        assertElementTextEquals();
    }

    @Deprecated
    public void verifyContainsText() {
        assertElementTextContains();
    }

    @Deprecated
    public void verifyContainsValue() {
        assertElementTextContains();
    }

    @Deprecated
    public void verifyValue() {
        assertElementTextEquals();
    }

    private String getMessage(Type type) {
        return String.format("%s's Text %s %s ", ObjectName, type.toString(), Data);
    }
}
