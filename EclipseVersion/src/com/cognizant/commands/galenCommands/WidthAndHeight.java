/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.SpecHeight;
import com.galenframework.specs.SpecWidth;
import galenWrapper.SpecValidation.SpecReader;

/**
 *
 * @author 394173
 */
public class WidthAndHeight extends General {

    public WidthAndHeight(CommandControl cc) {
        super(cc);
    }

    private void assertWidth(RelativeElement rElement) {
        SpecWidth spec = SpecReader.reader().getSpecWidth(rElement, Data, Condition);
        spec.setOriginalText(getMessage("width", rElement));
        validate(spec, rElement);
    }

    private void assertHeight(RelativeElement rElement) {
        SpecHeight spec = SpecReader.reader().getSpecHeight(rElement, Data, Condition);
        spec.setOriginalText(getMessage("height", rElement));
        validate(spec, rElement);
    }

    public void assertElementWidth() {
        assertWidth(RelativeElement.None);
    }

    public void assertElementWidthElement() {
        assertWidth(RelativeElement.WebElement);
    }

    public void assertElementHeight() {
        assertHeight(RelativeElement.None);
    }

    public void assertElementHeightElement() {
        assertHeight(RelativeElement.WebElement);
    }

    private String getMessage(String type, RelativeElement rElement) {
        String message = String.format("%s's %s is %s", ObjectName, type, Data);
        if (rElement.equals(RelativeElement.WebElement)) {
            message += " of " + Condition;
        }
        return message;
    }

}
