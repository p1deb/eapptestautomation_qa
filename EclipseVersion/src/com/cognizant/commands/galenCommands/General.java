/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.Spec;
import com.galenframework.validation.ValidationResult;

import galenWrapper.PageValidationWrapper;
import galenWrapper.PageWrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.openqa.selenium.WebElement;

/**
 *
 * @author 394173
 */
public class General extends Report {

    public enum RelativeElement {

        WebElement, WebElementList, None;

    };

    public General(CommandControl cc) {
        super(cc);
    }

    private Map<String, WebElement> getRelativeElement(RelativeElement elementType) {
        switch (elementType) {
            case WebElement:
                return getRelativeElement();
            case WebElementList:
                return getRelativeElementList();
            default:
                return new HashMap<>();
        }
    }

    private Map<String, WebElement> getRelativeElement() {
        Map<String, WebElement> elementMap = new HashMap<>();
        if (Condition != null && !Condition.trim().isEmpty()) {
            WebElement element = AObject.findElement(Condition, Reference);
            if (element != null) {
                elementMap.put(Condition, element);
            }
        }
        return elementMap;
    }

    private Map<String, WebElement> getRelativeElementList() {
        if (Condition != null && !Condition.trim().isEmpty()) {
            return getElementsfromList(Reference, Condition);
        }
        return null;
    }

    public Map<String, WebElement> getElementsfromList(String Page, String regexData) {
        return AObject.findElementsByRegex(regexData, Page);
    }

    public List<String> getElementsList() {
        if (Condition != null && !Condition.trim().isEmpty()) {
            return AObject.getObjectList(Reference, Condition);
        }
        return null;
    }

    public PageValidationWrapper getPageValidation(RelativeElement relativeElement) {
        Map<String, WebElement> elementMap = getRelativeElement(relativeElement);
        if (Element != null) {
            elementMap.put(ObjectName, Element);
        }
        return new PageValidationWrapper(getPage(relativeElement), elementMap);
    }

    private PageWrapper getPage(RelativeElement relativeElement) {
        return new PageWrapper(Driver, getRelativeElement(relativeElement));
    }

    public void validate(Spec spec, RelativeElement relativeElement) {
        try {
            PageValidationWrapper pageValidation = getPageValidation(relativeElement);
            ValidationResult result = pageValidation.check(ObjectName, spec);
            if (result.getError() != null) {
                onError(spec, result);
            } else {
                onSuccess(spec, result);

            }
        } catch (Exception ex) {
            Logger.getLogger(General.class
                    .getName()).log(Level.SEVERE, null, ex);
            onError(ex);
        }
    }

    public void validate(Spec spec) {
        validate(spec, RelativeElement.WebElement);
    }

}
