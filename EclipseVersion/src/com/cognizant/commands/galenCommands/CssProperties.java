/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.galenCommands;

import com.cognizant.core.CommandControl;
import com.galenframework.specs.SpecCss;
import com.galenframework.specs.SpecText.Type;
import galenWrapper.SpecValidation.SpecReader;
import java.util.Arrays;

/**
 *
 * @author 394173
 */
public class CssProperties extends General {

    public CssProperties(CommandControl cc) {
        super(cc);
    }

    private void assertElementCss(Type type) {
        SpecCss spec = SpecReader.reader().getSpecCSS(type, Data);
        spec.setOriginalText(getMessage(spec));
        validate(spec);
    }

    private void assertElementCssI(Type type) {
        SpecCss spec = SpecReader.reader().getSpecCSS(type, Data.toLowerCase());
        spec.setOperations(Arrays.asList(new String[]{"lowercase"}));
        spec.setOriginalText(getMessage(spec));
        validate(spec);
    }

    public void assertElementCssPropEquals() {
        assertElementCss(Type.IS);
    }

    public void assertElementCssPropContains() {
        assertElementCss(Type.CONTAINS);
    }

    public void assertElementCssPropStartsWith() {
        assertElementCss(Type.STARTS);
    }

    public void assertElementCssPropEndsWith() {
        assertElementCss(Type.ENDS);
    }

    public void assertElementCssPropMatches() {
        assertElementCss(Type.MATCHES);
    }

    public void assertElementCssPropIEquals() {
        assertElementCssI(Type.IS);
    }

    public void assertElementCssPropIContains() {
        assertElementCssI(Type.CONTAINS);
    }

    public void assertElementCssPropIStartsWith() {
        assertElementCssI(Type.STARTS);
    }

    public void assertElementCssPropIEndsWith() {
        assertElementCssI(Type.ENDS);
    }

    private String getMessage(SpecCss spec) {
        return String.format("%s's CssProperty %s %s %s ", ObjectName, spec.getCssPropertyName(), spec.getType().toString(), spec.getText());
    }
}
