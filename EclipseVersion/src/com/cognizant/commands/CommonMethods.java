/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

/**
 * Class to represent common methods for all web objects.
 *
 * @author Cognizant
 */
import com.cognizant.constants.FilePath;
import com.cognizant.core.CommandControl;
import com.cognizant.support.ElementException;
import com.cognizant.support.ElementException.ExceptionType;
import com.cognizant.support.Status;
import com.galenframework.page.selenium.WebPageElement;
import com.galenframework.rainbow4j.Rainbow4J;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class CommonMethods extends Command {

    // ##############################################################
    // Category : Navigation
    // ##############################################################
    public CommonMethods(CommandControl cc) {
        super(cc);
    }

    /**
     * **************************************
     * Function to refresh the page
     *
     * **************************************
     */
    public void refreshDriver() {
        try {
            Driver.navigate().refresh();
            Report.updateTestLog("refreshDriver", "Page is refreshed",
                    Status.DONE);
        } catch (WebDriverException e) {
            Report.updateTestLog("refreshDriver", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * **************************************
     * Function to navigate previous page **************************************
     */
    public void back() {
        try {
            Driver.navigate().back();
            Report.updateTestLog("back", "Navigate page back is success",
                    Status.DONE);
        } catch (WebDriverException e) {
            Report.updateTestLog("back", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * **************************************
     * Function to navigate Next page **************************************
     */
    public void forward() {
        try {
            Driver.navigate().forward();
            Report.updateTestLog("forward", "Navigate page forward is success",
                    Status.DONE);
        } catch (WebDriverException e) {
            Report.updateTestLog("forward", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * **************************************
     * Function to close the page. **************************************
     */
    public void close() {
        try {
            Driver.close();
            Report.updateTestLog("close", "Selenium Webdriver is closed",
                    Status.DONE);
        } catch (WebDriverException e) {
            Report.updateTestLog("close", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     *
     * **************************************
     * Function to double click on the object
     * **************************************
     */
    public void doubleClickElement() {
        try {
            if (Element != null) {
                new Actions(Driver).doubleClick(Element).build().perform();
                Report.updateTestLog("doubleClickElement", "'" + Element
                        + "' is doubleClicked", Status.DONE);
            } else {
                Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog("doubleClickElement", e.getMessage(),
                    Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * ************************************** Function to mouse over to
     * specific object
     *
     * @param Element - WebElement Object
     * @param report - Framework report object. return - void
     * **************************************
     */
    public void mouseOverElement() {
        try {
            if (Element != null) {
                new Actions(Driver).moveToElement(Element).build().perform();
                Report.updateTestLog("mouseOverElement",
                        "Mouse Over to Element '" + ObjectName + "'.",
                        Status.DONE);
            } else {
                Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog("mouseOverElement", e.getMessage(),
                    Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ************************************** Function to submit the action to
     * the specific object
     *
     * @param Element - WebElement Object
     * @param report - Framework report object. return - void
     * **************************************
     */
    public void submitElement() {
        try {
            if (Element != null) {
                Element.submit();
                Report.updateTestLog("submitElement", "'" + Element
                        + "' has been submitted", Status.DONE);
            } else {
                Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog("submitElement", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ************************************** Function to drag and drop the
     * action to the specific object
     *
     * @param Element - WebElement Object
     * @param report - Framework report object. return - void
     * **************************************
     */
    public void dragElement() {
        try {
            if (Element != null) {
                getRunTimeElement().push(Element);
                Report.updateTestLog("dragElement", "'" + ObjectName
                        + "' dragged", Status.DONE);
            } else {
                Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog("dragElement", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    public void dropElement() {
        try {
            if (Element != null) {
                if (!getRunTimeElement().empty()) {
                    new Actions(Driver)
                            .dragAndDrop(getRunTimeElement().pop(), Element)
                            .build().perform();
                    Report.updateTestLog("dropElement", "Element  dropped to '"
                            + ObjectName + "' ", Status.DONE);
                } else {
                    throw new Exception("No Element selected to drop");
                }
            } else {
                Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog("dropElement", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    public void dragAndDropBy() {
        try {
            if (Element != null) {
                String[] coords = Data.split(",", 2);
                new Actions(Driver).dragAndDropBy(Element, Integer.parseInt(coords[0]), Integer.parseInt(coords[1]))
                        .build().perform();
                Report.updateTestLog(Action, "Element [" + ObjectName + "] dropped at '"
                        + Data + "' ", Status.PASS);

            } else {
                Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog(Action, e.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    public void dragToAndDropElement() {
        try {
            String Page = Data.split(":", 2)[0];
            String Object = Data.split(":", 2)[1];
            if (Element != null) {
                WebElement DropElement = AObject.findElement(Object, Page);
                if (DropElement != null) {
                    new Actions(Driver).dragAndDrop(Element, DropElement)
                            .build().perform();
                    Report.updateTestLog("dragToAndDropElement", "'"
                            + ObjectName
                            + "' has been dragged and dropped to '" + Object
                            + "'", Status.PASS);
                } else {
                    throw new Exception("Drop Element not found");
                }
            } else {
                throw new Exception("Drag Element not found");
            }
        } catch (Exception e) {
            Report.updateTestLog("dragToAndDropElement", e.getMessage(),
                    Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ************************************** Function to click and hold the
     * Element
     *
     * @param Element - WebElement Object
     * @param report - Framework report object. return - void
     * **************************************
     */
    public void clickAndHoldElement() {
        try {
            if (Element != null) {
                new Actions(Driver).clickAndHold(Element).build().perform();
                Report.updateTestLog("clickAndHoldElement",
                        "clickAndHoldElement action is done", Status.DONE);
            } else {
                Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog("clickAndHoldElement", e.getMessage(),
                    Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * **************************************
     * Function to release the hold Element
     * **************************************
     */
    public void releaseElement() {
        try {
            if (Element != null) {
                new Actions(Driver).release(Element).build().perform();
                Report.updateTestLog("releaseElement",
                        "releaseElement action is done", Status.DONE);
            } else {
                Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog("releaseElement", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * **************************************
     * Function to add a name in the cookie
     * **************************************
     */
    public void addCookie() {

        try {
            String strCookieName = Data.split(":", 2)[0];
            String strCookieValue = Data.split(":", 2)[1];
            Cookie oCookie = new Cookie.Builder(strCookieName, strCookieValue)
                    .build();
            Driver.manage().addCookie(oCookie);
            Report.updateTestLog("addCookie", "Cookie Name- '" + strCookieName
                    + "' with value '" + strCookieValue + "' is added",
                    Status.DONE);
            oCookie = null;

        } catch (Exception e) {
            Report.updateTestLog("addCookie", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * **************************************
     * Function to delete the cookie by name
     * **************************************
     */
    public void deleteCookie() {
        try {
            String strCookieName = Data;
            Cookie oCookie = Driver.manage().getCookieNamed(strCookieName);
            if (oCookie != null) {
                Driver.manage().deleteCookie(oCookie);
                Report.updateTestLog("deleteCookie", "Cookie Name- '"
                        + strCookieName + "' is deleted", Status.DONE);
                oCookie = null;
            } else {
                Report.updateTestLog("deleteCookie", "Cookie doesn't exist",
                        Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog("deleteCookie", e.getMessage(), Status.FAIL);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * **************************************
     * Function to capture the screenshot and save in the specific location
     * **************************************
     */
    public void saveScreenshot() {
        try {
            String strFullpath = Data;

            File scrFile = ((TakesScreenshot) Driver)
                    .getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(strFullpath));
            Report.updateTestLog("saveScreenshot",
                    "Screenshot is taken and saved in this path -'"
                    + strFullpath + "'", Status.SCREENSHOT);
        } catch (IOException e) {
            Report.updateTestLog("saveScreenshot", e.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    public void takeScreenshot() {
        try {
            Report.updateTestLog(Action, "Screenshot is taken", Status.SCREENSHOT);
        } catch (Exception e) {
            Report.updateTestLog("saveScreenshot", e.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void saveElementScreenshot() {
        if (ObjectName.equals("Browser")) {
            savePageElementScreenshot();
        } else {
            saveCurrentElementScreenshot();
        }
    }

    private void savePageElementScreenshot() {
        try {
            if (Data != null && !Data.trim().isEmpty()) {
                Map<String, WebElement> elements = AObject.findAllElementsFromPage(Data);
                String plocation = FilePath.getORimagestorelocation() + File.separator + Data;
                if (!elements.isEmpty()) {
                    for (Map.Entry<String, WebElement> entry : elements.entrySet()) {
                        String objName = entry.getKey();
                        WebElement element = entry.getValue();
                        String location = plocation + File.separator + objName;
                        File file = new File(location);
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                        saveElementScreenshot(element, getDriverControl().createScreenShot(),
                                file.getAbsolutePath() + File.separator + objName + ".png");
                    }
                    Report.updateTestLog(Action, "Elements from Page '" + Data + "' Screenshot stored in Location : " + plocation, Status.DONE);
                } else {
                    Report.updateTestLog(Action, "No Element found from Page '" + Data + "'", Status.WARNING);
                }
            } else {
                Report.updateTestLog(Action, "Page Name Empty", Status.DEBUG);
            }
        } catch (Exception e) {
            Report.updateTestLog(Action, e.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void saveCurrentElementScreenshot() {
        try {
            if (Element != null) {
                String location;
                if (Data != null && !Data.trim().isEmpty()) {
                    location = Data;
                } else {
                    location = FilePath.getORimagestorelocation() + File.separator + Reference + File.separator + ObjectName;
                }
                File file = new File(location);
                if (!file.exists()) {
                    file.mkdirs();
                }
                saveElementScreenshot(Element, getDriverControl().createScreenShot(), file.getAbsolutePath() + File.separator + ObjectName + ".png");
                Report.updateTestLog(Action, ObjectName + "'s Screenshot stored in Location : " + location, Status.DONE);
            } else {
                throw new ElementException(ExceptionType.Element_Not_Found);
            }
        } catch (Exception e) {
            Report.updateTestLog(Action, e.getMessage(), Status.DEBUG);
            Logger
                    .getLogger(CommonMethods.class
                            .getName()).log(Level.SEVERE, null, e);
        }
    }

    private void saveElementScreenshot(WebElement Element, File screenshot, String location) {
        try {
            File dest = new File(location);
            BufferedImage image = ImageIO.read(screenshot);
            WebPageElement element = new WebPageElement(Driver, ObjectName, Element, null);
            int[] area = element.getArea().toIntArray();
            int availableHeight = image.getHeight() - area[1];
            int availableWidth = image.getWidth() - area[0];
            int subimageWidth = Math.min(area[2], availableWidth);
            int subimageHeight = Math.min(area[3], availableHeight);
            BufferedImage subImage = image.getSubimage(area[0], area[1], subimageWidth, subimageHeight);
            Rainbow4J.saveImage(subImage, dest);
            screenshot.delete();
        } catch (Exception ex) {
            Logger.getLogger(CommonMethods.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * **************************************
     * Function to print *************************************
     */
    public void print() {
        System.out.println(Data);
        Report.updateTestLog("print", Data, Status.DONE);
    }

    /**
     * **************************************
     * Function to pause *************************************
     */
    public void pause() {
        try {
            Thread.sleep(Long.parseLong(Data));
            Report.updateTestLog("pause",
                    "Thread sleep for '" + Long.parseLong(Data), Status.DONE);
        } catch (Exception e) {
            Report.updateTestLog("pause", e.getMessage(), Status.FAIL);
            Logger
                    .getLogger(CommonMethods.class
                            .getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * ******************************************
     * Function to handle alert popup *****************************************
     */
    public void answerAlert() {
        String setAlertText = Data;
        try {
            Driver.switchTo().alert().sendKeys(setAlertText);
            Report.updateTestLog("answerAlert", "Message '" + setAlertText
                    + "' is set in the alert window", Status.DONE);
        } catch (Exception e) {
            Report.updateTestLog("answerAlert", e.getMessage(), Status.FAIL);
            Logger
                    .getLogger(CommonMethods.class
                            .getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ******************************************
     * Function to accept the alert popup
     *
     * ******************************************
     */
    public void acceptAlert() {
        try {
            Driver.switchTo().alert().accept();
            Report.updateTestLog("acceptAlert", "Alert is accepted",
                    Status.DONE);
        } catch (Exception e) {
            Report.updateTestLog("acceptAlert", e.getMessage(), Status.FAIL);
            Logger
                    .getLogger(CommonMethods.class
                            .getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ******************************************
     * Function to dismiss the alert popup
     *
     * ******************************************
     */
    public void dismissAlert() {
        try {
            Driver.switchTo().alert().dismiss();
            Report.updateTestLog("dismissAlert", "Alert is dismissed",
                    Status.DONE);
        } catch (Exception e) {
            Report.updateTestLog("dismissAlert", e.getMessage(), Status.FAIL);
            Logger
                    .getLogger(CommonMethods.class
                            .getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     ******************************************
     * Function to getCellValue from the table
     *
     * ****************************************** @param Element
     * @param tr
     * @param td
     * @return
     */
    public HashMap<String, String> getCellValue(WebElement Element, int tr,
            int td) {
        int rowCounter = 0;
        int colCounter = 0;
        String rowKey = null;
        String colKey = null;
        HashMap<String, String> HashTable = new HashMap<String, String>();

        String strObj = Data;
        List<WebElement> tableList = Element.findElements(By
                .cssSelector("div[class='" + strObj + "'] tr td"));
        for (WebElement listIterator : tableList) {
            String TagName = listIterator.getTagName();
            if (TagName.equals("tr")) {
                rowKey = "R" + rowCounter++;
            }
            if (TagName.equals("td")) {
                colKey = "C" + colCounter++;
            }
            HashTable.put(rowKey + colKey, listIterator.getText().toString());
        }
        return HashTable;
    }

    /**
     ******************************************
     * Function to store a given text in variable
     * ******************************************
     */
    public void storeVariable() {
        String strObj = Input;
        String[] strTemp = strObj.split("=");
        if (strTemp[0].startsWith("%") && strTemp[0].endsWith("%")) {
            addVar(strTemp[0], strTemp[1]);
            Report.updateTestLog("storeVariable", "Value '" + strTemp[1]
                    + "' is stored in Variable '" + strTemp[0] + "'",
                    Status.DONE);
        } else {
            Report.updateTestLog("storeVariable",
                    "Variable format is not correct", Status.FAIL);
        }
    }

    /**
     * ******************************************
     * Function to store url in a variable
     * ******************************************
     */
    public void storeCurrentUrl() {
        String strObj = Input;

        if (strObj.startsWith("%") && strObj.endsWith("%")) {
            addVar(strObj, Driver.getCurrentUrl());
            Report.updateTestLog("storeCurrentUrl",
                    "Current URL '" + Driver.getCurrentUrl()
                    + "' is stored in variable '" + strObj + "'",
                    Status.PASS);
        } else {
            Report.updateTestLog("storeCurrentUrl",
                    "Variable format is not correct", Status.FAIL);
        }
    }

    /**
     * ******************************************
     * Function to store title in a variable
     * ******************************************
     */
    public void storeTitle() {
        String strObj = Input;

        if (strObj.startsWith("%") && strObj.endsWith("%")) {
            addVar(strObj, Driver.getTitle());
            Report.updateTestLog("storeTitle",
                    "Page title '" + Driver.getTitle() + "' is stored in '"
                    + strObj + "'", Status.PASS);
        } else {
            Report.updateTestLog("storeTitle",
                    "Variable format is not correct", Status.FAIL);
        }
    }

    @Deprecated
    public void storeBodyText() {
        storeText();
    }

    /**
     * ******************************************
     * Function to store text in a variable
     * ******************************************
     */
    public void storeText() {
        if (Element != null) {
            String strObj = Input;
            if (strObj.startsWith("%") && strObj.endsWith("%")) {
                addVar(strObj, getElementText());
                Report.updateTestLog(Action, "Element text " + getElementText()
                        + " is stored in variable " + strObj, Status.PASS);
            } else {
                Report.updateTestLog(Action,
                        "Variable format is not correct", Status.FAIL);
            }
        } else {
            throw new ElementException(ExceptionType.Element_Not_Found);
        }
    }

    public void storeTextinDataSheet() {
        if (Element != null) {
            String strObj = Input;
            if (strObj.matches("\\S*:\\S*")) {
                try {
                    String sheetName = strObj.split(":", 2)[0];
                    String columnName = strObj.split(":", 2)[1];
                    String elText = getElementText();
                    userData.putData(sheetName, columnName, elText.trim());
                    Report.updateTestLog(Action, "Element text [" + elText
                            + "] is stored in variable " + strObj, Status.DONE);
                } catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
                    Report.updateTestLog(Action, "Error Storing text in datasheet" + ex.getMessage(), Status.DEBUG);
                }

            } else {
                Report.updateTestLog(Action,
                        "Given input [" + Input + "] format is invalid. It should be [sheetName:ColumnName]", Status.DEBUG);
            }
        } else {
            throw new ElementException(ExceptionType.Element_Not_Found);
        }
    }

    /**
     * ******************************************
     * Function to store text present in a variable
     * ******************************************
     */
    public void storeTextPresent() {
        try {
            if (Element != null) {
                String strObj = Input;
                String[] strTemp = strObj.split("=");
                if (strTemp[0].startsWith("%") && strTemp[0].endsWith("%")) {

                    if (getElementText().contains(strTemp[1])) {
                        addVar(strTemp[0], "true");
                    } else {
                        addVar(strTemp[0], "false");
                    }
                    Report.updateTestLog("storeTextPresent", "Variable Stored",
                            Status.DONE);
                } else {
                    throw new Exception("Variable format is not correct");
                }
            } else {
                throw new Exception("Element not found");
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
            Report.updateTestLog("storeTextPresent", ex.getMessage(),
                    Status.FAIL);
        }
    }

    private String getElementText() {
        if (Element.getTagName().equalsIgnoreCase("input")) {
            return Element.getAttribute("value");
        } else if (Element.getTagName().equalsIgnoreCase("select")) {
            return new Select(Element).getFirstSelectedOption().getText();
        } else {
            return Element.getText();
        }
    }

    /**
     * ******************************************
     * Function to store page source in a variable
     *
     * ******************************************
     */
    public void storePageSource() {
        String strObj = Input;

        if (strObj.startsWith("%") && strObj.endsWith("%")) {
            addVar(strObj, Driver.getPageSource());
            Report.updateTestLog("storePageSource",
                    "Page source is stored in variable '" + strObj + "'",
                    Status.DONE);
        } else {
            Report.updateTestLog("storePageSource",
                    "Variable format is not correct", Status.FAIL);
        }
    }

    /**
     * ******************************************
     * Function to store Element selected n a variable
     * ******************************************
     */
    public void storeElementSelected() {
        if (Element != null) {
            String strObj = Input;

            if (strObj.startsWith("%") && strObj.endsWith("%")) {

                if (Element.isSelected()) {
                    addVar(strObj, "true");
                } else {
                    addVar(strObj, "false");
                }
                Report.updateTestLog("storeElementSelected",
                        "Element selected flag has been stored into variable '"
                        + strObj + "'", Status.DONE);
            } else {
                Report.updateTestLog("storeElementSelected",
                        "Variable format is not correct", Status.FAIL);
            }
        } else {
            Report.updateTestLog("storeElementSelected", "Element not found",
                    Status.FAIL);
        }
    }

    /**
     * ******************************************
     * Function to store Element attribute n a variable
     * ******************************************
     */
    public void storeElementAttribute() {
        if (Element != null) {
            String strObj = Input;
            String[] strTemp = strObj.split("=");
            if (strTemp[0].startsWith("%") && strTemp[0].endsWith("%")) {

                addVar(strTemp[0], Element.getAttribute(strTemp[1]));
                Report.updateTestLog("storeEIementAttribute",
                        "Element's attribute value is stored in variable '"
                        + strObj + "'", Status.PASS);
            } else {
                Report.updateTestLog("storeEIementAttribute",
                        "Variable format is not correct", Status.FAIL);
            }
        } else {
            Report.updateTestLog("storeElementAttribute", "Element not found",
                    Status.FAIL);
        }
    }

    /**
     * ******************************************
     * Function to store Element value n a variable
     * ******************************************
     */
    public void storeElementValue() {
        if (Element != null) {
            String strObj = Input;

            if (strObj.startsWith("%") && strObj.endsWith("%")) {

                addVar(strObj, Element.getAttribute("value"));
                Report.updateTestLog("storeElementValue", "Element's value "
                        + Element.getAttribute("value")
                        + " is stored in variable '" + strObj + "'",
                        Status.DONE);
            } else {
                Report.updateTestLog("storeElementValue",
                        "Variable format is not correct", Status.FAIL);
            }
        } else {
            Report.updateTestLog("storeElementValue", "Element not found",
                    Status.FAIL);
        }
    }

    /**
     * ******************************************
     * Function to store a cookie present status
     * ******************************************
     */
    public void storeCookiePresent() {
        String variableName = Condition;
        String cookieName = Data;
        if (variableName.matches("%.*%")) {
            if (Driver.manage().getCookieNamed(cookieName) != null) {
                addVar(variableName, "Exist");
            } else {
                addVar(variableName, "Not Exist");
            }

            Report.updateTestLog(
                    "storeCookiePresent",
                    "Cookie presense flag is stored in variable " + variableName + "",
                    Status.DONE);
        } else {
            Report.updateTestLog("storeCookiePresent",
                    "Variable format is not correct", Status.DEBUG);
        }
    }

    /**
     * ******************************************
     * Function to store a cookie by name in a variable
     * ******************************************
     */
    public void storeCookieByName() {
        String variableName = Condition;
        String cookieName = Data;
        if (variableName.matches("%.*%")) {
            addVar(variableName, Driver.manage().getCookieNamed(cookieName)
                    .getValue());
            Report.updateTestLog("storeCookieByName", "Cookie Stored",
                    Status.DONE);
        } else {
            Report.updateTestLog("storeCookieByName",
                    "Variable format is not correct", Status.DEBUG);
        }
    }

    /**
     * ******************************************
     * Function to store a alert text in a variable
     * ******************************************
     */
    public void storeAlertText() {
        String strObj = Input;

        if (strObj.startsWith("%") && strObj.endsWith("%")) {
            addVar(strObj, Driver.switchTo().alert().getText());
            Report.updateTestLog("storeAIertText", "Alert Text "
                    + Driver.switchTo().alert().getText()
                    + " is Stored in variable " + strObj + "", Status.DONE);
        } else {
            Report.updateTestLog("storeAIertText",
                    "Variable format is not correct", Status.FAIL);
        }
    }

    /**
     * ******************************************
     * Function to store a alert present in a variable
     * ******************************************
     */
    public void storeAlertPresent() {
        String strObj = Input;

        if (strObj.startsWith("%") && strObj.endsWith("%")) {
            if (isAlertPresent(Driver)) {
                addVar(strObj, "Exist");
            } else {
                addVar(strObj, "Not Exist");
            }
            Report.updateTestLog("storeAlertPresent",
                    "Alert Text Status Stored", Status.DONE);

        } else {
            Report.updateTestLog("storeAlertPresent",
                    "Variable format is not correct", Status.FAIL);
        }
    }

    /**
     * ******************************************
     * Function to store a JS script executor in a variable
     * ******************************************
     */
    public void storeEval() {
        String javaScript = Data;
        String variableName = Condition;
        if (variableName.matches("%.*%")) {
            JavascriptExecutor js = (JavascriptExecutor) Driver;
            addVar(variableName, js.executeScript(javaScript).toString());
            Report.updateTestLog("storeEval", "Eval Stored", Status.DONE);
        } else {
            Report.updateTestLog("storeEval", "Variable format is not correct",
                    Status.FAIL);
        }
    }

    private boolean isAlertPresent(WebDriver Driver) {
        try {
            Driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, e);
            return false;
        }
    }

    public void sendKeysToElement() {
        try {
            if (Element != null) {
                String[] Values = Data.toLowerCase().split("\\+");
                if (Values.length == 4) {
                    Element.sendKeys(Keys
                            .chord(getKeyCode(Values[0]),
                                    getKeyCode(Values[1]),
                                    getKeyCode(Values[2]),
                                    getKeyCode(Values[3]) != null ? getKeyCode(Values[3])
                                    : Values[3]));
                    Report.updateTestLog("sendKeysToElement", "Keys Submitted",
                            Status.DONE);
                } else if (Values.length == 3) {
                    Element.sendKeys(Keys
                            .chord(getKeyCode(Values[0]),
                                    getKeyCode(Values[1]),
                                    getKeyCode(Values[2]) != null ? getKeyCode(Values[2])
                                    : Values[2]));
                    Report.updateTestLog("sendKeysToElement", "Keys Submitted",
                            Status.DONE);
                } else if (Values.length == 2) {
                    Element.sendKeys(Keys
                            .chord(getKeyCode(Values[0]),
                                    getKeyCode(Values[1]) != null ? getKeyCode(Values[1])
                                    : Values[1]));
                    Report.updateTestLog("sendKeysToElement", "Keys Submitted",
                            Status.DONE);
                } else if (Values.length == 1) {
                    Element.sendKeys(Keys.chord(getKeyCode(Values[0])));
                    Report.updateTestLog("sendKeysToElement", "Keys Submitted",
                            Status.DONE);
                }
            } else {
                throw new Exception("Element not found");
            }
        } catch (Exception e) {
            Report.updateTestLog("sendKeysToElement", e.getMessage(), Status.FAIL);
            Logger
                    .getLogger(CommonMethods.class
                            .getName()).log(Level.SEVERE, null, e);

        }
    }

    public void sendKeysToWindow() {
        Actions builder = new Actions(Driver);
        try {
            String[] Values = Data.toLowerCase().split("\\+");
            if (Values.length == 4) {
                builder.sendKeys(
                        Keys.chord(
                                getKeyCode(Values[0]),
                                getKeyCode(Values[1]),
                                getKeyCode(Values[2]),
                                getKeyCode(Values[3]) != null ? getKeyCode(Values[3])
                                : Values[3])).perform();
                Report.updateTestLog("sendKeysToWindow", "Keys Submitted", Status.DONE);
            } else if (Values.length == 3) {
                builder.sendKeys(
                        Keys.chord(
                                getKeyCode(Values[0]),
                                getKeyCode(Values[1]),
                                getKeyCode(Values[2]) != null ? getKeyCode(Values[2])
                                : Values[2])).perform();
                Report.updateTestLog("sendKeysToWindow", "Keys Submitted", Status.DONE);
            } else if (Values.length == 2) {
                builder.sendKeys(
                        Keys.chord(
                                getKeyCode(Values[0]),
                                getKeyCode(Values[1]) != null ? getKeyCode(Values[1])
                                : Values[1])).build().perform();
                Report.updateTestLog("sendKeysToWindow", "Keys Submitted", Status.DONE);
            } else if (Values.length == 1) {
                builder.sendKeys(Keys.chord(getKeyCode(Values[0]))).build()
                        .perform();
                Report.updateTestLog("sendKeysToWindow", "Keys Submitted", Status.DONE);
            }
        } catch (Exception e) {
            Report.updateTestLog("sendKeysToWindow", "Input format is not correct",
                    Status.FAIL);
            Logger
                    .getLogger(CommonMethods.class
                            .getName()).log(Level.SEVERE, null, e);

        }
    }

    Keys getKeyCode(String data) {
        switch (data) {
            case "tab":
                return Keys.TAB;
            case "enter":
                return Keys.ENTER;
            case "shift":
                return Keys.SHIFT;
            case "ctrl":
                return Keys.CONTROL;
            case "alt":
                return Keys.ALT;
            case "esc":
                return Keys.ESCAPE;
            case "delete":
                return Keys.DELETE;
            case "backspace":
                return Keys.BACK_SPACE;
            case "home":
                return Keys.HOME;
            default:
                try {
                    return Keys.valueOf(data.toUpperCase());
                } catch (Exception ex) {
                    return null;
                }
        }
    }

}
