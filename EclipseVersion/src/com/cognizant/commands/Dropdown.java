/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Dropdown extends Command {

    String selector = "li a";

    enum SelectRange {

        Single, Multiple, All;

        public Boolean isMultiple() {
            return !this.equals(Single);
        }
    };

    enum SelectType {

        Select,
        DeSelect;

        @Override
        public String toString() {
            if (this.equals(Select)) {
                return "selected";
            }
            return "deselected";
        }
    }

    enum SelectBy {

        Index, Text, Value
    };

    public Dropdown(CommandControl cc) {
        super(cc);
    }

    public void selectValueFromUnorderedList() {
        selectFromUnorderedList(SelectBy.Text);
    }

    public void selectIndexFromUnorderedList() {
        selectFromUnorderedList(SelectBy.Index);
    }

    private void selectFromUnorderedList(SelectBy selectBy) {
        if (Element != null) {
            if (Condition != null && !Condition.trim().isEmpty()) {
                selector = Condition;
            }
            List<WebElement> options = Element.findElements(By.cssSelector(selector));
            if (options.isEmpty()) {
                options = Element.findElements(By.tagName("li"));
            }
            Boolean flag = false;
            if (selectBy.equals(SelectBy.Index)) {
                int index = Integer.parseInt(Data);
                if (index < options.size()) {
                    options.get(index).click();
                    flag = true;
                }
            } else {
                for (WebElement option : options) {
                    if (Data.equals(option.getText().trim())) {
                        option.click();
                        flag = true;
                        break;
                    }
                }
            }
            if (flag) {
                Report.updateTestLog(Action, Data + " is Selected", Status.PASS);
            } else {
                Report.updateTestLog(Action, "Element[" + ObjectName + "] Not Visible/Available", Status.FAIL);
            }
        } else {
            Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
        }

    }

    public void selectByVisibleText() {
        select(SelectType.Select, SelectRange.Single, SelectBy.Text);
    }

    public void selectByValue() {
        select(SelectType.Select, SelectRange.Single, SelectBy.Value);
    }

    public void selectByIndex() {
        select(SelectType.Select, SelectRange.Single, SelectBy.Index);
    }

    public void selectMultipleByText() {
        select(SelectType.Select, SelectRange.Multiple, SelectBy.Text);
    }

    public void selectMultipleByValue() {
        select(SelectType.Select, SelectRange.Multiple, SelectBy.Value);
    }

    public void selectMultipleByIndex() {
        select(SelectType.Select, SelectRange.Multiple, SelectBy.Index);
    }

    public void selectAll() {
        select(SelectType.Select, SelectRange.All, null);
    }

    public void deSelectByVisibleText() {
        select(SelectType.DeSelect, SelectRange.Single, SelectBy.Text);
    }

    public void deSelectByValue() {
        select(SelectType.DeSelect, SelectRange.Single, SelectBy.Value);
    }

    public void deSelectByIndex() {
        select(SelectType.DeSelect, SelectRange.Single, SelectBy.Index);
    }

    public void deSelectMultipleByText() {
        select(SelectType.DeSelect, SelectRange.Multiple, SelectBy.Text);
    }

    public void deSelectMultipleByValue() {
        select(SelectType.DeSelect, SelectRange.Multiple, SelectBy.Value);
    }

    public void deSelectMultipleByIndex() {
        select(SelectType.DeSelect, SelectRange.Multiple, SelectBy.Index);
    }

    public void deSelectAll() {
        select(SelectType.DeSelect, SelectRange.All, null);
    }

    public void assertSelectContains() {
        if (Element != null) {
            Boolean isPresent = false;
            Select select = new Select(Element);
            for (WebElement option : select.getOptions()) {
                if (option.getText().trim().equals(Data)) {
                    isPresent = true;
                    break;
                }
            }
            if (isPresent) {
                Report.updateTestLog(Action, ObjectName + " Contains the Option " + Data, Status.DONE);
            } else {
                Report.updateTestLog(Action, ObjectName + " doesn't Contains the Option " + Data, Status.DEBUG);
            }
        } else {
            Report.updateTestLog(Action, "Element [" + ObjectName + "] not Exists", Status.DONE);
        }
    }

    private void select(SelectType selectType, SelectRange selectRange, SelectBy selectBy) {
        if (Element != null) {
            Boolean flag = true;
            if (selectRange.isMultiple()) {
                flag = new Select(Element).isMultiple();
            }
            if (flag) {
                switch (selectRange) {
                    case Single:
                        selectSingle(selectType, selectBy);
                        break;
                    case Multiple:
                        selectMultiple(selectType, selectBy);
                        break;
                    case All:
                        selectAll(selectType);
                        break;
                }
                Report.updateTestLog(Action, "Item/s '" + Data
                        + "' is/are " + selectType.toString() + " from list " + ObjectName, Status.DONE);
            } else {
                Report.updateTestLog(Action, ObjectName + " is not a multiple Select Element ", Status.FAIL);
            }
        } else {
            Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
        }
    }

    private void selectSingle(SelectType selectType, SelectBy selectBy) {
        switch (selectType) {
            case Select:
                select(selectBy);
                break;
            case DeSelect:
                deSelect(selectBy);
                break;
        }

    }

    private void selectMultiple(SelectType selectType, SelectBy selectBy) {
        Select select = new Select(Element);
        String[] values = Data.split(",");
        for (String value : values) {
            switch (selectType) {
                case Select:
                    select(select, value, selectBy);
                    break;
                case DeSelect:
                    deSelect(select, value, selectBy);
                    break;
            }
        }

    }

    private void select(SelectBy selectBy) {
        select(new Select(Element), Data, selectBy);
    }

    private void select(Select select, String Data, SelectBy selectBy) {
        switch (selectBy) {
            case Index:
                select.selectByIndex(Integer.parseInt(Data));
                break;
            case Text:
                select.selectByVisibleText(Data);
                break;
            case Value:
                select.selectByValue(Data);
                break;
        }

    }

    private void deSelect(SelectBy selectBy) {
        deSelect(new Select(Element), Data, selectBy);
    }

    private void deSelect(Select select, String Data, SelectBy selectBy) {
        switch (selectBy) {
            case Index:
                select.deselectByIndex(Integer.parseInt(Data));
                break;
            case Text:
                select.deselectByVisibleText(Data);
                break;
            case Value:
                select.deselectByValue(Data);
                break;
        }

    }

    private void selectAll(SelectType selectType) {
        switch (selectType) {
            case Select:
                Select select = new Select(Element);
                for (int i = 0; i < select.getOptions().size(); i++) {
                    select.selectByIndex(i);
                }
                break;
            case DeSelect:
                new Select(Element).deselectAll();
                break;
        }

    }

}
