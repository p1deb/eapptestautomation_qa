/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.commands.soapui;

import com.cognizant.commands.Command;
import com.cognizant.constants.FilePath;
import com.cognizant.core.CommandControl;
import com.cognizant.support.SoapConnector;
import com.cognizant.support.Status;
import java.io.File;

/**
 *
 * @author 394173
 */
public class SoapCommand extends Command {

    public SoapCommand(CommandControl cc) {
        super(cc);
    }

    private String getResultLoc() {
        String val = File.separator + "SoapResults" + File.separator + Reference;
        int i = 0;
        String resLoc = val;
        while (new File(FilePath.getCurrentResultsPath() + resLoc).exists()) {
            resLoc = val + "_" + i++;
        }
        return resLoc;
    }

    private String getProjectKey() {
        if (!Condition.matches("((Start|End) (Loop|Param)(.)*)|Global Object|Continue")) {
            return Condition;
        }
        return null;
    }

    public void initSoapUI() {
        if (Data != null && !Data.trim().isEmpty() && SoapConnector.loadProject(getProjectKey(), Data.trim())) {
            Report.updateTestLog(Action, "SoapUI Project Loaded successfully", Status.DONE);
        } else {
            Report.updateTestLog(Action, "Couldn't Load SoapUI Project from location" + Data, Status.DEBUG);
        }
    }

    public void executeSoapUITestCase() {
        String testsuite = Data.trim().split(":", 2)[0];
        String testcase = Data.split(":", 2)[1];
        try {
            if (Reference.isEmpty()) {
                Reference = testsuite + "_" + testcase;
            }
            String resultLocation = getResultLoc();
            System.out.println("SoapUI TestCase Execution in Progress. It may take some time. Please wait for the execution to finish");
            Boolean result = SoapConnector.executeTestCase(Reference, getProjectKey(), testsuite, testcase, FilePath.getCurrentResultsPath() + resultLocation);
            if (result) {
                Report.updateTestLog(Action, "SoapUI Testcase [" + Data + "] executed sucessfully", Status.PASS, resultLocation + File.separator + "report.html");
            } else {
                Report.updateTestLog(Action, "Error in executing SoapUI Testcase [" + Data + "]", Status.FAIL, resultLocation + File.separator + "report.html");
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, "Couldn't execute SoapUI Testcase " + Data + "\nCause :\n" + ex.getMessage(), Status.FAIL);
        }
    }

    public void executeSoapUITestSuite() {
        String testsuite = Data.trim();
        try {
            if (Reference.isEmpty()) {
                Reference = Data;
            }
            String resultLocation = getResultLoc();
            System.out.println("SoapUI TestSuite Execution in Progress. It may take some time. Please wait for the execution to finish");
            Boolean result = SoapConnector.executeTestSuite(Reference, getProjectKey(), testsuite, FilePath.getCurrentResultsPath() + resultLocation);
            if (result) {
                Report.updateTestLog(Action, "SoapUI Testsuite [" + Data + "] executed sucessfully", Status.PASS, resultLocation + File.separator + "report.html");
            } else {
                Report.updateTestLog(Action, "Error in executing SoapUI Testsuite [" + Data + "]", Status.FAIL, resultLocation + File.separator + "report.html");
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, "Couldn't execute SoapUI Testsuite " + Data + "\nCause :\n" + ex.getMessage(), Status.FAIL);
        }
    }

    public void executeSoapUIProject() {
        try {
            if (Reference.isEmpty()) {
                Reference = "SoapProject";
            }
            String resultLocation = getResultLoc();
            System.out.println("SoapUI Project Execution in Progress. It may take some time. Please wait for the execution to finish");
            Boolean result = SoapConnector.executeAllTestSuite(Reference, getProjectKey(), FilePath.getCurrentResultsPath() + resultLocation);
            if (result) {
                Report.updateTestLog(Action, "SoapUI Project executed sucessfully", Status.PASS, resultLocation + File.separator + "report.html");
            } else {
                Report.updateTestLog(Action, "Error in executing SoapUI Project", Status.FAIL, resultLocation + File.separator + "report.html");
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action, "Couldn't execute SoapUI Project \nCause :\n" + ex.getMessage(), Status.FAIL);
        }
    }
}
