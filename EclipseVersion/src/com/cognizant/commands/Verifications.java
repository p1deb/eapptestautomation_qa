/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.commands.image.CommonImageMethods;
import com.cognizant.core.CommandControl;
import com.cognizant.data.UserDataAccess;
import com.cognizant.support.Status;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;

public class Verifications extends Command {

    public Verifications(CommandControl cc) {
        super(cc);
    }

    /**
     * ******************************************
     * Function to verify the title
     *
     * ******************************************
     */
    public void verifyTitle() {
        String strObj = Data;
        if (Driver.getTitle().equals(strObj)) {
            System.out.println(Action + " Passed");
            Report.updateTestLog(Action,
                    "Element Title value " + Driver.getTitle()
                    + " is matched with the expected result",
                    Status.PASS);
        } else {
            System.out.println(Action + " failed");
            Report.updateTestLog(Action,
                    "Element Title value " + Driver.getTitle()
                    + " doesn't match with the expected result",
                    Status.FAIL);

        }
    }

    /**
     * ******************************************
     * Function to verify the text present
     * ******************************************
     */
    public void verifyTextPresentInPage() {
        String strObj = Data;
        if (Driver.findElement(By.tagName("html")).getText().contains(strObj)) {
            Report.updateTestLog(Action, "Expected text "
                    + Data + " is present in the page", Status.PASS);
        } else {
            Report.updateTestLog(Action, "Expected text "
                    + Data + " is not present in the page", Status.FAIL);

        }
    }

    /**
     * ******************************************
     * Function to verify cookies present
     * ******************************************
     */
    public void verifyCookiePresent() {
        try {

            String strCookieName = Data;
            if ((Driver.manage().getCookieNamed(strCookieName) != null)) {
                System.out.println(Action + " Passed");
                Report.updateTestLog(Action,
                        "Cookie with name [" + Data + "] is available",
                        Status.PASS);
            } else {
                System.out.println(Action + " Failed");
                Report.updateTestLog(Action,
                        "Cookie with name [" + Data + "] is not available",
                        Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog(Action, e.getMessage(),
                    Status.FAIL);
            Logger.getLogger(Verifications.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ******************************************
     * Function to verify cookies by name
     * ******************************************
     */
    public void verifyCookieByName() {
        try {

            String strCookieName = Data.split(":", 2)[0];
            String strCookieValue = Data.split(":", 2)[1];
            Cookie cookie = Driver.manage().getCookieNamed(strCookieName);
            if (cookie != null) {
                if ((strCookieValue.equals(cookie.getValue()))) {
                    System.out.println(Action + " Passed");
                    Report.updateTestLog(Action,
                            "Cookie value  is matched with the expected result",
                            Status.PASS);
                } else {
                    Report.updateTestLog(Action, "Cookie value doesn't match with the expected result",
                            Status.FAIL);
                }
            } else {
                System.out.println(Action + " Failed");
                Report.updateTestLog(Action,
                        "Cookie with name [" + Data + "] is not available",
                        Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog(Action, e.getMessage(),
                    Status.FAIL);
            Logger.getLogger(Verifications.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ******************************************
     * Function to verify AlertText
     *
     * ******************************************
     */
    public void verifyAlertText() {
        try {

            String strExpAlertText = Data;
            if ((Driver.switchTo().alert().getText().equals(strExpAlertText))) {
                System.out.println(Action + " Passed");
                Report.updateTestLog(Action,
                        "Alert text is matched with the expected result",
                        Status.PASS);
            } else {
                Report.updateTestLog(Action,
                        "Alert text doesn't match with the expected result",
                        Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog(Action, e.getMessage(), Status.FAIL);
            Logger.getLogger(Verifications.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ******************************************
     * Function to verify AlertTextPresent
     * ******************************************
     */
    public void verifyAlertPresent() {
        try {
            if ((isAlertPresent(Driver))) {
                System.out.println(Action + " Passed");
                Report.updateTestLog(Action,
                        "Alert is present",
                        Status.PASS);
            } else {
                Report.updateTestLog(Action,
                        "Alert is not present",
                        Status.FAIL);
            }
        } catch (Exception e) {
            Report.updateTestLog(Action, e.getMessage(),
                    Status.FAIL);
            Logger.getLogger(Verifications.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * ******************************************
     * Function to verify variable
     *
     * ******************************************
     */
    public void verifyVariable() {
        String strObj = Data;
        String[] strTemp = strObj.split("=", 2);
        String strAns = getVar(strTemp[0]);
        if (strAns.equals(strTemp[1])) {
            System.out.println("Variable " + strTemp[0] + " equals "
                    + strTemp[1]);
            Report.updateTestLog(Action,
                    "Variable is matched with the expected result", Status.PASS);
        } else {
            System.out.println("Variable " + strTemp[0] + " not equals "
                    + strTemp[1]);
            Report.updateTestLog(Action,
                    "Variable doesn't match with the expected result",
                    Status.FAIL);
        }
    }

    public void verifyVariableFromDataSheet() {
        String strAns = getVar(Condition);
        if (strAns.equals(Data)) {
            System.out.println("Variable " + Condition + " equals "
                    + Input);
            Report.updateTestLog(Action,
                    "Variable is matched with the expected result", Status.DONE);

        } else {
            System.out.println("Variable " + Condition + " is not equal "
                    + Input);
            Report.updateTestLog(Action,
                    "Variable doesn't matched with the expected result",
                    Status.DEBUG);
        }
    }

    private boolean isAlertPresent(WebDriver Driver) {
        try {
            Driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, e);
            return false;
        }
    }

}
