/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.support.ForcedException;
import com.cognizant.support.Status;
import com.galenframework.specs.SpecText;
import static com.galenframework.specs.SpecText.Type.CONTAINS;
import static com.galenframework.specs.SpecText.Type.ENDS;
import static com.galenframework.specs.SpecText.Type.IS;
import static com.galenframework.specs.SpecText.Type.MATCHES;
import static com.galenframework.specs.SpecText.Type.STARTS;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.JavascriptExecutor;

public class SwitchTo extends Command {

    public SwitchTo(CommandControl cc) {
        super(cc);
    }

    /**
     * **************************************
     * Function to switch between frames
     *
     * **************************************
     */
    public void switchToFrame() {
        String strTargetFrame = Data;
        try {
            Driver.switchTo().frame(strTargetFrame);
            Report.updateTestLog(Action,
                    "Webdriver switched to new frame by name[" + strTargetFrame + "]",
                    Status.DONE);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, e);
            Report.updateTestLog(Action, e.getMessage(), Status.DEBUG);
        }
    }

    /**
     * ****************************************** Function to switch between
     * frames by index ******************************************
     */
    public void switchToFrameByIndex() {
        if (Data != null && Data.matches("[0-9]+")) {
            int frameIndex = Integer.parseInt(Data);
            try {
                Driver.switchTo().frame(frameIndex);
                Report.updateTestLog(Action,
                        "Webdriver switched to new frame by index[" + frameIndex + "]", Status.DONE);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, e);
                Report.updateTestLog(Action, e.getMessage(),
                        Status.DEBUG);
            }
        } else {
            Report.updateTestLog(Action,
                    "Invalid Frame Index[" + Data + "]", Status.DEBUG);
        }
    }

    /**
     * ******************************************
     * Function to switch between windows By Name
     *
     * ******************************************
     */
    private void switchToWindow(String title, SpecText.Type type) {
        Boolean windowFlag = false;
        Set<String> Handles = Driver.getWindowHandles();
        for (String handle : Handles) {
            Driver.switchTo().window(handle);
            String drivertitle = Driver.getTitle().trim();
            switch (type) {
                case IS:
                    windowFlag = drivertitle.equals(title);
                    break;
                case CONTAINS:
                    windowFlag = drivertitle.contains(title);
                    break;
                case STARTS:
                    windowFlag = drivertitle.startsWith(title);
                    break;
                case ENDS:
                    windowFlag = drivertitle.endsWith(title);
                    break;
                case MATCHES:
                    windowFlag = drivertitle.matches(title);
                    break;
            }
            if (windowFlag) {
                break;
            }
        }

        if (windowFlag) {
            Report.updateTestLog(Action, "Webdriver switched to new window by title[" + title + "]", Status.DONE);
        } else {
            throw new ForcedException(Action, "Can't find a Window by the given Title [" + Data + "]");
        }
    }

    public void switchToWindowByTitle() {
        switchToWindow(Data, SpecText.Type.IS);
    }

    public void switchToWindowByTitleContains() {
        switchToWindow(Data, SpecText.Type.CONTAINS);
    }

    public void switchToWindowByTitleStartsWith() {
        switchToWindow(Data, SpecText.Type.STARTS);
    }

    public void switchToWindowByTitleEndsWith() {
        switchToWindow(Data, SpecText.Type.ENDS);
    }

    public void switchToWindowByTitleMatches() {
        switchToWindow(Data, SpecText.Type.MATCHES);
    }

    /**
     * ******************************************
     * Function to switch between windows By Index
     * ******************************************
     */
    public void switchToWindowByIndex() {
        int wndIndex = Integer.parseInt(Data);
        Set<String> handles = Driver.getWindowHandles();
        if (handles.size() > wndIndex) {
            String handle = handles.toArray()[wndIndex].toString();
            Driver.switchTo().window(handle);
            Report.updateTestLog("switchToWindowByIndex",
                    "Webdriver switched to new window", Status.DONE);
        } else {
            throw new ForcedException(Action, "There are only [" + handles.size() + "] windows present at the moment.Requested window is [" + wndIndex + "] which is out of range");
        }
    }

    /**
     * ******************************************
     * Function to switch to default content
     * ******************************************
     */
    public void switchToDefaultContent() {
        try {
            Driver.switchTo().defaultContent();
            Report.updateTestLog(Action,
                    "Webdriver switched to default content", Status.DONE);
        } catch (Exception e) {
            Report.updateTestLog(Action, e.getMessage(),
                    Status.DEBUG);
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, e);
        }
    }

    public void createAndSwitchToWindow() {
        try {
            JavascriptExecutor js = (JavascriptExecutor) Driver;
            js.executeScript("window.open(arguments[0])", Data);
            Set<String> Handles = Driver.getWindowHandles();
            Driver.switchTo().window((String) Handles.toArray()[Handles.size() - 1]);
            Report.updateTestLog(Action, "New Window Created and Switched to that ", Status.DONE);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
            Report.updateTestLog(Action, "Error in Switching Window -" + ex.getMessage(), Status.DEBUG);
        }
    }

    public void closeAndSwitchToWindow() {
        try {
            Driver.close();
            Driver.switchTo().window((String) Driver.getWindowHandles().toArray()[0]);
            Report.updateTestLog(Action, "Current Window Closed and Switched to Default window ", Status.DONE);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
            Report.updateTestLog(Action, "Error in Switching Window -" + ex.getMessage(), Status.FAIL);
        }
    }

}
