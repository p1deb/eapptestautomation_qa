/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.support.ForcedException;
import com.cognizant.support.Status;

/**
 *
 * @author 394173
 */
public class AssertElement extends General {

    public AssertElement(CommandControl cc) {
        super(cc);
    }

    public void assertElementNotPresent() {
        assertNotElement(!elementPresent());
    }

    public void assertElementNotSelected() {
        assertNotElement(!elementSelected());
    }

    public void assertElementNotDisplayed() {
        assertNotElement(!elementDisplayed());
    }

    public void assertElementNotEnabled() {
        assertNotElement(!elementEnabled());
    }

    public void assertElementPresent() {
        assertElement(elementPresent());
    }

    public void assertElementSelected() {
        assertElement(elementSelected());
    }

    public void assertElementDisplayed() {
        assertElement(elementDisplayed());
    }

    public void assertElementEnabled() {
        assertElement(elementEnabled());
    }

    private void assertElement(Boolean status, String isNot) {
        String value = isNot + Action.replaceFirst("assertElement", "").replaceFirst("Not", "");
        String description = String.format("Element [%s] is %s", ObjectName, value);
        if (status) {
            Report.updateTestLog(Action, description, Status.PASS);
        } else {
            throw new ForcedException(Action, description);
        }
    }

    private void assertElement(Boolean status) {
        assertElement(status, status ? "" : "not ");
    }

    private void assertNotElement(Boolean status) {
        assertElement(status, status ? "not " : "");
    }

    /**
     * Function to assert the complete page source of the current page
     *
     *
     */
    public void assertPageSource() {
        if (Driver.getPageSource().equals(Data)) {
            Report.updateTestLog(
                    Action,
                    "Current Page Source is matched with the expected Page Source",
                    Status.DONE);
        } else {
            throw new ForcedException(Action,
                    "Current Page Source doesn't match with the expected Page Source");
        }
    }

    public void assertHScrollBarPresent() {
        assertHScorllBar("", isHScrollBarPresent());
    }

    public void assertHScrollBarNotPresent() {
        assertHScorllBar("not", !isHScrollBarPresent());
    }

    public void assertVScrollBarPresent() {
        assertVScorllBar("", isvScrollBarPresent());
    }

    public void assertVScrollBarNotPresent() {
        assertVScorllBar("not", !isvScrollBarPresent());
    }

    private void assertHScorllBar(String isNot, Boolean value) {
        assertScorllBar("Horizontal", isNot, value);
    }

    private void assertVScorllBar(String isNot, Boolean value) {
        assertScorllBar("Vertical", isNot, value);
    }

    private void assertScorllBar(String type, String isNot, Boolean value) {
        String desc = type + " Scrollbar is " + isNot + " present";
        if (value) {
            Report.updateTestLog(Action, desc, Status.PASS);
        } else {
            throw new ForcedException(Action, desc);
        }
    }

}
