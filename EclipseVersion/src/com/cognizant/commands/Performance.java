package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.core.Control;
import com.cognizant.data.LoadSettings;
import com.cognizant.reporting.TestCaseReport;
import com.cognizant.reporting.performance.PerformanceTimings;
import com.cognizant.reporting.performance.ResourceTimings;
import com.cognizant.reporting.performance.har.Entry;
import com.cognizant.reporting.performance.har.Har;
import com.cognizant.reporting.performance.har.Page;
import com.cognizant.support.Status;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author 316638
 */
public class Performance extends Command {

    public Performance(CommandControl cc) {
        super(cc);
    }

    public void ClearCache() {
        try {
            Driver.manage().deleteAllCookies();
        } catch (Exception e) {
            System.out.println("Failed to clear cache");
        }

    }

    /**
     * capture browser page navigation and resource timings and store it in the
     * report object
     */
    public void capturePageTimings() {
        if (LoadSettings.getSettings().canReportPerformanceLog()) {
            try {
                storePageTimings();
                Report.updateTestLog(Action, "Timings Updated in Har", Status.DONE);
            } catch (Exception ex) {
                Report.updateTestLog(Action, "Unable to update PageTimings : " + ex.getMessage(),
                        Status.FAIL);
                Logger.getLogger(Performance.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }

    private void storePageTimings() {
        JavascriptExecutor js = (JavascriptExecutor) Driver;
        String pt = js.executeScript(PerformanceTimings.script()).toString();
        String rt="[]";
        try{
          rt = js.executeScript(ResourceTimings.script()).toString();  
        }catch(Exception ex){
           Logger.getLogger(Performance.class.getName()).log(Level.SEVERE,
                   "Error on Resource Timings : {0}",ex.getMessage()); 
        }
        Har<?, ?> har = new Har<>();
        Page p = new Page(pt, har.pages());
        har.addPage(p);
        for (Object res : (JSONArray) JSONValue.parse(rt)) {
            JSONObject jse = (JSONObject) res;
            if (jse.size() > 14) {
                Entry e = new Entry(jse.toJSONString(0), p);
                har.addEntry(e);
            }
        }
        har.addRaw(pt, rt);
        Control.ReportManager.perf.addHar(har, (TestCaseReport) Report,
                Data.replaceAll("[^a-zA-Z0-9-]", "_").replaceAll("__+","_"));
    }
}
