/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.support.ElementException;
import com.cognizant.support.Status;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author 394173
 */
public class JSCommands extends General {

    public JSCommands(CommandControl cc) {
        super(cc);
    }

    public void clickByJS() {
        if (elementPresent()) {
            try {
                JavascriptExecutor js = (JavascriptExecutor) Driver;
                js.executeScript("arguments[0].click();", Element);
                Report.updateTestLog(Action, "Clicked on " + ObjectName, Status.DONE);
            } catch (Exception ex) {
                Logger.getLogger(JSCommands.class.getName()).log(Level.SEVERE, null, ex);
                Report.updateTestLog(Action, "Couldn't click on " + ObjectName + " - Exception " + ex.getMessage(), Status.FAIL);
            }
        } else {
            throw new ElementException(ElementException.ExceptionType.Element_Not_Found);
        }
    }

    public void setEncryptedByJS() {
        if (Data != null && Data.matches(".* Enc")) {
            if (elementEnabled()) {
                try {
                    Data = Data.substring(0, Data.lastIndexOf(" Enc"));
                    byte[] valueDecoded = Base64.decodeBase64(Data);
                    JavascriptExecutor js = (JavascriptExecutor) Driver;
                    js.executeScript("arguments[0].value='" + new String(valueDecoded) + "'", Element);
                    Report.updateTestLog(Action, "Entered Text '" + Data + "' on '" + ObjectName + "'", Status.DONE);
                } catch (Exception ex) {
                    Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
                    Logger.getLogger(JSCommands.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                throw new ElementException(ElementException.ExceptionType.Element_Not_Enabled);
            }
        } else {
            Report.updateTestLog(Action, "Data not encrypted '" + Data + "'", Status.DEBUG);
        }
    }

    public void setByJS() {
        if (elementPresent()) {
            try {
                JavascriptExecutor js = (JavascriptExecutor) Driver;
                js.executeScript("arguments[0].value='" + Data + "'", Element);
                Report.updateTestLog(Action, "Entered Text '" + Data + "' on '" + ObjectName + "'", Status.DONE);
            } catch (Exception ex) {
                Logger.getLogger(JSCommands.class.getName()).log(Level.SEVERE, null, ex);
                Report.updateTestLog(Action, "Couldn't set value on " + ObjectName + " - Exception " + ex.getMessage(), Status.FAIL);
            }
        } else {
            throw new ElementException(ElementException.ExceptionType.Element_Not_Found);
        }
    }

    public void clearByJS() {
        if (elementPresent()) {
            try {
                JavascriptExecutor js = (JavascriptExecutor) Driver;
                js.executeScript("arguments[0].value=''", Element);
                Report.updateTestLog(Action, "Cleared value from '" + ObjectName + "'", Status.DONE);
            } catch (Exception ex) {
                Logger.getLogger(JSCommands.class.getName()).log(Level.SEVERE, null, ex);
                Report.updateTestLog(Action, "Couldn't clear value on " + ObjectName + " - Exception " + ex.getMessage(), Status.FAIL);
            }
        } else {
            throw new ElementException(ElementException.ExceptionType.Element_Not_Found);
        }
    }

    public void selectByJS() {
        if (elementPresent()) {
            try {
                JavascriptExecutor js = (JavascriptExecutor) Driver;
                Object value = js.executeScript(
                        "var options=arguments[0].getElementsByTagName('option');"
                        + "for(var i=0;i<options.length;i++)"
                        + "{"
                        + "var value=options[i].textContent?options[i].textContent:options[i].innerText;"
                        + "if(value.trim()==='" + Data.trim() + "')"
                        + "{"
                        + "options[i].setAttribute('selected','selected');"
                        + "return true;"
                        + "}"
                        + "}"
                        + "return false;", Element);
                if (value != null && value.toString().trim().toLowerCase().equals("true")) {
                    Report.updateTestLog(Action, "Item " + Data + " is selected from" + ObjectName, Status.DONE);
                } else {
                    Report.updateTestLog(Action, "Item " + Data + " is not available in the" + ObjectName, Status.FAIL);
                }
            } catch (Exception ex) {
                Logger.getLogger(JSCommands.class.getName()).log(Level.SEVERE, null, ex);
                Report.updateTestLog(Action, "Couldn't select value from " + ObjectName + " - Exception " + ex.getMessage(), Status.FAIL);
            }
        } else {
            throw new ElementException(ElementException.ExceptionType.Element_Not_Found);
        }
    }
}
