/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CheckBox extends Command {

    public CheckBox(CommandControl cc) {
        super(cc);
    }

    public void check() {
        if (Element != null) {
            if (Element.isEnabled()) {
                if (!Element.isSelected()) {
                    Element.click();
                }
                if (Element.isSelected()) {
                    Report.updateTestLog("check", "Checkbox '" + Element
                            + "'  has been selected/checked successfully",
                            Status.DONE);
                } else {
                    Report.updateTestLog("check", "Checkbox '" + Element
                            + "' couldn't be selected/checked", Status.FAIL);
                }
            } else {
                Report.updateTestLog("check", "Checkbox '" + Element
                        + "' is not enabled", Status.FAIL);
            }
        } else {
            Report.updateTestLog(Action, "Object [" + ObjectName + "] not found", Status.FAIL);
        }
    }

    public void uncheck() {
        if (Element != null) {
            if (Element.isEnabled()) {
                if (Element.isSelected()) {
                    Element.click();
                }
                if (!Element.isSelected()) {
                    Report.updateTestLog("uncheck", "Checkbox '" + Element
                            + "'  has been un-checked successfully",
                            Status.DONE);
                } else {
                    Report.updateTestLog("uncheck", "Checkbox '" + Element
                            + "' couldn't be un-checked", Status.FAIL);
                }
            } else {
                Report.updateTestLog("uncheck", "Checkbox '" + Element
                        + "' is not enabled", Status.FAIL);
            }
        } else {
            Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
        }
    }

    public void checkAllCheckBoxes() {
        try {
            List<WebElement> checkboxes = Driver.findElements(By.cssSelector("input[type=checkbox]"));
            if (checkboxes.isEmpty()) {
                Report.updateTestLog(Action, "No Checkbox present in the page", Status.WARNING);
            } else {
                for (WebElement checkbox : checkboxes) {
                    if (checkbox.isDisplayed() && !checkbox.isSelected()) {
                        checkbox.click();
                    }
                }
                Report.updateTestLog(Action, "All checkboxes are checked", Status.PASS);
            }
        } catch (Exception ex) {           
            Report.updateTestLog(Action, "Error while checking checkboxes - " + ex, Status.FAIL);
            Logger.getLogger(CheckBox.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
