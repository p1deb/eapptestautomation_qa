/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.support.ElementException;
import com.cognizant.support.ElementException.ExceptionType;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;

/**
 *
 * @author 394173
 */
public class General extends Command {

    public General(CommandControl cc) {
        super(cc);
    }

    public Boolean checkIfDriverIsAlive() {
        if (isDriverAlive()) {
            return true;
        } else {
            throw new RuntimeException("Seems like Connection with the driver is lost/driver is closed");
        }
    }

    public Boolean elementPresent() {
        return checkIfDriverIsAlive() && Element != null;
    }

    public Boolean elementSelected() {
        if (!elementDisplayed()) {
            throw new ElementException(ExceptionType.Element_Not_Visible);
        }
        return Element.isSelected();
    }

    public Boolean elementDisplayed() {
        if (!elementPresent()) {
            throw new ElementException(ExceptionType.Element_Not_Found);
        }
        return Element.isDisplayed();
    }

    public Boolean elementEnabled() {
        if (!elementDisplayed()) {
            throw new ElementException(ExceptionType.Element_Not_Visible);
        }
        return Element.isEnabled();
    }

    public boolean isHScrollBarPresent() {
        return (boolean) ((JavascriptExecutor) Driver)
                .executeScript("return document.documentElement.scrollWidth>document.documentElement.clientWidth;");
    }

    public boolean isvScrollBarPresent() {
        return (boolean) ((JavascriptExecutor) Driver)
                .executeScript("return document.documentElement.scrollHeight>document.documentElement.clientHeight;");
    }

    public boolean isAlertPresent() {
        try {
            Driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, e);
            return false;
        }
    }

}
