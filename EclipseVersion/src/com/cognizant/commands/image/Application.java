/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.image;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sikuli.script.App;
import org.sikuli.script.Key;

public class Application extends ImageCommand {

    public static Map<String, App> appList = new HashMap<>();

    public Application(CommandControl cc) {
        super(cc);
    }

    /**
     * Open the given Application
     */
    public void openApp() {
        try {
            String loc, id;
            if (Data.contains(",")) {
                loc = Data.split(",")[0];
                id = Data.split(",")[1];
            } else {
                id = loc = Data;
            }
            appList.put(id, App.open(loc));
            Report.updateTestLog(Action, "Open action is done", Status.DONE);
            Thread.sleep(1000);
        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Close the given application window (should opened with openApp )
     */
    public void closeApp() {
        try {
            String param = Data;
            Thread.sleep(500);
            appList.get(param).close();
            Report.updateTestLog(Action, "Close action is done for app " + Data,
                    Status.DONE);
        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Focus the given application window (should opened with openApp )
     */
    public void focusApp() {
        try {
            String param = Data;
            appList.get(param).focus();
            Report.updateTestLog(Action, "Focus action is done for app " + Data,
                    Status.DONE);
            Thread.sleep(1000);
        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Perform the given shortcut operation
     */
    public void shortcutKeys() {
        try {
            String[] keys = Data.split("\\+", -1);
            shortcutKeys(new ArrayList<>(Arrays.asList(keys)));
            Report.updateTestLog(Action, "Short Cut " + Data + "  is done",
                    Status.DONE);
            Thread.sleep(1000);
        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Press keyboard keys (need manual release)
     */
    public void pressKeys() {
        try {
            String[] keys = Data.split("\\+", -1);
            pressKeys(new ArrayList<>(Arrays.asList(keys)));
            Report.updateTestLog(Action, "Key [" + Data + "] press  action is done",
                    Status.DONE);
            Thread.sleep(1000);
        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Release the keys pressed
     */
    public void releaseKeys() {
        try {
            String[] keys = Data.split("\\+", -1);
            releaseKeys(new ArrayList<>(Arrays.asList(keys)));
            Report.updateTestLog(Action, "Key [" + Data + "] release  action is done",
                    Status.DONE);
            Thread.sleep(1000);
        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Type single keyboard commands(Delete,Escape..)
     */
    public void keyboardKey() {

        try {
            SCREEN.type(getKeyCode(Data));
            Report.updateTestLog(Action, "Key [" + Data + "] action is done",
                    Status.DONE);

        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Press page down given number of times
     */
    public void pageDown() {
        try {
            int count = 1;
            if (Data != null) {
                count = Integer.valueOf(Data);
            }
            for (int i = 1; i <= count; i++) {
                SCREEN.type(Key.PAGE_DOWN);
            }

            Report.updateTestLog(Action, "PageDown [" + Data + "] action is done",
                    Status.DONE);

        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Function to click the give text on SCREEN
     */
    public void clickOn() {
        try {
            if (!Data.isEmpty()) {
                SCREEN.click(Data);
                Report.updateTestLog(Action, "Clicke on text " + Data, Status.DONE);
                Thread.sleep(500);
            } else {
                Report.updateTestLog(Action, "Action not performed,(Empty value received!)", Status.DONE);
            }

        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Function to store values into Data sheet. Input : [data
     * sheet],[column],[value]
     */
    public void storeData() {
        try {
            String sheet = Data.split(",", -1)[0];
            String col = Data.split(",", -1)[1];
            String val = Data.split(",", -1)[2];
            if (val.startsWith("%") && val.endsWith("%")) {
                val = this.getVar(val);
            }
            userData.putData(sheet, col, val);
            Report.updateTestLog(Action, "Store  action is done", Status.DONE);
        } catch (Exception ex) {
            Report.updateTestLog(Action,ex.getMessage(), Status.FAIL);
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
