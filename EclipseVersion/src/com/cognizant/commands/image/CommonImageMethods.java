/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.image;

import com.cognizant.core.CommandControl;
import com.cognizant.core.CommandControl.IObject;
import com.cognizant.support.ElementException.ExceptionType;
import com.cognizant.support.Flag;
import com.cognizant.support.ForcedException;
import com.cognizant.support.Status;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Region;

public class CommonImageMethods extends ImageCommand {

    public CommonImageMethods(CommandControl cc) {
        super(cc);
    }

    // ----------------------ACTIONS--------------------
    /**
     * Find and click on a image in SCREEN
     */
    public void imgClick() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.click(target, getKeyModifier()) == 1) {
                    Report.updateTestLog(Action, "clicked on "+ObjectName,
                            Status.DONE);
                    return;
                }

            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);

        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Type the given text on the image
     */
    public void imgType() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.type(target, Data) == 1) {
                    Report.updateTestLog(Action, "Typed "+Data+" on "+ObjectName,
                            Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Type the given text on the SCREEN(focused element)
     */
    public void typeOnScreen() {
        try {
            String param = Data;
            SCREEN.type(param);
            Report.updateTestLog(Action, " Typed "+ Data+" on screen",
                    Status.DONE);
            Thread.sleep(1000);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * clear all text in the given image
     */
    public void imgClearText() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                ((Region) target).click();

                robot.keyPress(getKeyEvent("Ctrl"));
                robot.keyPress(KeyEvent.VK_A);
                robot.keyRelease(KeyEvent.VK_A);
                robot.keyRelease(getKeyEvent("Ctrl"));
                robot.keyPress(getKeyEvent("BACKSPACE"));
                robot.keyRelease(getKeyEvent("BACKSPACE"));
                Report.updateTestLog(Action,
                        "Cleared Text  on "+ObjectName, Status.DONE);
                return;

            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * clear and set the value on a image
     */
    public void imgClearAndSet() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                ((Region) target).click();
                robot.keyPress(getKeyEvent("Ctrl"));
                robot.keyPress(KeyEvent.VK_A);
                robot.keyRelease(KeyEvent.VK_A);
                robot.keyRelease(getKeyEvent("Ctrl"));
                robot.keyPress(getKeyEvent("BACKSPACE"));
                robot.keyRelease(getKeyEvent("BACKSPACE"));
                Thread.sleep(99);
                SCREEN.paste(Data);
                Report.updateTestLog(Action,
                        "Clear and Set on "+ObjectName+" is done", Status.DONE);
                return;

            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Mouse over on a image
     */
    public void imgHover() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.hover(target) == 1) {
                    Report.updateTestLog(Action, "Hovered on "+ObjectName,
                            Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * paste/set the given text on the image
     */
    public void imgSet() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.paste(target, Data) == 1) {
                    Report.updateTestLog(Action, "Paste action is done on "+ObjectName,
                            Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * decrypt and paste/set the given text on the image
     */
    public void imgSetEncrypted() {

        try {
            if (Data.endsWith(" Enc")) {
                Data = Data.substring(0, Data.lastIndexOf(" Enc"));
                byte[] valueDecoded = Base64.decodeBase64(Data);
                Data = new String(valueDecoded);
            }
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.paste(target, Data) == 1) {
                    Report.updateTestLog(Action,
                            "Encrypted text set on "+ObjectName, Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(),
                    Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Double click on the Image
     */
    public void imgDoubleClick() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.doubleClick(target, getKeyModifier()) == 1) {
                    Report.updateTestLog(Action,
                            "DoubleClick action is done on "+ObjectName, Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Right click on the image
     */
    public void imgRightClick() {
        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.rightClick(target, getKeyModifier()) == 1) {
                    Report.updateTestLog(Action,
                            "RightClick action is done on "+ObjectName, Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * wait for given image to appear (default time is 10 seconds)
     */
    public void imgWait() {
        try {
            int timeout = parseToInt(Data, 10), timepassed = 0;
            Date startTime = new Date(), curTime;
            while (timepassed < timeout) {
                curTime = new Date();
                timepassed = (int) ((curTime.getTime() - startTime.getTime()) / 1000);
                target = findTarget(ObjectList, Flag.REGION_ONLY,
                        Flag.MATCH_ONLY);
                if (target != null) {
                    Report.updateTestLog(Action, "Wait action is done on "+ObjectName,
                            Status.DONE);
                    return;
                }
                Thread.sleep(200);
                System.out.println("checking------------->");
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * wait for the image to vanish
     */
    public void imgWaitVanish() {

        double msec = parseToDouble(Data, SCREEN.getAutoWaitTimeout());
        boolean found = false;
        try {
            for (IObject obj : ObjectList) {
                target = getPattern(obj);
                setSettings(obj);
                try {
                    if (SCREEN.find(target) != null) {
                        found = true;
                        if (SCREEN.waitVanish(target, msec)) {
                            Report.updateTestLog(Action,
                                    "WaitVanish action is done on "+ObjectName, Status.DONE);
                            return;
                        }
                    }
                } catch (FindFailed ex) {
                 Logger.getLogger(CommonImageMethods.class.getName()).log(Level.WARNING, null, ex);
                }
            }
            Report.updateTestLog(
                    Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group
                    : (found ? " is not vanishing" : ExceptionType.Not_Found_on_Screen)),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * verify the image present on SCREEN
     */
    public void imgVerifyImage() {

        try {
            target = findTarget(ObjectList, Flag.REGION_ONLY, Flag.MATCH_ONLY);
            if (target != null) {
                Report.updateTestLog(Action, "The Image "+ObjectName+" Exists on screen",
                        Status.PASS);
                return;
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.DEBUG);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * assert the image present on SCREEN (stop execution if fails on break on
 error mode)
     */
    public void imgAssertImage() {

        try {
            target = findTarget(ObjectList, Flag.REGION_ONLY, Flag.MATCH_ONLY);
            if (target != null) {
                Report.updateTestLog(Action,"The Image "+ObjectName+" Exists on screen",
                        Status.PASS);
                return;
            }
            throw new Exception(ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen));
        } catch (Exception ex) {
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
            throw new ForcedException(Action, ex.getMessage());
        }

    }

    /**
     * Fine the image on the web page by Page down for given timeout (default 20
     * seconds)
     */
    public void imgFindinPage() {
        try {
            int timeout = parseToInt(Data,20), timepassed = 0;
            Date startTime = new Date(), curTime;
            while (timepassed < timeout) {
                SCREEN.type(Key.PAGE_DOWN);
                Thread.sleep(500);
                curTime = new Date();
                timepassed = (int) ((curTime.getTime() - startTime.getTime()) / 1000);

                target = findTarget(ObjectList, Flag.REGION_ONLY,
                        Flag.MATCH_ONLY);
                if (target != null) {
                    ((Region) target).highlight(1);
                    Thread.sleep(700);
                    Report.updateTestLog(Action, "Finding "
                            + ObjectName + " action is done", Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);

        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
            Logger.getLogger(CommonImageMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
