/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.image;

import com.cognizant.core.CommandControl;
import com.cognizant.support.ElementException.ExceptionType;
import com.cognizant.support.Flag;
import com.cognizant.support.Status;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sikuli.script.Button;
import org.sikuli.script.Region;

public class Mouse extends ImageCommand {

    public Mouse(CommandControl cc) {
        super(cc);
    }

    /**
     * perform mouse down on the given image
     */
    public void imgmouseDown() {
        int button = parseToInt(Data, Button.LEFT);
        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                ((Region) target).mouseDown(button);
                Report.updateTestLog(Action, "Mouse Down action is done on " + ObjectName,
                        Status.DONE);
                return;

            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);

        } catch (Exception ex) {
            Report.updateTestLog(Action,
                    ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Mouse.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * perform mouse up on the given image
     */
    public void imgmouseUp() {
        int button = parseToInt(Data, Button.LEFT);
        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                ((Region) target).mouseUp(button);
                Report.updateTestLog(Action, "Mouse Up action is done on " + ObjectName,
                        Status.DONE);
                return;

            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);

        } catch (Exception ex) {
            Report.updateTestLog(Action,
                    ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Mouse.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void moveMouseTO() {
        try {
            Robot r = new Robot();
            int x = Integer.valueOf(Data.split(",")[0]);
            int y = Integer.valueOf(Data.split(",")[1]);
            //use graphic environment lib. if its is a multi SCREEN 
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            if (x < 0) {
                x += screenSize.getWidth();//x pixels left of right end
            }
            if (y < 0) {
                y += screenSize.getHeight();//y pixels above bottom end
            }
            r.mouseMove(x, y);
            Report.updateTestLog(Action, "Mouse moved to '" + x + "," + y + "' ", Status.DONE);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(), Status.FAIL);
            Logger.getLogger(Mouse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
