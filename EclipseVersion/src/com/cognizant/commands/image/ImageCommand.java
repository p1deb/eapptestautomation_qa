/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.image;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.core.CommandControl.IObject;
import com.cognizant.support.Flag;
import com.cognizant.support.UnCaughtException;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

public class ImageCommand extends Command {

    static final Screen SCREEN = new Screen();
    static int smartIndex = 0;
    int index = 0;
    Object target, droptarget;
    File tmp;
    Region r;
    List<Match> res;
    Flag iflag;

    public ImageCommand(CommandControl cc) {
        super(cc);
        Settings.ActionLogs = true;
        Settings.InfoLogs = true;
        Settings.MoveMouseDelay = 0.1f;
        Settings.SlowMotionDelay = 0.1f;
        Settings.DebugLogs = true;
        Settings.OcrTextRead = true;
        Settings.OcrTextSearch = true;
    }

    Object getPattern(IObject obj, Flag... flag) throws UnCaughtException {

        tmp = new File(obj.loc);
        iflag = Flag.IMAGE_AND_TEXT;
        if (flag.length > 0) {
            iflag = flag[0];
        }
        boolean validfile = tmp.exists() && tmp.isFile();

        if (iflag == Flag.TEXT_ONLY) {
            if (!"".equals(obj.text)) {
                return obj.text;
            } else {
                throw new UnCaughtException("Empty Text is Given",
                        "The Object '" + obj.name + "' contains Empty Text!!!");
            }
        } else if (iflag == Flag.IMAGE_ONLY) {
            if (validfile) {
                return new Pattern(obj.loc).targetOffset(obj.offsetx,
                        obj.offsety);
            } else {
                throw new UnCaughtException("File Not Found", obj.loc
                        + " is Missing!!!");
            }
        } else if (validfile) {
            return new Pattern(obj.loc).targetOffset(obj.offsetx, obj.offsety);
        } else if (!"".equals(obj.text)) {
            return obj.text;
        } else {
            throw new UnCaughtException("Empty Text is Given",
                    "The Object '" + obj.name + "' contains Empty Text!!!");
        }
    }

    public int getKeyModifier() {
        if (Data != null) {
            switch (Data.toUpperCase()) {
                case "SHIFT":
                    return KeyModifier.SHIFT;
                case "CTRL":
                    return KeyModifier.CTRL;
                case "ALT":
                    return KeyModifier.ALT;
                case "START":
                    return KeyModifier.WIN;
                default:
                    return 0;
            }
        } else {
            return 0;
        }
    }

    public String getKeyCode(String data) {
        switch (data.toUpperCase()) {
            case "TAB":
                return Key.TAB;
            case "ENTER":
                return Key.ENTER;
            case "SHIFT":
                return Key.SHIFT;
            case "CTRL":
                return Key.CTRL;
            case "ALT":
                return Key.ALT;
            case "START":
                return Key.WIN;
            case "ESC":
                return Key.ESC;
            case "DELETE":
                return Key.DELETE;
            case "BACKSPACE":
                return Key.BACKSPACE;
            case "HOME":
                return Key.HOME;
            case "CAPS LOCK":
                return Key.CAPS_LOCK;
            case "PAGE_UP":
            case "PAGEUP":
                return Key.PAGE_UP;
            case "PAGE_DOWN":
            case "PAGEDOWN":
                return Key.PAGE_DOWN;
            case "UP":
                return Key.UP;
            case "DOWN":
                return Key.DOWN;
            case "LEFT":
                return Key.LEFT;
            case "RIGHT":
                return Key.RIGHT;
            default:
                return data;

        }

    }

    public int getKeyEvent(String key) {
        switch (key.toUpperCase()) {
            case "TAB":
                return KeyEvent.VK_TAB;
            case "ENTER":
                return KeyEvent.VK_ENTER;
            case "SHIFT":
                return KeyEvent.VK_SHIFT;
            case "CTRL":
                return KeyEvent.VK_CONTROL;
            case "ALT":
                return KeyEvent.VK_ALT;
            case "START":
                return KeyEvent.VK_WINDOWS;
            case "DELETE":
                return KeyEvent.VK_DELETE;
            case "BACKSPACE":
                return KeyEvent.VK_BACK_SPACE;
            case "HOME":
                return KeyEvent.VK_HOME;
            case "PAGE_UP":
            case "PAGEUP":
                return KeyEvent.VK_PAGE_UP;
            case "PAGE_DOWN":
            case "PAGEDOWN":
                return KeyEvent.VK_PAGE_DOWN;
            case "UP":
                return KeyEvent.VK_UP;
            case "DOWN":
                return KeyEvent.VK_DOWN;
            case "LEFT":
                return KeyEvent.VK_LEFT;
            case "RIGHT":
                return KeyEvent.VK_RIGHT;
            default:
                return KeyEvent.VK_ESCAPE;
        }
    }

    public void shortcutKeys(ArrayList<String> keys) {
        try {
            int s = keys.size();
            String end = keys.get(s - 1).toLowerCase();
            keys.remove(end);
            pressKeys(keys);
            SCREEN.type(getKeyCode(end));
            releaseKeys(keys);
        } catch (Exception ex) {
            Logger.getLogger(ImageCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pressKeys(ArrayList<String> keys) {
        for (String key : keys) {
            robot.keyPress(getKeyEvent(key));
        }
    }

    public void releaseKeys(List<String> keys) {
        for (String key : keys) {
            robot.keyRelease(getKeyEvent(key));
        }
    }

    public double parseToDouble(String data, double val) {
        if (data != null && !data.equals("")) {
            try {
                val = Double.valueOf(data);
            } catch (Exception ex) {
                Logger.getLogger(ImageCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return val;
    }

    public int parseToInt(String data, int val) {
        if (data != null && !data.equals("")) {
            try {
                val = Integer.valueOf(data);
            } catch (Exception ex) {
                Logger.getLogger(ImageCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return val;
    }

    @SuppressWarnings("unchecked")
    Object findTarget(List<IObject> objects, Flag offserFlag,
            Flag coordinateFlag, Flag... imageflag) throws UnCaughtException {
        Object target;
        int currIndex = -1, dx = 0;
        if (smartIndex > 0 && smartIndex < objects.size()) {
            objects.add(0, objects.remove(smartIndex));
            dx = 1;
        }

        for (IObject obj : objects) {
            try {
                currIndex++;
                target = getPattern(obj, imageflag);
                setSettings(obj);
                res = (List<Match>) getList(SCREEN.findAll(target));
                target = null;
                if (res != null) {
                    r = getRegion();
                    if (offserFlag == Flag.SET_OFFSET) {
                        r.x += obj.offsetx;
                        r.y += obj.offsety;
                    }
                    if (currIndex > 0) {
                        smartIndex = currIndex - dx;
                    }
                    return r;
                }
            } catch (FindFailed | UnCaughtException ex) {
                Logger.getLogger(ImageCommand.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                throw new UnCaughtException(ex.getMessage());
            }
        }

        if (coordinateFlag == Flag.SET_COORDINATES && !ObjectList.isEmpty()) {
            r = Region.create(ObjectList.get(0).coordinates);
        } else {
            r = null;
        }
        return r;
    }

    Region getRegion() {
        if (res.size() < index) {
            index = res.size() - 1;
        }
        return res.get(index);
    }

    Region getRegion(Region r, String where, Integer... size) {

        switch (where.toUpperCase()) {
            case "RIGHT":
                return r.right(size.length == 1 ? size[0] : r.w);
            case "LEFT":
                return r.left(size.length == 1 ? size[0] : r.w);
            case "ABOVE":
                return r.above(size.length == 1 ? size[0] : r.h);
            case "BELOW":
                return r.below(size.length == 1 ? size[0] : r.h);
            default:
                return r;
        }
    }

    void setSettings(IObject obj) {
        SCREEN.setROI(obj.roi);
        org.sikuli.basics.Settings.MinSimilarity = obj.precision;
        index = obj.index;
    }

    List<?> getList(Iterator<?> it) {
        if (!it.hasNext()) {
            return null;
        }
        ls.clear();
        while (it.hasNext()) {
            ls.add(it.next());
        }
        return ls;
    }

}
