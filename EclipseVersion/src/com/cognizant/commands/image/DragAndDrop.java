/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.image;

import com.cognizant.core.CommandControl;
import com.cognizant.core.CommandControl.IObject;
import com.cognizant.drivers.AutomationObject;
import com.cognizant.support.ElementException.ExceptionType;
import com.cognizant.support.Flag;
import com.cognizant.support.Status;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sikuli.script.Region;

public class DragAndDrop extends ImageCommand {

    public DragAndDrop(CommandControl cc) {
        super(cc);
    }

    /**
     * Find and drag the given Image
     */
    public void imgDrag() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.drag(target) == 1) {
                    Report.updateTestLog(Action, "Dragged image " + ObjectName,
                            Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action, ex.getMessage(),
                    Status.DEBUG);
            Logger.getLogger(DragAndDrop.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Drop the dragged image to the given image
     */
    public void imgDropAt() {

        try {
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            if (target != null) {
                if (SCREEN.dropAt(target) == 1) {
                    Report.updateTestLog(Action, "Dropped the drageed on " + ObjectName,
                            Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action,
                    ex.getMessage(), Status.DEBUG);
            Logger.getLogger(DragAndDrop.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Drag and drop the image to another image
     */
    public void imgDragandDrop() {

        try {
            String page = Data.split(",", -1)[0];
            String object = Data.split(",", -1)[1];
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            List<IObject> DropList = new ArrayList<>();
            for (Map<String, String> prop : AutomationObject.getIObject(page, object)) {
                DropList.add(new IObject(prop, page));
            }

            droptarget = findTarget(DropList, Flag.SET_OFFSET, Flag.MATCH_ONLY);

            if (target != null) {
                if (SCREEN.dragDrop(target, droptarget) == 1) {
                    Report.updateTestLog(Action,
                            "Dragged  image " + ObjectName + " and Dropped on " + object, Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(
                    Action,
                    target == null ? ObjectName : object
                            + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception e) {
            Report.updateTestLog(Action,
                    e.getMessage(), Status.DEBUG);
        }

    }

    /**
     * Drag and drop to the given location
     */
    public void imgDragandDropAt() {

        try {
            int x = Integer.valueOf(Data.split(",", -1)[0]), y = Integer
                    .valueOf(Data.split(",", -1)[1]), width = Integer
                    .valueOf(Data.split(",", -1)[2]), height = Integer
                    .valueOf(Data.split(",", -1)[3]);
            target = findTarget(ObjectList, Flag.SET_OFFSET, Flag.MATCH_ONLY);
            droptarget = Region.create(x, y, width, height);

            if (target != null) {
                if (SCREEN.dragDrop(target, droptarget) == 1) {
                    Report.updateTestLog(Action,
                            "Draged image " + ObjectName + " and Dropped at given location", Status.DONE);
                    return;
                }
            }
            Report.updateTestLog(Action,
                    ObjectName
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen),
                    Status.FAIL);
        } catch (Exception ex) {
            Report.updateTestLog(Action,
                    ex.getMessage(), Status.DEBUG);
            Logger.getLogger(DragAndDrop.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
