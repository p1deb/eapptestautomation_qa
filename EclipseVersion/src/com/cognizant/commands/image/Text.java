/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands.image;

import com.cognizant.core.CommandControl;
import com.cognizant.support.ElementException.ExceptionType;
import com.cognizant.support.Flag;
import com.cognizant.support.ForcedException;
import com.cognizant.support.Status;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sikuli.script.Region;

public class Text extends ImageCommand {

    public Text(CommandControl cc) {
        super(cc);
    }

    public void imgAssertText() {
        assertText("");
    }

    public void imgAssertTextAbove() {
        assertText("Above");
    }

    public void imgAssertTextBelow() {
        assertText("Below");
    }

    public void imgAssertTextRight() {
        assertText("Right");
    }

    public void imgAssertTextLeft() {
        assertText("Left");
    }

    public void imgVerifyText() {
        verifyText("");
    }

    public void imgVerifyTextAbove() {
        verifyText("Above");
    }

    public void imgVerifyTextBelow() {
        verifyText("Below");
    }

    public void imgVerifyTextRight() {
        verifyText("Right");
    }

    public void imgVerifyTextLeft() {
        verifyText("Left");
    }

    public void imgStoreText() {
        storeText("");
    }

    public void imgStoreTextAbove() {
        storeText("Above");
    }

    public void imgStoreTextBelow() {
        storeText("Below");
    }

    public void imgStoreTextRight() {
        storeText("Right");
    }

    public void imgStoreTextLeft() {
        storeText("Left");
    }

    private void storeText(String where) {
        try {
            String varName = Data.contains(",") ? Data.split(",", -1)[0] : Data;
            if (varName.startsWith("%") && varName.endsWith("%")) {
                target = findTarget(ObjectList, Flag.REGION_ONLY,
                        Flag.SET_COORDINATES, Flag.IMAGE_ONLY);
                if (target != null) {
                    Region rx = getRegion((Region) target, where);
                    rx.highlight(1);
                    String text = rx.text();
                    if (text != null) {
                        addVar(varName, text);
                        Report.updateTestLog(Action, "Text '"
                                + text + "' is stored in '" + varName + "'",
                                Status.DONE);
                        return;
                    }
                }
                Report.updateTestLog(Action,
                        ObjectName
                        + (ObjectList.isEmpty() ? ExceptionType.Empty_Group :ExceptionType.Not_Found_on_Screen),
                        Status.FAIL);
            } else {
                Report.updateTestLog(Action,
                        "Variable format is not correct", Status.FAIL);
            }
        } catch (Exception ex) {
            Report.updateTestLog(Action,
                    ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void verifyText(String where) {
        try {
            Region rx;
            boolean found = false;
            rx = (Region) findTarget(ObjectList, Flag.REGION_ONLY,
                    Flag.SET_COORDINATES, Flag.IMAGE_ONLY);

            if (rx != null) {
                found = true;
                rx = getRegion(rx, where);
                if (rx.text().contains(Data)) {
                    rx.highlight(1);
                    Report.updateTestLog(Action, "The Text \""
                            + Data + "\" is  Exists", Status.PASS);
                    return;
                }
            }
            Report.updateTestLog(Action, (found ? Data
                    : ObjectName)
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group : ExceptionType.Not_Found_on_Screen), Status.FAIL);

        } catch (Exception ex) {
            Report.updateTestLog(Action,
                    ex.getMessage(), Status.DEBUG);
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void assertText(String where) {
        try {
            Region rx;
            boolean found = false;
            rx = (Region) findTarget(ObjectList, Flag.REGION_ONLY,
                    Flag.SET_COORDINATES, Flag.IMAGE_ONLY);

            if (rx != null) {
                found = true;
                rx = getRegion(rx, where);
                if (rx.text().contains(Data)) {
                    rx.highlight(1);
                    Report.updateTestLog(Action, "The Text \""
                            + Data + "\" is  Exists", Status.PASS);
                    return;
                }
            }
            throw new Exception((found ? Data
                    : ObjectName)
                    + (ObjectList.isEmpty() ? ExceptionType.Empty_Group :ExceptionType.Not_Found_on_Screen));

        } catch (Exception ex) {
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
            throw new ForcedException(Action, ex.getMessage());
        }

    }

}
