/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.constants.FilePath;
import java.io.IOException;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.security.UserAndPassword;

public class BrowserUtility extends Command {

    public BrowserUtility(CommandControl cc) {
        super(cc);
    }

    /**
     * Maximizes the browser window
     */
    public void maximize() {
        try {
            Driver.manage().window().maximize();
            Report.updateTestLog("maximize", " Window is maximized ",
                    Status.DONE);
        } catch (Exception ex) {
            Report.updateTestLog("maximize", "Unable to maximize the Window ",
                    Status.FAIL);
            Logger.getLogger(BrowserUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void authenticate() {
        Boolean skip = Condition != null && Condition.equals("optional");
        switch (getCurrentBrowserName()) {
            case "IE":
                authenticateIE();
                break;
            case "Chrome":
            case "Firefox":
            default:
                Report.updateTestLog(Action, "Action not supported for Browser " + getCurrentBrowserName(), skip ? Status.DONE : Status.DEBUG);
        }
    }

    private void authenticateIE() {
        if (Data != null) {
            try {
                String userName = Data.split("##")[0];
                String password = Data.split("##")[1];
                Driver.switchTo().alert().authenticateUsing(new UserAndPassword(userName, password));
                Report.updateTestLog(Action, "Authenticated using " + Data, Status.FAIL);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                Report.updateTestLog(Action, "Couldn't Authenticate" + ex.getMessage(), Status.FAIL);
            }
        } else {
            Report.updateTestLog(Action, "Invalid Credentials " + Data, Status.DEBUG);
        }
    }

    public void handleAuthentication(String browser) {
        String browserName = getCurrentBrowserName();
        if (browserName.equalsIgnoreCase("chrome")
                || browserName.equalsIgnoreCase("IE")) {
            handleChromeAuthentication();
        } else if (browserName.equalsIgnoreCase("firefox")) {
            handleFirefoxAuthentication();
        } else {
            handleChromeAuthentication();
        }
    }

    public int handleFirefoxAuthentication() {
        String exeLocation = FilePath.getSpritzPath()
                + "\\userdefined\\Firefoxauthentication.exe";
        String uData[] = Data.split(",", 2);
        exeLocation = exeLocation + " " + uData[0] + " " + uData[1];
        System.out.println(exeLocation);
        try {
            Runtime.getRuntime().exec(exeLocation);
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return 0;

    }

    public int handleChromeAuthentication() {
        String exeLocation = FilePath.getSpritzPath()
                + "\\userdefined\\Chromeauthentication.exe";
        String uData[] = Data.split(",", 2);
        exeLocation = exeLocation + " " + uData[0] + " " + uData[1];
        System.out.println(exeLocation);
        try {
            Runtime.getRuntime().exec(exeLocation);
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, e);
        }
        return 0;

    }

    public int handleIEAuthentication() {
        String exeLocation = FilePath.getSpritzPath()
                + "\\userdefined\\IEauthentication.exe";
        String userData[] = Data.split(",", 2);
        exeLocation = exeLocation + " " + userData[0] + " " + userData[1];
        System.out.println(exeLocation);
        try {
            Runtime.getRuntime().exec(exeLocation);
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, e);
        }
        return 0;
    }

}
