/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.constants.ObjectProperty;
import com.cognizant.constants.SystemDefaults;
import com.cognizant.core.CommandControl;
import com.cognizant.data.DataAccess;
import com.cognizant.support.UnCaughtException;
import com.cognizant.support.Status;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
//extend Command to access elements
//make sure you rename class name as well//Only For Inject script
//Make Sure U save or put ur java file in Userdefined folder//Only For Inject script

public class SampleScript extends Command {
    // create your own function

    public SampleScript(CommandControl cc) {
        super(cc);
    }

    public void prinThis(String argumentShouldNotBeGiven
    /**
     * No argument should be specifed Then only Spritz will execute[show in
     * action column]
     */
    ) {

        //To do any operations before and/or after execution of eachStep add your code to
        //functions beforeStepExecution / afterStepExecution in
        //Annotation.java inside com.cognizant.core package
        //To do any operation after execution [execution is finished] add your code to
        //afterReportComplete function in
        //SummaryReport.java inside com.cognizant.reporting package
        // do ur action
        try {
            Element.click();//Object in ObjectName is resolved as WebElement and assigned to this variable[Element]

            System.out.println(ObjectName + "ObjectName used in ObjectColumn in the currentTestStep");
            System.out.println(Description + "Description used in DescriptionColumn in the currentTestStep");
            System.out.println(Action + "Action/Command used in ActionColumn in the currentTestStep");
            System.out.println(Input + "Input used in InputColumn in the currentTestStep");
            System.out.println(Data + "Resolved Input used in InputColumn in the currentTestStep");
            System.out.println(Reference + "Reference/PageName used in ReferenceColumn in the currentTestStep");

            System.out.println(getCurrentBrowserName() + "To get the current browserName");

            //If you stored some dynamic value in a varaible[%newVar%] you can get the value from the varaible using
            String value = getVar("%newVar%");
            System.out.println(value);

            //If you want to access the userdefined data created from options pane you can use it in two ways
            //One
            value = getVar("%userdefinedVar%");
            value = getVar("userdefinedVar");//This also will work
            //Two
            value = getUserDefinedData("userdefinedVar");

            System.out.println(value);

            //If you want to store some value in a varaible[%dyanmicVar%] you can store the value into a varaible using
            //Scope is for Current Testcase
            addVar("%dyanmicVar%", "Value to be Stored");

            //Scope is for All
            addGlobalVar("%dyanmicVar%", "Value to be Stored");

            // Using Inbuilt findMethod
            //To find the current step's object
            WebElement element = AObject.findElement(ObjectName, Reference);
            List<WebElement> elementList = AObject.findElements(ObjectName, Reference);

            // to access the object value pass ObjectNameand PageName as inputs
            // ObjectName=p
            // PageName=Yahoo
            element = AObject.findElement("p", "Yahoo");
            elementList = AObject.findElements("p", "Yahoo");

            // -----Using Conditioned FindMethod
            element = AObject.findElement("p", "Yahoo", ObjectProperty.Id);
            elementList = AObject.findElements("p", "Yahoo", ObjectProperty.ClassName);

            //-----Using Own findMethod
            element = Driver.findElement(By.id(Data));

            // using this element u can perform selenium operations
            element.sendKeys("Normal");

            element.sendKeys(Data);

            //To get a property of an object from ObjectRepository
            String prop = AObject.getObjectProperty("pageName", "objectName", ObjectProperty.Id);
            //To get current step object's id property
            prop = AObject.getObjectProperty(Reference, ObjectName, ObjectProperty.Id);

            System.out.println("No of elements" + elementList.size());
            // to access the data from DataSheets pass TestDataname and
            // Column name as inputs
            // Don't pass GlobalData as inputsheet

            //                              SheetName,Columnname  
            String input = userData.getData("Sample", "Data1");

            //To get data from globalData
            String key = "#gd1";
            String column = "ColumnName";
            String val = userData.getGlobalData(key, column);

            //To get values from specified Iteration and subiteration
            input = userData.getData("Sample", "Data1", "1", "1");
            userData.getNoOfSubIterations();

            //To get values from specified Scenario, Testcase, Iteration and subiteration
            input = userData.getData("Sample", "Data1", "scenario", "testcase", "1", "1");

            element.sendKeys(input);

            //To write values into DataSheet  Don't pass GlobalData as inputsheet
            //                SheetName,Columnname,value
            userData.putData("Sample", "Data1", "kk");

            //to write values for specified Iteration and subiteration
            userData.putData("Sample", "Data1", "kk", "1", "1");

            //to write values for specified Scenario, Testcase, Iteration and subiteration
            userData.putData("Sample", "Data1", "kk", "scenario", "testcase", "1", "1");

            //to write values into global data
            userData.putGlobalData(key, column, "NewValue");

            //To display in Report
            Report.updateTestLog("Userdefined Action ", "Operation Done successfully", Status.PASS);

            //To display in Report with custom html tags
            Report.updateTestLog("Userdefined Action ", "#CTAG <b>Operation Done successfully</b>", Status.PASS);

            //To get the current Iteration
            userData.getIteration();

            //To get the current SubIteration
            userData.getSubIteration();

            //To get the current Scenario
            userData.getScenario();

            //To get the current Testcase
            userData.getTestCase();

            //To get the current BrowserName
            System.out.println(getCurrentBrowserName());

            // to stop the current iteration if u want to... based on condition
            Boolean something = false;
            if (something) {
                SystemDefaults.stopCurrentIteration.set(true);
                SystemDefaults.stopExecution.set(true);//Stop the execution
            }

            //simple way
            //make sure you set the Data,Element and other varaibles
            getCommander().Element = element;
            getCommander().Data = "NewValue";
            new Basic(getCommander()).Set();
            new Basic(getCommander()).Click();
            new AssertElement(getCommander()).assertElementPresent();

            //Old school
            executeMethod(element, "Click");
            executeMethod("open", "@http://something");
            executeMethod("open", input);
            executeMethod(element, "Set", input);

            //To execute Other Testcases//
            //               scearioname,testcasename,subiteration 
            executeTestCase("OnlineShopping", "BuyProduct", 2);
            //               scearioname,testcasename
            executeTestCase("OnlineShopping", "BuyProduct");

            // -----------------------//
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
        }

    }

    public void handleCondition(String argumentShouldNotBeGiven
    /**
     * No argument should be specifed Then only Spritz will execute[show in
     * action column]
     */
    ) throws UnCaughtException {
        //Getting object from the object repository
        WebElement element = AObject.findElement("ObjectName", "PageName");
        //Putting condition on object
        if (element.isDisplayed()) {
            //Calling another test case if the condition is matched
            //Pass the Scenario name,Test case name and sub-iteration index
            executeTestCase("testscenario1", "cancelTicket", 1);
            Report.updateTestLog("Userdefined Action ", "inside reusable", Status.PASS);
            //If needed you can break the test case also by calling existing functions
            executeMethod("StopBrowser");
        } else {
            Report.updateTestLog("Userdefined Action ", "switch to origional", Status.DONE);
        }
    }

    public void testglobal() {
        userData.putGlobalData("#gd1", "Data1", "adasd");
    }

}
