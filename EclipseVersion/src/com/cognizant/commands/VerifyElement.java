/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;

/**
 *
 * @author 394173
 */
public class VerifyElement extends General {

    public VerifyElement(CommandControl cc) {
        super(cc);
    }

    public void verifyElementNotPresent() {
        verifyNotElement(!elementPresent());
    }

    public void verifyElementNotSelected() {
        verifyNotElement(!elementSelected());
    }

    public void verifyElementNotDisplayed() {
        verifyNotElement(!elementDisplayed());
    }

    public void verifyElementNotEnabled() {
        verifyNotElement(!elementEnabled());
    }

    public void verifyElementPresent() {
        verifyElement(elementPresent());
    }

    public void verifyElementSelected() {
        verifyElement(elementSelected());
    }

    public void verifyElementDisplayed() {
        verifyElement(elementDisplayed());
    }

    public void verifyElementEnabled() {
        verifyElement(elementEnabled());
    }

    private void verifyElement(Boolean status, String isNot) {
        String value = isNot + Action.replaceFirst("assertElement", "").replaceFirst("Not", "");
        String description = String.format("Element [%s] is %s", ObjectName, value);
        Report.updateTestLog(Action, description, Status.getValue(status));
    }

    private void verifyElement(Boolean status) {
        verifyElement(status, status ? "" : "not ");
    }

    private void verifyNotElement(Boolean status) {
        verifyElement(status, status ? "not " : "");
    }

    public void verifyPageSource() {
        boolean value = Driver.getPageSource().equals(Data);
        Report.updateTestLog(
                Action,
                "Current Page Source is" + (value ? "" : " not") + " matched with the expected Page Source",
                Status.getValue(value));
    }

    public void verifyHScrollBarPresent() {
        verifyHScorllBar("", isHScrollBarPresent());
    }

    public void verifyHScrollBarNotPresent() {
        verifyHScorllBar("not", !isHScrollBarPresent());
    }

    public void verifyVScrollBarPresent() {
        verifyVScorllBar("", isvScrollBarPresent());
    }

    public void verifyVScrollBarNotPresent() {
        verifyVScorllBar("not", !isvScrollBarPresent());
    }

    private void verifyHScorllBar(String isNot, Boolean value) {
        verifyScorllBar("Horizontal", isNot, value);
    }

    private void verifyVScorllBar(String isNot, Boolean value) {
        verifyScorllBar("Vertical", isNot, value);
    }

    private void verifyScorllBar(String type, String isNot, Boolean value) {
        String desc = type + " Scrollbar is " + isNot + " present";
        Report.updateTestLog(Action, desc, Status.getValue(value));
    }
}
