/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;


import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import com.cognizant.core.CommandControl;
import com.cognizant.core.CommandControl.IObject;
import com.cognizant.data.UserDataAccess;
import com.cognizant.drivers.AutomationObject;
import com.cognizant.drivers.SeleniumDriver;
import com.cognizant.reporting.Report;
import com.cognizant.support.UnCaughtException;
import com.cognizant.support.MethodExecutor;
import com.cognizant.support.Status;
import com.cognizant.support.Step;
import com.google.common.base.Predicate;

import java.awt.Robot;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Command {

    public WebDriver Driver;
    public AutomationObject AObject;
    public String Data;
    public String ObjectName;
    public WebElement Element;
    public String Description;
    public String Condition;
    public String Input;
    public String Action;
    public Report Report;
    public String Reference;
    public List<IObject> ObjectList;
    public List<Object> ls;
    private final CommandControl Commander;
    public Robot robot;
    public UserDataAccess userData;
    public Wait wait =  new Wait();

    public Command(CommandControl cc) {
        Commander = cc;
        Driver = Commander.seDriver.driver;
        AObject = Commander.AObject;
        Data = Commander.Data;
        ObjectName = Commander.ObjectName;
        Element = Commander.Element;
        Description = Commander.Description;
        Condition = Commander.Condition;
        Input = Commander.Input;
        Report = Commander.Report;
        Reference = Commander.Reference;
        ObjectList = Commander.itarget;
        Action = Commander.Action;
        userData = Commander.userData;
        this.ls = Commander.ls;
        robot = CommandControl.robot;
    }

    public void addVar(String key, String val) {
        Commander.addVar(key, val);
    }

    public String getVar(String key) {
        return Commander.getVar(key);
    }

    public void addGlobalVar(String key, String val) {
        if (key.matches("%.*%")) {
            key = key.substring(1, key.length() - 1);
        }
        Commander.putUserDefinedData(key, val);
    }

    public String getUserDefinedData(String key) {
        return Commander.getUserDefinedData(key);
    }

    public Stack<WebElement> getRunTimeElement() {
        return Commander.getRunTimeElement();
    }

    public void executeMethod(String Action) {
        MethodExecutor.init();
        try {
            if (!MethodExecutor.executeMethod(Action, Commander)) {
                Report.updateTestLog(Action, "Method Not Found", Status.FAIL);
            }
        } catch (Throwable ex) {
            Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void executeMethod(WebElement element, String Action, String Input) {
        setElement(element);
        setInput(Input);
        executeMethod(Action);
    }

    public void executeMethod(String Action, String Input) {
        setInput(Input);
        executeMethod(Action);
    }

    public void executeMethod(WebElement element, String Action) {
        setElement(element);
        executeMethod(Action);
    }

    SeleniumDriver getDriverControl() {
        return Commander.seDriver;
    }

    public Boolean isDriverAlive() {
        return getDriverControl().isAlive();
    }

    private void setElement(WebElement element) {
        Commander.Element = element;
    }

    private void setInput(String input) {
        Commander.Data = input;
    }

    public String getCurrentBrowserName() {
        return Commander.seDriver.getCurrentBrowser();
    }

    public CommandControl getCommander() {
        return Commander;
    }

    public void executeTestCase(String scenarioName, String testCaseName, int subIteration) throws UnCaughtException {
        Step newStep = new Step(0);
        newStep.ObjectName = "Execute";
        newStep.Action = scenarioName + ":" + testCaseName;
        newStep.Input = "@" + subIteration;
        userData.insertStep(newStep);
    }

    public void executeTestCase(String scenarioName, String testCaseName) throws UnCaughtException {
        Step newStep = new Step(0);
        newStep.ObjectName = "Execute";
        newStep.Action = scenarioName + ":" + testCaseName;
        userData.insertStep(newStep);
    }
    
     
    // ------------------------------------------------------------------------------------
 	 // ---------------------- OneAmerica Reusable Library Starts Here --------------------- 	 
 	 // ------------------------------------------------------------------------------------

  		/**
  		 * Method to fetch the table row elements for the given table object.
  		 * 
  		 * @param tableObj
  		 * 
  		 * @return list of row web elements
  		 * 
  		 * @Example getTableRows(DirectLinkPageMap.tblAssignment) This will find the uimap element matching uimap. Table specific to tblAssignment object within
  		 *          DirectLinkPageMap uimap . Table rows are identified based on the 'TR' tag present in the given Table object and store it in List<WebElement>.
  		 * good  to go
  		 */
  		public List<WebElement> getTableRows(WebElement elementTable) {
  			// fetching the table rows using <TR>
  			List<WebElement> tableRows = elementTable.findElements(By.tagName("tr"));
  			return tableRows;
  		}
  		/**
  		 * Get the number of row excluding the header.
  		 * 
  		 * @return Number of rows.
  		 * 
  		 * * good  to go
  		 */
  		public int getRowCount(WebElement elementTable) {
  			
  			return elementTable.findElements(By.xpath(".//tr")).size() - 1;
  		}
  		
  		public int getRowIndexof(List<WebElement>rowElements,String searchValue)
  		{
  			
  			int rowIndex=-1;
  			  for(int rNum=0;rNum<rowElements.size();rNum++)
  			    {
  				 
  				    List<WebElement> columns=rowElements.get(rNum).findElements(By.tagName("td"));
  				    
  				 
  				    
  				    for(int cNum=1;cNum<columns.size()-1;cNum++)
  				    {
  				     
  				       
  				       
  					    if (columns.get(cNum).getText().trim().equalsIgnoreCase(searchValue.trim())) 
  					    {
  							// setting the column number
  					    	rowIndex = rNum;
  							break;
  						}
  				    
  				    }
  			    } 
  			return rowIndex;
  		}
  		
  		
  		/**
  		 * Method to get the column value from a table which we are searching for by
  		 * using the given row index and column index
  		 * 
  		 * @param WebElement
  		 *            - tableObj. use getElement(String tableObj) method to get
  		 *            table object int rowIndex - row index starts from 1 excluding
  		 *            header int columnIndex- column index can be retreived by
  		 *            passing column name and table obj input to
  		 *            getWebTableColumnNumber method.
  		 * 
  		 * @return String - column value if it exists or else failure is reported
  		 * 
  		 *  
  		 * 
  		 */
  		public String getValueFromTable(WebElement tableObj, int rowIndex,int columnIndex) 
  		{
  			try {
  				// Identifying the td object using row and column index. Returning
  				// the text of the found td element

  				return tableObj.findElement(By.xpath("/tr[" + rowIndex + "]/td["+ columnIndex + "]")).getText().trim();

  			} catch (Exception e) {
  				
  				System.out.println("getValueFromTable: Failed");
                  Report.updateTestLog("getValueFromTable: error while retrieving the value from table. Error Desc:",
                  		e.getMessage(),
                          Status.FAIL);
  				
  				return null;
  			}

  		}
  	
  		public void ImplicitWaitON()
  		{
  			Driver.manage().timeouts().implicitlyWait(150, TimeUnit.SECONDS);
  		}
  		public void ImplicitWaitOFF()
  		{
  			Driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
  		}

  	// compare any two numbers
  public int compare(Number n1, Number n2) {
      long l1 = n1.longValue();
      long l2 = n2.longValue();
      if (l1 != l2)
      {
     	 System.out.println("CompareNumbers");
 			 Report.updateTestLog("assertCompareNumbers",
                   "Expected Element '" + n1 + "," + n2
                   + "' is  present in the page", Status.PASS); 
      }
      return Double.compare(n1.doubleValue(), n2.doubleValue());
  }

  public  boolean isObjectPresent(WebElement element,String ElementName)
  {

  	wait.waitForElementToEnable(Driver, element);	
  	try{
  			
  		if(element.isDisplayed())
  		{
  			System.out.println("assertTextPresent passed");
  			 Report.updateTestLog("assertElementPresentInPage",
                       "Expected Element '" + ElementName
                       + "' is  present in the page", Status.PASS);

  		
  			return true;
  		}
  		else
  		{
  			 Report.updateTestLog("assertElementPresentInPage",
                       "Expected Element '" + ElementName
                       + "' is  not present in the page", Status.FAIL);

  			return false;
  		}	
  	}
  	
  	catch(org.openqa.selenium.NoSuchElementException e)
  	{
  		 Report.updateTestLog("assertElementPresentInPage",
                   "Expected Element '" + ElementName
                   + "' is not present in the page", Status.FAIL);
  		
  		return false;
  	}
  	
  	catch(Exception e)
  	{
  		 Report.updateTestLog("assertElementPresentInPage",
                   "Expected Element '" + ElementName
                   + "' is not present in the page", Status.FAIL);	
  		 return false;
  	}

   }

  public boolean isObjectPresentNoWait(WebElement element,String ElementName)
  {
  	ImplicitWaitOFF();
  	try{
  				
  		if(element.isDisplayed())
  		{
  			ImplicitWaitOFF();
  			 Report.updateTestLog("assertElementDisplayedInPage",
                       "Expected Element '" + ElementName
                       + "' is  displayed in the page", Status.PASS);
  			
  			ImplicitWaitON();
  			return true;
  		}
  		else
  		{
  			 Report.updateTestLog("assertElementDisplayedInPage",
                       "Expected Element '" + ElementName
                       + "' is not displayed in the page", Status.DONE);
  			
  			ImplicitWaitON();
  			return false;
  			
  	    }
  		
  	}
  	catch(org.openqa.selenium.NoSuchElementException e)
  	{
  		 Report.updateTestLog("assertElementDisplayedInPage",
                   "Expected Element '" + ElementName
                   + "' is not displayed in the page", Status.DONE);
  		
  		return false;
  	}
  	
  	catch(Exception e)
  	{
  		Report.updateTestLog("assertElementDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is not displayed in the page", Status.FAIL);
  		return false;
  	}

   }

  public boolean isObjectNotPresent(WebElement element,String ElementName)
  {
  	ImplicitWaitOFF();
  	try
  	{
  	
  	if(element.isDisplayed())
  	{
  		Report.updateTestLog("assertElementNotDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is displayed in the page", Status.FAIL);
  		
  		return false;
  	}
  	else
  	{
  		ImplicitWaitOFF();
  		Report.updateTestLog("assertElementNotDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is not displayed in the page", Status.PASS);
  				
  		ImplicitWaitON();
  		return true;
  	}
  		
  	}
  	catch(Exception e)
  	{
  		Report.updateTestLog("assertElementNotDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is not displayed in the page", Status.PASS);
  		
  		return true;
  	}

   }
  
  public boolean checkObjectPresent(WebElement element,String ElementName)
  {
    ImplicitWaitOFF();
    try
    {
    
    if(element.isDisplayed())
    {
        Report.updateTestLog("checkElementDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is displayed in the page", Status.DONE);
        
        return true;
    }
    else
    {
        ImplicitWaitOFF();
        Report.updateTestLog("checkElementDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is not displayed in the page", Status.DONE);
                
       // ImplicitWaitON();
        return false;
    }
        
    }
    catch(Exception e)
    {
        Report.updateTestLog("checkElementDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is displayed in the page", Status.DONE);
        
        return false;
    }

   }


  public void waitExplicitly()
  {
  	try {
  		Thread.sleep(1000);
  	} catch (InterruptedException e) {
  		// TODO Auto-generated catch block
  		e.printStackTrace();
  	}
  	}
  public void FluentPredicateWait(){
	   	new FluentWait<WebDriver>(Driver). 
	       withTimeout(120, TimeUnit.SECONDS). 
	       pollingEvery(100,TimeUnit.MILLISECONDS).         
	       until(new Predicate<WebDriver>() { 
	               @Override 
	               public boolean apply(WebDriver driver) { 
	                       try{ 
	                       		System.out.println("inside fluent driver method");
	                              // Boolean booFlag = driver.findElement(By.xpath("//*[@id='spinner']")).isDisplayed();  
	                       		Boolean booFlag = Driver.findElement(By.xpath("//*[@id='loading']")).isDisplayed();  
	                               if(booFlag) 
	                                       return false; 
	                               else 
	                                       return true; 
	                              
	                       } 
	                       catch(org.openqa.selenium.NoSuchElementException e) 
	                       { 
	                               return true; 
	                       } 
	                       catch(TimeoutException e)
	                       {
	                       	return true; 
	                       }
	                       catch(Exception e)
	                       {
	                       	return true; 
	                       }
	               } 
	       } 
	                       ); 

	       
	   }

  public void FluentPredicateWait(final WebElement element ){
  	new FluentWait<WebDriver>(Driver). 
      withTimeout(180, TimeUnit.SECONDS). 
      pollingEvery(100,TimeUnit.MILLISECONDS).         
      until(new com.google.common.base.Predicate<WebDriver>() { 
              @Override 
              public boolean apply(WebDriver driver) { 
                      try{ 
                          //Boolean booFlag = driver.findElement(By.xpath("//*[@id='spinner']")).isDisplayed();
                      	System.out.println("inside fluent webelemnt method");
                  		Boolean booFlag;
                  		//String s = element.getText();
                          if(element.getText().contains("Loading"));
                          {
                          	booFlag = true;
                          }
                          if(booFlag) 
                                  return true; 
                          else 
                                  return false; 
                      } 
                      catch(Exception e) 
                      { 
                              return true; 
                      } 
              } 
      } 
                      ); 

      
  }


  public void waitforelement(final WebElement element ){
  	new FluentWait<WebDriver>(Driver). 
      withTimeout(60, TimeUnit.SECONDS). 
      pollingEvery(100,TimeUnit.MILLISECONDS).         
      until(new com.google.common.base.Predicate<WebDriver>() { 
              @Override 
              public boolean apply(WebDriver driver) { 
                      try{ 
                         Boolean booFlag = element.isDisplayed();
                      	System.out.println("inside fluent webelemnt method");
                  		if(booFlag)
                  			return true;
                  		else
                  			return false;
                  		
                      } 
                      catch(Exception e) 
                      { 
                          return true; 
                      } 
              } 
      } 
                      ); 

      
  }



  public void ComboSelectValue(WebElement element,String strValue,String strdesc) {
  	try 
  	{		
  		Select select = new Select(element);
  		if (strValue != null && strValue!="") 
  		{
  			
  			select.selectByVisibleText(strValue);
  			Report.updateTestLog("Select Value :",
  	                 strValue + " is selected in "+ strdesc, Status.PASS);
  						
  		}	        
  	} 
  	catch (NoSuchElementException e) 
  	{
  		Report.updateTestLog("Select Value :",
                  strValue + " is not selected in "+ strdesc, Status.FAIL);
  						}
  	catch(Exception e1)
  	{
  		Report.updateTestLog("Select Value : Exception",
                  strValue + " Throws error "+ e1.getStackTrace(), Status.FAIL);
  					
  		
  	}

  }

  public void EnterText(WebElement element, String strValue, String strdesc)
  {
  	try
  	{
  		
  		//if(!strValue.isEmpty())
  		//{
  			strValue = strValue.trim();
  			element.clear();			
  			element.sendKeys(strValue);
  			
  		//}
  		
  		Report.updateTestLog("EnterText: ",
                  strValue + "  is entered in  "+ strdesc, Status.PASS);
  				
  			
  	}	
  	catch(Exception e)
  	{
  		Report.updateTestLog("EnterText:Exception Message : "+e.getLocalizedMessage(),
                  strValue + "  is not entered in  "+ strdesc, Status.FAIL);
  				
  	}
  }
  
  public void EnterValue(WebElement element, String intValue, String strdesc)
  {
  	try
  	{
  		
  		if(!intValue.isEmpty())
  		{
  			intValue = intValue.trim();
  			element.clear();			
  			element.sendKeys(intValue);
  			
  		}
  		
  		Report.updateTestLog("EnterValue: ",
  				intValue + "  is entered in  "+ strdesc, Status.PASS);
  				
  			
  	}	
  	catch(Exception e)
  	{
  		Report.updateTestLog("EnterValue:Exception Message : "+e.getLocalizedMessage(),
  				intValue + "  is not entered in  "+ strdesc, Status.FAIL);
  				
  	}
  }

  public void getSelectedComboValue(WebElement element,String strValue)
  {
        
        Select select = new Select(element);
        WebElement selectedvalue = select.getFirstSelectedOption();
        
      	  if(selectedvalue.getText().contains(strValue))
            {
      		  Report.updateTestLog("DropDown: ",
      	                strValue + "  is selected ", Status.PASS);
      		 		  
            }
      	  else
      	  {
      		  Report.updateTestLog("DropDown: ",
    	                strValue + "  is not selected ", Status.FAIL);
            }
        
  }

  /**
   * Constructor to click the Web Element of the Object
   * @param report The {@link Report} object
   * @param strButtonName The {@link String} object
   * @param strobj The {@link By} object
   */
  public void ClickElement(WebElement element, String strButtonName)
  {
  	try 
  	{	
  	wait.waitForElementToEnable(Driver,element);
  	element.click();	
  	 Report.updateTestLog("Click : ",
  			 strButtonName + "  is clicked ", Status.DONE);
  	
  		
  	}
  	catch(Exception e)
  	{
  		
  		 Report.updateTestLog("Click : e.getLocalizedMessage() ",
  				 strButtonName + "  is not clicked ", Status.FAIL);		
  		
  	}
  }

  public boolean StringCompare(String string1, String string2)
  {
  	if(string1.equalsIgnoreCase(string2))
  	 {
  		
  		Report.updateTestLog("String Compare:" , string1+","+string2 + " are same",
  				  Status.PASS);		 
  		 return true;
  	 }
  	 else			 
  	 {
  		 Report.updateTestLog("String Compare:" , string1+","+string2 + " are not same",
  				  Status.FAIL);	
  		 return false;
  	 }
  	}   

  public boolean StringContains(String string1, String string2)
  {
  	if(string1.contains(string2))
  	 {
  		Report.updateTestLog("String Contains:" , string1+" contains "+string2 ,
  				  Status.PASS);	
  		 return true;
  	 }
  	 else			 
  	 {
  		 Report.updateTestLog("String Contains:" , string1+" contains "+string2 ,
  				  Status.FAIL);	
  		 return false;
  	 }
  	}


  public void SendTAB(WebElement element)
  {
  	try
  	{
  		
  		element.sendKeys(Keys.TAB);
  		Report.updateTestLog("Enter TAB:" , "TAB is entered",
  				  Status.DONE);	
  		
  	}	
  	catch(Exception e)
  	{
  		Report.updateTestLog("Enter TAB:" , "TAB is not entered" + "Exception Message : " + e.getLocalizedMessage(),
  				  Status.FAIL);			
  	}
  	}

  public void clear(WebElement element)
  {
  	
  	try
  	{
  		
  		element.clear();
  		Report.updateTestLog("Clear : " , "Clear Field",
  				  Status.DONE);	
  		
  		
  	}	
  	catch(Exception e)
  	{
  		Report.updateTestLog("Clear : " , "Exception : "+e.getLocalizedMessage(),
  				  Status.FAIL);	
  		
  	}
  	
  }

  /**
   * method to Click JavaScriptor Web Element of the Object
   * @param strButtonName The {@link String} object
   * @param strobj The {@link webelement} object
   */
  public void ClickJSElement(WebElement element, String strButtonName)
  {
  	try
  	{
  		
  		JavascriptExecutor executor = (JavascriptExecutor)Driver;
  		executor.executeScript("arguments[0].click();",element);
  		
  		Report.updateTestLog("Click:" , strButtonName+ " Clicked",
  				  Status.PASS);
  		
  		
  		
  	}
  	catch(Exception e)
  	{
  		Report.updateTestLog("Click:" , strButtonName+ " Clicked",
  				  Status.FAIL);
  	}
  }
  
  public String getSelectedComboOption(WebElement element)
  {
        
        Select select = new Select(element);
        WebElement selectedvalue = select.getFirstSelectedOption();
        
      	  return selectedvalue.getText();
           
        
  }

  public boolean isObjectNotPresentByReference(String xpath,String ElementName)
  {
  	ImplicitWaitOFF();
  	try
  	{
  	
  	if(Driver.findElement(By.xpath(xpath)).isDisplayed())
  	{
  		Report.updateTestLog("Element Not Present:" , ElementName+ " is displayed",
  				  Status.FAIL);
  		
  		return false;
  	}
  	else
  	{
  		ImplicitWaitOFF();
  		Report.updateTestLog("Element Not Present:" , ElementName+ " is not displayed",
  				  Status.PASS);		
  		ImplicitWaitON();
  		return true;
  	}
  		
  	}
  	catch(Exception e)
  	{
  		Report.updateTestLog("Element Not Present:" , ElementName+ " is not displayed",
  				  Status.PASS);	
  		return true; 
  	}

   }
  
  public boolean isSecurityExceptionNotPresent(WebElement element,String ElementName)
  {
  	ImplicitWaitOFF();
  	try
  	{
  	
  	if(element.isDisplayed())
  	{
  		Report.updateTestLog("assertElementNotDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is displayed in the page", Status.DONE);
  		
  		return false;
  	}
  	else
  	{
  		ImplicitWaitOFF();
  		Report.updateTestLog("assertElementNotDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is not displayed in the page", Status.PASS);
  				
  		ImplicitWaitON();
  		return true;
  	}
  		
  	}
  	catch(Exception e)
  	{
  		Report.updateTestLog("assertElementNotDisplayedInPage",
                  "Expected Element '" + ElementName
                  + "' is not displayed in the page", Status.PASS);
  		
  		return true;
  	}
  }
  
  //EApp
  
  	public void selectGender(String gender, WebElement male, WebElement female)
	{
		if(gender.equalsIgnoreCase("Male"))
		{
			ClickElement(male, "Male");
		}
		else
		{
			ClickElement(female, "Female");
		}
	}
  	
  	public void enterDOB(String dob, WebElement month, WebElement day, WebElement year)
	{
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];		
		
		year.sendKeys(strYear);
		day.sendKeys(strDay);
		month.sendKeys(strMonth);
			
		Report.updateTestLog("Date of Birth", dob + " is entered in DOB", Status.DONE);	
	}
}

