/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.commands;

import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import org.openqa.selenium.WebElement;

/**
 *
 * @author 394173
 */
public class RelativeCommand extends Command {

    private enum RelativeAction {

        CLICK, SET
    };

    public RelativeCommand(CommandControl cc) {
        super(cc);
    }

    private Boolean isConditionValid() {
        return !Condition.matches("((Start|End) (Loop|Param)(:\\s)*)|Global Object|Continue");
    }

    private void doRelative(RelativeAction action) {
        if (isConditionValid()) {
            WebElement parent = AObject.findElement(Condition, Reference);
            if (parent != null) {
                Element = AObject.findElement(parent, ObjectName, Reference);
                getCommander().Element = Element;
                switch (action) {
                    case CLICK:
                        new Basic(getCommander()).Click();
                        break;
                    case SET:
                        new Basic(getCommander()).Set();
                        break;
                }
            } else {
                Report.updateTestLog(Action, "Relative Element[" + Condition + "] Not Found in the Page", Status.FAIL);
            }
        } else {
            Report.updateTestLog(Action, "No Relative Element Found in Condition Column", Status.DEBUG);
        }
    }

    public void click_Relative() {
        doRelative(RelativeAction.CLICK);
    }

    public void set_Relative() {
        doRelative(RelativeAction.SET);
    }

}
