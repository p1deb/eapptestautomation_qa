/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.commands;

import com.cognizant.constants.SystemDefaults;
import com.cognizant.core.CommandControl;
import com.cognizant.support.ForcedException;
import com.cognizant.support.Status;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitFor extends Command {

    enum WaitType {

        VISIBLE,
        INVISIBLE,
        CLICKABLE,
        SELECTED,
        TEXT_CONTAINS,
        VALUE_CONTAINS,
        TITLE_IS,
        TITLE_CONTAINS,
        EL_SELECT_TRUE,
        EL_SELECT_FALSE,
        ALERT_PRESENT,
        CUSTOM_SCRIPT
    };

    public WaitFor(CommandControl cc) {
        super(cc);
    }

    public void clickAndWait() {
        if (Element != null) {
            Element.click();
            waitForPageLoaded();
            Report.updateTestLog(Action, "Click and wait for page load is done",
                    Status.DONE);
        } else {
            Report.updateTestLog(Action, "Object[" + ObjectName + "] not found", Status.FAIL);
        }
    }

    public void waitForPageLoaded() {
        waitFor(WaitType.CUSTOM_SCRIPT,
                "Page load completed in stipulated time",
                "return document.readyState==='complete'");
    }

    public void waitForAlertPresent() {
        waitFor(WaitType.ALERT_PRESENT,
                "Alert popped up in stipulated time");
    }

    public void waitForElementToBeVisible() {
        waitForElement(WaitType.VISIBLE, "'"
                + this.ObjectName
                + "' Element becomes visible in stipulated time");
    }

    public void waitForElementToBeInVisible() {
        waitForElement(WaitType.INVISIBLE, "'"
                + this.ObjectName
                + "' Element becomes invisible in stipulated time");
    }

    public void waitForElementToBeClickable() {
        waitForElement(WaitType.CLICKABLE, "'"
                + this.ObjectName
                + "' Element becomes Clickable in stipulated time");
    }

    public void waitForElementToBeSelected() {
        waitForElement(WaitType.SELECTED, "'"
                + this.ObjectName
                + "' Element Selected in stipulated time");
    }

    public void waitForElementToContainText() {
        waitForElement(WaitType.TEXT_CONTAINS, "'"
                + this.ObjectName + "' Element contained the text: "
                + Data + " in stipulated Time");
    }

    public void waitForElementToContainValue() {
        waitForElement(WaitType.VALUE_CONTAINS, "'"
                + this.ObjectName + "' Element contained the value: "
                + Data + " in stipulated Time");
    }

    public void waitForElementSelectionToBeTrue() {
        waitForElement(WaitType.EL_SELECT_TRUE, "'"
                + this.ObjectName
                + "' Element got Selected in the stipulated time");
    }

    public void waitForElementSelectionToBeFalse() {
        waitForElement(WaitType.EL_SELECT_FALSE, "'"
                + this.ObjectName
                + "' Element got Deselected in the stipulated time");
    }

    public void waitForTitleToBe() {
        waitFor(WaitType.TITLE_IS,
                "Title Equals '"
                + Data + "' in stipulated Time");
    }

    public void waitForTitleToContain() {
        waitFor(WaitType.TITLE_CONTAINS,
                "Title Contains the value '"
                + Data + "' in stipulated Time");
    }

    public void waitTillCustomScript() {
        if (Data != null && !Data.trim().isEmpty()) {
            if (Data.contains("return")) {
                waitFor(WaitType.CUSTOM_SCRIPT,
                        "Condition passed in stipulated time",
                        Data);
            } else {
                Report.updateTestLog(Action, "Javascript condition should have atleast one return and the condtion should return Boolean value", Status.DEBUG);
            }
        } else {
            Report.updateTestLog(Action, "Include a proper javascript condition to check", Status.DEBUG);
        }
    }

    public void waitForElementToBePresent() {
        AObject.setWaitTime(getWaitTime());
        try {
            Element = AObject.findElement(ObjectName, Reference);
            AObject.setWaitTime(SystemDefaults.elementWaitTime.get());
            if (Element != null) {
                Report.updateTestLog(Action, "'"
                        + this.ObjectName
                        + "' Element Present in the stipulated time",
                        Status.PASS);
            } else {
                throw new ForcedException(Action, "'"
                        + this.ObjectName
                        + "' Element not present in the stipulated time");
            }

        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
            throw new ForcedException(Action,
                    ex.getMessage());
        }
    }

    private void waitForElement(WaitType command, String message) {
        if (Element != null) {
            waitFor(command, message);
        } else {
            throw new ForcedException(Action, "Object [" + ObjectName + "] not found");
        }
    }

    private void waitFor(WaitType command, String message) {
        waitFor(command, message, null);
    }

    private void waitFor(WaitType command, String message, String customScript) {
        int time = getWaitTime();
        WebDriverWait wait = new WebDriverWait(Driver, time);
        try {
            waitFor(wait, command, customScript);
            Report.updateTestLog(Action, message, Status.DONE);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.OFF, null, ex);
            Report.updateTestLog(Action, "Couldn't wait for action to complete in given time - " + time + " seconds", Status.DEBUG);
        }
    }

    private int getWaitTime() {
        if (Condition != null && Condition.matches("[0-9]+")) {
            return Integer.valueOf(Condition);
        } else {
            return SystemDefaults.waitTime.get();
        }
    }

    private void waitFor(WebDriverWait wait, WaitType command, String customScript) {
        switch (command) {
            case VISIBLE:
                wait.until(ExpectedConditions.visibilityOf(Element));
                break;
            case INVISIBLE:
                wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(Element)));
                break;
            case CLICKABLE:
                wait.until(ExpectedConditions.elementToBeClickable(Element));
                break;
            case SELECTED:
                wait.until(ExpectedConditions.elementToBeSelected(Element));
                break;
            case TITLE_IS:
                wait.until(ExpectedConditions.titleIs(Data));
                break;
            case TITLE_CONTAINS:
                wait.until(ExpectedConditions.titleContains(Data));
                break;
            case TEXT_CONTAINS:
                wait.until(ExpectedConditions.textToBePresentInElement(Element, Data));
                break;
            case VALUE_CONTAINS:
                wait.until(ExpectedConditions.textToBePresentInElementValue(Element, Data));
                break;
            case EL_SELECT_TRUE:
                wait.until(ExpectedConditions.elementSelectionStateToBe(Element, true));
                break;
            case EL_SELECT_FALSE:
                wait.until(ExpectedConditions.elementSelectionStateToBe(Element, false));
                break;
            case ALERT_PRESENT:
                wait.until(ExpectedConditions.alertIsPresent());
                break;
            case CUSTOM_SCRIPT:
                wait.until(getCustomCondition(customScript));
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    private ExpectedCondition getCustomCondition(final String javascript) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return (Boolean) ((JavascriptExecutor) driver).executeScript(javascript);
            }
        };
    }

}
