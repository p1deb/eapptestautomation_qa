/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.data;

import com.cognizant.constants.FilePath;
import com.cognizant.core.TestRunner;
import com.cognizant.support.CSVMgr;
import com.cognizant.support.UnCaughtException;
import com.cognizant.support.ParameterControl;
import com.cognizant.support.Step;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 394173
 */
public class UserDataAccess {

    private String Scenario;
    private String TestCase;
    private String currScenario;
    private String currTestCase;
    private String Iteration;
    private String SubIteration;

    private TestRunner runner;

    private ParameterControl param;

    public void setIteration(String scenario, String testCase, String currscenario, String currtestCase, int iteration,
            int subIteration) {
        Scenario = scenario;
        TestCase = testCase;
        currScenario = currscenario;
        currTestCase = currtestCase;
        Iteration = "" + iteration;
        SubIteration = "" + subIteration;
        System.out.println(Scenario + " " + TestCase + " " + Iteration);
    }

    public void setParamControl(ParameterControl param) {
        this.param = param;
    }

    public String getScenario() {
        return Scenario;
    }

    public String getTestCase() {
        return TestCase;
    }

    public String getIteration() {
        return Iteration;
    }

    public String getSubIteration() {
        return SubIteration;
    }

    public String getData(String Sheet, String Column) {
        String value = getData(Sheet, Column, Scenario, TestCase, Iteration,
                SubIteration);
        if (value == null) {
            value = getData(Sheet, Column, Scenario, TestCase, "1",
                    SubIteration);
        }
        if (value == null) {
            value = getData(Sheet, Column, Scenario, TestCase, "1",
                    "1");
        }
        if (value == null) {
            value = getData(Sheet, Column, currScenario, currTestCase, Iteration,
                    SubIteration);
        }
        if (value == null) {
            value = getData(Sheet, Column, currScenario, currTestCase, "1",
                    SubIteration);
        }
        if (value == null) {
            value = getData(Sheet, Column, currScenario, currTestCase, "1",
                    "1");
        }
        return value;
    }

    public String getData(String Sheet, String Column, String Iteration, String SubIteration) {
        String value = getData(Sheet, Column, Scenario, TestCase, Iteration, SubIteration);
        if (value == null) {
            value = getData(Sheet, Column, currScenario, currTestCase, Iteration, SubIteration);
        }
        return value;
    }

    public String getData(String Sheet, String Column, String Scenario,
            String TestCase, String Iteration, String SubIteration) {
        String Value = null;
        try {
            File f = new File(FilePath.getTestDataPath(Sheet));
            HashMap<String, ArrayList<String[]>> Data = CSVMgr.getHashCSV(f,
                    new String[]{"Scenario", "Flow", "Iteration",
                        "SubIteration"});
            if (Data != null) {
                List<String> Header = Arrays
                        .asList(Data.get("#Headers").get(0));
                ArrayList<String[]> rows = Data.get(Scenario + TestCase
                        + Iteration + SubIteration);
                if (Header != null && rows != null) {
                    String[] rowVal = rows.get(0);
                    Value = rowVal[Header.indexOf(Column)];
                }
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return Value;
    }

    public void putData(String Sheet, String Column, String Value) {
        if (!putData(Sheet, Column, Value, Scenario, TestCase, Iteration,
                SubIteration)) {
            if (!putData(Sheet, Column, Value, Scenario, TestCase, "1",
                    SubIteration)) {
                if (!putData(Sheet, Column, Value, Scenario, TestCase, "1",
                        "1")) {
                    if (!putData(Sheet, Column, Value, currScenario, currTestCase, Iteration,
                            SubIteration)) {
                        if (!putData(Sheet, Column, Value, currScenario, currTestCase, "1",
                                SubIteration)) {
                            if (!putData(Sheet, Column, Value, currScenario, currTestCase, "1",
                                    "1")) {
                                System.out.println("No Column[" + Column + "] available in Sheet[" + Sheet + "]");
                            }
                        }
                    }
                }
            }
        }
    }

    public void putData(String Sheet, String Column, String Value,
            String Iteration, String SubIteration) {
        if (!putData(Sheet, Column, Value, Scenario, TestCase, Iteration,
                SubIteration)) {
            if (!putData(Sheet, Column, Value, currScenario, currTestCase, Iteration,
                    SubIteration)) {
                System.out.println("No Column[" + Column + "] available in Sheet[" + Sheet + "]");
            }
        }
    }

    public Boolean putData(String Sheet, String Column, String Value, String Scenario, String TestCase, String Iteration, String SubIteration) {
        try {
            String f = FilePath.getTestDataPath(Sheet);
            List<String[]> Data = CSVMgr.getData(f);
            if (Data != null) {
                List<String> Header = Arrays.asList(Data.get(0));
                if (Header != null) {
                    for (String[] row : Data) {
                        if (row[0].equals(Scenario) && row[1].equals(TestCase)
                                && row[2].equals(Iteration)
                                && row[3].equals(SubIteration)) {
                            int column = Header.indexOf(Column);
                            if (column >= 0) {
                                row[column] = Value.replaceAll("\t", "").replaceAll("\n", "");
                                param.putData(Sheet, Column, row[column], Integer.valueOf(SubIteration));
                                break;
                            } else {
                                throw new UnCaughtException("The Specified Column '" + Column + "' was not found in DataSheet '" + Sheet + "' "
                                        + "for Scenario[" + Scenario + "] and Testcase [" + TestCase + "] for "
                                        + "Iteration[" + Iteration + "] and SubIteration[" + SubIteration + "]");
                            }
                        }

                    }

                    CSVMgr.putData(f, Data);
                    System.out.println("The Data '" + Value + "' has been written successfully!!!");
                    return true;
                }
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }

    public void putGlobalData(String key, String column, String value) {
        DataAccess.GlobalValues.get(key).set(DataAccess.GlobalColumn.indexOf(column), value);
        putGlobalDataSheet(key, column, value);
    }

    public String getGlobalData(String key, String column) {
        return DataAccess.GlobalValues.get(key).get(DataAccess.GlobalColumn.indexOf(column));
    }

    private Boolean putGlobalDataSheet(String key, String Column, String Value) {
        try {
            String f = FilePath.getTestDataPath("GlobalData");
            List<String[]> Data = CSVMgr.getData(f);
            if (Data != null) {
                List<String> Header = Arrays.asList(Data.get(0));
                if (Header != null) {
                    for (String[] row : Data) {
                        if (row[0].equals(key)) {
                            int column = Header.indexOf(Column);
                            if (column >= 0) {
                                row[column] = Value.replaceAll("\t", "").replaceAll("\n", "");
                                break;
                            } else {
                                throw new UnCaughtException("The Specified Column '" + Column + "' was not found in GlobalData"
                                        + "for key [" + key + "]");
                            }
                        }

                    }
                    CSVMgr.putData(f, Data);
                    System.out.println("The Data '" + Value + "' has been written successfully!!!");
                    return true;
                }
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }

    public void insertStep(Step newstep) throws UnCaughtException {
        newstep.StepNum = runner.stepList.size();
        runner.runStep(newstep);
    }

    public void setRunner(TestRunner runner) {
        this.runner = runner;
    }

    public int getNoOfSubIterations() {
        return param.maxIteration();
    }
}
