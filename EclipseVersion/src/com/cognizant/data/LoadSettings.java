/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.data;

import com.cognizant.constants.FilePath;
import com.cognizant.constants.SystemDefaults;
import com.cognizant.drivers.SeleniumDriver;
import com.cognizant.settings.RunSettings;
import com.cognizant.settings.Settings;
import com.cognizant.settings.SettingsProperties;
import com.cognizant.settings.SettingsProperties.DriverProperties;
import com.cognizant.settings.SettingsProperties.EmulatorProperties;
import com.cognizant.settings.SettingsProperties.ExecProperties;
import com.cognizant.settings.SettingsProperties.RunProperties;
import com.cognizant.settings.SettingsProperties.TMProperties;
import com.cognizant.settings.TMSettings;
import com.galenframework.config.GalenConfig;
import com.galenframework.config.GalenProperty;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 394173
 */
public class LoadSettings {

    static LoadSettings settings;
    Settings<ExecProperties> execSettings = new Settings<>();
    RunSettings runSettings = new RunSettings();
    Settings<DriverProperties> driverSettings = new Settings<>();
    Settings<EmulatorProperties> emuSettings = new Settings<>();
    Settings<TMProperties> tmTestSetSettings = new Settings<>();
    TMSettings tmSettings = new TMSettings();
    Settings<String> userdefinedSettings = new Settings<>();

    String runSettingsFileName = "RunSettings.properties";
    String driverSettingsFileName = "DriverSettings.properties";
    String userDefinedSettingsFileName = "userDefinedSettings.properties";
    String tmSettingsFileName = "TestMgmtSettings.properties";

    String settingsfolderName = "Settings";
    String emulatorfolderName = "Emulators";

    SeleniumDriver seleniumDriver;

    public static LoadSettings getSettings() {
        return settings != null ? settings : (settings = new LoadSettings());
    }

    public void loadSettings(String globalPropertiesPath) {
        execSettings.setProperties(loadProperties(globalPropertiesPath));
        runSettings.setProperties(loadSettingsForTestSet(runSettingsFileName));
        driverSettings.setProperties(loadSettingsFor(driverSettingsFileName));
        tmTestSetSettings.setProperties(loadSettingsForTestSet(tmSettingsFileName));
        tmSettings.setProperties(loadSettingsFor(tmSettingsFileName));
        userdefinedSettings.setProperties(loadSettingsFor(userDefinedSettingsFileName));
        userdefinedSettings.getProperties().putAll(SystemDefaults.CLVars);
        userdefinedSettings.update();
        GalenConfig.getConfig().setProperty(GalenProperty.SCREENSHOT_FULLPAGE, runSettings.getProperty(RunProperties.TakeFullPageScreenShot, "true"));
        GalenConfig.getConfig().setProperty(GalenProperty.SCREENSHOT_AUTORESIZE, "false");
//        GalenConfig.getConfig().setProperty(GalenProperty.SCREENSHOT_FULLPAGE_SCROLLWAIT, "500");
        overrideWithEnv();
    }

    private Properties loadSettingsForTestSet(String fileName) {
        String path = execSettings.getProperty(ExecProperties.ProjectPath)
                + File.separator + settingsfolderName;
        if (Boolean.valueOf(execSettings.getProperty(ExecProperties.TestRun))) {
            path = path + File.separator + fileName;
        } else {
            path = path + File.separator
                    + "TestExecution" + File.separator
                    + execSettings.getProperty(ExecProperties.Release) + File.separator
                    + execSettings.getProperty(ExecProperties.TestSet) + File.separator
                    + fileName;
        }
        return loadProperties(path);
    }

    public Properties loadSettingsFor(String fileName) {
        return loadProperties(getSettingsFolder() + fileName);
    }

    public Settings<EmulatorProperties> loadSettingsForEmulators(String browser) {
        String path = getSettingsFolder() + emulatorfolderName + File.separator + browser + ".properties";
        emuSettings.setProperties(loadProperties(path));
        return emuSettings;
    }

    public String getSettingsFolder() {
        return execSettings.getProperty(ExecProperties.ProjectPath)
                + File.separator + settingsfolderName + File.separator;
    }

    private Properties loadProperties(String location) {
        Properties prop = new Properties();
        if (new File(location).exists()) {
            try (FileInputStream inputStream = new FileInputStream(location)) {
                prop.load(inputStream);
            } catch (IOException ex) {
                Logger.getLogger(LoadSettings.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return prop;
    }

    public Settings<ExecProperties> getExecSettings() {
        return execSettings;
    }

    public RunSettings getRunSettings() {
        return runSettings;
    }

    public Settings<DriverProperties> getDriverSettings() {
        return driverSettings;
    }

    public Boolean useMarionette() {
        return Boolean.valueOf(getDriverSettings().getProperty(DriverProperties.UseMarionetteDriver, "false"));
    }

    public TMSettings getTmSettings() {
        return tmSettings;
    }

    public Settings<TMProperties> getTmTestSetSettings() {
        return tmTestSetSettings;
    }

    public Settings<String> getUserdefinedSettings() {
        return userdefinedSettings;
    }

    public Boolean isTestRun() {
        return Boolean.valueOf(execSettings.getProperty(ExecProperties.TestRun));
    }

    public Boolean canReportPerformanceLog() {
        return Boolean.valueOf(runSettings.getProperty(RunProperties.reportPerformanceLog, "false"));
    }

    public Boolean isGridExecution() {
        return getExecutionMode().equalsIgnoreCase("grid");
    }

    public String getExecutionMode() {
        return runSettings.getProperty(RunProperties.ExecutionMode, "Local");
    }

    public int getExecTimeOut() {
        return Integer.parseInt(runSettings.getProperty(RunProperties.ExecutionTimeOut, "300"));
    }

    public int getRerunTimes() {
        return Integer.parseInt(runSettings.getProperty(RunProperties.RerunTimes, "0"));
    }

    public int getThreadCount() {
        return useExistingDriver() ? 1
                : Integer.parseInt(runSettings.getProperty(RunProperties.ThreadCount, "1"));
    }

    public boolean isContinueOnError() {
        return getIterationMode().equalsIgnoreCase("ContinueOnError");
    }

    public String getIterationMode() {
        return runSettings.getProperty(RunProperties.IterationMode, "ContinueOnError");
    }

    public String getChromeDriverPath() {
        String chromeDriverPath = LoadSettings.getSettings().getDriverSettings()
                .getProperty(SettingsProperties.DriverProperties.ChromeDriverPath, "./lib/Drivers/chromedriver.exe");
        if (chromeDriverPath.startsWith(".")) {
            return FilePath.getSpritzPath() + chromeDriverPath;
        }
        return chromeDriverPath;
    }

    public String getInternetExplorerDriverPath() {
        String internetExplorerDriverPath = LoadSettings.getSettings().getDriverSettings()
                .getProperty(SettingsProperties.DriverProperties.IEDriverPath, "./lib/Drivers/IEDriverServer.exe");
        if (internetExplorerDriverPath.startsWith(".")) {
            return FilePath.getSpritzPath() + internetExplorerDriverPath;
        }
        return internetExplorerDriverPath;
    }

    public String getGekcoDriverPath() {
         String marionetteDriverPath = LoadSettings.getSettings().getDriverSettings()
                .getProperty(SettingsProperties.DriverProperties.GeckoDriverPath, "./lib/Drivers/geckodriver.exe");
        if (marionetteDriverPath.startsWith(".")) {
            return FilePath.getSpritzPath() + marionetteDriverPath;
        }
        return marionetteDriverPath;
    }

    public String getEdgeDriverPath() {
        String edgeDriverPath = LoadSettings.getSettings().getDriverSettings()
                .getProperty(SettingsProperties.DriverProperties.EdgeDriverPath, "./lib/Drivers/MicrosoftWebDriver.exe");
        if (edgeDriverPath.startsWith(".")) {
            return FilePath.getSpritzPath() + edgeDriverPath;
        }
        return edgeDriverPath;
    }

    public boolean useExistingDriver() {
        return Boolean.valueOf(runSettings.getProperty(RunProperties.ExistingDriver, "false"));
    }

    public SeleniumDriver getSeDriver() {
        return seleniumDriver;
    }

    public void setSeDriver(SeleniumDriver seDriver) {
        seleniumDriver = seDriver;
    }

    private void overrideWithEnv() {
        Map<String, String> prop = new LinkedHashMap<>();
        /*
         * get the env settings from SET spritz.* in command line or terminal
         * in java words Environment variables
         */
        for (Map.Entry<String, String> e : System.getenv().entrySet()) {
            if (e.getKey().startsWith("spritz.")) {
                prop.put(e.getKey().replace("spritz.", ""), e.getValue());
            }
        }
        /**
         * update with spritz CLI's setEnv settings (will override the System
         * Env)
         */

        prop.putAll(SystemDefaults.EnvVars);
        if (!prop.isEmpty()) {
            System.out.println("Override with Environment Settings :\n " + prop);
            /*
             * update the exe/run/user settings with CLI's Env settings
             * (case sensitive)
             */
            for (Map.Entry<String, String> e : prop.entrySet()) {
                try {
                    String key = e.getKey(), value = e.getValue();
                    if (key.startsWith("run.")) {
                        runSettings.getProperties().put(
                                key.replace("run.", ""), value);
                    } else if (key.startsWith("exe.")) {
                        execSettings.getProperties().put(
                                key.replace("exe.", ""), value);
                    } else if (key.startsWith("user.")) {
                        userdefinedSettings.getProperties().put(
                                key.replace("user.", ""), value);
                    } else if (key.startsWith("tm.")) {
                        tmSettings.getProperties().put(
                                key.replace("tm.", ""), value);
                    } else if (key.startsWith("driver.")) {
                        driverSettings.getProperties().put(
                                key.replace("driver.", ""), value);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(LoadSettings.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
