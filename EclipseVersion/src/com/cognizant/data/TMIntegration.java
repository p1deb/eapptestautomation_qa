/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.data;

import com.cognizant.reporting.SummaryReport;
import com.cognizant.reporting.sync.Unknown;
import com.cognizant.reporting.sync.jira.JIRASync;
import com.cognizant.reporting.sync.qc.QCSync;
import com.cognizant.settings.SettingsProperties.TMProperties;
import com.cognizant.settings.TMSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 394173
 */
public class TMIntegration {

    private static final Logger Log = LoggerFactory.getLogger(TMIntegration.class.getName());

    public static void init(SummaryReport ReportManager) {
        Log.debug("Trying to Initialize TestManagement Integration");
        if (!LoadSettings.getSettings().isTestRun()) {
            TMSettings tmSettings = LoadSettings.getSettings().getTmSettings();
            String module = LoadSettings.getSettings().getTmTestSetSettings().getProperty(TMProperties.UpdateResultsToTM, "None");
            switch (module) {
                case "None":
                    Log.debug("TEST MANAGEMENT INTEGRATION DISABLED for the testset");
                    break;
                case "JIRA - Zephyr":
                    Log.debug("Initializing TEST MANAGEMENT INTEGRATION with JIRA-Zephyr");
                    ReportManager.sync = new JIRASync(
                            tmSettings.getProperty(TMProperties.jiraZephyrSettings.JIRAZephyrUrl),
                            decrypt(tmSettings.getProperty(TMProperties.jiraZephyrSettings.JIRAZephyrUserName)),
                            decrypt(tmSettings.getProperty(TMProperties.jiraZephyrSettings.JIRAZephyrPassword)),
                            tmSettings.getProperty(TMProperties.jiraZephyrSettings.JIRAZephyrProject));
                    break;
                case "Quality Center":
                    Log.debug("Initializing TEST MANAGEMENT INTEGRATION with Quality Center");
                    ReportManager.sync = new QCSync(
                            tmSettings.getProperty(TMProperties.qcSettings.QCUrl),
                            decrypt(tmSettings.getProperty(TMProperties.qcSettings.QCUserName)),
                            decrypt(tmSettings.getProperty(TMProperties.qcSettings.QCPassword)),
                            tmSettings.getProperty(TMProperties.qcSettings.QCDomain),
                            tmSettings.getProperty(TMProperties.qcSettings.QCProject));

                    break;
                default:
                    Log.warn("Initializing TEST MANAGEMENT INTEGRATION with Unknown - " + module);
                    ReportManager.sync = new Unknown();
                    break;
            }
        } else {
            Log.info("TEST MANAGEMENT INTEGRATION DISABLED for testcases running via design mode");
        }
    }

    public static String decrypt(String value) {
        byte[] valueDecoded = org.apache.commons.codec.binary.Base64.decodeBase64(value);
        return new String(valueDecoded);
    }

}
