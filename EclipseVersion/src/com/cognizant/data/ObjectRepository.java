/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.data;

import com.cognizant.constants.FilePath;
import com.cognizant.constants.ObjectAttribute;
import com.cognizant.constants.XMLTag;
import com.cognizant.support.OR.ORObject;
import com.cognizant.support.OR.ObjectRepo;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ObjectRepository {

    public static ConcurrentHashMap<String, ObjectRepo> loadOR() {
        return loadOR(FilePath.getORPath());
    }

    public static ConcurrentHashMap<String, ObjectRepo> loadMOR() {
        return loadOR(FilePath.getMORPath());
    }

    public static ConcurrentHashMap<String, ObjectRepo> loadOR(String filePath) {
        ConcurrentHashMap<String, ObjectRepo> oRMap = new ConcurrentHashMap<>();

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            if (new File(filePath).exists()) {
                docBuilder = docFactory.newDocumentBuilder();
                Document doc = docBuilder.parse(filePath);
                Element rootElement = doc.getDocumentElement();
                oRMap = loadObjects(rootElement);
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ObjectRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return oRMap;
    }

    private static ConcurrentHashMap<String, ObjectRepo> loadObjects(Element rootElement) {
        ConcurrentHashMap<String, ObjectRepo> oRMap = new ConcurrentHashMap<>();
        NodeList pageNodes = rootElement.getElementsByTagName(XMLTag.PageTag);
        for (int j = 0; j < pageNodes.getLength(); j++) {
            Node pageNode = pageNodes.item(j);
            ObjectRepo newPage = new ObjectRepo();
            String pageName = getAttribute(pageNode, ObjectAttribute.name);
            newPage.setPageName(pageName);
            loadObject((Element) pageNode, newPage);
            oRMap.put(pageName, newPage);
        }
        return oRMap;
    }

    private static void loadObject(Element pageNode, ObjectRepo newPage) {
        NodeList objectNodes = pageNode.getElementsByTagName(XMLTag.ObjectTag);
        for (int j = 0; j < objectNodes.getLength(); j++) {
            Node objectNode = objectNodes.item(j);
            ORObject newObject = new ORObject();
            String objectName = getAttribute(objectNode, ObjectAttribute.name);
            String frameData = getAttribute(objectNode, ObjectAttribute.frame);
            newObject.setObjectName(objectName);
            newObject.setFrameData(frameData);
            loadProperties((Element) objectNode, newObject);
            newPage.addObject(objectName, newObject);
        }
    }

    private static void loadProperties(Element objectNode, ORObject newObject) {
        NodeList propertyNodes = objectNode.getElementsByTagName(XMLTag.PropertyTag);
        int length = propertyNodes.getLength();
        for (int j = 0; j < length; j++) {
            Node propertyNode = propertyNodes.item(j);
            String propertyName = getAttribute(propertyNode, ObjectAttribute.name);
            String propertyValue = getAttribute(propertyNode, ObjectAttribute.value);
            String preferenceIndex = getAttribute(propertyNode, ObjectAttribute.preference);
            int prefindex = j;
            if (preferenceIndex != null && preferenceIndex.matches("[0-9]+")) {
                prefindex = Integer.parseInt(preferenceIndex);
            }
            newObject.addObjectProperty(propertyName, propertyValue, prefindex);
        }
    }

    private static String getAttribute(Node node, String attribute) {
        Node attr = node.getAttributes().getNamedItem(attribute);
        return attr == null ? null : attr.getTextContent();
    }

}
