/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.data;

import com.cognizant.constants.FilePath;
import com.cognizant.constants.SystemDefaults;
import com.cognizant.support.CSVMgr;
import com.cognizant.support.DataSource;
import com.cognizant.support.FieldData;
import com.cognizant.support.UnCaughtException;
import com.cognizant.support.Step;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DataAccess {

    public static List<String> GlobalColumn;
    public static Map<String, List<String>> GlobalValues;

    public String ScenarioName;
    public String TestCaseName;

    public DataAccess(String srcName, String srcTable) {

        ScenarioName = srcName;
        TestCaseName = srcTable;

    }

    public static void initializeGlobalData() {
        GlobalValues = new HashMap<>();
        File GlobalFile = new File(FilePath.getGlobalTestDataPath());
        HashMap<String, ArrayList<String[]>> GlobalData = CSVMgr.getHashCSV(
                GlobalFile, "GlobalDataID");
        if (GlobalData != null) {
            GlobalColumn = Arrays.asList(GlobalData.get("#Headers").get(0));
            for (String Id : GlobalData.keySet()) {
                GlobalValues.put(Id, Arrays.asList(GlobalData.get(Id).get(0)));
            }
        }
    }

    public void setSheet(String strScenario, String strTC) {
        ScenarioName = strScenario;
        TestCaseName = strTC;
    }

    public static ConcurrentHashMap<String, Map<String, List<Map<String, String>>>> getIObjects() {
        ConcurrentHashMap<String, Map<String, List<Map<String, String>>>> map = new ConcurrentHashMap<>();
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc;
            Element rootElement = null;
            try {
                doc = docBuilder.parse(FilePath.getIORPath());
                rootElement = doc.getDocumentElement();
            } catch (IOException | SAXException ex) {
                Logger.getLogger(DataAccess.class.getName()).log(Level.OFF, null, ex);
                return map;
            }
            NodeList Page = rootElement.getElementsByTagName("Page");
            int totalpage = Page.getLength();
            Element pageref;
            String pageKey, objectKey;
            List<Map<String, String>> objList;
            for (int i = 0; i < totalpage; i++) {
                Map<String, List<Map<String, String>>> pagemap = new HashMap<>();
                pageref = (Element) Page.item(i);
                pageKey = pageref.getAttribute("ref");
                NodeList group = pageref.getElementsByTagName("ObjectGroup");

                int totalgroup = group.getLength();
                for (int g = 0; g < totalgroup; g++) {
                    Element groupref = (Element) group.item(g);
                    NodeList objects = groupref.getElementsByTagName("Object");
                    objectKey = groupref.getAttribute("ref");
                    int totalobject = objects.getLength();
                    objList = new ArrayList<>();
                    for (int j = 0; j < totalobject; j++) {
                        Element objectref = (Element) objects.item(j);
                        String objname = objectref.getAttribute("ref");
                        Map<String, String> prop = new HashMap<>();
                        prop.put(
                                "Url",
                                objectKey + File.separatorChar + objname
                                + File.separatorChar
                                + objectref.getAttribute("Url"));

                        objList.add(getPropMap(prop, objectref));
                    }
                    pagemap.put(objectKey, objList);
                }

                NodeList object = pageref.getChildNodes();
                int totalobject = object.getLength();
                for (int j = 0; j < totalobject; j++) {
                    Node objectNode = object.item(j);
                    if (objectNode.getNodeType() != Node.ELEMENT_NODE) {
                        continue;
                    }
                    Element objectref = (Element) objectNode;
                    if (objectref.getNodeType() == Node.ELEMENT_NODE && objectref.getTagName().equalsIgnoreCase("Object")) {
                        objList = new ArrayList<>();
                        objectKey = objectref.getAttribute("ref");
                        Map<String, String> prop = new HashMap<>();
                        prop.put("Url", objectKey + File.separatorChar
                                + objectref.getAttribute("Url"));
                        objList.add(getPropMap(prop, objectref));
                        pagemap.put(objectKey, objList);
                    }
                }
                map.put(pageKey, pagemap);
            }

        } catch (Exception ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    private static Map<String, String> getPropMap(Map<String, String> prop,
            Element objectref) {
        prop.put("Name", objectref.getAttribute("ref"));
        prop.put("Offset", objectref.getAttribute("Offset"));
        prop.put("Precision", objectref.getAttribute("Precision"));
        prop.put("ROI", objectref.getAttribute("ROI"));
        prop.put("Coordinates", objectref.getAttribute("Coordinates"));
        prop.put("Index", objectref.getAttribute("Index"));
        prop.put("Text", objectref.getAttribute("Text"));
        return prop;
    }

    public static ArrayList<Step> getStepList(String scenario, String testcase) throws UnCaughtException {

        ArrayList<Step> stepList = new ArrayList<>();
        File file = new File(FilePath.getTestCasePath(scenario, testcase));
        if (file.exists()) {
            HashMap<String, ArrayList<String[]>> Data = CSVMgr.getHashCSV(file,
                    "Step");
            if (Data != null) {
                List<String> Header = Arrays.asList(Data.get("#Headers").get(0));
                for (int i = 1; i < Data.keySet().size(); i++) {
                    ArrayList<String[]> Rows = Data.get("" + i);
                    if (Rows == null) {
                        Rows = Data.get("*" + i);
                    }
                    if (Rows != null) {
                        for (String[] Values : Rows) {
                            String step = Values[Header.indexOf("Step")];
                            Boolean breakPoint = false;
                            if (step.startsWith("*")) {
                                step = step.replace("*", "");
                                breakPoint = SystemDefaults.debugMode.get();
                            }
                            int stepNum = Integer.parseInt(step);
                            Step currStep = new Step(stepNum);
                            currStep.BreakPoint = breakPoint;
                            currStep.ObjectName = Values[Header.indexOf("ObjectName")];
                            currStep.Description = Values[Header.indexOf("Description")];
                            currStep.Input = Values[Header.indexOf("Input")];
                            currStep.Action = Values[Header.indexOf("Action")];
                            currStep.Condition = Values[Header.indexOf("Condition")];
                            currStep.Reference = Values[Header.indexOf("Reference")];
                            stepList.add(currStep);
                        }
                    }
                }

            } else {
                throw new UnCaughtException(
                        "[DATA ACCESS] Step Map Could Not Be Generated. Possible Error In: "
                        + scenario + ":" + testcase);
            }
        } else {
            throw new UnCaughtException(
                    scenario + ":" + testcase + " File Doesn't exist - " + file.getAbsolutePath());
        }
        return stepList;

    }

    public HashMap<String, FieldData> getParamData(ArrayList<String> fNames,
            DataSource Source) throws UnCaughtException {

        if (Source.IterationSetFlag) {
            Queue<Integer> iters = getDataIteration(Source.Scenario,
                    Source.TestCase, fNames.get(0).split(":", 2)[0],
                    Source.getStartIteration(), Source.getEndIteration());
            if (!iters.isEmpty()) {
                iters.remove(Source.getStartIteration());
                Source.Iterations = iters;
            }
            Source.IterationSetFlag = false;
        }

        HashMap<String, FieldData> map = new HashMap<>();
        Map<String, ArrayList<String>> valmap = new HashMap<>();
        for (String f : fNames) {
            String key = f.split(":", 2)[0];
            String value = f.split(":", 2)[1];
            if (valmap.get(key) == null) {
                valmap.put(key, new ArrayList<String>());
            }
            valmap.get(key).add(value);
            map.put(f, new FieldData());
        }

        for (Map.Entry<String, ArrayList<String>> entry : valmap.entrySet()) {

            File file = new File(FilePath.getTestDataPath(entry.getKey()));
            HashMap<String, ArrayList<String[]>> Data = CSVMgr.getHashCSV(file,
                    new String[]{"Scenario", "Flow", "Iteration"});
            if (Data != null) {
                List<String> Header = Arrays
                        .asList(Data.get("#Headers").get(0));
                ArrayList<String> params = entry.getValue();
                ArrayList<String[]> Rows = Data.get(Source.Scenario + Source.TestCase + Source.CurrentTestCaseIteration);
                if (Rows == null) {
                    Rows = Data.get(Source.Scenario + Source.TestCase + "1");
                    System.out.println("No Data available for Testcase[" + Source.Scenario + ":" + Source.TestCase + "] for Iteration [" + Source.CurrentTestCaseIteration + "]\nTrying to get Data from Default Iteration");
                }
                if (Rows == null) {
                    Rows = Data.get(ScenarioName + TestCaseName + Source.CurrentTestCaseIteration);
                    System.out.println("No Data available for Testcase[" + Source.Scenario + ":" + Source.TestCase + "]\nTrying to get Data from resuable component[" + ScenarioName + ":" + TestCaseName + "] itself");
                }
                if (Rows == null) {
                    Rows = Data.get(ScenarioName + TestCaseName + "1");
                    System.out.println("No Data available for Testcase[" + ScenarioName + ":" + TestCaseName + "] for Iteration [" + Source.CurrentTestCaseIteration + "]\nTrying to get Data from Default Iteration");
                }
                if (Rows != null) {
                    try {
                        for (String[] Values : Rows) {
                            for (String Field : params) {
                                if (Header.indexOf(Field) != -1) {
                                    String Value = Values[Header.indexOf(Field)];
                                    if (Value.startsWith("#")) {
                                        if (GlobalValues.get(Value) != null) {
                                            Value = GlobalValues.get(Value).get(
                                                    GlobalColumn.indexOf(Field));
                                        }
                                    }
                                    map.get(entry.getKey() + ":" + Field).values.add(Value);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                throw new UnCaughtException("[Error in Accessing Data from TestdataSheet[" + entry.getKey() + "]"
                        + "for Testcase"
                        + "");
            }

        }

        if (map.isEmpty()) {
            throw new UnCaughtException(
                    "[DATA ACCESS] Cannot Find Corresponding Data Fields:"
                    + fNames.toString() + " in " + ScenarioName + ":"
                    + TestCaseName + " for " + Source.Scenario + " : "
                    + Source.TestCase);
        }

        return map;

    }

    public ArrayList<String> getFieldNames() {

        ArrayList<String> fields = new ArrayList<>();

        File f = new File(FilePath.getTestCasePath(ScenarioName, TestCaseName));
        HashMap<String, ArrayList<String[]>> Data = CSVMgr.getHashCSV(f,
                "Input");
        if (Data != null) {
            for (Map.Entry<String, ArrayList<String[]>> entry : Data.entrySet()) {
                String input = entry.getKey();
                ArrayList<String[]> value = entry.getValue();
                if (!value.isEmpty()
                        && !value.get(0)[0].startsWith("//")
                        && input.matches("(?!(@|#Headers|=|%)).+:.+")) {
                    fields.add(input);
                }
            }
        } else {
            System.out.println("Cannot find any Fields");
        }
        return fields;

    }

    public ArrayList<String> getLoopVals() {

        ArrayList<String> fields = new ArrayList<String>();

        File f = new File(FilePath.getTestCasePath(ScenarioName, TestCaseName));
        HashMap<String, ArrayList<String[]>> Data = CSVMgr.getHashCSV(f,
                "Condition");
        if (Data != null) {
            for (String Condition : Data.keySet()) {
                if ((Condition.startsWith("End Param:") || Condition
                        .startsWith("End Loop:")) && !Condition.contains(":@")) {
                    fields.add(Condition.split(":", 2)[1]);
                }
            }
        } else {
            System.out.println("Cannot find any Fields");
        }
        return fields;

    }

    public HashMap<String, String> setLoop(ArrayList<String> CNames,
            String scenario, String flow, int testIteration)
            throws UnCaughtException {
        //
        HashMap<String, String> map = new HashMap<>();
        Map<String, ArrayList<String>> valmap = new HashMap<>();
        for (String f : CNames) {
            String key = f.split(":", 2)[0];
            String value = f.split(":", 2)[1];
            if (valmap.get(key) == null) {
                valmap.put(key, new ArrayList<String>());
            }
            valmap.get(key).add(value);

        }

        for (Map.Entry<String, ArrayList<String>> entry : valmap.entrySet()) {

            File file = new File(FilePath.getTestDataPath(entry.getKey()));
            HashMap<String, ArrayList<String[]>> Data = CSVMgr.getHashCSV(file,
                    new String[]{"Scenario", "Flow", "Iteration"});
            if (Data != null) {
                List<String> Header = Arrays
                        .asList(Data.get("#Headers").get(0));
                ArrayList<String> params = entry.getValue();
                ArrayList<String[]> Rows = Data.get(scenario + flow
                        + testIteration);

                for (String[] Values : Rows) {
                    for (String Field : params) {
                        String Value = Values[Header.indexOf(Field)];
                        if (!Value.equals("")) {
                            if (Value.startsWith("#")) {
                                if (GlobalValues.get(Value) != null) {
                                    Value = GlobalValues.get(Value).get(
                                            GlobalColumn.indexOf(Field));
                                }
                            }
                            map.put(entry.getKey() + ":" + Field, Value);
                        }
                    }

                }
            } else {
                throw new UnCaughtException("[Error in handling Run Manager]");
            }

        }

        if (map.isEmpty()) {
            throw new UnCaughtException(
                    "[DATA ACCESS] Cannot Find Corresponding Data Fields:"
                    + CNames.toString() + " in " + ScenarioName + ":"
                    + TestCaseName + " for " + scenario + " : " + flow);
        }

        return map;
    }

    public Queue<Integer> getDataIteration(String scenario, String testcase,
            String TestData, int startIteration, int endIteration) {

        // String DataSheet = getDataSheetFromAssocaiation(scenario, testcase);
        LinkedList<Integer> iterations = new LinkedList<Integer>();
        try {
            File f = new File(FilePath.getTestDataPath(TestData));
            HashMap<String, ArrayList<String[]>> Data = CSVMgr.getHashCSV(f,
                    new String[]{"Scenario", "Flow"});
            if (Data != null) {
                List<String> Header = Arrays
                        .asList(Data.get("#Headers").get(0));
                ArrayList<String[]> rows = Data.get(scenario + testcase);
                if (Header != null && rows != null) {

                    for (String[] rowVal : rows) {
                        int i = 0;
                        try {
                            i = Integer.parseInt(rowVal[Header
                                    .indexOf("Iteration")]);
                        } catch (Exception ex) {
                            Logger.getLogger(DataAccess.class.getName()).log(Level.OFF, null, ex);
                            i = 0;
                        }
                        if (i >= startIteration
                                && (i <= endIteration || endIteration == -1)) {
                            if (!iterations.contains(i)) {
                                iterations.add(i);
                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }

        return iterations;

    }

}
