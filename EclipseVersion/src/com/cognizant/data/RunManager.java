/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.data;

import com.cognizant.constants.CommonConstants.Browser;
import com.cognizant.constants.FilePath;
import com.cognizant.core.RunContext;
import com.cognizant.settings.SettingsProperties.ExecProperties;
import com.cognizant.support.CSVMgr;
import com.cognizant.support.UnCaughtException;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.Platform;

public class RunManager {

    public static void init() throws UnCaughtException {
        LoadSettings.getSettings().loadSettings(FilePath.getPropertiesPath());
    }

    public static Queue<RunContext> loadRunManager() throws Exception {
        if (LoadSettings.getSettings().isTestRun()) {
            return getTestCaseRunManager();
        } else {
            return getTestSetRunManager();
        }
    }

    static Queue<RunContext> getTestCaseRunManager() {
        Queue<RunContext> execQ = new LinkedList<>();
        RunContext exe = new RunContext();
        exe.Scenario = LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.Scenario);
        exe.TestCase = LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.TestCase);
        exe.Description = "Test Run";
        exe.BrowserName = LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.Browser);
        exe.Browser = Browser.fromString(exe.BrowserName);
        exe.Platform = Platform.ANY;
        exe.BrowserVersion = "default";
        exe.Iteration = "Single";
        exe.print();
        execQ.add(exe);
        return execQ;
    }

    static Queue<RunContext> getTestSetRunManager() throws Exception {
        Queue<RunContext> execQ = new LinkedList<>();
        File file = new File(FilePath.getTestSetPath());
        HashMap<String, ArrayList<String[]>> Data = CSVMgr.getHashCSV(file,
                "Execute");
        if (Data != null) {
            List<String> Header = Arrays.asList(Data.get("#Headers").get(0));
            ArrayList<String[]> Rows = Data.get("true");
            for (String[] Values : Rows) {
                RunContext exe = new RunContext();
                exe.Scenario = Values[Header.indexOf("TestScenario")];
                exe.TestCase = Values[Header.indexOf("TestCase")];
                exe.Description = "Test Set";
                exe.PlatformValue = Header.indexOf("Platform") != -1 ? Values[Header
                        .indexOf("Platform")] : "";
                exe.Platform = getPlatform(exe.PlatformValue);
                exe.BrowserName = Values[Header.indexOf("Browser")].trim();
                exe.Browser = Browser.fromString(exe.BrowserName);
                exe.BrowserVersionValue = Header
                        .indexOf("BrowserVersion") != -1 ? Values[Header
                                .indexOf("BrowserVersion")] : "";
                exe.BrowserVersion = getBrowserVersion(exe.BrowserVersionValue);
                exe.Iteration = Values[Header.indexOf("Iteration")];
                exe.print();
                execQ.add(exe);
            }
        } else {
            throw new Exception("[Error in handling Run Manager]"
                    + "\n No testset ["
                    + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.TestSet)
                    + "]"
                    + " is available under Release ["
                    + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.Release)
                    + "]\n in the path - " + file.getAbsolutePath()
                    + "\n Configure the testset via RunSettings.exe in the EclipseVersion Folder");
        }
        System.out.println("----------------------------------------------------------");
        System.out.println("No. of Testcases added to Execution [" + execQ.size() + "]");
        System.out.println("----------------------------------------------------------");
        return execQ;
    }

    static Boolean getValue(String value) {
        return value != null && Boolean.valueOf(value);
    }

    public static int getThreadCount(String threadCount) {
        switch (threadCount.toLowerCase()) {
            case "single":
                return 1;
            case "2 threads":
                return 2;
            case "3 threads":
                return 3;
            case "4 threads":
                return 4;
            case "5 threads":
                return 5;
            default:
                return getThread(threadCount);
        }
    }

    static int getThread(String threadCount) {
        try {
            int x = Integer.parseInt(threadCount);
            return x > 0 ? x : 1;
        } catch (NumberFormatException ex) {
            System.out.println("Error Converting value[" + threadCount + "] Resetting thread to 1");
            return 1;
        }
    }

    static long getExecutionTimeout(String exeTimeout) {
        try {
            int l = Integer.parseInt(exeTimeout);
            return l > 0 ? l : 60L;
        } catch (NumberFormatException ex) {
            return 60L;
        }
    }

    static String getBrowserVersion(String bVersion) {
        if (bVersion == null || bVersion.isEmpty()) {
            return "default";
        }
        return bVersion;
    }

    static Platform getPlatform(String platform) {
        if (platform != null && !platform.trim().isEmpty()) {
            return Platform.fromString(platform.toUpperCase());
        }
        return Platform.ANY;
    }
}
