/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.constants;

/**
 *
 * @author 394173
 */
public final class ObjectProperty {

    public static final String Name = "name";
    public static final String Id = "id";
    public static final String ClassName = "class";
    public static final String LinkText = "link_text";
    public static final String RXpath = "relative_xpath";
    public static final String Xpath = "xpath";
    public static final String Css = "css";
    public static final String Type = "type";
    public static final String UiAutomator = "UiAutomator";
    public static final String Accessibility = "Accessibility";
}
