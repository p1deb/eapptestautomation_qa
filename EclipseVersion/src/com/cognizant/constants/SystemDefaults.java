/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.constants;

import com.cognizant.constants.CommonConstants.Keys;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SystemDefaults {

    public static AtomicInteger waitTime = new AtomicInteger(10);
    public static AtomicInteger elementWaitTime = new AtomicInteger(10);
    public static AtomicBoolean stopExecution = new AtomicBoolean();
    public static AtomicBoolean debugMode = new AtomicBoolean();
    public static AtomicBoolean stopCurrentIteration = new AtomicBoolean();
    public static AtomicBoolean getClassesFromJar = new AtomicBoolean();
    public static AtomicBoolean reportComplete = new AtomicBoolean();
    public static AtomicBoolean nextStepflag = new AtomicBoolean(true);
    public static AtomicBoolean breakPointflag = new AtomicBoolean();
    public static Map<String, String> CLVars = new HashMap<>();
    public static Map<String, String> EnvVars = new HashMap<>();

    public static void pollWait() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SystemDefaults.class.getName()).log(Level.SEVERE,
                    ex.getMessage(), ex);
        }
    }

    public static void resetAll() {
        waitTime = new AtomicInteger(10);
        elementWaitTime = new AtomicInteger(10);
        stopExecution = new AtomicBoolean();
        debugMode = new AtomicBoolean();
        stopCurrentIteration = new AtomicBoolean();
        reportComplete = new AtomicBoolean();
        nextStepflag = new AtomicBoolean(true);
        breakPointflag = new AtomicBoolean();

    }

    public static boolean canLaunchSummary() {
        return !CLVars.containsKey(Keys.DONT_LAUNCH_SUMMARY);
    }

    public static void printSystemInfo() {
        System.out.println("Run Information");
        System.out.println("========================");
        System.out.println("spritz.engine : 4.2.1");
        printSystemInfo("java.runtime.name");
        printSystemInfo("java.version");
        printSystemInfo("java.home");
        printSystemInfo("os.name");
        printSystemInfo("os.arch");
        printSystemInfo("os.version");
        printSystemInfo("file.encoding");
        System.out.println("========================");
    }

    private static void printSystemInfo(String key) {
        System.out.println(String.format("%s : %s", new Object[]{key, System.getProperty(key)}));
    }

}
