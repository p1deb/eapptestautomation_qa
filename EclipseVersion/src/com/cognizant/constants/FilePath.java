/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.constants;

import com.cognizant.data.LoadSettings;
import com.cognizant.reporting.DateTimeUtils;
import com.cognizant.settings.SettingsProperties.ExecProperties;
import java.io.File;

public class FilePath {

    private static final String ORName = "OR.object";
    private static final String IORName = "IOR.object";
    private static final String MORName = "MOR.object";
    private static final String fileFormat = ".csv";
    private final static String ScenarioFolder = "TestPlan";
    private final static String TestSuiteFolder = "TestLab";
    private final static String IORImageStoreLocation = "ImageObjectRepository";
    private final static String ORImageStoreLocation = "ObjectRepository";
    private final static String PageDumpLocation = "PageDump";
    private final static String TestData = "TestData";
    private final static String GlobalTestData = "GlobalData" + fileFormat;
    private final static String ResultsFolder = "Results";
    private final static String reportTemplateFolder = "ReportTemplate";
    private final static String configurationFolder = "Configuration";
    private final static String globalProperties = "Global Settings.properties";
    private final static String summaryHtml = "summary.html";
    private final static String reportHtml = "testCase.html";
    private final static String detailedHtml = "detailed.html";
    private final static String perfReportHtml = "perfReport.html";
    private final static String reportHistoryHtml = "ReportHistory.html";
    private final static String reportData = "data.js";
    private final static String reportHisotryData = "reportHistory.js";
    private final static String projectDatabase = "data.db";
    private final static String emulatorLocation = "Emulators.csv";
    private final static String emulatorFileLocation = "Emulators.json";
    final public static String stepMapFile = "StepMap.csv";
    private final static String explorerConfig = "ExplorerConfig.properties";
    private final static String addonLocation = "Extensions"; //Subjected to change
    private final static String firefoxAddonLocation = "FireFox" + File.separator + "spritz.xpi";//Subjected to change
    private final static String chromeAddonLocation = "Chrome" + File.separator + "Spritz.crx";//Subjected to change
//    private final static String safariAddonLocation = "Safari" + File.separator + "spritz.safariextz";//Subjected to change
    private final static String spritzJarLocation = "lib" + File.separator + "spritz.jar";
    private static String date;
    private static String time;

    public static String getSpritzPath() {
        if (SystemDefaults.getClassesFromJar.get()) {
            return System.getProperty("user.dir");
        } else {
            return new File(System.getProperty("user.dir")).getParent();
        }
    }

    public static String getPropertiesPath() {
        return getSpritzPath() + File.separatorChar + configurationFolder
                + File.separatorChar + globalProperties;
    }

    public static String getConfigurationPath() {
        return getSpritzPath() + File.separatorChar + configurationFolder;
    }

    public static String getORPath() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + ORName;
    }

    public static String getIORPath() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + IORName;
    }

    public static String getMORPath() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + MORName;
    }

    public static String getIORimagestorelocation() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + IORImageStoreLocation;
    }

    public static String getORimagestorelocation() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath) + File.separatorChar + ORImageStoreLocation;
    }

    public static String getPageDumpLocation() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath) + File.separatorChar + PageDumpLocation;
    }

    public static String getORpageListJsonFile() {
        return getPageDumpLocation()
                + File.separatorChar + "pageDetails.js";
    }

    public static String getTestLabPath() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + TestSuiteFolder;
    }

    public static String getTestPlanPath() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + ScenarioFolder;
    }

    public static String getReleasePath() {
        return getTestLabPath() + File.separatorChar
                + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.Release);
    }

    public static String getTestSetPath() {
        return getReleasePath() + File.separatorChar
                + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.TestSet) + fileFormat;
    }

    public static String getScenarioPath() {
        return getTestPlanPath() + File.separatorChar
                + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.Scenario);
    }

    public static String getTestCasePath() {
        return getScenarioPath() + File.separatorChar
                + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.TestCase) + fileFormat;
    }

    public static String getScenarioPath(String scenario) {
        return getTestPlanPath() + File.separatorChar + scenario;
    }

    public static String getTestCasePath(String scenario, String testCase) {
        return getScenarioPath(scenario) + File.separatorChar + testCase
                + fileFormat;
    }

    public static String getTestDataPath() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + TestData;
    }

    public static String getTestDataPath(String testData) {
        return getTestDataPath() + File.separatorChar + testData + fileFormat;
    }

    public static String getGlobalTestDataPath() {
        return getTestDataPath() + File.separatorChar + GlobalTestData;
    }

    public static String getProjectDatabaseLocation() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + projectDatabase;
    }

    public static String getEmulatorlocation() {
        return getConfigurationPath() + File.separatorChar + emulatorLocation;
    }

    public static String getEmulatorFilelocation() {
        return LoadSettings.getSettings().getSettingsFolder()
                + File.separatorChar + emulatorFileLocation;
    }

    public static String getExplorerConfig() {
        return getConfigurationPath() + File.separatorChar + explorerConfig;
    }

    public static String getReportThemePreviewPath() {
        return getConfigurationPath() + File.separatorChar
                + reportTemplateFolder + File.separatorChar
                + "preview";
    }

    public static String getReportThemePath() {
        return getReportResourcePath() + File.separatorChar
                + "theme";
    }

    public static String getReportResourcePath() {
        return getConfigurationPath() + File.separatorChar
                + reportTemplateFolder + File.separatorChar
                + "media";
    }

    public static String getReportTemplatePath() {
        return getConfigurationPath() + File.separatorChar
                + reportTemplateFolder + File.separator + "html";
    }

    public static String getSummaryHTMLPath() {
        return getReportTemplatePath() + File.separatorChar + summaryHtml;
    }

    public static String getDetailedHTMLPath() {
        return getReportTemplatePath() + File.separatorChar + detailedHtml;
    }

    public static String getReportHTMLPath() {
        return getReportTemplatePath() + File.separatorChar + reportHtml;
    }

    public static String getReportHistoryHTMLPath() {
        return getReportTemplatePath() + File.separatorChar + reportHistoryHtml;
    }

    public static void initDateTime() {
        date = DateTimeUtils.DateNowForFolder();
        time = DateTimeUtils.TimeNowForFolder();
    }

    public static String getResultsPath() {
        return LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.ProjectPath)
                + File.separatorChar + ResultsFolder;
    }

    private static String getResultPath() {
        if (LoadSettings.getSettings().isTestRun()) {
            return File.separatorChar + "TestDesign"
                    + File.separatorChar
                    + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.Scenario)
                    + File.separatorChar
                    + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.TestCase);
        }
        return File.separatorChar + "TestExecution"
                + File.separatorChar
                + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.Release)
                + File.separatorChar
                + LoadSettings.getSettings().getExecSettings().getProperty(ExecProperties.TestSet);
    }

    public static String getCurrentResultsPath() {
        return getCurrentResultsLocation() + File.separatorChar + date + " " + time;
    }

    public static String getCurrentResultsLocation() {
        return getResultsPath() + getResultPath();
    }

    public static String getCurrentSummaryHTMLPath() {
        return getCurrentResultsPath() + File.separatorChar + summaryHtml;
    }

    public static String getCurrentSummaryHTMLPathRelative() {
        return getCurrentReportFolderName() + File.separatorChar + summaryHtml;
    }

    public static String getCurrentReportFolderName() {
        return date + " " + time;
    }

    public static String getCurrentPerfReportHarPath() {
        return getResultsPath() + File.separator + "perf" + File.separator + "har";
    }

    public static String getCurrentReportDataPath() {
        return getCurrentResultsPath() + File.separatorChar + reportData;
    }

    public static String getCurrentReportHistoryDataPath() {
        return getCurrentResultsLocation() + File.separatorChar + reportHisotryData;
    }

    public static String getDate() {
        return date;
    }

    public static String getTime() {
        return time;
    }

    /**
     * Addon Path
     *
     * @return
     */
    public static File getFireFoxAddOnPath() {
        return new File(getAddonPath() + File.separatorChar + firefoxAddonLocation);
    }

    public static File getChromeAddOnPath() {
        return new File(getAddonPath() + File.separatorChar + chromeAddonLocation);
    }

    public static File getSafariAddOnPath() {
        return new File(getAddonPath() + File.separatorChar + firefoxAddonLocation);
    }

    public static String getAddonPath() {
        return getSpritzPath() + File.separatorChar + addonLocation;
    }

    public static String getSpritzJarPath() {
        return getSpritzPath() + File.separatorChar + spritzJarLocation;
    }

    /**
     *
     * @return location of application configuration file
     */
    public static String getAppSettings() {
        return getConfigurationPath() + File.separator + "app.settings";
    }

    public static String getStepMapFile() {
        return getConfigurationPath() + File.separator + stepMapFile;
    }

    public static String getPropertiesPath(String fileName) {
        return getConfigurationPath() + File.separator + fileName;
    }

    public static String getPerfReportHTMLPath() {
        return getReportTemplatePath() + File.separatorChar + perfReportHtml;
    }

    public static String getSoapUIUtilPath() {
        return getSpritzPath() + File.separator + "Tools" + File.separator + "SoapUI";
    }

}
