/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.constants;

import java.util.ArrayList;
import java.util.Arrays;
import org.openqa.selenium.Platform;

public class CommonConstants {

    public enum Browser {

        FireFox("Firefox"),
        Chrome("Chrome"),
        IE("IE", "Internet Explorer"),
        Edge("Edge", "Microsoft Edge"),
        HtmlUnit("HtmlUnit"),
        Opera("Opera"),
        Safari("Safari"),
        Appium("Appium"),
        PhantomJS("PhantomJS"),
        Emulator("Emulator");

        private final ArrayList<String> browserValue;

        Browser(String... value) {
            browserValue = new ArrayList<>(Arrays.asList(value));
        }

        public String getBrowserValue() {
            return browserValue.get(0);
        }

        @Override
        public String toString() {
            return getBrowserValue();
        }

        public static Browser fromString(String browserName) {
            for (Browser browser : values()) {
                for (String string : browser.browserValue) {
                    if (string.equalsIgnoreCase(browserName)) {
                        return browser;
                    }
                }
            }
            return Emulator;
        }

        public static ArrayList<String> getValuesAsList() {
            ArrayList<String> browserList = new ArrayList<>();
            for (Browser browser : values()) {
                if (!browser.equals(Emulator)) {
                    browserList.add(browser.getBrowserValue());
                }
            }
            return browserList;
        }
    }

    public static ArrayList<String> getPlatFormList() {
        ArrayList<String> platFormList = new ArrayList<>();
        for (Platform platForm : Platform.values()) {
            platFormList.add(platForm.name());
        }
        return platFormList;
    }

    public final static ArrayList<String> packageList = new ArrayList<>(
            Arrays.asList(new String[]{"com.cognizant.commands", "com.cognizant.TestComponents"}));

    public static class Keys {

        public static final String DONT_LAUNCH_SUMMARY = "dontLaunchSummary";
    }

}
