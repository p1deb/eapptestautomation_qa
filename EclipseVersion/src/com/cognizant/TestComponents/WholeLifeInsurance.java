package com.cognizant.TestComponents;

import com.cognizant.BusinessComponents.BeneficiaryPage;
import com.cognizant.BusinessComponents.CaseInformationPage;
import com.cognizant.BusinessComponents.ChildrenBenefitRiderPage;
import com.cognizant.BusinessComponents.CoverageAndRidersPage;
import com.cognizant.BusinessComponents.ExistingInsurancePage;
import com.cognizant.BusinessComponents.OwnerInformation;
import com.cognizant.BusinessComponents.PersonalInformationPage;
import com.cognizant.BusinessComponents.PremiumInformationPage;
import com.cognizant.BusinessComponents.ProposedInsuredInfoPage;
import com.cognizant.BusinessComponents.USPAtrioticActOwnerIdentification;
import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.UnCaughtException;

public class WholeLifeInsurance extends Command{
	
	CaseInformationPage caseInfo;
	ProposedInsuredInfoPage proposedInfo;
	ExistingInsurancePage existingInsurance;
	PremiumInformationPage premiumInfo; 
	USPAtrioticActOwnerIdentification usPatrioticAct;
	OwnerInformation ownerInfo;
	BeneficiaryPage beneInfo;
	CoverageAndRidersPage covergeRiders;
	PersonalInformationPage personalInfo;
	ChildrenBenefitRiderPage childRiders;
	String dataTableName = "WholeLifeInsurance";
	

	public WholeLifeInsurance(CommandControl cc) {
		super(cc);
		caseInfo = new CaseInformationPage(cc);
		proposedInfo = new ProposedInsuredInfoPage(cc);
		existingInsurance = new ExistingInsurancePage(cc);
		premiumInfo = new PremiumInformationPage(cc);
		usPatrioticAct = new USPAtrioticActOwnerIdentification(cc);
		ownerInfo = new OwnerInformation(cc);
		beneInfo = new BeneficiaryPage(cc);
		covergeRiders = new CoverageAndRidersPage(cc);
		personalInfo = new PersonalInformationPage(cc);
		childRiders = new ChildrenBenefitRiderPage(cc);
	}
	
	//Case Info Page
	
	public void enterDOB_CaseInformation()
	{
		String strDOB = Data;
		caseInfo.enterDOB(strDOB);
	}
	
	public void selectGender_CaseInformation()
	{
		String strGender = Data;
		caseInfo.enterGender(strGender);		
	}
	
	public void select_Illustrations_eApp()
	{
		String strProduct = Data;
		String illustrations = userData.getData("WholeLifeInsurance", "Illustrations");
		caseInfo.clickOnSelect(strProduct, illustrations);
	}
	
	//Proposed Insured Information
	
	public void interviewFamilyMember()
	{
		String strData = Data;
		proposedInfo.interviewFamilyMember(strData);

	}

	public void isInsuredOwner()
	{
		String strData = Data;
		proposedInfo.insuredOwner(strData);		
	}
	
	public void insuredHasLicense()
	{
		String strData = Data;
		String strLicenseNum = userData.getData(dataTableName, "DriversLicenseNo");
		String strLicenseIssueState = userData.getData(dataTableName, "IssueState");		
		proposedInfo.insuredLicense(strData, strLicenseNum, strLicenseIssueState);
	}
	
	public void enterEmploymentDetails()
	{
		String strEmpData = Data;
		proposedInfo.employmentDetails(strEmpData);
	}	
	
	//Owner Information
	
	public void enterOwnerInformation()
	{
		String strIsOwner = userData.getData(dataTableName, "WillTheProposedInsuredBeOwner");
		if(strIsOwner.equalsIgnoreCase("No"))
		{
			try 
			{
				executeTestCase("CreatePolicy", "OwnerInformation");
			} catch (UnCaughtException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void enterOwnerTypeAndDetails()
	{
		String strOwnerType = Data;
		ownerInfo.selectOwnerType(strOwnerType);
	}
	
	public void enterContactInformation()
	{
		String strContactInfo = Data;
		ownerInfo.contactInfo(strContactInfo);
	}
	
	public void multipleOwner()
	{
		String strMultipleOwners = Data;
		ownerInfo.multipleOwner(strMultipleOwners);
	}
	
	public void enterDOB()
	{
		String strDOB = Data;
		ownerInfo.enterDOB_OwnerInformation(strDOB);
	}
	
	//Children Benefit Rider
	
	public void enterGender_Child()
	{
		String strBirthDate = Data;
		childRiders.enterGender_Child(strBirthDate);
		
	}
	
	//Beneficiary
	
	public void sharePercentage()
	{
		String strData = Data;
		beneInfo.sharePercentage(strData);
	}
	
	public void enterPrimaryBeneficiaryDetails()
	{
		String strNumOfBeneficiary = Data;
		beneInfo.enterBeneficiaryDetails(strNumOfBeneficiary);
	}
	
	//Whole Life - Coverage and Riders
	
	public void dividendOption()
	{
		String strDividendOption = Data;
		covergeRiders.dividendOption(strDividendOption);
	}
	
	public void automaticPremiumLoan()
	{
		String strData = Data;
		covergeRiders.automaticPremiumLoan(strData);
	}
	
	public void qualifiedRetirementPlan()
	{
		String strData = Data;
		covergeRiders.qualifiedRetirementPlan(strData);
	}
	
	public void waiverofPremiumforDisability()
	{
		String strData = Data;
		covergeRiders.waiverofPremiumforDisability(strData);
	}
	
	public void blendedInsuranceRider()
	{
		String strData = Data;
		covergeRiders.blendedInsuranceRider(strData);
	}
	
	public void acceleratorPaidUpAdditionsRider()
	{
		String strData = Data;
		covergeRiders.acceleratorPaidUpAdditionsRider(strData);
	}
	
	public void blendAccelerator()
	{
		String strData = Data;
		covergeRiders.blendAccelerator(strData);
	}
	
	public void sameInsuredTerm()
	{
		String strData = Data;
		covergeRiders.sameInsuredTerm(strData);
	}
	
	public void otherInsuredTerm()
	{
		String strData = Data;
		covergeRiders.otherInsuredTerm(strData);
	}
	
	public void childrenBenefitRider()
	{
		String strData = Data;
		covergeRiders.childrenBenefitRider(strData);
	}
	
	public void guaranteedInsurabilityOptionRider()
	{
		String strData = Data;
		covergeRiders.guaranteedInsurabilityOptionRider(strData);
	}
	
	public void creditofPaidUpAdditionsforDisability()
	{
		String strData = Data;
		covergeRiders.creditofPaidUpAdditionsforDisability(strData);
	}
	
	public void survivorPurchaseOption()
	{
		String strData = Data;
		covergeRiders.survivorPurchaseOption(strData);
	}
	
	public void otherRiders()
	{
		String strData = Data;
		covergeRiders.otherRiders(strData);
	}
	
	//Personal Information
	
	public void proposedUsedNicotine_Tobacco()
	{
		String strData = Data;
		personalInfo.proposedInsdUsedNicotine_Tobacco(strData);
	}
	
	public void otherInsdUsedNicotine_Tobacco()
	{
		String strData = Data;
		personalInfo.otherInsdUsedNicotine_Tobacco(strData);
	}
	
	public void spoInsdUsedNicotine_Tobacco()
	{
		String strData = Data;
		personalInfo.spoInsdUsedNicotine_Tobacco(strData);
	}
	
	//Proposed Other Insured Information
	
	public void enterGender_OtherInsured()
	{
		String strGender = Data;
		proposedInfo.enterGender_OtherInsured(strGender);
	}
	
	public void insuredHasLicense_OtherInsured()
	{
		String strData = Data;
		String strLicenseNum = userData.getData(dataTableName, "LicenseNumber_OI");
		String strLicenseIssueState = userData.getData(dataTableName, "LicenseIssueState_OI");		
		proposedInfo.insuredLicense(strData, strLicenseNum, strLicenseIssueState);
	}
	
	public void residenceInformation()
	{
		String strResicenceIngo = Data;
		proposedInfo.residenceInfo(strResicenceIngo);
	}
	
	//Survivor Insured Information
	
	public void insuredHasLicense_SPO()
	{
		String strData = Data;
		String strLicenseNum = userData.getData(dataTableName, "LicenseNumber_SPO");
		String strLicenseIssueState = userData.getData(dataTableName, "LicenseIssueState_SPO");		
		proposedInfo.insuredLicense(strData, strLicenseNum, strLicenseIssueState);
	}
	
	public void residenceInformation_SPO()
	{
		String strResicenceInfo = Data;
		proposedInfo.residenceInfo_SPO(strResicenceInfo);
	}	
				

	//Beneficiary Other Insured
	
	public void sharePercentage_OtherInsured()
	{
		String strData = Data;
		beneInfo.sharePercentage_OtherInsured(strData);
	}
	
	public void enterPrimaryBeneficiaryDetails_OtherInsured()
	{
		String strNumOfBeneficiary = Data;
		beneInfo.enterBeneficiaryDetails_OtherInsured(strNumOfBeneficiary);
	}
	
	
	//Existing Insurance 
	
	public void enterExistingInsuranceDetails()
	{
		existingInsurance.existingInsurance();
	}
	
	public void policyBeingReplaced_or_Changed()
	{
		String strData  = Data;
		existingInsurance.beingReplacedChanged(strData); 
	}
	
	//Premium Information
	
	public void paymentMode()
	{
		String strPaymentMode = Data;
		premiumInfo.paymentMode(strPaymentMode);
	}
	
	public void premiumPayor()
	{
		String strPaymentMode = Data;
		premiumInfo.premiumPayor(strPaymentMode);
	}
	
	public void premiumFundedDetails()
	{
		String strData = Data;
		premiumInfo.premiumFunded(strData);
	}
	
	public void otherPremiumPayor() 
	{
		try {
		String strData = Data;
		if(strData.equalsIgnoreCase("Other"))
		{
			executeTestCase("CreatePolicy", "PremiumPayorInformation");}
		} catch (UnCaughtException e) {
			e.printStackTrace();
		}
	}
	
	public void provideDetails()
	{
		String strData = Data;
		String strPremiumPayor = userData.getData("WholeLifeInsurance", "PremiumPayor");
		if(strPremiumPayor.equalsIgnoreCase("Other"))
		{
			premiumInfo.provideDetails(strData);
		}
	}
	
	public void enterExpiryDate_OwnerIdentification()
	{
		String strDOB = Data;
		usPatrioticAct.enterExpiryDate_OwnerIdentity(strDOB);
	}
	
	//Page Selections
	
	public void proposedOtherInsuredPage()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("OtherInsuredTerm(OIR)", "ProposedOtherInsuredInformation");
				executeTestCase("OtherInsuredTerm(OIR)", "Beneficiary-OI1");
			}
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void survivorPurchaseOptionPage()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("SurvivorPurchaseOption", "SPO_InsuredInformation");
			}
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void childrenBenefitRiderPage()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("ChildrenBenefitRider", "ChildrenBenefitRider");
			}
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void personalInformationPage()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("OtherInsuredTerm(OIR)", "PrimaryAndOtherInsured_PersonalInfo");
			}
			else
			{
				executeTestCase("CreatePolicy", "PersonalInformation");
			}
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void personalInformationPage_SPO()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("SurvivorPurchaseOption", "PersonalInformation_SPO");
			}			
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void healthQuestionsPage()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("OtherInsuredTerm(OIR)", "PrimaryAnOtherInsured_HealthQustions");
				executeTestCase("OtherInsuredTerm(OIR)", "PrimaryAndOtherInsured_HealthQuestionsCont");
			}
			else
			{
				executeTestCase("CreatePolicy", "HealthQuestions");
				executeTestCase("CreatePolicy", "HealthQuestions_Continued");
			}
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void healthQuestionsPage_CBR()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("ChildrenBenefitRider", "PersonalInformation_SPO");
			}			
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void familyHistoryPage_OI()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("CreatePolicy", "FamilyHistory");
			}			
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void familyHistoryPage_SPO()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("CreatePolicy", "FamilyHistory");
			}			
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void hiv_ConsentPage_OI()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("CreatePolicy", "HIV_Consent");
			}			
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void hiv_ConsentPage_SPO()
	{
		try {
			String strData = Data;
			if(strData.equalsIgnoreCase("Yes"))
			{
				executeTestCase("CreatePolicy", "HIV_Consent");
			}			
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
	public void usa_PatrioticAct_OwnerIddentification()
	{
		try 
		{
			String strData = Data;
			if(strData.equalsIgnoreCase("1"))
			{
				executeTestCase("CreatePolicy", "USA_PatrioticAct_OwnerIdentification");
			}	
			else if(strData.equalsIgnoreCase("2"))
			{
				executeTestCase("OwnerInformation", "USA_PatrioticAct_OwnerIdentification_MO1");
			}
			else
			{
				executeTestCase("OwnerInformation", "USA_PatrioticAct_OwnerIdentification_MO2");
			}
			} catch (UnCaughtException e) {
				e.printStackTrace();
			}
	}
	
}
