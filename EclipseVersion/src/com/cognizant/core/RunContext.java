/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.core;

import com.cognizant.constants.CommonConstants.Browser;

public class RunContext {

    public String Scenario;
    public String TestCase;
    public String Description;
    public Browser Browser;
    public String BrowserName;
    public String BrowserVersion;
    public org.openqa.selenium.Platform Platform;
    public String Iteration;
    public String PlatformValue;
    public String BrowserVersionValue;
    public boolean useExistingDriver = false;

    public void print() {
        System.out.println("[Scenario:" + Scenario + "] [TestCase: " + TestCase + "]"
                + " [Description: " + Description + "] [Browser: " + BrowserName + "] "
                + "[BrowserVersion: " + BrowserVersion + "] [Platform: " + Platform.toString()
                + "][ExistingBrowser: " + useExistingDriver + "]"
        );
    }

}
