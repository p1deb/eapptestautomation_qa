/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognizant.core;

import com.cognizant.commands.General;

/**
 *
 * @author 394173
 */
public class Annotation extends General {

    public Annotation(CommandControl cc) {
        super(cc);
    }

    public void beforeStepExecution() {
    }

    public void afterStepExecution() {
        Report.getCurrentStatus();//To get the status of current executed step
        if (Report.isStepPassed()) {
            //do something
        } else {

        }
    }

}
