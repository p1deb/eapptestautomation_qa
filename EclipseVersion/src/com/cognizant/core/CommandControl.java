/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.core;

import com.cognizant.constants.FilePath;
import com.cognizant.data.LoadSettings;
import com.cognizant.data.UserDataAccess;
import com.cognizant.drivers.AutomationObject;
import com.cognizant.drivers.SeleniumDriver;
import com.cognizant.reporting.Report;
import com.cognizant.support.FParser;
import com.cognizant.support.UnCaughtException;
import com.cognizant.support.Status;
import com.cognizant.support.Step;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import org.openqa.selenium.WebElement;

public class CommandControl {

    public SeleniumDriver seDriver;
    public AutomationObject AObject;
    public String Data;
    public String Action;
    public String ObjectName;
    public String Reference;
    public WebElement Element;
    public String Condition;
    public String Description;
    public String Input;
    public Report Report;
    public List<IObject> itarget;
    public List<Object> ls;
    public static Robot robot;
    public String it = null; //to be implemented
    public UserDataAccess userData;
    private HashMap<String, String> runTimeVars = new HashMap<>();
    private Stack<WebElement> runTimeElement = new Stack<>();
    FParser parser;

    public CommandControl(SeleniumDriver driver, Report report) {
        seDriver = driver;
        AObject = new AutomationObject(seDriver.driver);
        Report = report;
        parser = new FParser(this);
        ls = new java.util.ArrayList<>();
        try {
            robot = new Robot();
        } catch (AWTException e) {
            System.out.println("[Object Creation Failed!!!]");
        }

    }

    public void refresh() {
        Data = "";
        ObjectName = "";
        Element = null;
        Condition = "";
        Description = "";
        Input = "";
        Reference = "";
        Action = "";

    }

    public void sync(Step curr) throws UnCaughtException {
        refresh();
        AObject.setDriver(seDriver.driver);
        this.Description = curr.Description;
        this.Action = curr.Action;
        if (curr.Input != null && curr.Input.length() > 0) {
            this.Input = curr.Input;
            this.Data = curr.Data;

            if (this.Data.startsWith("%") && this.Data.endsWith("%")) {
                System.out.println("Checking for " + this.Data);
                String resolve = getDynamicValue(this.Data);
                if (resolve != null) {
                    this.Data = resolve;
                    System.out.println("Changed to " + this.Data);
                }
            } else if (this.Data.startsWith("=") && this.Data.length() > 1) {
                this.Data = (String) parser.OP(this.Data.substring(1));
            }
            curr.Data = Data;
        }
        if (curr.Condition != null && curr.Condition.length() > 0) {
            this.Condition = curr.Condition;
        }

        if (curr.ObjectName != null && curr.ObjectName.length() > 0) {
            this.ObjectName = curr.ObjectName;

            if (!(ObjectName.equalsIgnoreCase("Execute")
                    || ObjectName.equalsIgnoreCase("Browser")
                    || ObjectName.equalsIgnoreCase("App")
                    || ObjectName.equalsIgnoreCase("WebService"))) {
                this.Reference = curr.Reference;
                if (!curr.Action.startsWith("img")) {
                    Element = AObject.findElement(ObjectName, Reference, Condition.equalsIgnoreCase("GlobalObject"));
                } else {
                    List<Map<String, String>> prop = AutomationObject.getIObject(this.Reference, this.ObjectName);
                    itarget = new ArrayList<>();
                    for (Map<String, String> obj : prop) {
                        IObject iobj = new IObject(obj, this.Reference);
                        itarget.add(iobj);
                    }
                }
            }
        }
    }

    public void addVar(String key, String val) {

        if (runTimeVars.containsKey(key)) {
            System.err.println("runTimeVars already contains " + key + ".Forcing change to " + val);
            System.out.println("Already contains " + key);

        }
        System.out.println("Adding to runTimeVars " + key + ":" + val);
        runTimeVars.put(key, val);

    }

    public String getVar(String key) {

        System.out.println("Getting runTimeVar " + key);
        String val = getDynamicValue(key);
        if (val == null) {
            System.err.println("runTimeVars does not contain " + key + ".Returning Empty");
            Report.updateTestLog("Get Var", "Getting From runTimeVars " + key + " Failed", Status.WARNING);
            return "";
        } else {
            return val;
        }

    }

    public String getDynamicValue(String key) {
        if (!runTimeVars.containsKey(key)) {
            key = key.matches("\\%(\\S)+\\%") ? key.substring(1, key.length() - 1) : key;
            return getUserDefinedData(key);
        }
        return runTimeVars.get(key);
    }

    public String getUserDefinedData(String key) {
        return LoadSettings.getSettings().getUserdefinedSettings().getProperty(key);
    }

    public void putUserDefinedData(String key, String value) {
        LoadSettings.getSettings().getUserdefinedSettings().getProperties().put(key, value);
    }

    public Stack<WebElement> getRunTimeElement() {
        return runTimeElement;
    }

    public static class IObject {

        public String loc, name, text;
        public int offsetx = 0, offsety = 0, index = 0;
        public float precision = 0.7f;
        public Rectangle roi, coordinates;

        public IObject(String loc, int x, int y, float score) {
            this.loc = loc;
            offsetx = x;
            offsety = y;
            precision = score;
        }

        public IObject(Map<String, String> prop, String Page) {
            String offset = prop.get("Offset");
            name = prop.get("Name");
            text = prop.get("Text");
            offsetx = getInt(offset.split(",", -1)[0]);
            offsety = getInt(offset.split(",", -1)[1]);
            precision = Float.valueOf(prop.get("Precision"));
            this.loc = FilePath.getIORimagestorelocation()
                    + File.separatorChar + Page + File.separatorChar + prop.get("Url");
            roi = getRect(prop.get("ROI"));
            coordinates = getRect(prop.get("Coordinates"));
            try {
                index = getInt(prop.get("Index"));
            } catch (Exception ex) {
                index = 0;
            }

        }

        int getInt(String val) {
            try {
                return Integer.valueOf(val);
            } catch (Exception ex) {
                return 0;
            }

        }

        private Rectangle getRect(String val) {
            try {
                String[] r = val.split(",", -1);
                return new Rectangle(Integer.valueOf(r[0]), Integer.valueOf(r[1]), Integer.valueOf(r[2]), Integer.valueOf(r[3]));
            } catch (Exception ex) {
                Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                return new Rectangle(0, 0, d.width, d.height);
            }
        }
    }
}
