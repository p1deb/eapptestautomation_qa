/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.core;

import com.cognizant.constants.SystemDefaults;
import com.cognizant.data.DataAccess;
import com.cognizant.data.LoadSettings;
import com.cognizant.data.UserDataAccess;
import com.cognizant.reporting.Report;
import com.cognizant.support.DataSource;
import com.cognizant.support.ElementException;
import com.cognizant.support.ForcedException;
import com.cognizant.support.UnCaughtException;
import com.cognizant.support.InputException;
import com.cognizant.support.MethodExecutor;
import com.cognizant.support.ParameterControl;
import com.cognizant.support.Status;
import com.cognizant.support.Step;
import com.galenframework.parser.SyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestRunner {

    public ArrayList<Step> stepList;
    CommandControl commandControl;
    Report Report;
    int count;
    String Scenario;
    String TestCase;
    ParameterControl param;
    public int flowRun = -1, subIteration = 1;

    public TestRunner(String scenario, String testcase, Report report,
            CommandControl cc, DataSource ds) {
        Report = report;
        Scenario = scenario;
        TestCase = testcase;
        param = new ParameterControl(scenario, testcase, ds);
        commandControl = cc;
    }

    public void setFlowRun(int count) {
        flowRun = count;

    }

    public synchronized boolean startRunning() throws UnCaughtException {
        try {
            stepList = DataAccess.getStepList(Scenario, TestCase);
            if (!stepList.isEmpty()) {
                param.initData();
                commandControl.userData = new UserDataAccess();
                commandControl.userData.setParamControl(param);
                commandControl.userData.setRunner(this);
                executeSteps();
            }
            return true;
        } catch (UnCaughtException fEx) {
            Logger.getLogger(TestRunner.class.getName()).log(Level.SEVERE, null, fEx);
            if (!LoadSettings.getSettings().isContinueOnError()) {
                throw fEx;
            } else {
                return false;
            }
        }

    }

    private void executeSteps() throws UnCaughtException {
        SystemDefaults.stopCurrentIteration.set(false);
        count = 0;
        Stepset stepSet = getStepset();
        setsubiteration(0, 0);
        runSteps(stepSet);
    }

    Stepset getStepset() throws UnCaughtException {
        Stepset stepSet = new Stepset();
        ArrayList<Object> steps = new ArrayList<>();
        Object x;
        for (; count < stepList.size();) {
            Step currStep = stepList.get(count++);
            x = currStep;
            if (isStartParamRLoop(currStep.Condition)) {
                x = getStepsss(x);
            } else if (isEndParam(currStep.Condition)) {
                steps.add(currStep);
                stepSet.times = getCount(currStep.Condition);
                stepSet.type = Condition.param;
                break;
            } else if (isEndLoop(currStep.Condition)) {
                steps.add(currStep);
                stepSet.times = getCount(currStep.Condition);
                stepSet.type = Condition.loop;
                break;
            }
            steps.add(x);
        }
        stepSet.steps = steps;
        return stepSet;
    }

    Stepset getStepsss(Object x) throws UnCaughtException {
        Stepset idk = getStepset();
        idk.steps.add(0, x);
        return idk;
    }

    public Boolean isStartParamRLoop(String value) {
        return checkAndReturn(value, "Start (Param|Loop)");
    }

    public Boolean isStartParam(String value) {
        return checkAndReturn(value, "Start Param");
    }

    public Boolean isEndParam(String value) {
        return checkAndReturn(value, "End Param.*");
    }

    public Boolean isStartLoop(String value) {
        return checkAndReturn(value, "Start Loop");
    }

    public Boolean isEndLoop(String value) {
        return checkAndReturn(value, "End Loop.*");
    }

    public Boolean checkAndReturn(String value, String match) {
        return value != null && value.matches(match);
    }

    public int getCount(String value) throws UnCaughtException {
        if (value.matches("End (Param|Loop):@\\d+")) {
            return Integer.parseInt(value.replaceFirst("End (Param|Loop):@", ""));
        } else if (value.matches("End (Param|Loop):\\w+:\\w+")) {
            return Integer.parseInt(param.get(value.replaceFirst("End (Param|Loop):", ""), 0));
        } else if (value.equals("End Param")) {
            return param.maxIteration();
        } else {
            return 1;
        }
    }

    protected void runSteps(Stepset stepSet) throws UnCaughtException {
        for (int i = 0; i < stepSet.times && !SystemDefaults.stopCurrentIteration.get() && !SystemDefaults.stopExecution.get(); i++) {
            for (Object object : stepSet.steps) {
                if (stepSet.isParam()) {
                    setsubiteration(stepSet.times, i);
                }
                if (object instanceof Step) {

                    Step x = (Step) object;
                    try {
                        x.Data = param.get(x.Input, stepSet.isLoop() ? stepSet.position : subIteration);
                        runStep(x);
                    } catch (InputException ex) {
                        Logger.getLogger(TestRunner.class.getName()).log(Level.SEVERE, null, ex);
                        Report.updateTestLog(x.Action, x.Input + " - " + ex.getMessage(), Status.FAIL);
                    }
                } else if (object instanceof Stepset) {
                    Stepset newSet = (Stepset) object;
                    if (newSet.isLoop()) {
                        newSet.position = subIteration;
                    }
                    runSteps(newSet);
                }
            }
        }
    }

    public void setsubiteration(int times, int i) {
        subIteration = times > 0 ? i : flowRun != -1 ? flowRun : 0;
        setIteration();
    }

    private void setIteration() {
        System.out.println("-----------------(SubIteration: " + (subIteration + 1)
                + ")--------------");
        commandControl.userData.setIteration(param.getDataSource().Scenario,
                param.getDataSource().TestCase,
                Scenario, TestCase,
                param.getDataSource().CurrentTestCaseIteration, subIteration + 1);
    }

    public void runStep(Step curr) throws UnCaughtException {
        if (!SystemDefaults.stopCurrentIteration.get() && !SystemDefaults.stopExecution.get()) {
            curr.printStep();
            SystemDefaults.nextStepflag.set(true);
            if (curr.BreakPoint) {
                SystemDefaults.breakPointflag.set(true);
            }
            while (SystemDefaults.breakPointflag.get() && SystemDefaults.nextStepflag.get() && !SystemDefaults.stopExecution.get()) {
                SystemDefaults.pollWait();
            }

            switch (curr.ObjectName) {
                case "Execute":
                    executeTestCase(curr);
                    break;
                default:
                    commandControl.userData.setParamControl(param);
                    commandControl.userData.setRunner(this);
                    commandControl.sync(curr);
                    Annotation ann = new Annotation(commandControl);
                    ann.beforeStepExecution();
                    executeStep(curr);
                    ann.afterStepExecution();
                    break;
            }
        }
    }

    private void executeTestCase(Step curr) throws UnCaughtException {
        System.out.println("[Execute Command Detected]");
        String NewScenario;
        String NewTestCase;
        if (!curr.Action.contains(":")) {
            NewScenario = this.Scenario;
            NewTestCase = curr.Action;
        } else {
            String[] splitVals = curr.Action.split(":", 2);
            NewScenario = splitVals[0];
            NewTestCase = splitVals[1];
        }
        this.Report.startComponent(NewScenario + ":" + NewTestCase);
        TestRunner tr = new TestRunner(NewScenario, NewTestCase,
                this.Report, this.commandControl, param.getDataSource());
        if (curr.Input != null && curr.Input.length() > 0) {
            String x = curr.Input.substring(1, curr.Input.length());
            tr.setFlowRun(Integer.parseInt(x) - 1);
        } else {
            tr.setFlowRun(subIteration);
        }
        try {
            System.out.println("[Running:" + NewScenario + "-" + NewTestCase + "]");
            tr.startRunning();
        } catch (UnCaughtException ex) {
            throw ex;
        } finally {
            System.out.println("[Running:" + NewScenario + "-" + NewTestCase + "][Done]");
            this.Report.endComponent(NewScenario + ":" + NewTestCase);
        }

    }

    private void executeStep(Step curr) throws UnCaughtException {
        try {
            Report.updateCurrentStepInfo(curr);
            MethodExecutor.init();
            boolean found = MethodExecutor.executeMethod(curr.Action, commandControl);
            if (!found) {
                System.out.println("[ERROR][Could not find Action:" + curr.Action + "]");
                if (LoadSettings.getSettings().isContinueOnError()) {
                    Report.updateTestLog("Method Exception", "[Method Executor Error][Could not find Action:" + curr.Action + "]", Status.DEBUG);
                } else {
                    throw new UnCaughtException("Could not find Action: " + curr.Action);
                }
            }

        } catch (ForcedException | ElementException forEx) {
            if (curr.Condition != null && curr.Condition.equalsIgnoreCase("Continue")) {
                System.out.println("Continue Detected");
            } else {
                System.out.println("[Error in executing Action:" + curr.Action + "]");
                Report.updateTestLog(curr.Action, curr.ObjectName + " - " + forEx.getMessage(), Status.FAIL);
                if (!LoadSettings.getSettings().isContinueOnError()) {
                    System.out.println("Force closing Execution as BreakOnError is specified");
                    throw new UnCaughtException(curr.Action, forEx.getMessage());
                }
            }
        } catch (SyntaxException ex) {
            Logger.getLogger(TestRunner.class.getName()).log(Level.SEVERE, null, ex);
            Report.updateTestLog(curr.Action, "Check the Galen Input Syntax - " + ex.getMessage(), Status.DEBUG);
            if (!LoadSettings.getSettings().isContinueOnError()) {
                System.out.println("Force closing Execution as BreakOnError is specified");
                throw new UnCaughtException(curr.Action, ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.getLogger(TestRunner.class.getName()).log(Level.SEVERE, null, ex);
            Report.updateTestLog(curr.Action, ex.getMessage(), Status.FAIL);
            if (!LoadSettings.getSettings().isContinueOnError()) {
                throw new UnCaughtException(curr.Action, ex.getMessage());
            }
        } catch (Throwable ex) {
            Logger.getLogger(TestRunner.class.getName()).log(Level.SEVERE, null, ex);
            Report.updateTestLog(curr.Action, ex.getMessage(), Status.FAIL);
            if (!LoadSettings.getSettings().isContinueOnError()) {
                throw new UnCaughtException(curr.Action, ex.getMessage());
            }
        }
    }
}

class Stepset {

    ArrayList<Object> steps;
    int times = 1;
    String type = Condition.normal;
    int position = 0;

    public Boolean isLoop() {
        return type.equals(Condition.loop);
    }

    public Boolean isParam() {
        return type.equals(Condition.param);
    }
}

class Condition {

    public static final String param = "param";
    public static final String loop = "loop";
    public static final String normal = "normal";

}
