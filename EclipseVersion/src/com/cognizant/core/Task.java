/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.core;

import com.cognizant.constants.SystemDefaults;
import com.cognizant.data.LoadSettings;
import com.cognizant.data.UserDataAccess;
import com.cognizant.drivers.SeleniumDriver;
import com.cognizant.reporting.DateTimeUtils;
import com.cognizant.reporting.TestCaseReport;
import com.cognizant.support.DataSource;
import com.cognizant.support.UnCaughtException;
import com.cognizant.support.Status;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Task implements Runnable {

    TestCaseReport report;
    RunContext runContext;
    SeleniumDriver seleniumDriver;
    DateTimeUtils runTime;
    UserDataAccess userData;

    public Task(RunContext RC) {
        runContext = RC;
    }

    @Override
    public void run() {
        runTime = new DateTimeUtils();
        report = new TestCaseReport();
        report.createReport(runContext, DateTimeUtils.DateTimeNow());
        DataSource dS = new DataSource(runContext.Scenario, runContext.TestCase, runContext.Iteration);

        while (dS.nextTestCaseItertation() && !SystemDefaults.stopExecution.get()) {
            try {
                System.out.println("Running Iteration " + dS.CurrentTestCaseIteration);
                runIteration(dS);
            } catch (Exception ex) {
                Logger.getLogger(Task.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (report != null) {
            Status s = report.finalizeReport();
            Control.ReportManager.updateTestCaseResults(runContext, report, s, runTime.timeRun());
            Control.ReportManager.updateResults();
            SystemDefaults.reportComplete.set(false);
        }

    }

    public boolean runIteration(DataSource dS) {
        boolean success = true;
        if (!LoadSettings.getSettings().useExistingDriver()
                || LoadSettings.getSettings().getSeDriver() == null) {
            seleniumDriver = new SeleniumDriver();
            LoadSettings.getSettings().setSeDriver(seleniumDriver);
        } else {
            seleniumDriver = LoadSettings.getSettings().getSeDriver();
        }
        try {
            SystemDefaults.reportComplete.set(true);
            report.startIteration(dS.CurrentTestCaseIteration);
            if (!LoadSettings.getSettings().useExistingDriver() || seleniumDriver.driver == null) {
                seleniumDriver.launchDriver(runContext);
            }
            report.setDriver(seleniumDriver);
            SystemDefaults.stopCurrentIteration.set(false);
            CommandControl commandControl = new CommandControl(seleniumDriver, report);
            TestRunner runner = new TestRunner(runContext.Scenario, runContext.TestCase, report, commandControl, dS);
            runner.startRunning();
        } catch (UnCaughtException fEx) {
            success = false;
        } catch (Exception ex) {
            if (report != null) {
                report.updateTestLog("Unhandled Exception", ex.getMessage(), Status.FAIL);
                Logger.getLogger(Task.class.getName()).log(Level.SEVERE, null, ex);
            }
            success = false;
        } finally {
            if (seleniumDriver != null
                    && !LoadSettings.getSettings().useExistingDriver()) {
                try {
                    seleniumDriver.closeBrowser();
                } catch (Exception ex) {
                    System.out.println("Driver Closed Unexpectedly");
                    report.updateTestLog("Couldn't Close Browser ", ex.getMessage(), Status.DEBUG);
                    Logger.getLogger(Task.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            report.endIteration(dS.CurrentTestCaseIteration);
        }

        return success;

    }

}
