/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.core;

import com.cognizant.constants.FilePath;
import com.cognizant.constants.SystemDefaults;
import com.cognizant.data.DataAccess;
import com.cognizant.data.LoadSettings;
import com.cognizant.data.RunManager;
import com.cognizant.data.TMIntegration;
import com.cognizant.drivers.AutomationObject;
import com.cognizant.drivers.CustomWebDriver;
import com.cognizant.reporting.ConsoleReport;
import com.cognizant.reporting.DateTimeUtils;
import com.cognizant.reporting.SummaryReport;
import com.cognizant.support.SoapConnector;
import com.cognizant.support.Status;
import com.cognizant.support.ThreadPool;
import java.util.Date;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Control {

    Queue<RunContext> runQ;
    public static SummaryReport ReportManager;
    public Boolean executionFinished = false;

    public static void main(String[] Args) {
        call();
        SoapConnector.killIt();
    }

    public static void call() {
        Control control = new Control();
        control.startRun();
        control.resetAll();
    }

    public static void cleanup() {
        SoapConnector.killIt();
    }

    void resetAll() {
        SystemDefaults.resetAll();
        com.cognizant.support.Discovery.Done = false;
        ReportManager = null;
        runQ = null;
    }

    public void startRun() {

        try {

            executionFinished = false;
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    if (!executionFinished) {
                        endExecution();
                    }
                    SoapConnector.killIt();
                }
            });
            SystemDefaults.printSystemInfo();
            System.out.println("Initializing Settings");
            SoapConnector.init();
            RunManager.init();
            FilePath.initDateTime();
            ConsoleReport.init();
            System.out.println("Run Started on " + new Date().toString());
            System.out.println("Loading Global Data");
            DataAccess.initializeGlobalData();
            System.out.println("Loading Objects");
            AutomationObject.updateObjectMap();
            System.out.println("Loading Browser Profile");
            CustomWebDriver.Load();
            System.out.println("Loading RunManager");
            runQ = RunManager.loadRunManager();
            ReportManager = new SummaryReport();
            System.out.println("Initializing Report");
            TMIntegration.init(ReportManager);
            ReportManager.createReport(DateTimeUtils.DateTimeNow());
            ThreadPool threadPool = new ThreadPool(
                    LoadSettings.getSettings().getThreadCount(),
                    LoadSettings.getSettings().getExecTimeOut(),
                    LoadSettings.getSettings().isGridExecution());
            while (!runQ.isEmpty() && !SystemDefaults.stopExecution.get()) {
                Task t = null;
                try {
                    RunContext currentContext = runQ.remove();
                    t = new Task(currentContext);
                    threadPool.execute(t, currentContext.Browser);
                } catch (Exception ex) {
                    Logger.getLogger(Control.class.getName()).log(Level.SEVERE,
                            null, ex);
                    if (t != null) {
                        t.seleniumDriver.closeBrowser();
                    }
                }
            }
            threadPool.shutdownExecution();

            if (threadPool.awaitTermination(LoadSettings.getSettings().getExecTimeOut(), TimeUnit.MINUTES)) {
            } else {
                Logger.getLogger(Control.class.getName()).log(Level.SEVERE, "Execution stopped due to Timeout [{0}]", LoadSettings.getSettings().getExecTimeOut());
                threadPool.shutdownNow();
                SystemDefaults.stopExecution.set(true);
            }

        } catch (Exception ex) {
            Logger.getLogger(Control.class.getName()).log(Level.SEVERE,
                    null, ex);
            if (ReportManager != null) {
                SystemDefaults.reportComplete.set(false);
                ReportManager.updateTestCaseResults("[Unknown Error]", "---",
                        ex.getMessage(), "", "Unknown", "Unknown", Status.FAIL,
                        "");
            }
        } finally {
            while (SystemDefaults.reportComplete.get()) {
                SystemDefaults.pollWait();
            }
            endExecution();
        }

    }

    private void endExecution() {
        executionFinished = true;
        try {
            if (ReportManager != null) {
                ReportManager.finalizeReport();
                if (SystemDefaults.canLaunchSummary()) {
                    ReportManager.launchResultSummary();
                }
                if (ReportManager.sync != null) {
                    ReportManager.sync.disConnect();
                }
            }
            if (LoadSettings.getSettings().getSeDriver() != null) {
                LoadSettings.getSettings().getSeDriver().closeBrowser();
            }

        } catch (Exception ex) {
            Logger.getLogger(Control.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        System.out.println("Run Finished on " + new Date().toString());
        ConsoleReport.reset();
    }

}
