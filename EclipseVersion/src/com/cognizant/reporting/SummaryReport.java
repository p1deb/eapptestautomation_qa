/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import com.cognizant.constants.FilePath;
import com.cognizant.core.RunContext;
import com.cognizant.data.LoadSettings;
import com.cognizant.reporting.performance.PerformanceReport;
import com.cognizant.reporting.sync.Sync;
import com.cognizant.settings.SettingsProperties;
import com.cognizant.support.Status;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class SummaryReport implements OverviewReport {

    JSONObject testSetData = new JSONObject();
    JSONArray executions = new JSONArray();
    public boolean RunComplete = false;
    int FailedTestCases = 0;
    int PassedTestCases = 0;
    DateTimeUtils RunTime;
    public Sync sync;
    public PerformanceReport perf;

    public SummaryReport() {
        if (LoadSettings.getSettings().canReportPerformanceLog()) {
            perf = new PerformanceReport();
        }
    }

    /**
     * initialize the report data file.
     *
     * @param runTime
     */
    @Override
    public synchronized void createReport(String runTime) {

        try {
            ReportUtils.loadDefaultTheme(testSetData);
            RunTime = new DateTimeUtils();
            new File(FilePath.getCurrentResultsPath()).mkdirs();
            LoadSettings.getSettings().getExecSettings().getProperty(SettingsProperties.ExecProperties.ProjectName);
            testSetData.put(RDS.TestSet.PROJECT_NAME,
                    LoadSettings.getSettings().getExecSettings().getProperty(SettingsProperties.ExecProperties.ProjectName));
            testSetData.put(RDS.TestSet.RELEASE_NAME,
                    LoadSettings.getSettings().getExecSettings().getProperty(SettingsProperties.ExecProperties.Release));
            testSetData.put(RDS.TestSet.TESTSET_NAME,
                    LoadSettings.getSettings().getExecSettings().getProperty(SettingsProperties.ExecProperties.TestSet));
            testSetData.put(RDS.TestSet.ITERATION_MODE,
                    LoadSettings.getSettings().getIterationMode());
            testSetData.put(RDS.TestSet.RUN_CONFIG,
                    LoadSettings.getSettings().getExecutionMode());
            testSetData.put(RDS.TestSet.MAX_THREADS,
                    LoadSettings.getSettings().getThreadCount());
            testSetData.put(RDS.TestSet.START_TIME, runTime);
            testSetData.put(RDS.TestSet.TEST_RUN, LoadSettings.getSettings().isTestRun());
        } catch (Exception ex) {
            Logger.getLogger(SummaryReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * update the result of each test case result
     *
     * @param runContext
     * @param report
     * @param state
     * @param executionTime
     */
    public synchronized void updateTestCaseResults(RunContext runContext, TestCaseReport report, Status state, String executionTime) {

        executions.add(report.testCaseData);
        String fileName = report.ReportFile.getName();
        String status;
        if (state.equals(Status.PASS)) {
            ReportToDataBase.WriteReport("Passed", runContext);
            status = "Passed";
            PassedTestCases++;
        } else {
            ReportToDataBase.WriteReport("Failed", runContext);
            FailedTestCases++;
            status = "Failed";
        }
        if (!LoadSettings.getSettings().isTestRun()) {
            if (!LoadSettings.getSettings().getTmTestSetSettings().getProperty(SettingsProperties.TMProperties.UpdateResultsToTM, "None").equals("None")) {
                if (sync != null && sync.isConnected()) {
                    System.out.println("[UPLOADING RESULTS TO TEST MANAGEMENT MODULE!!!]");
                    TestCase tc = new TestCase(runContext.Scenario, runContext.TestCase,
                            runContext.Description, runContext.Iteration, executionTime,
                            FilePath.getDate(), FilePath.getTime(),
                            runContext.BrowserName, runContext.BrowserVersion, runContext.PlatformValue);
                    List<File> attach = new ArrayList<>();
                    attach.add(new File(FilePath.getCurrentResultsPath(), fileName));
                    String prefix = tc.testScenario + "_" + tc.testCase + "_Step-";
                    File imgFolder = new File(FilePath.getCurrentResultsPath() + File.separator + "img");
                    if (imgFolder.exists()) {
                        for (File image : imgFolder.listFiles()) {
                            if (image.getName().startsWith(prefix)) {
                                attach.add(image);
                            }
                        }
                    }
                    if (!sync.updateResults(tc, status, attach)) {
                        report.updateTestLog(sync.getModule(), "Unable to Update Results to " + sync.getModule(), Status.DEBUG);
                    }
                } else {
                    System.out.println("[ERROR:UNABLE TO REACH TEST MANAGEMENT MODULE!!!]");
                    report.updateTestLog(sync.getModule(), "Unable to Connect to " + sync.getModule(), Status.DEBUG);
                }
            }
        }
        if (LoadSettings.getSettings().canReportPerformanceLog()) {
            perf.updateTestCase(report.Scenario, report.TestCase);
        }
    }

    /**
     * update the test set details to the json data file and write the data file
     */
    public synchronized void updateResults() {
        String exeTime = RunTime.timeRun();
        String endTime = DateTimeUtils.DateTimeNow();

        try {
            if (RunComplete) {
                testSetData.put(RDS.TestSet.EXECUTIONS, executions);
                testSetData.put(RDS.TestSet.END_TIME, endTime);
                testSetData.put(RDS.TestSet.EXE_TIME, exeTime);
                testSetData.put(RDS.TestSet.NO_OF_FAIL_TESTS, String.valueOf(FailedTestCases));
                testSetData.put(RDS.TestSet.NO_OF_PASS_TESTS, String.valueOf(PassedTestCases));
                testSetData.put(RDS.TestSet.NO_OF_TESTS, String.valueOf(FailedTestCases + PassedTestCases));
                RDS.writeToFile(FilePath.getCurrentReportDataPath(), testSetData);
            } else {

            }
        } catch (Exception ex) {
            Logger.getLogger(SummaryReport.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * finalize the summary report creation
     */
    public synchronized void finalizeReport() {
        RunComplete = true;
        updateResults();
        afterReportComplete();
        if (!LoadSettings.getSettings().isTestRun()) {
            updateReportHistoryData();
        }
        try {
            File Save = new File(FilePath.getSpritzPath() + File.separatorChar
                    + "GeneratedReport.txt");
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(Save));) {
                bw.write(FilePath.getCurrentResultsPath());
            }
            FileUtils.copyFileToDirectory(new File(FilePath.getSummaryHTMLPath()),
                    new File(FilePath.getCurrentResultsPath()));
            FileUtils.copyFileToDirectory(new File(FilePath.getDetailedHTMLPath()),
                    new File(FilePath.getCurrentResultsPath()));
            if (LoadSettings.getSettings().canReportPerformanceLog()) {
                perf.exportReport();
                FileUtils.copyFileToDirectory(new File(FilePath.getPerfReportHTMLPath()),
                        new File(FilePath.getCurrentResultsPath()));
            }
        } catch (IOException ex) {

            Logger.getLogger(SummaryReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        printReport();
    }

    /**
     * open the summary report when execution is finished
     */
    public synchronized void launchResultSummary() {
        com.cognizant.support.DesktopApi.open(new File(FilePath.getCurrentSummaryHTMLPath()));
    }

    /**
     * updates the history of execution report
     */
    private void updateReportHistoryData() {
        File file = new File(FilePath.getCurrentReportHistoryDataPath());
        ObjectMapper objectMapper = new ObjectMapper();
        String name = "var reportName=\""
                + LoadSettings.getSettings().getExecSettings().getProperty(SettingsProperties.ExecProperties.Release)
                + ":"
                + LoadSettings.getSettings().getExecSettings().getProperty(SettingsProperties.ExecProperties.TestSet) + "\";";
        String varaible = "var dataSet=";
        ArrayList<Map<String, String>> reportlist = new ArrayList<>();
        try {
            FileUtils.copyFileToDirectory(new File(FilePath.getReportHistoryHTMLPath()), new File(FilePath.getCurrentResultsLocation()));
            if (file.exists()) {
                String value = FileUtils.readFileToString(file);
                value = value.replace(name, "").replace(varaible, "");
                reportlist = objectMapper.readValue(value, ArrayList.class);
            } else {
                file.createNewFile();
            }
            reportlist.add(getReportData());
            String jsonVal = objectMapper.writeValueAsString(reportlist);
            FileUtils.writeStringToFile(file, name + varaible + jsonVal);
        } catch (IOException ex) {
            Logger.getLogger(SummaryReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @return the test set result details
     */
    private Map<String, String> getReportData() {
        Map<String, String> reportMap = new HashMap<>();
        reportMap.put("ExecutionDate", FilePath.getDate() + " " + FilePath.getTime());
        reportMap.put("ExecTC", String.valueOf(PassedTestCases + FailedTestCases));
        reportMap.put("PassTC", String.valueOf(PassedTestCases));
        reportMap.put("FailTC", String.valueOf(FailedTestCases));
        reportMap.put("ExecTime", RunTime.timeRun());
        reportMap.put("ReportPath", FilePath.getCurrentSummaryHTMLPathRelative());
        return reportMap;
    }

    private void printReport() {
        System.out.println("-----------------------------------------------------");
        System.out.println("ExecutionDate - " + FilePath.getDate() + " " + FilePath.getTime());
        System.out.println("Executed TestCases - " + String.valueOf(PassedTestCases + FailedTestCases));
        System.out.println("Passed TestCases - " + String.valueOf(PassedTestCases));
        System.out.println("Failed TestCases - " + String.valueOf(FailedTestCases));
        System.out.println("Time Taken - " + RunTime.timeRun());
        System.out.println("ReportPath - " + "file:///" + FilePath.getCurrentSummaryHTMLPath());
        System.out.println("-----------------------------------------------------");
    }

    /**
     * update the result when any error in execution
     *
     * @param testScenario
     * @param testCase
     * @param Iteration
     * @param testDescription
     * @param executionTime
     * @param fileName
     * @param state
     * @param Browser
     */
    @Override
    public void updateTestCaseResults(String testScenario, String testCase,
            String Iteration, String testDescription, String executionTime,
            String fileName, Status state, String Browser) {

        System.out.println("--------------->[UPDATING SUMMARY]");
        if (state.equals(Status.PASS)) {
            PassedTestCases++;
        } else {
            FailedTestCases++;
        }
    }

    private void afterReportComplete() {
        if (FailedTestCases > 0) {
            try {
//                System.out.println("Failed");
            } catch (Exception ex) {
                Logger.getLogger(SummaryReport.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }
        } else {
            try {
//                System.out.println("Passed");
            } catch (Exception ex) {
                Logger.getLogger(SummaryReport.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }
        }
    }

}
