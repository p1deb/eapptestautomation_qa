/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.sync.jira;

import com.cognizant.support.DLogger;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@SuppressWarnings("deprecation")
public class BasicHttpClient {

    private final String userName;
    private final String password;
    public URL url;

    UsernamePasswordCredentials creds;
    CloseableHttpClient client;
    /**
     * false - if the server has untrusted SSL (accept all cert) true - for
     * default system keystore
     */
    boolean trusted = false;

    public BasicHttpClient(URL urL, String uN, String pW) {
        url = urL;
        this.password = pW;
        this.userName = uN;
        creds = new UsernamePasswordCredentials(this.userName, this.password);
        client = trusted ? getClient() : getCustomClient();
    }

    /**
     * returns systen Def client
     *
     * @return
     */
    CloseableHttpClient getClient() {
        return HttpClients.createSystem();
    }

    /**
     * custom http client for server with SSL errors
     *
     * @return
     */
    CloseableHttpClient getCustomClient() {
        try {
            HttpClientBuilder b = HttpClientBuilder.create();
            //  b.setDefaultCredentialsProvider(creds);
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                    return true;
                }
            }).build();
            b.setSslcontext(sslContext);
            HostnameVerifier hostnameVerifier = new NoopHostnameVerifier();

            SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", sslSocketFactory)
                    .build();

            PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            b.setConnectionManager(connMgr);
            return b.build();
        } catch (Exception ex) {
            Logger.getLogger(BasicHttpClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return getClient();
    }

    /**
     * Http Post request for uploading files
     *
     * @param targetUrl
     * @param toUplod
     * @return
     * @throws Exception
     */
    public JSONObject post(URL targetUrl, File toUplod) throws Exception {
        HttpPost httppost = new HttpPost(targetUrl.toURI());
        setHeader(httppost);
        httppost.addHeader(BasicScheme.authenticate(creds, "US-ASCII", false));
        final MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody("file", toUplod,
                ContentType.APPLICATION_OCTET_STREAM, toUplod.getName());
        final HttpEntity multipart = builder.build();
        httppost.setEntity(multipart);

        HttpResponse response = execute(httppost);
        return parseResponse(response);
    }

    /**
     * execute the given URI request
     *
     * @param req
     * @return
     * @throws Exception
     */
    public CloseableHttpResponse execute(HttpUriRequest req) throws Exception {
        DLogger.Log("Executing  :" + req.toString());
        return client.execute(req);
    }

    /**
     * custom header for respective client
     *
     * @param httppost
     */
    public void setHeader(HttpPost httppost) {

    }

    /**
     * Http Post request for given data as JSON string
     *
     * @param targetUrl
     * @param jsonStr
     * @return
     * @throws Exception
     */
    public JSONObject post(URL targetUrl, String jsonStr) throws Exception {
        HttpPost httppost = new HttpPost(targetUrl.toURI());
        setHeader(httppost);
        StringEntity input = new StringEntity(jsonStr);
        input.setContentType("application/json");

        httppost.addHeader(BasicScheme.authenticate(creds, "US-ASCII", false));
        httppost.addHeader("accept", "application/json");
        httppost.setEntity(input);

        HttpResponse response = execute(httppost);
        return parseResponse(response);

    }

    /**
     * Http Get request for given url
     *
     * @param targetUrl
     * @return
     * @throws Exception
     */
    public JSONObject Get(URL targetUrl) throws Exception {
        return Get(targetUrl, "");
    }

    /**
     * Http Get request for given params as JSON string
     *
     * @param targetUrl
     * @param jsonStr
     * @return
     * @throws Exception
     */
    public JSONObject Get(URL targetUrl, String jsonStr) throws Exception {
        URIBuilder builder = new URIBuilder(targetUrl.toString());
        setParams(builder, jsonStr);
        HttpGet httpGet = new HttpGet(setParams(builder, jsonStr).build());
        httpGet.addHeader(BasicScheme.authenticate(creds, "US-ASCII", false));
        httpGet.addHeader("accept", "application/json");
        HttpResponse response = execute(httpGet);
        return parseResponse(response);
    }

    /**
     * convert the response stream as string
     *
     * @param is
     * @return
     */
    private static String convertStreamToString(InputStream is) {

        StringBuilder sb = new StringBuilder();

        String line;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is));) {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(BasicHttpClient.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sb.toString();
    }

    /**
     * Parse http response as JSON
     *
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private JSONObject parseResponse(HttpResponse response) throws Exception {
        @SuppressWarnings("unused")
        HeaderIterator HI = response.headerIterator();
        HttpEntity entity = response.getEntity();
        String resp = "";
        try {
            if (entity != null) {

                try (InputStream instream = entity.getContent()) {
                    resp = convertStreamToString(instream);
                }
                JSONParser parser = new JSONParser();
                Object data = parser.parse(resp);
                JSONObject jobj;
                if (data instanceof JSONObject) {
                    jobj = (JSONObject) data;
                } else {
                    jobj = new JSONObject();
                    jobj.put("array", (JSONArray) data);
                }

                EntityUtils.consume(entity);
                return jobj;
            } else {
                return null;
            }
        } catch (Exception ex) {
            DLogger.Log("Unknown Response : ", resp);
            Logger.getLogger(BasicHttpClient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Builds URL params from input JSON string
     *
     * @param builder
     * @param jsonStr
     * @return
     * @throws ParseException
     */
    private URIBuilder setParams(URIBuilder builder, String jsonStr) throws ParseException {

        if (jsonStr != null && !"".equals(jsonStr)) {
            try {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(jsonStr);

                for (Object Key : json.keySet()) {
                    builder.setParameter(Key.toString(), (String) json.get(Key));
                }

            } catch (Exception ex) {
                DLogger.LogE(ex.getMessage());
                Logger.getLogger(BasicHttpClient.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return builder;
    }

    /**
     * create url query string
     *
     * @deprecated
     * @param jsonStr
     * @return
     * @throws ParseException
     */
    @SuppressWarnings("unused")
    private String getParam(String jsonStr) throws ParseException {
        String q = "";
        if (jsonStr != null && !"".equals(jsonStr)) {
            try {
                q = "?";
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(jsonStr);

                for (Object Key : json.keySet()) {
                    Object v = json.get(Key);
                    if (v instanceof String) {
                        v = "'" + v + "'";
                    }
                    String param = Key.toString() + "=" + v;
                    q += param + "&";
                }

            } catch (Exception ex) {
                Logger.getLogger(BasicHttpClient.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return q;
    }

    /**
     * Set client proxy
     *
     * @deprecated as using system proxy settings feel free to customize
     * @param host
     * @param port
     * @param userName
     * @param password
     */
    public void setProxy(String host, int port, String userName, String password) {

    }
}
