/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.sync.jira;

import com.cognizant.data.LoadSettings;
import com.cognizant.reporting.TestCase;
import com.cognizant.reporting.sync.Sync;
import com.cognizant.settings.SettingsProperties;
import com.cognizant.support.DLogger;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

public class JIRASync implements Sync {

    static class JIRA {

        static String server = "http://00.00.00.00:8080";
        static String uname = "xyz";
        static String pass = "***";
    }

    JIRAClient conn;
    String project = "";

    public JIRASync() {
        conn = new JIRAClient(JIRA.server, JIRA.uname, JIRA.pass);
    }

    public JIRASync(String server, String uname, String pass, String project) {
        conn = new JIRAClient(server, uname, pass);
        this.project = project;
    }

    @Override
    public boolean isConnected() {
        try {
            JIRAHttpClient jc = new JIRAHttpClient(conn.url, conn.userName,
                    conn.password);
            return conn.isConnected(jc) && conn.containsProject(project, jc);
        } catch (Exception ex) {
            Logger.getLogger(JIRASync.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public synchronized boolean updateResults(TestCase tc, String status,
            List<File> attach) {
        try {
            JIRAHttpClient jc = new JIRAHttpClient(conn.url, conn.userName,
                    conn.password);
            String rls = LoadSettings.getSettings().getExecSettings()
                    .getProperty(SettingsProperties.ExecProperties.Release);
            String tset = LoadSettings.getSettings().getExecSettings()
                    .getProperty(SettingsProperties.ExecProperties.TestSet);

            int eid = conn.updateResult(getStatus("UE"), tc.testCase, tset,
                    rls, project, jc);
            if (eid > 0) {
                conn.updateResult(getStatus(status), eid, jc);
                for (File f : attach) {
                    String res = conn.addAttachment(eid, f, jc);
                    DLogger.Log(res);
                }
            } else {
                return false;
            }
            DLogger.LogE("Results updated for TestCase/Test [" + tc.testCase + "]");
            return true;
        } catch (Exception ex) {
            Logger.getLogger(JIRASync.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    @Override
    public synchronized String createIssue(JSONObject issue, List<File> attach) {
        String result = "[JIRA : Issue Creation Failed!!]\n";
        try {
            JIRAHttpClient jc = new JIRAHttpClient(conn.url, conn.userName,
                    conn.password);
            JSONObject res = conn.createIssue(jc, issue, attach);
            Object key = res.get("key");
            if (key != null) {
                result = "[JIRA : Issue " + key + " Created successfully!!]";
            }
        } catch (Exception ex) {
            Logger.getLogger(JIRASync.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getMessage();
        }

        return result;
    }

    private int getStatus(String status) {
        switch (status.toUpperCase()) {
            case "PASSED":
                return 1;
            case "FAILED":
                return 2;
            case "WIP":
                return 3;
            default:
                return -1;

        }

    }

    @Override
    public void disConnect() {
        conn = null;
    }

    @Override
    public String getModule() {
        return "JIRA";
    }

}
