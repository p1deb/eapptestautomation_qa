/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.sync.jira;

import com.cognizant.support.DLogger;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;

public class JIRAClient {
    
    String userName;
    String password;
    private String urlStr;
    public URL url;
    
    public static boolean proxyUse = false;
    public static String proxyHost = "";
    public static String proxyUserName = "";
    public static String proxyPassword = "";
    public static int proxyPort = 0;
    public Map<String, Map<Object, Integer>> tsets = new HashMap<>();
    public static String JIRAIssue = "/rest/api/latest/issue",
            JIRAIssueAttach = "/rest/api/latest/issue/issuekey/attachments";
    
    public static void setProxy(String host, int port, String userName,
            String password) {
        proxyHost = host;
        proxyUserName = userName;
        proxyPassword = decode(password);
        proxyPort = port;
        proxyUse = true;
    }
    
    String jsonStr = null;
    
    public JIRAClient(String url, String username, String password) {
        this.setUrl(url);
        this.userName = username;
        this.setPassword(password);
    }
    
    public void setUrl(String jiraUrl) {
        try {
            
            if (!jiraUrl.endsWith("/")) {
                jiraUrl = jiraUrl.concat("/");
            }
            
            URL main = new URL(jiraUrl);
            this.url = main;
            this.urlStr = main.toExternalForm();
            
        } catch (MalformedURLException ex) {
          Logger.getLogger(JIRAClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void setUsername(String username) {
        this.userName = username;
    }
    
    public void setPassword(String password) {
        this.password = decode(password);
    }

    /**
     * connect with ZAPIClient to execution get execution id and call the update
     * result
     *
     * @param status
     * @param tc
     * @param ts
     * @param rc
     * @param proj
     * @param httpclient
     * @return
     * @throws Exception
     * @see #updateResult(int, int,
     * com.cognizant.reporting.sync.jira.JIRAHttpClient)
     */
    public int updateResult(int status, String tc, String ts, String rc,
            String proj, JIRAHttpClient httpclient) throws Exception {
        DLogger.Log("Req EID with", "Testcase : ", tc, "TestSet :", ts,
                "Release :", rc, "Project :", proj);
        int eid = ZAPIClient.getExecutionID(tc, ts, rc, proj, httpclient);
        if (eid > 0) {
            updateResult(status, eid, httpclient);
        }
        return eid;
    }

    /**
     *
     * connect with ZAPIClient to update execution(test case) result
     *
     * @param status execution status
     * @param eid execution ID
     * @param httpclient
     * @throws Exception
     */
    public void updateResult(int status, int eid, JIRAHttpClient httpclient)
            throws Exception {
        ZAPIClient.updateResult(status, eid, httpclient);
    }

    /**
     * connect with ZAPIClient to update execution(test case) attachment (report
     * )
     *
     * @param eid execution ID
     * @param toattach file to upload
     * @param httpclient
     * @return
     */
    public String addAttachment(int eid, File toattach,
            JIRAHttpClient httpclient) {
        return ZAPIClient.addAttachment(eid, toattach, httpclient);
    }

    /**
     * upload the bug from given details (using JIRA rest api)
     *
     * @param client
     * @param issue issue details to upload
     * @param attchmns dependent attachments to upload(report)
     * @return
     */
    @SuppressWarnings("unchecked")
    public JSONObject createIssue(JIRAHttpClient client, JSONObject issue,
            List<File> attchmns) {
        JSONObject res = null;
        try {
            res = client
                    .post(new URL(client.url + JIRAIssue), issue.toString());
            String issyeKey = (String) res.get("id");
            String restAttach = JIRAIssueAttach.replace("issuekey", issyeKey);
            for (File f : attchmns) {
                res.put("Attachments",
                        client.post(new URL(client.url + restAttach), f));
            }
            
        } catch (Exception ex) {
           Logger.getLogger(JIRAClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    /**
     * create the json data for JIRA api from map
     *
     * @param dataMap
     * @return
     */
    @SuppressWarnings("unchecked")
    public static JSONObject getJsonified(LinkedHashMap<String, String> dataMap) {
        JSONObject fields = new JSONObject();
        
        JSONObject project = new JSONObject();
        project.put("key", dataMap.get("project"));
        dataMap.remove("project");
        
        JSONObject issueType = new JSONObject();
        issueType.put("name", dataMap.get("issueType"));
        dataMap.remove("issueType");
        
        fields.put("project", project);
        fields.put("issuetype", issueType);
        
        for (String key : dataMap.keySet()) {
            JSONObject obj;
            switch (key) {
                case "priority":
                    obj = new JSONObject();
                    obj.put("name", dataMap.get(key));
                    fields.put(key, obj);
                    break;
                case "assignee":
                    
                    obj = new JSONObject();
                    obj.put("name", dataMap.get(key));
                    fields.put(key, obj);
                    break;
                default:
                    fields.put(key, dataMap.get(key));
                    break;
            }
        }
        
        JSONObject data = new JSONObject();
        data.put("fields", fields);
        DLogger.Log(data);
        return data;
        
    }

    /**
     * check for project existence , used for test connection feature
     *
     * @param project
     * @param jc
     * @return
     */
    public boolean containsProject(String project, JIRAHttpClient jc) {
        
        try {
            String res = jc.Get(new URL(jc.url + "/rest/api/latest/project"))
                    .toString();
            return res.contains("\"key\":\"" + project + "\"")
                    || ZAPIClient.getProjID(project, jc) != -1;
            
        } catch (Exception ex) {
            Logger.getLogger(JIRAClient.class.getName()).log(Level.SEVERE, null, ex);
            return false;
            
        }
    }
    
    public boolean isConnected(JIRAHttpClient httpclient) {
        try {
            DLogger.Log(httpclient.Get(
                    new URL(httpclient.url + "/rest/api/latest/project"))
                    .toString());
            return true;
        } catch (Exception ex) {
            Logger.getLogger(JIRAClient.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public static String encode(String pass) {
        byte[] a = Base64.encodeBase64(pass.getBytes());
        String ans = "%".concat(new String(a)).concat("%");
        return ans;
        
    }
    
    private static String decode(String pass) {
        if (pass.startsWith("%") && pass.endsWith("%")) {
            String actual = pass.substring(1, pass.length() - 1);
            byte[] b = Base64.decodeBase64(actual.getBytes());
            return new String(b);
        } else {
            return pass;
        }
    }

    /**
     * Unit test functions with sample
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        DLogger.debug = true;
        JIRAClient jc = new JIRAClient(Data.server, Data.uname, Data.pass);
        jc.updateResult();
    }
    
    public String updateResult() throws Exception {
        String f = "D:/report.txt";
        JIRAHttpClient httpclient = new JIRAHttpClient(new URL(urlStr),
                this.userName, this.password);
        
        int eid = updateResult(ZAPIClient.status.UNEXECUTED, "buyProduct",
                "TestSet_Demo", "Release_Demo", "DemonProject", httpclient);
        this.updateResult(ZAPIClient.status.PASS, eid, httpclient);
        String out = addAttachment(eid, new File(f), httpclient);
        DLogger.Log(out);
        return "";
    }
    
    public JSONObject createIssue(JSONObject issue, List<File> atchmns) {
        JSONObject res = null;
        try {
            JIRAHttpClient httpclient = new JIRAHttpClient(
                    new URL(Data.server), this.userName, this.password);
            res = createIssue(httpclient, issue, atchmns);
        } catch (MalformedURLException ex) {
            Logger.getLogger(JIRAClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    public static class Data {
        
        public static String server = "https://jira.domain.com",
                uname = "user", pass = "pass", project = "STJZI";
        
        @SuppressWarnings("unchecked")
        public static JSONObject getIssue() {
            JSONObject fields = new JSONObject();
            
            JSONObject project = new JSONObject();
            project.put("key", Data.project);
            
            JSONObject issueType = new JSONObject();
            issueType.put("name", "Test");
            
            JSONObject priority = new JSONObject();
            priority.put("name", "Critical");
            fields.put("project", project);
            fields.put("summary", "api test ");
            fields.put("description", "desc");
            fields.put("issuetype", issueType);
            fields.put("priority", priority);
            fields.put("environment", "WIN 7 32 bit chrome 36");
            
            JSONObject data = new JSONObject();
            data.put("fields", fields);
            return data;
        }
        
    }
    
}
