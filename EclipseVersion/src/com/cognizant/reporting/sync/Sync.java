/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.sync;

import java.io.File;
import java.util.List;

import org.json.simple.JSONObject;

import com.cognizant.reporting.TestCase;

public interface Sync {

	public String getModule();
	
	public boolean isConnected();

	public boolean updateResults(TestCase TestCase, String status,
			List<File> attach);
	public String createIssue(JSONObject issue, List<File> attach) ;
	public void disConnect();
}
