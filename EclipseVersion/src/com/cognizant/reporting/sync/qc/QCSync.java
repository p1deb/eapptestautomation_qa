/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.sync.qc;

import com.cognizant.data.LoadSettings;
import com.cognizant.reporting.TestCase;
import com.cognizant.reporting.sync.Sync;
import com.cognizant.settings.SettingsProperties.RunProperties.QcDetails;
import com.cognizant.support.KeyMap;
import java.io.File;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.json.simple.JSONObject;
import qcupdation.ITDConnection4;
import qualitycenter.QualityCenter;

public class QCSync implements Sync {

    String url = "";
    String uName = "";
    String pass = "";
    String domain = "";
    String proj = "";
    public QualityCenter qc;
    public ITDConnection4 qcCon;

    public QCSync(String Url, String UserID, String Password, String Domain,
            String Project) {
        this.uName = UserID;
        this.url = Url;
        this.pass = Password;
        this.domain = Domain;
        this.proj = Project;
        qc = new QualityCenter();
        try {
            qcCon = qc.QClogin(Url, UserID, Password, Domain, Project);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(QCSync.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean isConnected() {
        try {
            return qcCon != null && qcCon.connected();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(QCSync.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public synchronized boolean updateResults(TestCase tc, String status,
            List<File> attach) {
        try {
            String tsPath = LoadSettings.getSettings().getRunSettings().getProperty(QcDetails.qcTestsetLocation);
            String tsName = LoadSettings.getSettings().getRunSettings().getProperty(QcDetails.qcTestsetName);
            Properties vMap = tc.getMap();
            vMap.putAll(LoadSettings.getSettings().getUserdefinedSettings().getProperties());
            tsPath = KeyMap.replaceKeys(tsPath, vMap);
            tsName = KeyMap.replaceKeys(tsName, vMap);
            if (qc.QCUpdateTCStatus(qcCon, tsPath, tsName, tc.testScenario,
                    tc.testCase, "[1]", tc.testCase + "@" + tc.date + "_"
                    + tc.time, status, attach)) {
                System.out.println("Result Updated  in QC-ALM !!!");
                return true;
            } else {
                System.out.println("Error while Updating Result in QC-ALM !!!");
            }
        } catch (Exception ex) {
            System.out.println("Error while Updating Result in QC-ALM !!!");
            java.util.logging.Logger.getLogger(QCSync.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public void disConnect() {
        qcCon.logout();
        qcCon.disconnect();
        qcCon.releaseConnection();
        qcCon.dispose();
    }

    @Override
    public String getModule() {
        return "QC";
    }

    @Override
    public String createIssue(JSONObject issue, List<File> attach) {
        // TODO Auto-generated method stub
        return null;
    }

}
