/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.sync;

import com.cognizant.constants.FilePath;
import com.cognizant.reporting.sync.jira.JIRAClient;
import com.cognizant.reporting.sync.jira.JIRASync;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;

public class DefectModules {

    public static String uploadDefect(String moduleId,
            LinkedHashMap<String, String> fields, List<File> attach) {
        String project = fields.get("project");
        String result = null;
        Sync sync = null;

        try {
            Data.initData();
            switch (moduleId) {
                case "JIRA":
                    sync = new JIRASync(Data.server, Data.uname, Data.pass, project);
                    if (sync.isConnected()) {
                        JSONObject data = JIRAClient.getJsonified(fields);
                        result = sync.createIssue(data, attach);
                    } else {
                        result = "Error: Unable to Connect to Server";
                    }
                    break;
                case "QC":
                    sync = null;
                    break;

            }
        } catch (Exception ex) {
            result = "Error: " + ex.getMessage();
            Logger.getLogger(DefectModules.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }
}

class Data {

    public static String server = "", uname = "", pass = "", project = "DEMO",
            domain = "";

    public static void initData() throws Exception {

        Properties p = new Properties();

        p.load(new FileReader(FilePath.getExplorerConfig()));
        server = p.getProperty("URL");
        checkServer(server);
        uname = getDecoded(p.getProperty("UserName"));
        pass = getDecoded(p.getProperty("Password"));
        domain = p.getProperty("Domain");
        project = p.getProperty("Project");

    }

    static String getDecoded(String val) {
        byte[] valueDecoded = Base64.decodeBase64(val);
        return new String(valueDecoded);
    }

    static void checkServer(String url) throws Exception {
        if (url == null || url.isEmpty()) {
            throw new Exception("Server URL is Empty!!");
        }
        new URI(url);
    }

}
