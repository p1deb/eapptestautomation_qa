/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import com.cognizant.constants.FilePath;
import com.cognizant.core.RunContext;
import com.cognizant.data.LoadSettings;
import com.cognizant.drivers.CustomWebDriver;
import com.cognizant.drivers.SeleniumDriver;
import com.cognizant.settings.SettingsProperties;
import com.cognizant.support.Status;
import com.cognizant.support.Step;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class TestCaseReport implements Report {

    JSONObject testCaseData = new JSONObject();
    JSONArray Steps = new JSONArray();
    JSONObject iteration;
    JSONObject reusable;
    String DATAF = "[<DATA>]";
    boolean isIteration = true;
    Stack<JSONObject> reusableStack = new Stack<>();
    public static volatile int TestCaseNumber;

    public String Scenario;
    public String TestCase;

    private final StringBuffer SourceDoc = new StringBuffer();
    public File ReportFile;
    public boolean RunComplete = false;

    String CurrentComponent = "";

    int ComponentCounter = 0;
    int IterationCounter = 0;

    int FailedSteps = 0;
    int PassedSteps = 0;
    int DoneSteps = 0;

    int StepNo = 0;

    DateTimeUtils startTime = new DateTimeUtils();
    SeleniumDriver seleniumdriver = null;
    String bv, platf;
    Step curr;

    public TestCaseReport() {

    }

    /**
     * sets the selenium driver
     *
     * @param driver
     */
    @Override
    public void setDriver(SeleniumDriver driver) {
        seleniumdriver = driver;
        testCaseData.put(RDS.TestCase.B_VERSION, seleniumdriver.getBrowserVersion());
        testCaseData.put(RDS.TestCase.PLATFORM, seleniumdriver.getPlatformName());
    }

    public SeleniumDriver getDriver() {
        return seleniumdriver;
    }

    public int getIter() {
        return IterationCounter;
    }

    public static synchronized int getTestCaseNumber() {
        return ++TestCaseNumber;
    }

    /**
     * initializes the test case report details
     *
     * @param runContext
     * @param runTime
     */
    public synchronized void createReport(RunContext runContext, String runTime) {
        try {
            this.Scenario = runContext.Scenario;
            this.TestCase = runContext.TestCase;
            int TCNo = getTestCaseNumber();
            ReportFile = new File(FilePath.getCurrentResultsPath()
                    + File.separatorChar + Scenario + "_" + TestCase + "_"
                    + TCNo + ".html");
            ReportFile.createNewFile();

            try (Scanner fileToRead = new Scanner(new File(FilePath.getReportHTMLPath()));) {
                for (String line; fileToRead.hasNextLine()
                        && (line = fileToRead.nextLine()) != null;) {
                    SourceDoc.append(line).append("\n");
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TestCaseReport.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (IOException ex) {
            Logger.getLogger(TestCaseReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        testCaseData.put(RDS.TestCase.SCENARIO_NAME, runContext.Scenario);
        testCaseData.put(RDS.TestCase.TESTCASE_NAME, runContext.TestCase);
        testCaseData.put(RDS.TestCase.DESCRIPTION, runContext.Description);
        testCaseData.put(RDS.TestCase.START_TIME, runTime);
        testCaseData.put(RDS.TestCase.ITERATION_TYPE, runContext.Iteration);
        testCaseData.put(RDS.TestCase.BROWSER, CustomWebDriver.getBrowserName(runContext));
        bv = runContext.BrowserVersion;
        platf = runContext.Platform.toString();
    }

    @Override
    public void updateTestLog(String stepName, String stepDescription, Status state) {
        updateTestLog(stepName, stepDescription, state, null, null);
    }

    @Override
    public void updateTestLog(String stepName, String stepDescription, Status state, String optionalLink) {
        updateTestLog(stepName, stepDescription, state, optionalLink, null);
    }

    /**
     * updates the step results to the test case json DATA
     *
     * @param stepName
     * @param stepDescription
     * @param state
     * @param optional
     */
    @Override
    public void updateTestLog(String stepName, String stepDescription, Status state, List<String> optional) {
        updateTestLog(stepName, stepDescription, state, null, optional);

    }

    private void updateTestLog(String stepName, String stepDescription, Status state, String optionalLink, List<String> optional) {
        currentStatus = state;
        StepNo++;
        System.out.println(String.format("|--| %s |--| %s |--|", stepDescription, state));
        System.out.println();
        for (int i = 0; i < 100; i++) {
            System.out.print("=");
        }
        System.out.println();
        String time = DateTimeUtils.DateTimeNow();
        JSONObject step = RDS.getNewStep(curr.Description);
        JSONObject data = (JSONObject) step.get(RDS.Step.DATA);
        data.put(RDS.Step.Data.STEP_NO, StepNo + "");
        data.put(RDS.Step.Data.STEP_NAME, stepName);
        data.put(RDS.Step.Data.ACTION, curr.Action);
        data.put(RDS.Step.Data.DESCRIPTION, resolveNull(stepDescription));
        data.put(RDS.Step.Data.TIME_STAMP, time);
        data.put(RDS.Step.Data.STATUS, state.toString());
        if (optionalLink != null) {
            data.put(RDS.Step.Data.LINK, optionalLink);
        }
        putStatus(state, optional, optionalLink, data);
        if (isIteration) {
            ((JSONArray) iteration.get(RDS.Step.DATA)).add(step);
        } else {
            ((JSONArray) reusable.get(RDS.Step.DATA)).add(step);
        }

    }

    private void putStatus(Status state, List<String> optional, String optionalLink, JSONObject data) {
        switch (state) {
            case DONE:
                DoneSteps++;
                onSetpDone();
                break;
            case PASS:
            case FAIL:
            case SCREENSHOT:
                takeScreenShot(state, optional, optionalLink, data);
                break;
            case DEBUG:
            case WARNING:
                FailedSteps++;
                onSetpFailed();
                break;

        }
    }

    private void takeScreenShot(Status status, List<String> optional, String optionalLink, JSONObject data) {
        String imgSrc = getScreenShotName();
        switch (status) {
            case PASS:
            case FAIL:
                if (!canTakeScreenShot(status)) {
                    break;
                }
                if (optionalLink != null) {
                    break;
                }
            case SCREENSHOT:
                takeSSAndPutDetail(data, optional, imgSrc);
                break;
            default:
                break;
        }
    }

    private Boolean canTakeScreenShot(Status status) {
        if (status.equals(Status.FAIL)) {
            FailedSteps++;
            onSetpFailed();
            return LoadSettings.getSettings().
                    getRunSettings().getProperty(SettingsProperties.RunProperties.ScreenShotFor, "Both").matches("(Fail|Both)");
        }
        if (status.equals(Status.PASS)) {
            PassedSteps++;
            onSetpPassed();
            return LoadSettings.getSettings().
                    getRunSettings().getProperty(SettingsProperties.RunProperties.ScreenShotFor, "Both").matches("(Pass|Both)");

        }
        return false;
    }

    /**
     * takes new screen shot and updates the the json object for that step
     *
     * @param data
     * @param imgSrc
     */
    private void takeSSAndPutDetail(JSONObject data, List<String> optional, String imgSrc) {
        if (optional != null && optional.size() == 3) {
            data.put(RDS.Step.Data.EXPECTED, optional.get(0));
            data.put(RDS.Step.Data.ACTUAL, optional.get(1));
            data.put(RDS.Step.Data.COMPARISION, optional.get(2));
        } else {
            if (optional != null) {
                data.put(RDS.Step.Data.OBJECTS, optional.get(0));
            }
            if (ReportUtils.takeScreenshot(seleniumdriver, imgSrc)) {
                data.put(RDS.Step.Data.LINK, imgSrc);
            }
        }

    }

    /**
     * updates the current step details and resolves DESCRIPTION if not
     * available
     *
     * @param curr
     *
     */
    @Override
    public void updateCurrentStepInfo(Step curr) {
        this.curr = curr;
        if (this.curr.Description == null || this.curr.Description.trim().isEmpty()) {
            this.curr.Description = ReportUtils.getDescription(curr.Action, curr.Data, curr.ObjectName, curr.Condition);
        }
    }

    /**
     * update the test case execution details to the json DATA file
     *
     * @return
     */
    public Status updateResults() {
        String endTime = DateTimeUtils.DateTimeNow();
        String exeTime = startTime.timeRun();
        testCaseData.put(RDS.TestCase.STEPS, Steps);
        testCaseData.put(RDS.TestCase.END_TIME, endTime);
        testCaseData.put(RDS.TestCase.EXE_TIME, exeTime);
        testCaseData.put(RDS.TestCase.ITERATIONS, IterationCounter + "");
        testCaseData.put(RDS.TestCase.NO_OF_TESTS, StepNo + "");
        testCaseData.put(RDS.TestCase.NO_OF_FAIL_TESTS, String.valueOf(this.FailedSteps));
        testCaseData.put(RDS.TestCase.NO_OF_PASS_TESTS, String.valueOf(this.DoneSteps + this.PassedSteps));
        Status stat = Status.PASS;
        testCaseData.put(RDS.TestCase.STATUS, "PASS");
        if (FailedSteps > 0 || (PassedSteps + DoneSteps) == 0) {
            stat = Status.FAIL;
            testCaseData.put(RDS.TestCase.STATUS, "FAIL");
        }
        return stat;
    }

    /**
     * finalize the test case execution and create standalone test case report
     * file for upload purpose
     *
     * @return
     */
    public Status finalizeReport() {
        RunComplete = true;
        Status stat = updateResults();
        try (BufferedWriter bufwriter = new BufferedWriter(new FileWriter(ReportFile));) {
            JSONObject singleTestcasereport = (JSONObject) testCaseData.clone();
            ReportUtils.loadDefaultTheme(singleTestcasereport);
            String tempDoc = SourceDoc.toString().replace(DATAF, singleTestcasereport.toJSONString(0));
            bufwriter.write(tempDoc);
        } catch (Exception ex) {
            Logger.getLogger(TestCaseReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        printReport();
        return stat;
    }

    private void printReport() {
        System.out.println("\n---------------------------------------------------");
        System.out.println("Testcase Name - " + testCaseData.get(RDS.TestCase.SCENARIO_NAME) + ":" + testCaseData.get(RDS.TestCase.TESTCASE_NAME));
        System.out.println("Executed Steps - " + testCaseData.get(RDS.TestCase.NO_OF_TESTS));
        System.out.println("Passed Steps - " + testCaseData.get(RDS.TestCase.NO_OF_PASS_TESTS));
        System.out.println("Failed Steps - " + testCaseData.get(RDS.TestCase.NO_OF_FAIL_TESTS));
        System.out.println("Time Taken - " + testCaseData.get(RDS.TestCase.EXE_TIME));
        System.out.println("-----------------------------------------------------\n");
    }

    /**
     *
     * @return the screen shot NAME for the current step
     */
    private String getScreenShotName() {
        String fileName = "/img/" + Scenario + "_" + TestCase + "_Step-" + StepNo + "_"
                + DateTimeUtils.TimeNowForFolder() + ".png";
        return fileName;
    }

    @Override
    public void createReport(String scenario, String testCase,
            String Iteration, String runTime, String initBrowser,
            String execMode) {
    }

    /**
     * creates new iteration object
     *
     * @param iterationNo
     */
    @Override
    public void startIteration(int iterationNo) {
        reusableStack.clear();
        ++IterationCounter;
        String Iterationid = "Iteration_" + IterationCounter;
        iteration = RDS.getNewIteration(Iterationid);
        isIteration = true;
    }

    /**
     * creates new reusable object
     *
     * @param component
     */
    @Override
    public void startComponent(String component) {
        reusable = RDS.getNewReusable(component);
        reusableStack.push(reusable);
        isIteration = false;
    }

    @Override
    public void endComponent(String string) {
        if (reusable.get(RDS.TestCase.STATUS).equals("")) {
            reusable.put(RDS.TestCase.STATUS, "FAIL");
        }
        reusableStack.pop();
        if (reusableStack.empty()) {
            ((JSONArray) iteration.get(RDS.Step.DATA)).add(reusable);
            reusable = null;
            isIteration = true;
        } else {
            ((JSONArray) reusableStack.peek().get(RDS.Step.DATA)).add(reusable);
            reusableStack.peek().put(RDS.TestCase.STATUS, reusable.get(RDS.TestCase.STATUS));
            reusable = reusableStack.peek();
        }

    }

    @Override
    public void endIteration(int CurrentTestCaseIteration) {
        if (iteration.get(RDS.TestCase.STATUS).equals("")) {
            iteration.put(RDS.TestCase.STATUS, "FAIL");
        }
        Steps.add(iteration);
    }

    private void onSetpDone() {
        if (reusable != null && reusable.get(RDS.TestCase.STATUS).equals("")) {
            reusable.put(RDS.TestCase.STATUS, "PASS");
        }
        if (iteration != null && iteration.get(RDS.TestCase.STATUS).equals("")) {
            iteration.put(RDS.TestCase.STATUS, "PASS");
        }
    }

    private void onSetpPassed() {
        if (reusable != null && reusable.get(RDS.TestCase.STATUS).equals("")) {
            reusable.put(RDS.TestCase.STATUS, "PASS");
        }
        if (iteration != null && iteration.get(RDS.TestCase.STATUS).equals("")) {
            iteration.put(RDS.TestCase.STATUS, "PASS");
        }
    }

    private void onSetpFailed() {
        if (iteration != null) {
            iteration.put(RDS.TestCase.STATUS, "FAIL");
        }
        if (reusable != null) {
            reusable.put(RDS.TestCase.STATUS, "FAIL");
        }
    }

    @Override
    public String getTestCaseName() {
        return TestCase;
    }

    @Override
    public String getScenarioName() {
        return Scenario;
    }

    String resolveNull(String val) {
        return val == null ? " UnCaught Exception " : val;
    }

    Status currentStatus;

    @Override
    public Status getCurrentStatus() {
        return currentStatus;
    }

    @Override
    public Boolean isStepPassed() {
        if (currentStatus != null) {
            return currentStatus.equals(Status.PASS) || currentStatus.equals(Status.DONE) || currentStatus.equals(Status.SCREENSHOT);
        }
        return false;
    }

}
