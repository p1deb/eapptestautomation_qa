/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import java.lang.reflect.Field;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestCase {

    public String testScenario = "";
    public String testCase = "";
    public String testDescription = "";
    public String iteration = "";
    public String executionTime = "";
    public String browser = "";
    public String browserVersion = "";
    public String platform = "";
    public String date = "";
    public String time = "";

    public TestCase(String ts, String tc, String tDesc, String iteration,
            String etime, String date, String time, String browser, String browserVersion, String platform) {
        this.testScenario = ts;
        this.testCase = tc;
        this.testDescription = tDesc;
        this.iteration = iteration;
        this.executionTime = etime;
        this.date = date;
        this.time = time;
        this.browser = browser;
        this.browserVersion = browserVersion;
        this.platform = platform;

    }

    public Properties getMap() {
        Properties map = new Properties();
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value;
            try {
                value = field.get(this);
                if (value != null) {
                    map.put(field.getName(), value.toString());
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(TestCase.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return map;
    }
}
