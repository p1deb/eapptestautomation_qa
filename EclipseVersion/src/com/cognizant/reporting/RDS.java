/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author 389747
 */
@SuppressWarnings("unchecked")
public class RDS {

    private final static String BEFORE = "var DATA=",AFTER=";", DATA = "[<DATA>]";

    public synchronized static JSONObject getNewStep(String name) {
        JSONObject step = new JSONObject();
        step.put(Step.TYPE, "step");
        step.put(Step.NAME, name == null ? "Description Not Given" : name);
        step.put(Step.DATA, new JSONObject());
        return step;
    }

    public synchronized static JSONObject getNewIteration(String name) {
        JSONObject iteration = new JSONObject();
        iteration.put(Step.TYPE, "iteration");
        iteration.put(Step.NAME, name);
        iteration.put(Step.DATA, new JSONArray());
        iteration.put(TestCase.STATUS, "");
        return iteration;
    }

    public synchronized static JSONObject getNewReusable(String name) {
        JSONObject reusable = new JSONObject();
        reusable.put(Step.TYPE, "reusable");
        reusable.put(Step.NAME, name);
        reusable.put(Step.DATA, new JSONArray());
        reusable.put(TestCase.STATUS, "");
        return reusable;
    }

    public synchronized static void writeToFile(String fileToWrite, JSONObject data) {        
        try (BufferedWriter bufwriter = new BufferedWriter(new FileWriter(fileToWrite))) {
            bufwriter.write(BEFORE);
            bufwriter.write(data.toString());
            bufwriter.write(AFTER);
        } catch (IOException ex) {
            Logger.getLogger(RDS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public class TestSet {

        public static final String PROJECT_NAME = "projectName";
        public static final String RELEASE_NAME = "releaseName";
        public static final String TESTSET_NAME = "testsetName";
        public static final String ITERATION_MODE = "iterationMode";
        public static final String RUN_CONFIG = "runConfiguration";
        public static final String MAX_THREADS = "maxThreads";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String EXE_TIME = "exeTime";
        public static final String NO_OF_TESTS = "noTests";
        public static final String NO_OF_PASS_TESTS = "nopassTests";
        public static final String NO_OF_FAIL_TESTS = "nofailTests";
        public static final String THEME = "theme";
        public static final String THEMES = "themes";
        public static final String TEST_RUN = "testRun";
        public static final String EXECUTIONS = "EXECUTIONS";
    }

    public class TestCase {

        public static final String SCENARIO_NAME = "scenarioName";
        public static final String TESTCASE_NAME = "testcaseName";
        public static final String DESCRIPTION = "description";
        public static final String ITERATIONS = "iterations";
        public static final String ITERATION_TYPE = "iterationType";
        public static final String PLATFORM = "platform";
        public static final String B_VERSION = "bversion";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String EXE_TIME = "exeTime";
        public static final String NO_OF_TESTS = "noTests";
        public static final String NO_OF_PASS_TESTS = "nopassTests";
        public static final String NO_OF_FAIL_TESTS = "nofailTests";
        public static final String BROWSER = "browser";
        public static final String STATUS = "status";
        public static final String STEPS = "STEPS";
    }

    public class Step {

        public static final String NAME = "name";
        public static final String TYPE = "type";
        public static final String DATA = "data";

        public class Data {

            public static final String STEP_NO = "stepno";
            public static final String STEP_NAME = "stepName";
            public static final String ACTION = "action";
            public static final String DESCRIPTION = "description";
            public static final String STATUS = "status";
            public static final String TIME_STAMP = "tStamp";
            public static final String LINK = "link";
            public static final String EXPECTED = "expected";
            public static final String ACTUAL = "actual";
            public static final String COMPARISION = "comparison";
            public static final String OBJECTS = "objects";
        }
    }

}
