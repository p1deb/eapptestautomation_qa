/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import com.cognizant.constants.FilePath;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 394173
 */
public class ConsoleReport {

    private static FileOutputStream fout, ferr;

    public static void init() {
        try {
            String loc = FilePath.getCurrentResultsPath() + File.separator + "console.txt";
            new File(FilePath.getCurrentResultsPath()).mkdirs();
            new File(loc).createNewFile();

            fout = new FileOutputStream(loc, true);
            ferr = new FileOutputStream(loc, true);

            MultiOutputStream multiOut = new MultiOutputStream(System.out, fout);
            MultiOutputStream multiErr = new MultiOutputStream(System.err, ferr);

            PrintStream stdout = new PrintStream(multiOut) {
                @Override
                public void println(String a) {
                    if (a == null || "null".equals(a)) {
                        Logger.getLogger(ConsoleReport.class.getName()).
                                log(Level.FINEST, "Redirecting 'null' string", (Object) null);
                    } else {
                        super.println(a);
                    }
                }
            };
            PrintStream stderr = new PrintStream(multiErr);

            System.setOut(stdout);
            System.setErr(stderr);

        } catch (Exception ex) {
            Logger.getLogger(ConsoleReport.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public static void reset() {
        System.setOut(System.out);
        System.setErr(System.err);
        try {
            fout.close();
            ferr.close();
        } catch (IOException ex) {
            Logger.getLogger(ConsoleReport.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
}

class MultiOutputStream extends OutputStream {

    OutputStream[] outputStreams;

    public MultiOutputStream(OutputStream... outputStreams) {
        this.outputStreams = outputStreams;
    }

    StringBuilder sb = new StringBuilder();

    @Override
    public void write(int b) throws IOException {
        if (b == '\r') {
            return;
        }
        if (b == '\n') {
            final String text = sb.toString() + "\n";
            if (!text.contains("Marionette") && !text.contains("addons")) {
                for (OutputStream outputStream : outputStreams) {
                    outputStream.write(text.getBytes());
                }
            }
            sb.setLength(0);
            return;
        }
        sb.append((char) b);

    }

    @Override
    public void flush() throws IOException {
        for (OutputStream out : outputStreams) {
            out.flush();
        }
    }

    @Override
    public void close() throws IOException {
        for (OutputStream out : outputStreams) {
            out.close();
        }
    }
}
