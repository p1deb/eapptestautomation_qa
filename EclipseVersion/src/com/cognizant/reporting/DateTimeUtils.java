/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import java.text.SimpleDateFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class DateTimeUtils {

    private static final String DATE_FORMAT_NOW = "dd-MMM-yyyy";

    private static final String TIME_FORMAT_NOW = "HH:mm:ss";

    private static final String DATE_FORMAT_FOR_FOLDER = "dd-MMM-yyyy";

    private static final String TIME_FORMAT_FOR_FOLDER = "HH-mm-ss";
    private static final String FORMAT = "%02d:%02d:%02d";
    public final long startTime;

    public DateTimeUtils() {
        startTime = System.currentTimeMillis();
    }

    public static String TimeNow() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    public static String DateNow() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    public static String DateNowForFolder() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_FOR_FOLDER);
        return sdf.format(cal.getTime());
    }

    public static String TimeNowForFolder() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT_FOR_FOLDER);
        return sdf.format(cal.getTime());
    }

    public static String DateTimeNow() {
        return DateTimeUtils.DateNow() + " " + DateTimeUtils.TimeNow();
    }

    public static String DateTimeNowForFolder() {
        return DateTimeUtils.DateNowForFolder() + " " + DateTimeUtils.TimeNowForFolder();
    }

    public String timeRun() {
        final long duration = System.currentTimeMillis() - startTime;
        return parseTime(duration);
    }

    public static String parseTime(long milliseconds) {
        return String.format(FORMAT,
                TimeUnit.MILLISECONDS.toHours(milliseconds),
                TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(
                        TimeUnit.MILLISECONDS.toHours(milliseconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
    }


}
