/*
 * © Cognizant Technology Solutions 2016. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.performance;

import com.cognizant.reporting.performance.har.MIME;

/**
 * Resource Timings api
 * @author 389747
 */
public class ResourceTimings {

    public Double connectEnd,
            connectStart,
            domainLookupEnd,
            domainLookupStart,
            duration,
            fetchStart,
            redirectEnd,
            redirectStart,
            requestStart,
            responseEnd,
            responseStart,
            secureConnectionStart,
            startTime;
    public String entryType,
            initiatorType,
            name;

    /**
     * initial adjustment makes sure timings in the desired order
     */
    public void adjust() {

        fetchStart = Math.max(fetchStart, startTime);
        connectStart = Math.max(fetchStart, connectStart);
        redirectStart = Math.max(redirectStart, startTime);
        responseEnd = Math.max(redirectStart, responseEnd);
        domainLookupStart = Math.max(domainLookupStart, fetchStart);
        domainLookupEnd = Math.max(domainLookupStart, domainLookupEnd);
        secureConnectionStart = Math.max(secureConnectionStart, connectStart);
        connectEnd = Math.max(secureConnectionStart, connectEnd);
        requestStart = Math.max(requestStart, connectEnd);
        responseStart = Math.max(requestStart, responseStart);

    }

    public String mimeType() {

        if ("script".equalsIgnoreCase(initiatorType) || name.endsWith(".js")) {
            return MIME.JS.val();
        } else if (this.name.endsWith(".css")) {
            return MIME.CSS.val();
        } else if (this.name.endsWith(".png")) {
            return MIME.PNG.val();
        } else if (this.name.endsWith(".html")) {
            return MIME.HTML.val();
        } else if ("image".equalsIgnoreCase(initiatorType)) {
            return "image/" + (name.contains(".") ? name.substring(name.lastIndexOf(".") + 1) : initiatorType);
        }
        return this.initiatorType;
    }
/**
 * java script to extract resource timings
 * @return 
 */
    public static String script() {
        return "var dmp=window.performance.getEntriesByType('resource');"
                + "var resources=[];"
                + "for(var r in dmp){"
                + "var resource={};"
                + "for(var k in dmp[r]){resource[k]=dmp[r][k];}"
                + "resource.toJSON=undefined;"
                + "resources.push(resource);}"
                + "return JSON.stringify(resources);";
    }

}
