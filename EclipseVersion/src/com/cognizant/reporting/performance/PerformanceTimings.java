/*
 * © Cognizant Technology Solutions 2016. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.performance;

/**
 *
 * @author 389747
 */
public class PerformanceTimings {

    public long connectEnd,
            connectStart,
            domComplete,
            domContentLoadedEventEnd,
            domContentLoadedEventStart,
            domInteractive,
            domLoading,
            domainLookupEnd,
            domainLookupStart,
            fetchStart,
            loadEventEnd,
            loadEventStart,
            navigationStart,
            redirectEnd,
            redirectStart,
            requestStart,
            responseEnd,
            responseStart,
            secureConnectionStart,
            unloadEventEnd,
            unloadEventStart;
    public String url, title;

    /**
     * convert page timings data to a resource data page request as a resource
     * entry
     *
     * @return ResourceTimings
     */
    public ResourceTimings toResourceTimings() {
        ResourceTimings rt = new ResourceTimings();
        rt.name = url;
        rt.initiatorType = "text/html";
        rt.entryType = "Page";
        rt.startTime = 0d;

        long start = this.navigationStart;
        rt.responseEnd = new Double(this.responseEnd - start);
        rt.responseStart = new Double(this.responseStart - start);
        rt.requestStart = new Double(this.requestStart - start);
        rt.connectEnd = new Double(this.connectEnd - start);
        rt.connectStart = new Double(this.connectStart - start);
        rt.secureConnectionStart = new Double(
                (this.secureConnectionStart == 0 ? 0 : this.secureConnectionStart - start));
        rt.domainLookupEnd = new Double(this.domainLookupEnd - start);
        rt.domainLookupStart = new Double(this.domainLookupStart - start);

        rt.redirectStart = new Double(
                (this.redirectStart == 0 ? 0 : this.redirectStart - start));
        rt.redirectEnd = new Double(
                (this.redirectEnd == 0 ? 0 : this.redirectEnd - start));

        rt.fetchStart = new Double(this.fetchStart - start);

        rt.duration = rt.responseEnd;

        return rt;
    }

    /**
     * java script to extract timings data.
     *
     * @return
     */
    public static String script() {
        return "  var pt=performance.timing;"
                + "var t={};"
                + "t.title=document.title;"
                + "t.url=window.location.href;"
                + "for(var k in pt){t[k]=pt[k];}"
                + "t.toJSON=undefined;"
                + "return JSON.stringify(t);";
    }
}
