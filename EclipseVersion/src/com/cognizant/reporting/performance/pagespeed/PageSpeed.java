/*
 * © Cognizant Technology Solutions 2016. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.performance.pagespeed;

import com.cognizant.support.DesktopApi;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * page speed metrics api
 *
 * @author 389747
 * @since 4.1.0+
 */
public class PageSpeed implements Callable<Object> {

    int score = 0;
    JSONArray insights;
    String url;
    File har, exe;

    public PageSpeed(String url, File har, File exe) {
        this.url = url;
        this.exe = exe;
        this.har = har;
    }

    @Override
    public Object call() throws Exception {
        if (har.exists() && DesktopApi.getOs().isWindows()) {
            try {
                insights = new JSONArray();
                System.out.print("|");
                parseHar(har, exe);
                return get();
            } catch (Exception ex) {
                Logger.getLogger(PageSpeed.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        JSONObject data = new JSONObject();
        data.put("name", url);
        data.put("score", insights.size() > 0 ? score / insights.size() : 0);
        data.put("logs", insights);
        return data.toJSONString(1);
    }

    /**
     * convert har data from file to page speed metrics using the executable and
     * parse it to insights data(JSON)
     *
     * @param har
     * @param exe
     */
    private void parseHar(File har, File exe) throws Exception {

        List<String> command = new ArrayList<>();
        command.add(exe.getAbsolutePath());
        command.add(har.getAbsolutePath());
        ProcessBuilder pb = new ProcessBuilder(command);
        Process p;
        p = pb.start();
        Thread.sleep(2000);
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        Pattern insightP = Pattern.compile("_(.*)_ \\(score=(0|[0-9][0-9]|100)\\)");
        Pattern descP = Pattern.compile("  (\\w+.*)");
        Pattern dataP = Pattern.compile("    \\* (\\w+.*)");
        Pattern includes = Pattern.compile("Combine external CSS|Combine external JavaScript");
        Insight in = null;
        score=-1;
        while ((line = reader.readLine()) != null) {
            //parse each line
            Matcher m = insightP.matcher(line);
            if (m.matches()) {

                int sc = -1;
                if (includes.matcher(m.group(1)).matches()) {
                    sc = Integer.parseInt(m.group(2));
                    //score += sc;
                }
                //if line matched insight then create new insight
                in = new Insight(m.group(1), sc);
                insights.add(in);
            } else if (in != null) {
                // if line matches description add desc to the insight
                m = descP.matcher(line);
                if (m.matches()) {
                    in.desc(m.group(1));
                } else {
                    m = dataP.matcher(line);
                    if (m.matches()) {
                        in.addDescData(m.group(1));
                    }
                }
            }
        }
    }

    public JSONObject get() {
        JSONObject data = new JSONObject();
        data.put("name", url);
        //calculated score of the hat
        data.put("score", insights.size() > 0 ? score / insights.size() : 0);
        data.put("logs", insights);
        return data;
    }

}
