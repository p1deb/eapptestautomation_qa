/*
 * © Cognizant Technology Solutions 2016. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.performance.pagespeed;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * page speed insights helper object
 * with validation and rules
 * @author 389747
 * @since 4.1.0+
 */
public class Insight extends JSONObject {

    private static final long serialVersionUID = 1L;

    public Insight(String name, int score) {
        put("name", name);
        put("score", score);
    }

    public void desc(String desc) {
        put("desc", desc);
    }

    public void addDescData(String data) {
        if (this.get("data") == null) {
            put("data", new JSONArray());
        }
        ((JSONArray) this.get("data")).add(parseData(data));

    }
/**
 * check the data toe url and comments if comments exists
 * add it as separate entry
 * @param dataS data to parse
 * @return data entry
 */
    public Object parseData(String dataS) {
        Matcher m = Pattern.compile("(.*) \\((.*)\\)").matcher(dataS);

        JSONObject data = new JSONObject();
        if (m.matches()) {
            data.put("url", m.group(1));
            data.put("comments", m.group(2));
        } else {
            data.put("url", dataS);
        }

        return data;
    }

}
