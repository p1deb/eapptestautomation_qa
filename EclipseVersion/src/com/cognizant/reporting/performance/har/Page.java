/*
 * © Cognizant Technology Solutions 2016. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.performance.har;

import com.cognizant.reporting.performance.PerformanceTimings;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Page helper object for har
 *
 * @author 389747
 * @since 4.1.0+
 * @see Har.java
 */
@SuppressWarnings("unchecked")
public class Page extends JSONObject {

    private static final long serialVersionUID = 1L;
    private static final String DF = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    PerformanceTimings pt;

    @SuppressWarnings("unchecked")
    public Page(String navTimings, int index) {
        super();
        Gson gson = new Gson();
        //parse string(json) result form timings api to #PerformanceTimings.class
        pt = gson.fromJson(navTimings, PerformanceTimings.class);
        put("startedDateTime", getMillstoDate(pt.navigationStart));
        put("id", "page_" + index);
        put("title", pt.url == null ? "" : pt.url);
        put("pageTimings", new PageTimings(pt));
        put("raw",JSONValue.parse(navTimings));        
    }

    public static String getMillstoDate(long nStart) {
        SimpleDateFormat df = new SimpleDateFormat(DF);
        return df.format(new Date(nStart));
    }

    public String getID() {
        return get("id").toString();
    }

    class PageTimings extends JSONObject {

        private static final long serialVersionUID = 1L;

        public PageTimings(PerformanceTimings pt) {
            put("onContentLoad", pt.domContentLoadedEventStart - pt.navigationStart);
            //get max of l.e.start || dom.Complete || dom.c.l.e.end
            put("onLoad", Math.max(pt.loadEventStart,Math.max(pt.domComplete, pt.domContentLoadedEventEnd))
                    - pt.navigationStart);
        }
    }
}
