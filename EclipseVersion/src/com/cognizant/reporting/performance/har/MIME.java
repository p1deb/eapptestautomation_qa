/*
 * © Cognizant Technology Solutions 2016. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.performance.har;

/**
 * MIME helper class for har
 *
 * @author 389747
 * @since 4.1.0+
 */
public enum MIME {

    HTML("text/html", ".html"),
    PNG("image/png", ".png"),
    JS("application/javascript", ".js"),
    CSS("text/css", ".css");
    /**
     * need to be added
     */
    public String val, ext;

    private MIME(String mime, String ext) {
        this.val = mime;
        this.ext = ext;
    }

    public String val() {
        return val;
    }

}
