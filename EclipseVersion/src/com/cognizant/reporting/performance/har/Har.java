/*
 * © Cognizant Technology Solutions 2016. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting.performance.har;

import com.cognizant.reporting.TestCaseReport;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * har helper api for json storage
 *
 * @author 389747
 * @param <String>
 * @param <Log>
 * @since 4.1.0+
 * @see https://dvcs.w3.org/hg/webperf/raw-file/tip/specs/HAR/Overview.html
 */
@SuppressWarnings("unchecked")
public class Har<String, Log> extends JSONObject {

    private static final long serialVersionUID = 1L;
    private final com.cognizant.reporting.performance.har.Har.Log log;
    public Map<Object, Object> raw;

    public Har() {
        log = new com.cognizant.reporting.performance.har.Har.Log();
        put("log", log);
        raw = new LinkedHashMap<>();
    }

    public void addRaw(java.lang.String pt, java.lang.String rt) {
        raw.put(JSONValue.parse(pt), JSONValue.parse(rt));
    }

    public void addPage(Page p) {
        log.pages.add(p);
        addEntry(getPageEntry(p));
    }

    public int pages() {
        return log.pages.size();
    }

    public com.cognizant.reporting.performance.har.Har.Log log() {
        return log;
    }

    public void addEntry(com.cognizant.reporting.performance.har.Entry e) {
        log.entries.add(e);
    }

    private com.cognizant.reporting.performance.har.Entry getPageEntry(Page p) {
        return new com.cognizant.reporting.performance.har.Entry(p);
    }

    public void updateConfig(TestCaseReport testCaseReport,java.lang.String pageName,int index) {
        put("config", Prop.getConfig(testCaseReport,pageName,index));
    }
  
    public class Log extends JSONObject {

        private static final long serialVersionUID = 1L;
        public final JSONArray pages;
        public final JSONArray entries;

        public Log() {
            pages = new JSONArray();
            entries = new JSONArray();
            put("version", Prop.version);
            put("creator", Prop.creator);
            put("pages", pages);
            put("entries", entries);
        }

        public JSONArray pages() {
            return pages;
        }
    }

}

class Prop {

    public static String version = "1.2";
    public static Creator creator = new Creator();

    static class Creator extends JSONObject {

        private static final long serialVersionUID = 1L;

        @SuppressWarnings("unchecked")
        public Creator() {
            put("name", "Spritz");
            put("version", "4.5.x");
        }
    }

    public static Object getEmpty() {
        return new JSONObject();
    }

    public static JSONArray getEmptyArray() {
        return new JSONArray();
    }

    public static Object getConfig(TestCaseReport r,String pageName,int index) {
        JSONObject conf = new JSONObject();
        conf.put("name", pageName);
        conf.put("index", index);
        conf.put("testcase", r.TestCase);
        conf.put("scenario", r.TestCase);
        conf.put("iteration", r.getIter());
        conf.put("version", r.getDriver().getBrowserVersion());
        conf.put("browser", r.getDriver().getCurrentBrowser());
        conf.put("platform", r.getDriver().getPlatformName());
        conf.put("driver",r.getDriver().getDriverName(r.getDriver().getCurrentBrowser()));
        return conf;
    }
;
}
