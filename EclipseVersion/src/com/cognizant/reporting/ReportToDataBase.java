/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import com.cognizant.constants.FilePath;
import com.cognizant.core.RunContext;
import com.cognizant.data.LoadSettings;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReportToDataBase {

    public static void WriteReport(String status, RunContext runContext) {
        if (!LoadSettings.getSettings().isTestRun()) {
            updateTestCaseSheet(runContext, status);
        }
    }

    public static void updateTestCaseSheet(RunContext runContext, String status) {
        ArrayList<String> qList = new ArrayList<>();
        try (BufferedReader dataReader = new BufferedReader(new FileReader(
                FilePath.getTestSetPath()))) {
            String stringRead = dataReader.readLine();
            String check = "true," + runContext.Scenario + ","
                    + runContext.TestCase + "," + runContext.Iteration;
            String endString = runContext.BrowserName + "," + runContext.BrowserVersionValue + "," + runContext.PlatformValue;
            String replaceMent = "," + status + "," + endString;

            while (stringRead != null && !stringRead.isEmpty()) {
                if (stringRead.startsWith(check)
                        && stringRead.endsWith(endString)) {
                    stringRead = check + replaceMent;
                }
                qList.add(stringRead);
                stringRead = dataReader.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(ReportToDataBase.class.getName()).log(Level.SEVERE, null, ex);
        }

        try (BufferedWriter dataWriter = new BufferedWriter(new FileWriter(
                FilePath.getTestSetPath()))) {

            for (String qry : qList) {
                dataWriter.write(qry);
                dataWriter.newLine();
            }
        } catch (IOException e) {
            Logger.getLogger(ReportToDataBase.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
