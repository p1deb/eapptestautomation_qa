/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import com.cognizant.constants.FilePath;
import com.cognizant.drivers.SeleniumDriver;
import com.cognizant.support.CSVMgr;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author 389747
 */
@SuppressWarnings({"unchecked"})
public class ReportUtils {   
    static Properties themeProp = new Properties();

    static void loadDefaultTheme(JSONObject obj) {
        try {
            File appSettings = new File(FilePath.getAppSettings());
            if (appSettings.exists()) {
                themeProp.load(new FileReader(appSettings));
            }
            Object Theme = themeProp.get(RDS.TestSet.THEME);
            if (Theme == null || Theme.toString().isEmpty()) {
                Theme = getFirstTheme();
            }
            obj.put(RDS.TestSet.THEME, Theme.toString());
            if (themeProp.get(RDS.TestSet.THEMES) != null) {
                String[] themes = themeProp.get(RDS.TestSet.THEMES).toString().split(",");
                JSONArray jsthemes = new JSONArray();
                jsthemes.addAll(Arrays.asList(themes));
                obj.put(RDS.TestSet.THEMES, jsthemes);
            }
        } catch (Exception ex) {
            Logger.getLogger(ReportUtils.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    static String getDescription(String method, String input, String objectName, String condition) {
        String step = getConvertedStep(method, objectName, input, condition);
        if (step != null) {
            return step;
        }
        return "user defined Action";
    }

    private static String getConvertedStep(String action, String object, String data ,String condition) {
        List<String[]> stepTemplate;
        HashMap<String, ArrayList<String[]>> template = CSVMgr.getHashCSV(new File(FilePath.getStepMapFile()), "Step");
        stepTemplate = template.get(action.toLowerCase());
        if (stepTemplate != null) {
            String desc = (stepTemplate.get(0))[1];
            return desc.replaceAll("<Data>", Matcher.quoteReplacement(data))
                    .replaceAll("<Object>", Matcher.quoteReplacement(object))
                    .replaceAll("<Object2>", Matcher.quoteReplacement(condition));
        }
        return null;
    }

    private static Object getFirstTheme() {
        File themeFolder = new File(FilePath.getReportThemePath());
        for (File file : themeFolder.listFiles()) {
            if (file.getName().endsWith(".css")) {
                return file.getName().replace(".css", "");
            }
        }
        return "default";
    }

    static Boolean takeScreenshot(SeleniumDriver seleniumdriver, String imgSrc) {
        try {
            File scrFile = seleniumdriver.createScreenShot();
            if (scrFile != null) {
                File imgFile = new File(FilePath.getCurrentResultsPath() + imgSrc);
                FileUtils.copyFile(scrFile, imgFile, true);
                scrFile.delete();
                return true;
            }
        } catch (Exception ex) {
            Logger.getLogger(ReportUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
