/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package com.cognizant.reporting;

import com.cognizant.drivers.SeleniumDriver;
import com.cognizant.support.Status;
import com.cognizant.support.Step;
import java.util.List;

public interface Report {

    public void setDriver(SeleniumDriver dr);

    public void createReport(String scenario, String testCase, String Iteration, String runTime, String initBrowser, String execMode);

    public String getTestCaseName();

    public void updateCurrentStepInfo(Step curr);

    public String getScenarioName();

    public void updateTestLog(String stepName, String stepDescription, Status state);

    public void updateTestLog(String stepName, String stepDescription, Status state, List<String> optional);

    public void updateTestLog(String stepName, String stepDescription, Status state, String optional);

    public void startComponent(String component);

    public void startIteration(int iteration);

    public void endComponent(String string);

    public void endIteration(int iteration);

    public Status getCurrentStatus();

    public Boolean isStepPassed();

}
