	package com.cognizant.BusinessComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import com.cognizant.support.UnCaughtException;

public class ProposedInsuredInfoPage extends Command{

	public ProposedInsuredInfoPage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
	
	WebElement element;
	
	@FindBy (id = "rdo_96_1")
	WebElement interviewFamilyMember_Yes;
	
	@FindBy (id = "rdo_96_2")
	WebElement interviewFamilyMember_No;
	
	@FindBy (id = "rdo_101_1")
	WebElement insuredOwner_Yes;
	
	@FindBy (id = "rdo_101_2")
	WebElement insuredOwner_No;
	
	@FindBy (id = "rdo_33_1")
	WebElement insuredLicense_Yes;
	
	@FindBy (id = "rdo_33_2")
	WebElement insuredLicense_No;
	
	@FindBy(id = "flda_1")  //xpath = "//div[@id='div_1']/input")
	WebElement txtLicenseNum;
		
	@FindBy (id = "lb_36")  //(xpath = "//div[@id='div_36']/select")
	WebElement drpdownIssueState;	
	
	@FindBy (id="flda_111")
	WebElement txtProvideDetails;
	
	//Other Insured
	
	@FindBy (id= "rdo_23_1")
	WebElement radioGender_Male;
	
	@FindBy (id= "rdo_23_2")
	WebElement radioGender_Female;
	
	@FindBy (id= "cb_78")
	WebElement checkBoxSamdAddress;
	
	String reusableSecnario = "OtherInsuredTerm(OIR)";
	String reusableSecnario_SPO = "SurvivorPurchaseOption";
	String dataTableName = "WholeLifeInsurance";
	
	public void interviewFamilyMember(String value)
	{
		value.trim();
		if(value.equalsIgnoreCase("Yes"))
		{
			ClickElement(interviewFamilyMember_Yes, "Yes");
		}
		else if(value.equalsIgnoreCase("No"))
		{
			ClickElement(interviewFamilyMember_No, "No");
		}
	}
	
	public void insuredOwner(String value)
	{
		value.trim();
		if(value.equalsIgnoreCase("Yes"))
		{
			ClickElement(insuredOwner_Yes, "Yes");
		}
		else if(value.equalsIgnoreCase("No"))
		{
			ClickElement(insuredOwner_No, "No");
		}
	}
	
	public void insuredLicense(String value, String licenseNum, String licenseIssueState)
	{	
		if(value.equalsIgnoreCase("Yes"))
		{
			ClickElement(insuredLicense_Yes, "Yes");
			//EnterText(txtLicenseNum, licenseNum, "License Number");
			txtLicenseNum.sendKeys(licenseNum);
			drpdownIssueState.click();
			ComboSelectValue(drpdownIssueState, licenseIssueState, "License Issue State");
			
		}
		else if(value.equalsIgnoreCase("No"))
		{
			ClickElement(insuredLicense_No, "No");
		}
	}
		
	public void employmentDetails(String employmentStatus)
	{
		try
		{
		String strEmploymentStatus = employmentStatus.trim();
		String strProvideDetails = userData.getData(dataTableName, "ProvideDetails");
		switch (strEmploymentStatus)
		{
			case "Employed":
			{
				executeTestCase("CreatePolicy", "EmploymentInformation");				
				break;
			}
			case "Unemployed":
			{
				EnterText(txtProvideDetails, strProvideDetails, "Provide Details");
				break;
			}
			case "Other":
			{
				EnterText(txtProvideDetails, strProvideDetails, "Provide Details");
				break;
			}
		}
		} catch (UnCaughtException e) {
			e.printStackTrace();
		}
	}
	
	//Proposed Other Insured Information Page
	
	public void enterGender_OtherInsured(String gender)
	{
		selectGender(gender, radioGender_Male, radioGender_Female);
	}
	
	public void residenceInfo(String residenceInfo)
	{
		try {
		if(residenceInfo.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkBoxSamdAddress, "Proposed Other Insured address same as the proposed insured");			
		}
		else if(residenceInfo.equalsIgnoreCase("No"))
		{
			executeTestCase(reusableSecnario, "ResidenceInformation");			
		}
		} catch (UnCaughtException e) {
			e.printStackTrace();
		}		
	}
	
	//Survivor Purchase Option Insured Information Page
	
	public void residenceInfo_SPO(String residenceInfo)
	{
		try {
		if(residenceInfo.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkBoxSamdAddress, "Proposed Other Insured address same as the proposed insured");			
		}
		else if(residenceInfo.equalsIgnoreCase("No"))
		{
			executeTestCase(reusableSecnario, "ResidenceInformation");			
		}
		} catch (UnCaughtException e) {
			e.printStackTrace();
		}		
	}

}
