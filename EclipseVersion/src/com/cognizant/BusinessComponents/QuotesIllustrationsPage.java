package com.cognizant.BusinessComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import com.cognizant.support.UnCaughtException;

public class QuotesIllustrationsPage extends Command{

	public QuotesIllustrationsPage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
	
}
