package com.cognizant.BusinessComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import com.cognizant.support.UnCaughtException;

public class PremiumInformationPage extends Command{

	public PremiumInformationPage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
	
	@FindBy(id ="rdo_25_1")
	WebElement radioFundedviaPremiunFinLoan_Yes;
	
	@FindBy(id ="rdo_25_2")
	WebElement radioFundedviaPremiunFinLoan_No;
	
	@FindBy(id ="lb_29")
	WebElement drpdownRelationToOwner;
	
	@FindBy(id ="flda_32")
	WebElement txtProvideDetails;
	
	@FindBy(id ="flda_57")
	WebElement txtOtherMode;
	
	@FindBy(id ="rdo_43_1")
	WebElement radioChecking;
	
	@FindBy(id ="rdo_43_2")
	WebElement radioSavings;
	
	@FindBy(xpath = "//input[@alt_id='PREM_FundedDetails']")
	WebElement txt_PREM_FundedDetails;
		
	@FindBy(id="rdo_7_1")
	WebElement radioExistingAPP;
	
	@FindBy(id="rdo_7_2")
	WebElement radioNewDraft;	
	
	@FindBy(id="flda_6")
	WebElement txtPremiumPolicyNumber;
	
	String dataTableName = "WholeLifeInsurance";
	
	public void premiumFunded(String policyFunded)
	{	
		if(policyFunded.equalsIgnoreCase("Yes"))
		{
			ClickElement(radioFundedviaPremiunFinLoan_Yes, "Policy Funded via Premiun Financing Loan - Yes");
			String strPREM_FundedDetails = userData.getData(dataTableName, "Provide Details");
			EnterText(txt_PREM_FundedDetails, strPREM_FundedDetails, "PREM_FundedDetails");
		}
		else
		{
			ClickElement(radioFundedviaPremiunFinLoan_No, "Policy Funded via Premiun Financing Loan - No");
		}
	}	
	
	public void provideDetails(String details)
	{
		executeMethod(txtProvideDetails, Action, details);		
		EnterText(txtProvideDetails, details, "Provide Details");
	}
	
	public void accountInformation(String paymentMode)
	{
		if(paymentMode.equalsIgnoreCase("Monthly APP"))
		{
			
		}
	}
	
	public void premiumPayor(String premiumPayor)
	{
		if((premiumPayor.equalsIgnoreCase("Other")) || (premiumPayor.equalsIgnoreCase("Primary Insured")))
		{
			String strRelToOwner = userData.getData(dataTableName, "RelationshipToOwner");
			String strProvideDetails = userData.getData(dataTableName, "ProvideDetails");
			ComboSelectValue(drpdownRelationToOwner, strRelToOwner, "Relationship to Owner");
			if(strRelToOwner.equalsIgnoreCase("Other"))
			{
				EnterText(txtProvideDetails, strProvideDetails, "Provide Details");
			}
		}
	}
	
	public void paymentMode(String paymentMode)
	{
		String strOtherModeDetails = userData.getData(dataTableName, "OtherModeDetails");		
		String strExistingAPP = userData.getData(dataTableName, "AddThisPremiumToExistingAPPForPolicy");
		String strPremiumPolicyNumber = userData.getData(dataTableName, "PremiumPolicyNum");
		String strStartANewDraft = userData.getData(dataTableName, "StartANewDraft");
		String strChecking = userData.getData(dataTableName, "Checking");
		String strSavings = userData.getData(dataTableName, "Savings");
		
		
		
		if(paymentMode.equalsIgnoreCase("Monthly APP"))
		{			
			if(strExistingAPP.equalsIgnoreCase("Yes"))
			{
				ClickElement(radioExistingAPP, "Add this Premium to Existing APP for Policy #");
				EnterText(txtPremiumPolicyNumber, strPremiumPolicyNumber, "Premium Policy Number");
				
			}
			if(strStartANewDraft.equalsIgnoreCase("Yes"))
			{
				ClickElement(radioNewDraft, "Start A New Draft");
				if(strChecking.equalsIgnoreCase("Yes"))
				{
					ClickElement(radioChecking, "Checking");
				}			
				if(strSavings.equalsIgnoreCase("Yes"))
				{
					ClickElement(radioSavings, "Savings");
				}
			}
			
		}
		
		else if(paymentMode.equalsIgnoreCase("Other"))
		{
			EnterText(txtOtherMode, strOtherModeDetails, "Other Mode Details");
		}
	}
	
}
