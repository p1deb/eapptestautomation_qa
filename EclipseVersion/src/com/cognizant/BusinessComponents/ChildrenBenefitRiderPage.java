package com.cognizant.BusinessComponents;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;

public class ChildrenBenefitRiderPage extends Command{

	public ChildrenBenefitRiderPage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
	
	@FindBy(id="rdo_2_1")
	WebElement radioGender_Male;
	
	@FindBy(id="rdo_2_2")
	WebElement radioGender_Female;
	
	
	public void enterGender_Child(String gender)
	{	
		selectGender(gender, radioGender_Male, radioGender_Female);
	}	
	
}
