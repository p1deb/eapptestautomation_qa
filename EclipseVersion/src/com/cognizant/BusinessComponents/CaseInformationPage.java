package com.cognizant.BusinessComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import com.cognizant.support.UnCaughtException;

public class CaseInformationPage extends Command{

	public CaseInformationPage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
	
	WebElement element;
	
	@FindBy (xpath = "//span/input[@class='jq-dte-month hint']")
	WebElement txtMonth;
	
	@FindBy (xpath = "//input[@class='jq-dte-day hint']")
	WebElement txtDay;
	
	@FindBy (xpath = "//span/input[@class='jq-dte-year hint']")
	WebElement txtYear;
	
	@FindBy (xpath = "//div[@id='divGEN']/div/button/span")
	WebElement drpdownGender;
	
	@FindBy (xpath = "//div[@id='divGEN']/div/div[2]/ul/li[3]/a")
	WebElement drpdownGender_Male;
	
	@FindBy (xpath = "//div[@id='divGEN']/div/div[2]/ul/li[2]/a")
	WebElement drpdownGender_Female;
	
	//Illustrations
	
	@FindBy (id = "spanIllustrations")
	WebElement tabIllustrations;
	
	@FindBy(xpath ="//h3[@id='lbl_55']/span")//   "//span[text()='Client Information']"
	WebElement labelClientInformation;			
	
	@FindBy (id = "btn_156")
	WebElement btnNext;
	
	@FindBy (xpath = "//h3[@id='lbl_28']/span")  //  "//span[text()='Riders Available']"
	WebElement headerRidersAvailable;
	
	@FindBy (id = "btn_96")
	WebElement btnNext_Riders;
	
	@FindBy (xpath = "//span[text()='Assumptions']")
	WebElement headerAssumptions;
	
	@FindBy (id = "spanApplication")
	WebElement tabApplication;
	

	public void enterDOB(String dob)
	{
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		txtMonth.sendKeys(strMonth);
		txtYear.sendKeys(strYear);
		txtDay.sendKeys(strDay);
		
		Report.updateTestLog("Case Information", dob + " is entered in DOB", Status.DONE);		
	}
	
	public void enterGender(String gender)
	{
		ClickElement(drpdownGender, "Gender");
		if(gender.equalsIgnoreCase("Female"))
		{
			drpdownGender_Female.click();
			Report.updateTestLog("Case Information", "Female is selected", Status.DONE);
		}
		else if(gender.equalsIgnoreCase("Male"))
		{
			drpdownGender_Male.click();
			Report.updateTestLog("Case Information", "Male is selected", Status.DONE);
		}
	}
	
	public void clickOnSelect(String product, String illustrations)
	{
		try{
		if(illustrations.equalsIgnoreCase("Yes"))
		{
			String xpath = "//div[@id='divAvailableProductsGrid1']/div/table/tbody/tr[td[3][contains(text(),'"+product+"')]]/td[4]/input";
			element = Driver.findElement(By.xpath(xpath));
			ClickElement(element, "Select button for "+product);
			FluentPredicateWait();			
			wait.waitForElementToDisplay(Driver, tabIllustrations);		
			executeTestCase("CreatePolicy", "Quotes_Illustrations");
		}
		else
		{
			String xpath = "//div[@id='divAvailableProductsGrid1']/div/table/tbody/tr[td[3][contains(text(),'"+product+"')]]/td[5]/input";
			element = Driver.findElement(By.xpath(xpath));
			ClickElement(element, "Select button for "+product);	
		}
		}catch(NoSuchElementException |UnCaughtException e)
		{
			e.printStackTrace();
			System.out.println("");
		}
		
	}
	
}
