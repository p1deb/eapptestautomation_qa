package com.cognizant.BusinessComponents;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;

public class BeneficiaryPage extends Command{

	public BeneficiaryPage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
		
	@FindBy (id = "rdo_109_1")
	WebElement radioSharePercentage_Yes;	
	
	@FindBy (id = "rdo_109_2")
	WebElement radioSharePercentage_No;
	
	@FindBy (xpath = "//div[@id='div_3']/select")
	WebElement drpdownDeceasedSharePaid;
	
	@FindBy (id = "lb_35")
	WebElement drpdownRelationInd_1;
	
	@FindBy (id = "flda_41")
	WebElement txtRelationOther_1;
	
	@FindBy (id = "flda_31")
	WebElement txtFirstName_1;
	
	@FindBy (id = "flda_32")
	WebElement txtMiddleName_1;
	
	@FindBy (id = "flda_33")
	WebElement txtLastName_1;
	
	@FindBy (id = "rdo_130_1")
	WebElement radioGender_Male_1;
	
	@FindBy (id = "rdo_130_2")
	WebElement radioGender_Female_1;
	
	@FindBy (id = "flda_24")
	WebElement txtADDR_Full_1;
	
	@FindBy (id = "flda_25")
	WebElement txtSSNTIN_1;
	
	@FindBy (id = "flda_44_text")
	WebElement txtDOB_DOT_1;	
	
	@FindBy (xpath = "//input[@class='jq-dte-month hint']")
	WebElement txtMonth_1;
	
	@FindBy (xpath = "//input[@class='jq-dte-day hint']")
	WebElement txtDate_1;
	
	@FindBy (xpath = "//input[@class='jq-dte-year hint']")
	WebElement txtYear_1;
		
	@FindBy (id = "flda_39")
	WebElement txtShare_1;
	
	@FindBy (id = "flda_27")
	WebElement txtEntityName_1;
	
	//2nd
	@FindBy (id = "lb_55")
	WebElement drpdownRelationInd_2;
	
	@FindBy (id = "flda_60")
	WebElement txtRelationOther_2;
	
	@FindBy (id = "flda_52")
	WebElement txtFirstName_2;
	
	@FindBy (id = "flda_53")
	WebElement txtMiddleName_2;
	
	@FindBy (id = "flda_54")
	WebElement txtLastName_2;
	
	@FindBy (id = "rdo_129_1")
	WebElement radioGender_Male_2;
	
	@FindBy (id = "rdo_129_2")
	WebElement radioGender_Female_2;
	
	@FindBy (id = "flda_49")
	WebElement txtADDR_Full_2;
	
	@FindBy (id = "flda_50")
	WebElement txtSSNTIN_2;
	
	@FindBy (id = "flda_57_text")
	WebElement txtDOB_DOT_2;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[1]")
	WebElement txtMonth_2;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[2]")
	WebElement txtDate_2;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[3]")
	WebElement txtYear_2;
	
	@FindBy (id ="flda_56")
	WebElement txtShare_2;
	
	@FindBy (id = "flda_51")
	WebElement txtEntityName_2;
	
	//3rd 
	@FindBy (id = "lb_76")
	WebElement drpdownRelationInd_3;
	
	@FindBy (id = "flda_75")
	WebElement txtRelationOther_3;
	
	@FindBy (id = "flda_68")
	WebElement txtFirstName_3;
	
	@FindBy (id = "flda_69")
	WebElement txtMiddleName_3;
	
	@FindBy (id = "flda_70")
	WebElement txtLastName_3;
	
	@FindBy (id = "rdo_128_1")
	WebElement radioGender_Male_3;
	
	@FindBy (id = "rdo_128_2")
	WebElement radioGender_Female_3;
	
	@FindBy (id = "flda_65")
	WebElement txtADDR_Full_3;
	
	@FindBy (id = "flda_66")
	WebElement txtSSNTIN_3;
	
	@FindBy (id = "flda_72_text")
	WebElement txtDOB_DOT_3;
	
	@FindBy (xpath = "//input[@class='jq-dte-month hint']")
	WebElement txtMonth_3;
	
	@FindBy (xpath = "//input[@class='jq-dte-day hint']")
	WebElement txtDate_3;
	
	@FindBy (xpath = "//input[@class='jq-dte-year hint']")
	WebElement txtYear_3;
	
	@FindBy (id ="flda_71")
	WebElement txtShare_3;
	
	@FindBy (id = "flda_67")
	WebElement txtEntityName_3;
	
    //4th
	@FindBy (id = "lb_92")
	WebElement drpdownRelationInd_4;
	
	@FindBy (id = "flda_91")
	WebElement txtRelationOther_4;
	
	@FindBy (id = "flda_84")
	WebElement txtFirstName_4;
	
	@FindBy (id = "flda_85")
	WebElement txtMiddleName_4;
	
	@FindBy (id = "flda_86")
	WebElement txtLastName_4;
	
	@FindBy (id = "rdo_127_1")
	WebElement radioGender_Male_4;
	
	@FindBy (id = "rdo_127_2")
	WebElement radioGender_Female_4;
	
	@FindBy (id = "flda_81")
	WebElement txtADDR_Full_4;
	
	@FindBy (id = "flda_82")
	WebElement txtSSNTIN_4;
	
	@FindBy (id = "flda_88_text")
	WebElement txtDOB_DOT_4;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[1]")
	WebElement txtMonth_4;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[2]")
	WebElement txtDate_4;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[3]")
	WebElement txtYear_4;
	
	@FindBy (id = "flda_49")
	WebElement txtShare_4;
	
	@FindBy (id = "flda_67")
	WebElement txtEntityName_4;
	
	//5th
	@FindBy (id = "lb_108")
	WebElement drpdownRelationInd_5;
	
	@FindBy (id = "flda_107")
	WebElement txtRelationOther_5;
	
	@FindBy (id = "flda_100")
	WebElement txtFirstName_5;
	
	@FindBy (id = "flda_101")
	WebElement txtMiddleName_5;
	
	@FindBy (id = "flda_103")
	WebElement txtLastName_5;
	
	@FindBy (id = "rdo_126_1")
	WebElement radioGender_Male_5;
	
	@FindBy (id = "rdo_126_2")
	WebElement radioGender_Female_5;
	
	@FindBy (id = "flda_97")
	WebElement txtADDR_Full_5;
	
	@FindBy (id = "flda_98")
	WebElement txtSSNTIN_5;
	
	@FindBy (id = "flda_104_text")
	WebElement txtDOB_DOT_5;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[1]")
	WebElement txtMonth_5;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[2]")
	WebElement txtDate_5;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner']/input[3]")
	WebElement txtYear_5;
	
	@FindBy (id = "flda_103")
	WebElement txtShare_5;
	
	@FindBy (id = "flda_99")
	WebElement txtEntityName_5;
	
	String tableName = "WholeLifeInsurance";	
	
	public void sharePercentage(String sharePercentage)
	{
		if(sharePercentage.equalsIgnoreCase("Yes"))
		{
			ClickElement(radioSharePercentage_Yes, "Share Percentage_Yes");
			String strDeceasedSharePaid = userData.getData(tableName, "DeceasedSharePaid");
			ComboSelectValue(drpdownDeceasedSharePaid, strDeceasedSharePaid, "Deceased Share Paid");
		}
		else
		{
			ClickElement(radioSharePercentage_No, "Share Percentage_No");
		}
	}
	
	public void enterBeneficiaryDetails(String number)	
	{	
		String strSharePercentage = userData.getData("BeneficiaryInfo", "SharePercentage");
		int num = Integer.parseInt(number); 
		if(num>=1)
		{			
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_Bene1");
			String strRelationOther = userData.getData(tableName, "RelationOther_Bene1");
			String strFirstName = userData.getData(tableName, "FirstName_Bene1");
			String strMiddleName = userData.getData(tableName, "MiddleName_Bene1");
			String strLastName = userData.getData(tableName, "LastName_Bene1");
			String strGender = userData.getData(tableName, "Gender_Bene1");
			String strAddress = userData.getData(tableName, "Address_Bene1");
			String strSSN = userData.getData(tableName, "SSN_Bene1");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_Bene1");
			String strShare = userData.getData(tableName, "Share%_Bene1");
			String strEntityName = userData.getData(tableName, "EntityName_Bene1");
			ClickElement(drpdownRelationInd_1, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_1, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_1, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_1, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_1, strAddress, "Address");
					EnterText(txtSSNTIN_1, strSSN, "SSN");
					EnterText(txtDOB_DOT_1, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_1, txtDate_1, txtYear_1);
				}
				else
				{
				EnterText(txtFirstName_1, strFirstName, "FirstName");
				EnterText(txtMiddleName_1, strMiddleName, "MiddleName");
				EnterText(txtLastName_1, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_1, radioGender_Female_1);
				EnterText(txtADDR_Full_1, strAddress, "Address");
				EnterText(txtSSNTIN_1, strSSN, "SSN");
				EnterText(txtDOB_DOT_1, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_1, txtDate_1, txtYear_1);					
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_1, strShare, "Share %");
			}
		}
		
		if(num>=2)
		{	
			
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_Bene2");
			String strRelationOther = userData.getData(tableName, "RelationOther_Bene2");
			String strFirstName = userData.getData(tableName, "FirstName_Bene2");
			String strMiddleName = userData.getData(tableName, "MiddleName_Bene2");
			String strLastName = userData.getData(tableName, "LastName_Bene2");
			String strGender = userData.getData(tableName, "Gender_Bene2");
			String strAddress = userData.getData(tableName, "Address_Bene2");
			String strSSN = userData.getData(tableName, "SSN_Bene2");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_Bene2");
			String strShare = userData.getData(tableName, "Share%_Bene2");
			String strEntityName = userData.getData(tableName, "EntityName_Bene2");
			ClickElement(drpdownRelationInd_2, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_2, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_2, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_2, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_2, strAddress, "Address");
					EnterText(txtSSNTIN_2, strSSN, "SSN");
					EnterText(txtDOB_DOT_2, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_2, txtDate_2, txtYear_2);
				}
				else
				{
				EnterText(txtFirstName_2, strFirstName, "FirstName");
				EnterText(txtMiddleName_2, strMiddleName, "MiddleName");
				EnterText(txtLastName_2, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_2, radioGender_Female_2);
				EnterText(txtADDR_Full_2, strAddress, "Address");
				EnterText(txtSSNTIN_2, strSSN, "SSN");
				EnterText(txtDOB_DOT_2, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_2, txtDate_2, txtYear_2);	
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_2, strShare, "Share %");
			}
		}
		
		if(num>=3)
		{	
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_Bene3");
			String strRelationOther = userData.getData(tableName, "RelationOther_Bene3");
			String strFirstName = userData.getData(tableName, "FirstName_Bene3");
			String strMiddleName = userData.getData(tableName, "MiddleName_Bene3");
			String strLastName = userData.getData(tableName, "LastName_Bene3");
			String strGender = userData.getData(tableName, "Gender_Bene3");
			String strAddress = userData.getData(tableName, "Address_Bene3");
			String strSSN = userData.getData(tableName, "SSN_Bene3");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_Bene3");
			String strShare = userData.getData(tableName, "Share%_Bene3");	
			String strEntityName = userData.getData(tableName, "EntityName_Bene3");
			ClickElement(drpdownRelationInd_3, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_3, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_3, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_3, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_3, strAddress, "Address");
					EnterText(txtSSNTIN_3, strSSN, "SSN");
					EnterText(txtDOB_DOT_3, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_3, txtDate_3, txtYear_3);
				}
				else
				{
				EnterText(txtFirstName_3, strFirstName, "FirstName");
				EnterText(txtMiddleName_3, strMiddleName, "MiddleName");
				EnterText(txtLastName_3, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_3, radioGender_Female_3);
				EnterText(txtADDR_Full_3, strAddress, "Address");
				EnterText(txtSSNTIN_3, strSSN, "SSN");
				EnterText(txtDOB_DOT_3, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_3, txtDate_3, txtYear_3);	
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_3, strShare, "Share %");
			}
		}
		
		if(num>=4)
		{				
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_Bene4");
			String strRelationOther = userData.getData(tableName, "RelationOther_Bene4");
			String strFirstName = userData.getData(tableName, "FirstName_Bene4");
			String strMiddleName = userData.getData(tableName, "MiddleName_Bene4");
			String strLastName = userData.getData(tableName, "LastName_Bene4");
			String strGender = userData.getData(tableName, "Gender_Bene4");
			String strAddress = userData.getData(tableName, "Address_Bene4");
			String strSSN = userData.getData(tableName, "SSN_Bene4");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_Bene4");
			String strShare = userData.getData(tableName, "Share%_Bene4");
			String strEntityName = userData.getData(tableName, "EntityName_Bene4");
			ClickElement(drpdownRelationInd_4, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_4, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_4, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_4, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_4, strAddress, "Address");
					EnterText(txtSSNTIN_4, strSSN, "SSN");
					EnterText(txtDOB_DOT_4, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_4, txtDate_4, txtYear_4);
				}
				else
				{
				EnterText(txtFirstName_4, strFirstName, "FirstName");
				EnterText(txtMiddleName_4, strMiddleName, "MiddleName");
				EnterText(txtLastName_4, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_4, radioGender_Female_4);
				EnterText(txtADDR_Full_4, strAddress, "Address");
				EnterText(txtSSNTIN_4, strSSN, "SSN");
				EnterText(txtDOB_DOT_4, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_4, txtDate_4, txtYear_4);	
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_4, strShare, "Share %");
			}		
		}
		
		if(num>=5)
		{	
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_Bene5");
			String strRelationOther = userData.getData(tableName, "RelationOther_Bene5");
			String strFirstName = userData.getData(tableName, "FirstName_Bene5");
			String strMiddleName = userData.getData(tableName, "MiddleName_Bene5");
			String strLastName = userData.getData(tableName, "LastName_Bene5");
			String strGender = userData.getData(tableName, "Gender_Bene5");
			String strAddress = userData.getData(tableName, "Address_Bene5");
			String strSSN = userData.getData(tableName, "SSN_Bene5");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_Bene5");
			String strShare = userData.getData(tableName, "Share%_Bene5");
			String strEntityName = userData.getData(tableName, "EntityName_Bene5");
			ClickElement(drpdownRelationInd_5, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_5, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_5, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_5, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_5, strAddress, "Address");
					EnterText(txtSSNTIN_5, strSSN, "SSN");
					EnterText(txtDOB_DOT_5, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_5, txtDate_5, txtYear_5);
				}
				else
				{
				EnterText(txtFirstName_5, strFirstName, "FirstName");
				EnterText(txtMiddleName_5, strMiddleName, "MiddleName");
				EnterText(txtLastName_5, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_5, radioGender_Female_5);
				EnterText(txtADDR_Full_5, strAddress, "Address");
				EnterText(txtSSNTIN_5, strSSN, "SSN");
				EnterText(txtDOB_DOT_5, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_5, txtDate_5, txtYear_5);	
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_5, strShare, "Share %");
			}
		}
	}
	
	public void sharePercentage_OtherInsured(String sharePercentage)
	{
		if(sharePercentage.equalsIgnoreCase("Yes"))
		{
			ClickElement(radioSharePercentage_Yes, "Share Percentage_Yes");
			String strDeceasedSharePaid = userData.getData(tableName, "DeceasedSharePaid");
			ComboSelectValue(drpdownDeceasedSharePaid, strDeceasedSharePaid, "Deceased Share Paid - Othet Insured");
		}
		else
		{
			ClickElement(radioSharePercentage_No, "Share Percentage_No");
		}
	}
	
	public void enterBeneficiaryDetails_OtherInsured(String number)	
	{	
		String strSharePercentage = userData.getData("BeneficiaryInfo", "SharePercentage");
		int num = Integer.parseInt(number); 
		if(num>=1)
		{			
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_OI_Bene1");
			String strRelationOther = userData.getData(tableName, "RelationOther_OI_Bene1");
			String strFirstName = userData.getData(tableName, "FirstName_OI_Bene1");
			String strMiddleName = userData.getData(tableName, "MiddleName_OI_Bene1");
			String strLastName = userData.getData(tableName, "LastName_OI_Bene1");
			String strGender = userData.getData(tableName, "Gender_OI_Bene1");
			String strAddress = userData.getData(tableName, "Address_OI_Bene1");
			String strSSN = userData.getData(tableName, "SSN_OI_Bene1");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_OI_Bene1");
			String strShare = userData.getData(tableName, "Share%_OI_Bene1");
			String strEntityName = userData.getData(tableName, "EntityName_OI_Bene1");
			ClickElement(drpdownRelationInd_1, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_1, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_1, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_1, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_1, strAddress, "Address");
					EnterText(txtSSNTIN_1, strSSN, "SSN");
					EnterText(txtDOB_DOT_1, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_1, txtDate_1, txtYear_1);
				}
				else
				{
				EnterText(txtFirstName_1, strFirstName, "FirstName");
				EnterText(txtMiddleName_1, strMiddleName, "MiddleName");
				EnterText(txtLastName_1, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_1, radioGender_Female_1);
				EnterText(txtADDR_Full_1, strAddress, "Address");
				EnterText(txtSSNTIN_1, strSSN, "SSN");
				EnterText(txtDOB_DOT_1, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_1, txtDate_1, txtYear_1);					
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_1, strShare, "Share %");
			}
		}
		
		if(num>=2)
		{	
			
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_OI_Bene2");
			String strRelationOther = userData.getData(tableName, "RelationOther_OI_Bene2");
			String strFirstName = userData.getData(tableName, "FirstName_OI_Bene2");
			String strMiddleName = userData.getData(tableName, "MiddleName_OI_Bene2");
			String strLastName = userData.getData(tableName, "LastName_OI_Bene2");
			String strGender = userData.getData(tableName, "Gender_OI_Bene2");
			String strAddress = userData.getData(tableName, "Address_OI_Bene2");
			String strSSN = userData.getData(tableName, "SSN_OI_Bene2");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_OI_Bene2");
			String strShare = userData.getData(tableName, "Share%_OI_Bene2");
			String strEntityName = userData.getData(tableName, "EntityName_OI_Bene2");
			ClickElement(drpdownRelationInd_2, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_2, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_2, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_2, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_2, strAddress, "Address");
					EnterText(txtSSNTIN_2, strSSN, "SSN");
					EnterText(txtDOB_DOT_2, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_2, txtDate_2, txtYear_2);
				}
				else
				{
				EnterText(txtFirstName_2, strFirstName, "FirstName");
				EnterText(txtMiddleName_2, strMiddleName, "MiddleName");
				EnterText(txtLastName_2, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_2, radioGender_Female_2);
				EnterText(txtADDR_Full_2, strAddress, "Address");
				EnterText(txtSSNTIN_2, strSSN, "SSN");
				EnterText(txtDOB_DOT_2, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_2, txtDate_2, txtYear_2);	
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_2, strShare, "Share %");
			}
		}
		
		if(num>=3)
		{	
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_OI_Bene3");
			String strRelationOther = userData.getData(tableName, "RelationOther_OI_Bene3");
			String strFirstName = userData.getData(tableName, "FirstName_OI_Bene3");
			String strMiddleName = userData.getData(tableName, "MiddleName_OI_Bene3");
			String strLastName = userData.getData(tableName, "LastName_OI_Bene3");
			String strGender = userData.getData(tableName, "Gender_OI_Bene3");
			String strAddress = userData.getData(tableName, "Address_OI_Bene3");
			String strSSN = userData.getData(tableName, "SSN_OI_Bene3");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_OI_Bene3");
			String strShare = userData.getData(tableName, "Share%_OI_Bene3");	
			String strEntityName = userData.getData(tableName, "EntityName_OI_Bene3");
			ClickElement(drpdownRelationInd_3, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_3, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_3, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_3, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_3, strAddress, "Address");
					EnterText(txtSSNTIN_3, strSSN, "SSN");
					EnterText(txtDOB_DOT_3, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_3, txtDate_3, txtYear_3);
				}
				else
				{
				EnterText(txtFirstName_3, strFirstName, "FirstName");
				EnterText(txtMiddleName_3, strMiddleName, "MiddleName");
				EnterText(txtLastName_3, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_3, radioGender_Female_3);
				EnterText(txtADDR_Full_3, strAddress, "Address");
				EnterText(txtSSNTIN_3, strSSN, "SSN");
				EnterText(txtDOB_DOT_3, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_3, txtDate_3, txtYear_3);	
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_3, strShare, "Share %");
			}
		}
		
		if(num>=4)
		{				
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_OI_Bene4");
			String strRelationOther = userData.getData(tableName, "RelationOther_OI_Bene4");
			String strFirstName = userData.getData(tableName, "FirstName_OI_Bene4");
			String strMiddleName = userData.getData(tableName, "MiddleName_OI_Bene4");
			String strLastName = userData.getData(tableName, "LastName_OI_Bene4");
			String strGender = userData.getData(tableName, "Gender_OI_Bene4");
			String strAddress = userData.getData(tableName, "Address_OI_Bene4");
			String strSSN = userData.getData(tableName, "SSN_OI_Bene4");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_OI_Bene4");
			String strShare = userData.getData(tableName, "Share%_OI_Bene4");
			String strEntityName = userData.getData(tableName, "EntityName_OI_Bene4");
			ClickElement(drpdownRelationInd_4, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_4, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_4, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_4, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_4, strAddress, "Address");
					EnterText(txtSSNTIN_4, strSSN, "SSN");
					EnterText(txtDOB_DOT_4, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_4, txtDate_4, txtYear_4);
				}
				else
				{
				EnterText(txtFirstName_4, strFirstName, "FirstName");
				EnterText(txtMiddleName_4, strMiddleName, "MiddleName");
				EnterText(txtLastName_4, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_4, radioGender_Female_4);
				EnterText(txtADDR_Full_4, strAddress, "Address");
				EnterText(txtSSNTIN_4, strSSN, "SSN");
				EnterText(txtDOB_DOT_4, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_4, txtDate_4, txtYear_4);	
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_4, strShare, "Share %");
			}		
		}
		
		if(num>=5)
		{	
			String strRelationshipToInsured = userData.getData(tableName, "RelationshipToInsured_OI_Bene5");
			String strRelationOther = userData.getData(tableName, "RelationOther_OI_Bene5");
			String strFirstName = userData.getData(tableName, "FirstName_OI_Bene5");
			String strMiddleName = userData.getData(tableName, "MiddleName_OI_Bene5");
			String strLastName = userData.getData(tableName, "LastName_OI_Bene5");
			String strGender = userData.getData(tableName, "Gender_OI_Bene5");
			String strAddress = userData.getData(tableName, "Address_OI_Bene5");
			String strSSN = userData.getData(tableName, "SSN_OI_Bene5");
			String strDateOfBirth = userData.getData(tableName, "DateOfBirth_OI_Bene5");
			String strShare = userData.getData(tableName, "Share%_OI_Bene5");
			String strEntityName = userData.getData(tableName, "EntityName_OI_Bene5");
			ClickElement(drpdownRelationInd_5, "Relationship to Insured");
			ComboSelectValue(drpdownRelationInd_5, strRelationshipToInsured, "RelationshipToInsured");			
			if(strRelationshipToInsured.equalsIgnoreCase("Other"))
			{
				EnterText(txtRelationOther_5, strRelationOther, "RelationOther");
			}
			if(!(strRelationshipToInsured.equalsIgnoreCase("Lawful Children of Insured")||
					strRelationshipToInsured.equalsIgnoreCase("Estate of Insured")))
			{

				if((strRelationshipToInsured.equalsIgnoreCase("Charitable Organization"))||
						(strRelationshipToInsured.equalsIgnoreCase("Employer")))
				{
					EnterText(txtEntityName_5, strEntityName, "Entity Name");
					EnterText(txtADDR_Full_5, strAddress, "Address");
					EnterText(txtSSNTIN_5, strSSN, "SSN");
					EnterText(txtDOB_DOT_5, strDateOfBirth, "DOB/DOT");
					//enterDOB(strDateOfBirth, txtMonth_5, txtDate_5, txtYear_5);
				}
				else
				{
				EnterText(txtFirstName_5, strFirstName, "FirstName");
				EnterText(txtMiddleName_5, strMiddleName, "MiddleName");
				EnterText(txtLastName_5, strLastName, "LastName");
				selectGender(strGender, radioGender_Male_5, radioGender_Female_5);
				EnterText(txtADDR_Full_5, strAddress, "Address");
				EnterText(txtSSNTIN_5, strSSN, "SSN");
				EnterText(txtDOB_DOT_5, strDateOfBirth, "DOB/DOT");
				//enterDOB(strDateOfBirth, txtMonth_5, txtDate_5, txtYear_5);	
				}
			}				
			if(strSharePercentage.equalsIgnoreCase("Yes"))
			{
				EnterText(txtShare_5, strShare, "Share %");
			}
		}
	}
	
	/*public void enterDOB(String dob, WebElement month, WebElement day, WebElement year)
	{
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		month.sendKeys(strMonth);
		day.sendKeys(strYear);
		year.sendKeys(strDay);
			
		Report.updateTestLog("Primary Beneficiary Information", dob + " is entered in DOB", Status.DONE);	
	}*/
	
	
	/*public void selectGender(String gender, WebElement male, WebElement female)
	{
		if(gender.equalsIgnoreCase("Male"))
		{
			ClickElement(male, "Male");
		}
		else
		{
			ClickElement(female, "Female");
		}
	}*/

}
