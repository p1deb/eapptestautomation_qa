package com.cognizant.BusinessComponents;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;

public class CoverageAndRidersPage extends Command{

	public CoverageAndRidersPage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
		
	@FindBy (id = "flda_57")
	WebElement txtOtherDividendOption;	
	
	@FindBy (id = "flda_166")
	WebElement txtIndexedDividendAllocation;
	
	@FindBy (id = "rdo_93_1")
	WebElement radioAutomaticPremiumLoan_Yes;
	
	@FindBy (id = "rdo_93_2")
	WebElement radioAutomaticPremiumLoan_No;
	
	@FindBy (id = "rdo_90_1")
	WebElement radioQualifiedRetirementPlan_Yes;
	
	@FindBy (id = "rdo_90_2")
	WebElement radioQualifiedRetirementPlan_No;
	
	@FindBy (id = "cb_87")
	WebElement checkboxWaiverofPremiumforDisability;
	
	@FindBy (id = "cb_83")
	WebElement checkboxBlendedInsuranceRider;
	
	@FindBy (id = "cb_71")
	WebElement checkboxAcceleratorPaidUpAdditionsRider;
	
	@FindBy (id = "cb_75")
	WebElement checkboxBlendAccelerator;
	
	@FindBy (id = "cb_65")
	WebElement checkboxSameInsuredTerm;
	
	@FindBy (id = "cb_64")
	WebElement checkboxOtherInsuredTerm;
	
	@FindBy (id = "cb_85")
	WebElement checkboxChildrenBenefitRider;
	
	@FindBy (id = "cb_86")
	WebElement checkboxGuaranteedInsurabilityOptionRider;
	
	@FindBy (id = "cb_84")
	WebElement checkboxCreditofPaidUpAdditionsforDisability;
	
	@FindBy (id = "flda_162")
	WebElement txtMonthlyBenefitAmount;
	
	@FindBy (id = "cb_123")
	WebElement checkboxSurvivorPurchaseOption;
	
	@FindBy (id = "cb_121")
	WebElement checkboxOtherRiders;
	
	@FindBy (id = "rdo_12_1")
	WebElement radioOwnOccupation_2Years;
	
	@FindBy (id = "rdo_12_2")
	WebElement radioOwnOccupation_5Years;
	
	@FindBy (id = "flda_150")
	WebElement txtFaceAmount_BIR;
	
	@FindBy (id = "flda_154")
	WebElement txtBilled_Modal_Premium_APUAR;
	
	@FindBy (id = "flda_152")
	WebElement txt1035SinglePremium_APUAR;;
	
	@FindBy (id = "flda_153")
	WebElement txtNon1035SinglePremium_APUAR;;
	
	@FindBy (id = "flda_156")
	WebElement txtBilled_Modal_Premium_BA;
	
	@FindBy (id = "flda_155")
	WebElement txt1035SinglePremium_BA;
	
	@FindBy (id = "flda_157")
	WebElement txtNon1035SinglePremium_BA;
	
	@FindBy (id = "flda_160")
	WebElement txtAmount_SIR;	
	
	@FindBy (xpath = "//div[@id='div_21']/select")
	WebElement drpdownGuaranteePeriod_SIR;

	@FindBy (id = "rdo_8_1")
	WebElement radioWaiverConversionOption_Yes;
	
	@FindBy (id = "rdo_8_2")
	WebElement radioWaiverConversionOption_No;
	
	@FindBy (id = "flda_164")
	WebElement txtAmount_OIR;
	
	@FindBy (xpath = "//div[@id='div_22']/select")
	WebElement drpdownGuaranteePeriod_OIR;
	
	@FindBy (xpath = "//div[@id='div_62']/select")
	WebElement drpdownUnits;
	
	@FindBy (id = "flda_163")
	WebElement txtAmount_GIO;
	
	@FindBy (id = "flda_161")
	WebElement txtFaceAmount_SPO;
	
	@FindBy (id = "flda_129")
	WebElement txtFN_InsuredBene;
	
	@FindBy (id = "flda_131")
	WebElement txtMI_InsuredBene;
	
	@FindBy (id = "flda_133")
	WebElement txtLN_InsuredBene;
	
	@FindBy (id = "flda_122")
	WebElement txtOther;
	
	@FindBy (id = "")
	WebElement txt;
	
	String tableName = "WholeLifeInsurance";
	String strOther = userData.getData(tableName, "OtherDividendOption");
	String strIndexedDividendAllocation = userData.getData(tableName, "IndexedDividendAllocation%");
	String strOwnOccupation = userData.getData(tableName, "OwnOccupation");
	String strFaceAmount_BIR = userData.getData(tableName, "FaceAmount_BIR");
	String strBilledModalPremium_APUAR = userData.getData(tableName, "Billed(Modal)Premium_APUAR");
	String str1035SinglePremium_APUAR = userData.getData(tableName, "1035SinglePremium_APUAR");
	String strNon1035SinglePremium_APUAR = userData.getData(tableName, "Non1035SinglePremium_APUAR");
	String strBilledModalPremium_BA = userData.getData(tableName, "Billed(Modal)Premium_BA");
	String str1035SinglePremium_BA = userData.getData(tableName, "1035SinglePremium_BA");
	String strNon1035SinglePremium_BA = userData.getData(tableName, "Non_1035SinglePremium_BA");
	String strAmount_OIR = userData.getData(tableName, "Amount_OIR");
	String strGuaranteedPeriod_OIR = userData.getData(tableName, "GuaranteedPeriod_OIR");
	String strAmount_SIR = userData.getData(tableName, "Amount_SIR");
	String strGuaranteedPeriod_SIR = userData.getData(tableName, "GuaranteedPeriod_SIR");
	String strAmount_GIO = userData.getData(tableName, "Amount_GIO");			
	String strWaiverofPremiumDisabilityRider = userData.getData(tableName, "WaiverofPremiumDisabilityRider(WPD)");
	String strWaiverConversionOption = userData.getData(tableName, "WaiverConversionOption?");
	String strUnits = userData.getData(tableName, "Units");
	String strFaceAmount_SPO = userData.getData(tableName, "FaceAmount_SPO");
	String strFirstName_SPO = userData.getData(tableName, "FirstName_SPO");
	String strMI_SPO = userData.getData(tableName, "MI_SPO");
	String strLastName_SPO = userData.getData(tableName, "LastName_SPO");
	String strMonthlyBenefitAmount_CPUAD = userData.getData(tableName,  "CreditOfPaidUpAdditionsForDisability_PUADR");
	
	public void dividendOption(String dividendOption)
	{
		if(dividendOption.equalsIgnoreCase("Other"))
		{			
			EnterText(txtOtherDividendOption, strOther, "Other Dividend Option");
		}
		if((dividendOption.equalsIgnoreCase("Accumulate at Interest (Opt 2)"))||(dividendOption.equalsIgnoreCase("Paid-Up Additions (Opt 4)")))
		{
			EnterText(txtIndexedDividendAllocation, strIndexedDividendAllocation, "Indexed Dividend Allocation %");
		}
	}
	
	public void automaticPremiumLoan(String automaticPremiumLoan)
	{
		if(automaticPremiumLoan.equalsIgnoreCase("Yes"))
		{
			ClickElement(radioAutomaticPremiumLoan_Yes, "Automatic Premium Loan_Yes");
		}
		else if(automaticPremiumLoan.equalsIgnoreCase("No"))
		{
			ClickElement(radioAutomaticPremiumLoan_No, "Automatic Premium Loan_No");	
		}
	}
	
	public void qualifiedRetirementPlan(String qualifiedRetirementPlan)
	{
		if(qualifiedRetirementPlan.equalsIgnoreCase("Yes"))
		{
			ClickElement(radioQualifiedRetirementPlan_Yes, "Qualified Retirement Plan_Yes");
		}
		else if(qualifiedRetirementPlan.equalsIgnoreCase("No"))
		{
			ClickElement(radioQualifiedRetirementPlan_No, "Qualified Retirement Plan_No");	
		}
	}
	
	public void waiverofPremiumforDisability(String waiverofPremiumforDisability)
	{
		if(waiverofPremiumforDisability.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxWaiverofPremiumforDisability, "Waiver of Premium for Disability (WPD)");
			if(strOwnOccupation.contains("2"))
			{
				ClickElement(radioOwnOccupation_2Years, "2 Years");
			}
			else if(strOwnOccupation.contains("5"))
			{
				ClickElement(radioOwnOccupation_5Years, "5 Years");
			}
				
		}
	}
	
	public void blendedInsuranceRider(String blendedInsuranceRider)
	{
		if(blendedInsuranceRider.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxBlendedInsuranceRider, "Blended Insurance Rider (BIR)");
			EnterText(txtFaceAmount_BIR, strFaceAmount_BIR, "Face Amount");
			
		}
	}
	
	public void acceleratorPaidUpAdditionsRider(String acceleratorPaidUpAdditionsRider)
	{
		if(acceleratorPaidUpAdditionsRider.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxAcceleratorPaidUpAdditionsRider, "Accelerator Paid Up Additions Rider");
			EnterText(txtBilled_Modal_Premium_APUAR, strBilledModalPremium_APUAR, "Billed(Modal)Premium_APUAR");
			EnterText(txt1035SinglePremium_APUAR, str1035SinglePremium_APUAR, "1035SinglePremium_APUAR");
			EnterText(txtNon1035SinglePremium_APUAR, strNon1035SinglePremium_APUAR, "Non1035SinglePremium_APUAR");			
		}
	}
	
	public void blendAccelerator(String blendAccelerator)
	{
		if(blendAccelerator.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxBlendAccelerator, "Blend Accelerator");
			
			EnterText(txtBilled_Modal_Premium_BA, strBilledModalPremium_BA, "Billed(Modal)Premium_BA");
			EnterText(txt1035SinglePremium_BA, str1035SinglePremium_BA, "1035SinglePremium_BA");
			EnterText(txtNon1035SinglePremium_BA, strNon1035SinglePremium_BA, "Non1035SinglePremium_BA");	
		}
	}
	
	public void sameInsuredTerm(String sameInsuredTerm)
	{
		if(sameInsuredTerm.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxSameInsuredTerm, "Same Insured Term (SIR)");
			EnterText(txtAmount_SIR, strAmount_SIR, "Amount");
			ComboSelectValue(drpdownGuaranteePeriod_SIR, strGuaranteedPeriod_SIR, "Guaranteed Period");
			if(strWaiverofPremiumDisabilityRider.equalsIgnoreCase("Yes"))
			{
				if(strWaiverConversionOption.equalsIgnoreCase("Yes"))
				{
					ClickElement(radioWaiverConversionOption_Yes, "Waiver Conversion Option - Yes");
				}
				else if(strWaiverConversionOption.equalsIgnoreCase("No"))
				{
					ClickElement(radioWaiverConversionOption_No, "Waiver Conversion Option - No");
				}
			}
			
			
		}
	}
	
	public void otherInsuredTerm(String otherInsuredTerm)
	{
		if(otherInsuredTerm.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxOtherInsuredTerm, "Other Insured Term (OIR)");
			EnterText(txtAmount_OIR, strAmount_OIR, "Amount");
			ComboSelectValue(drpdownGuaranteePeriod_OIR, strGuaranteedPeriod_OIR, "Guaranteed Period");
		}
	}
	
	public void childrenBenefitRider(String childrenBenefitRider)
	{
		if(childrenBenefitRider.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxChildrenBenefitRider, "Children's Benefit Rider (CBR)");
			ComboSelectValue(drpdownUnits, strUnits, "Units");
		}
	}
	
	public void guaranteedInsurabilityOptionRider(String guaranteedInsurabilityOptionRider)
	{
		if(guaranteedInsurabilityOptionRider.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxGuaranteedInsurabilityOptionRider, "Guaranteed Insurability Option Rider (GIO)");
			EnterText(txtAmount_GIO, strAmount_GIO, "Amount");
		}
	}
	
	public void creditofPaidUpAdditionsforDisability(String creditofPaidUpAdditionsforDisability)
	{
		if(creditofPaidUpAdditionsforDisability.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxCreditofPaidUpAdditionsforDisability, "Credit of Paid Up Additions for Disability (CPUAD) ");
			EnterText(txtMonthlyBenefitAmount, strMonthlyBenefitAmount_CPUAD, "Monthly Benefit Amount $");
		}
	}
	
	public void survivorPurchaseOption(String survivorPurchaseOption)
	{
		if(survivorPurchaseOption.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxSurvivorPurchaseOption, "Survivor Purchase Option (SPO)");
			
			EnterText(txtFaceAmount_SPO, strFaceAmount_SPO, "Face Amount");
			EnterText(txtFN_InsuredBene, strFirstName_SPO, "First Name");
			EnterText(txtMI_InsuredBene, strMI_SPO, "Middle Name");
			EnterText(txtLN_InsuredBene, strLastName_SPO, "Last Name");
		}
	}
	
	public void otherRiders(String otherRiders)
	{
		if(otherRiders.equalsIgnoreCase("Yes"))
		{
			ClickElement(checkboxOtherRiders, "Other");
			String strOther = userData.getData(tableName, "OtherRider");
			EnterText(txtOther, strOther, "Other");
		}
	}
	
	
	
	
	
}
