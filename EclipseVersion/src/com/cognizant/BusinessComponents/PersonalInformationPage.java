package com.cognizant.BusinessComponents;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;

public class PersonalInformationPage extends Command{

	public PersonalInformationPage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
	
	@FindBy (id ="rdo_113_1")
	WebElement radio_PI_UsedNicotine_Tobacco_Yes;
	
	@FindBy (id ="rdo_113_2")
	WebElement radio_PI_UsedNicotine_Tobacco_No;
	
	@FindBy (id ="rdo_112_1")
	WebElement radio_OI_UsedNicotine_Tobacco_Yes;
	
	@FindBy (id ="rdo_112_2")
	WebElement radio_OI_UsedNicotine_Tobacco_No;
	
	@FindBy (id ="rdo_103_1")
	WebElement radio_SPOInsd_UsedNicotine_Tobacco_Yes;
	
	@FindBy (id ="rdo_103_2")
	WebElement radio_SPOInsd_UsedNicotine_Tobacco_No;
	
	@FindBy (id ="btn_102")
	WebElement btnDetails;
	
	@FindBy (id ="rdo_14_1")
	WebElement radioPresentUse;
	
	@FindBy (id ="rdo_14_2")
	WebElement radioFormerUse;
	
	@FindBy (id ="flda_3")
	WebElement txtType;
	
	@FindBy (id ="flda_17")
	WebElement txtMonth_Year;
	
	@FindBy (id ="btn_9")
	WebElement btnSave;
	
	@FindBy (id ="btn_109")
	WebElement btnPrimaryInsured;	
	
	@FindBy (id ="btn_108")
	WebElement btnOtherInsured;
	
	String sheetName = "WholeLifeInsurance";
	
	public void nicotineUsed(String used, WebElement yes, WebElement no)
	{
		if(used.equalsIgnoreCase("Yes"))
		{
			ClickElement(yes, "Yes");
		}
		else if(used.equalsIgnoreCase("No"))
		{
			ClickElement(no, "No"); 
		}
	}
	
	public void proposedInsdUsedNicotine_Tobacco(String used)
	{
		nicotineUsed(used, radio_PI_UsedNicotine_Tobacco_Yes, radio_PI_UsedNicotine_Tobacco_No);
		if(used.equalsIgnoreCase("Yes"))
		{
			ClickElement(btnPrimaryInsured, "Primary Insured");
			wait.waitForElementToDisplay(Driver, radioPresentUse);
			if(radioPresentUse.isDisplayed())
			{
				String strUser = userData.getData(sheetName, "TobaccoUse_Present Use/Former Use_PrimaryInsured");
				String strType = userData.getData(sheetName, "NicotineType_PrimaryInsured");
				String strMonth_Year = userData.getData(sheetName, "Quit_MonthOrYear_PrimaryInsured");
				tobaccoDetails(strUser, strType, strMonth_Year);				
			}
		}
	}
	
	public void otherInsdUsedNicotine_Tobacco(String used)
	{
		nicotineUsed(used, radio_OI_UsedNicotine_Tobacco_Yes, radio_OI_UsedNicotine_Tobacco_No);
		if(used.equalsIgnoreCase("Yes"))
		{
			ClickElement(btnOtherInsured, "Other Insured");
			wait.waitForElementToDisplay(Driver, radioPresentUse);
			if(radioPresentUse.isDisplayed())
			{
				String strUser = userData.getData(sheetName, "TobaccoUse_Present Use/Former Use_OtherInsured");
				String strType = userData.getData(sheetName, "NicotineType_OtherInsured");
				String strMonth_Year = userData.getData(sheetName, "Quit_MonthOrYear_OtherInsured");
				tobaccoDetails(strUser, strType, strMonth_Year);				
			}
		}
	}
	
	public void spoInsdUsedNicotine_Tobacco(String used)
	{
		nicotineUsed(used, radio_SPOInsd_UsedNicotine_Tobacco_Yes, radio_SPOInsd_UsedNicotine_Tobacco_No);
		if(used.equalsIgnoreCase("Yes"))
		{
			ClickElement(btnDetails, "SPO Insured");
			wait.waitForElementToDisplay(Driver, radioPresentUse);
			if(radioPresentUse.isDisplayed())
			{
				String strUser = userData.getData(sheetName, "TobaccoUse_Present Use/Former Use_SPOInsured");
				String strType = userData.getData(sheetName, "NicotineType_SPOInsured");
				String strMonth_Year = userData.getData(sheetName, "Quit_MonthOrYear_SPOInsured");
				tobaccoDetails(strUser, strType, strMonth_Year);				
			}
		}
		
	}
	
	public void tobaccoDetails(String use, String type, String monthYear)
	{	
		
		if(use.equalsIgnoreCase("Present Use"))
		{
			ClickElement(radioPresentUse, "Present Use");
			EnterText(txtType, type, "Type of Tobacco or Nicotine Used");
		}
		else if(use.equalsIgnoreCase("Former Use"))
		{
			ClickElement(radioFormerUse, "Former Use"); 
			EnterText(txtType, type, "Type of Tobacco or Nicotine Used");
			EnterText(txtMonth_Year, monthYear, "Quit - Month/Year");			
		}
	}
	
}
