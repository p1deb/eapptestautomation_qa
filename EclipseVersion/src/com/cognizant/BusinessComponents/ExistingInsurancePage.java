package com.cognizant.BusinessComponents;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.UnCaughtException;

public class ExistingInsurancePage extends Command{

	public ExistingInsurancePage(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
WebElement element;
	
	@FindBy (id = "rdo_19_1")
	WebElement radioExistingIns_Yes;	
	
	@FindBy (id = "rdo_19_2")
	WebElement radioExistingIns_No;
	
	@FindBy (id = "rdo_28_1")
	WebElement radioReplacingExistingIns_Yes;
	
	@FindBy (id = "rdo_28_2")
	WebElement radioReplacingExistingIns_No;
	
	@FindBy (id = "rdo_17_1")
	WebElement radioUsingExistingFunds_Yes;
	
	@FindBy (id = "rdo_17_2")
	WebElement radioUsingExistingFunds_No;
	
	@FindBy (id = "rdo_18_1")
	WebElement radioConsiderDiscount_Yes;
	
	@FindBy (id = "rdo_18_2")
	WebElement radioConsiderDiscount_No;
	
	@FindBy (id = "rdo_66_1")
	WebElement radioApplicationPending_Yes;
	
	@FindBy (id = "rdo_66_2")
	WebElement radioApplicationPending_No;
	
	@FindBy (id = "rdo_19_1")
	WebElement radioSoldPolicy_Yes;
	
	@FindBy (id = "rdo_19_1")
	WebElement radioSoldPolicy_No;
	
	@FindBy (name = "rdo_69_1")
	WebElement radioReadReplacementNotice_Yes;
	
	@FindBy (name = "rdo_69_2")
	WebElement radioReadReplacementNotice_No;
	
	@FindBy (id = "flda_67")
	WebElement txtOthIns_ReaForRepl;
	
	@FindBy(id= "rdo_33_1")
	WebElement radioReplaced_Yes;
	
	@FindBy(id= "rdo_33_2")
	WebElement radioReplaced_No;
	
	@FindBy(id= "rdo_56_1")
	WebElement radio1035Exchange_Yes;
	
	@FindBy(id= "rdo_56_2")
	WebElement radio1035Exchange_No;
	
	@FindBy(id= "rdo_63_1")
	WebElement radioPolicyReplaced;
	
	@FindBy(id= "cb_66")
	WebElement checkboxOTHINS_1035_Option_Full;
	
	@FindBy(id= "rdo_46_3")
	WebElement radioPartialExchange;
	
	String dataTableName = "WholeLifeInsurance";
		
	
	public void existingInsurance()
	{
		String strExistingIns = userData.getData(dataTableName, "ExistingInsurance");
		String strReplacingExistingIns = userData.getData(dataTableName, "ReplacingExistingInsurance");
				
		if(strExistingIns.equalsIgnoreCase("Yes"))
		{
			ClickElement(radioExistingIns_Yes, "Existing Insurance - Yes");
			if(strReplacingExistingIns.equalsIgnoreCase("Yes"))
			{
				ClickElement(radioReplacingExistingIns_Yes, "Replacing Existing Insurance - Yes");
				ClickElement(radioUsingExistingFunds_No, "Use Existing Funds - No");	
				ClickElement(radioConsiderDiscount_No, "Consider Discount - No");
				EnterText(txtOthIns_ReaForRepl, "NA", "Replacement Reason");
				try {
					executeTestCase("CreatePolicy", "EnterExistingInsuranceInformation");
				} catch (UnCaughtException e) {
					e.printStackTrace();
				}	
			}
			else if(strReplacingExistingIns.equalsIgnoreCase("No"))
			{
				ClickJSElement(radioReplacingExistingIns_No, "Replacing Existing Insurance_No");
				try {
					executeTestCase("CreatePolicy", "EnterExistingInsuranceInformation");
				} catch (UnCaughtException e) {
					e.printStackTrace();
				}				
			}
		}
		else if (strExistingIns.equalsIgnoreCase("No"))
		{
			ClickElement(radioExistingIns_No, "Existing Insurance - No");	
			ClickElement(radioApplicationPending_No, "Application Pending with this or other company - No");
			ClickElement(radioSoldPolicy_No, "Ever Sold Policy - No");
		}			
	}
	
	public void beingReplacedChanged(String addExistingInsDetails)
	{		
		String str1035Exchange = userData.getData("WholeLifeInsurance", "1035Exchange");
		if(addExistingInsDetails.equalsIgnoreCase("No"))
		{
			ClickElement(radioReplaced_No, "Replaced - No");
		}
		else if(addExistingInsDetails.equalsIgnoreCase("Yes"))
		{
			ClickElement(radioReplaced_Yes, "Replaced - Yes");
			ClickElement(radioPolicyReplaced, "Policy Replaced");
			if(str1035Exchange.equalsIgnoreCase("Yes"))
			{
				ClickElement(radio1035Exchange_Yes, "1035 Exchange - Yes");	
				ClickElement(checkboxOTHINS_1035_Option_Full, "Other Insurance_1035_Option_Full");
				ClickElement(radioPartialExchange, "Partial Exchange");
			}
			else if(str1035Exchange.equalsIgnoreCase("No"))
			{
				ClickElement(radio1035Exchange_No, "1035 Exchange - No");
				ClickElement(radioPolicyReplaced, "Policy Replaced");
			}
		}
	}

}
