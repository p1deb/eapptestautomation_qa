package com.cognizant.BusinessComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import com.cognizant.support.UnCaughtException;

public class OwnerInformation extends Command{

	public OwnerInformation(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
	
	String reusableScenarioName = "CreatePolicy";
	String reusableOwnerInfo = "OwnerInformation";
	String dataTableName = "WholeLifeInsurance";
	
	String strOwnerType = userData.getData(dataTableName, "Type of Owner");
	String strNumberOfOwners = userData.getData(dataTableName, "NumberOfMultipleOwners");
	/*String individual_UDMA = "MultipleOwnerDetails";
	String business_QualifiedPlan = "Business_QualifiedPlan";
	String establishedTrust = "EstablishedTrust";*/
	String strJointTenants = userData.getData(dataTableName, "JointTenantswithRightofSurvivorship");
	String strTenantsInCommon = userData.getData(dataTableName, "TenantsInCommon");
	

	String strDesignatedRepresentative = userData.getData(dataTableName, "DesignatedRepresentative");
	String strAnyOwner = userData.getData(dataTableName, "AnyOwner");
	String strAllOwner = userData.getData(dataTableName, "AllOwner");
	String strRepresentativeName = userData.getData(dataTableName, "RepresentativeName");
	
	
	@FindBy (xpath = "//div[@id='div_44']/select")
	WebElement drpdownRelationshipToInsured;
	
	@FindBy (id = "flda_3")
	WebElement txtOtherDetails;
	
	@FindBy (id = "flda_15")
	WebElement txtFirstName;
	
	@FindBy (id = "flda_17")
	WebElement txtLastName;
	
	@FindBy (id = "flda_26")
	WebElement txtSSN;	

	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[1]")
	WebElement txtMonth;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[2]")
	WebElement txtDay;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[3]")
	WebElement txtYear;	
	
	@FindBy (id = "lbl_54_1")
	WebElement radioGender_Male;
	
	@FindBy (id = "lbl_54_2")
	WebElement radioGender_Female;
	
	@FindBy (id = "flda_2")
	WebElement txtPhoneNumber ;
	
	@FindBy (xpath = "//div[@id='div_70']/select")
	WebElement drpdownBusinessType;
	
	@FindBy (xpath = "//div[@id='div_72']/select")
	WebElement drpdownInsurableInterest;
	
	@FindBy (id = "flda_59")
	WebElement txtEntityName;
	
	@FindBy (id = "flda_81")
	WebElement txtTrust;
	
	@FindBy (id = "flda_26")
	WebElement txtTIN;
	
	@FindBy (id = "flda_80")
	WebElement txtFirstName_Custodian;
	
	@FindBy (id = "flda_79")
	WebElement txtLastName_Custodian;
	
	@FindBy (id = "lb_60")
	WebElement drpdownCustodianState;	
	
	@FindBy (id = "cb_5")
	WebElement ckeckboxSame;
	
	@FindBy (id = "rdo_91_1")
	WebElement radioJointTenants;
	
	@FindBy (id = "rdo_91_2")
	WebElement radioCommonTenants;
	
	@FindBy (id = "rdo_64_1")
	WebElement radioMultipleOwners_Yes;
	
	@FindBy (id = "rdo_64_2")
	WebElement radioMultipleOwners_No;
	
	@FindBy (xpath = "//tr[@id='addRowRow']/td[@class='btn-primary']")
	WebElement btnAdd;
	
	@FindBy (id = "flda_15")
	WebElement txtOwnershipPercent;
	
	@FindBy (id = "btn_13")
	WebElement btnSave;	
	
	@FindBy (id = "cb_13")
	WebElement checkboxAnyRep;
	
	@FindBy (xpath = "//span[@input='cb_15']")
	WebElement checkboxAnyOwner;
	
	@FindBy (xpath = "//span[@input='cb_14']")
	WebElement checkboxAllOwner;
	
	@FindBy (id = "flda_6")
	WebElement txtRepresentativeName;
	
	@FindBy(id = "btn_12")
	WebElement btnNext;
	
	@FindBy(id = "btn_10")
	WebElement btnNext_MultipleOwners;
	
	@FindBy(id = "flda_48")
	WebElement txtFirst;
	
	String strRelationship = userData.getData(dataTableName, "RelationshipToInsured");
	String strOtherDetails = userData.getData(dataTableName, "OtherDetails");
	String strFirstName = userData.getData(dataTableName, "FirstName_Owner");
	String strLastName = userData.getData(dataTableName, "LastName_Owner");
	String strSSN = userData.getData(dataTableName, "SSN_Owner");
	String strDOB = userData.getData(dataTableName, "DateOfBirth_Owner");
	String strgender = userData.getData(dataTableName, "Gender_Owner");
	String strBuinessType = userData.getData(dataTableName, "BuinessType");
	String strInsurableInterest = userData.getData(dataTableName, "InsurableInterest");
	String strTIN = userData.getData(dataTableName, "TIN_Owner");
	String strTrust = userData.getData(dataTableName, "Trust");
	String strDateOfTrust = userData.getData(dataTableName, "DateOfTrust");
	String strEntityName = userData.getData(dataTableName, "EntityName");
	String strCustodianState = userData.getData(dataTableName, "CustodianState");
	String strGender = userData.getData(dataTableName, "Gender");
	
	
	
	
	public void selectOwnerType(String ownerType)
	{		
		switch (ownerType)
		{
		case "Individual":
		{
			/*String strRelationship = userData.getData(dataTableName, "RelationshipToInsured");
			String strOtherDetails = userData.getData(dataTableName, "OtherDetails");
			String strFirstName = userData.getData(dataTableName, "FirstName");
			String strLastName = userData.getData(dataTableName, "LastName");
			String strSSN = userData.getData(dataTableName, "SSN");
			String strDOB = userData.getData(dataTableName, "DateOfBirth");
			String strgender = userData.getData(dataTableName, "Gender");*/
			ComboSelectValue(drpdownRelationshipToInsured, strRelationship, "Relationship to Proposed Insured");
			if(strRelationship.equalsIgnoreCase("Other"))
			{
				EnterText(txtOtherDetails, strOtherDetails, "Other Details");
			}
			EnterText(txtFirstName, strFirstName, "First Name");
			EnterText(txtLastName, strLastName, "Last Name");
			EnterText(txtSSN, strSSN, "SSN");
			enterDOB_OwnerInformation(strDOB);
			enterGender(strgender);
			
			break;
		}
		case "Business":
		{
			/*String strBuinessType = userData.getData(dataTableName, "BuinessType");
			String strInsurableInterest = userData.getData(dataTableName, "InsurableInterest");
			String strEntityName = userData.getData(dataTableName, "EntityName");
			String strTIN = userData.getData(dataTableName, "TIN");*/
			
			ComboSelectValue(drpdownBusinessType, strBuinessType, "Business Type");
			ComboSelectValue(drpdownInsurableInterest, strInsurableInterest, "Insurable Interest");
			EnterText(txtEntityName, strEntityName, "Entity Name");
			EnterText(txtTIN, strTIN, "TIN");		
			break;
		}
		case "Established Trust":
		{
			/*String strTrust = userData.getData(dataTableName, "Trust");
			String strTIN = userData.getData(dataTableName, "TIN");
			String strDateOfTrust = userData.getData(dataTableName, "DateOfTrust");*/
			
			EnterText(txtTrust, strTrust, "Trust");
			EnterText(txtTIN, strTIN, "TIN");
			enterDOB_OwnerInformation(strDateOfTrust);
			break;
		}
		case "Qualified Plan":
		{
			/*String strEntityName = userData.getData(dataTableName, "EntityName");
			String strTIN = userData.getData(dataTableName, "TIN");
			*/
			EnterText(txtEntityName, strEntityName, "Entity Name");
			EnterText(txtTIN, strTIN, "TIN");
			break;
		}
		case "UGMA/UTMA":
		{
			/*String strFirstName = userData.getData(dataTableName, "FirstName");
			String strLastName = userData.getData(dataTableName, "LastName");
			String strSSN = userData.getData(dataTableName, "SSN");
			String strCustodianState = userData.getData(dataTableName, "CustodianState");
			String strGender = userData.getData(dataTableName, "Gender");*/
			
			EnterText(txtFirstName_Custodian, strFirstName, "First Name");
			EnterText(txtLastName_Custodian, strLastName, "Last Name");
			EnterText(txtSSN, strSSN, "SSN");
			ComboSelectValue(drpdownCustodianState, strCustodianState, "CustodianState");
			enterGender(strGender);			
			break;
		}
		}
	}	

	/*public void enterDOB_OwnerInformation(String dob)
	{
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		txtMonth.sendKeys(strMonth);
		txtYear.sendKeys(strYear);
		txtDay.sendKeys(strDay);
			
		Report.updateTestLog("Date of Birth", dob + " is entered in DOB", Status.DONE);	
	}*/
	
	public void enterDOB_OwnerInformation(String dob)
	{
		enterDOB(dob, txtMonth, txtDay, txtYear);
	}
	
	
	
	public void enterGender(String gender)
	{
		if(gender.equalsIgnoreCase("Female"))
		{
			ClickElement(radioGender_Female, "Female");
		}
		else if(gender.equalsIgnoreCase("Male"))
		{
			ClickElement(radioGender_Male, "Male");
		}
	}
	
	public void contactInfo(String contactInfo)
	{
		try {
		if(contactInfo.equalsIgnoreCase("Yes"))
		{
			ClickElement(ckeckboxSame, "Owner address same as the proposed insured");
			String strPhoneNumber = userData.getData("WholeLifeInsurance", "OwnerPhoneNumber");
			EnterText(txtPhoneNumber, strPhoneNumber, "Phone Number");
		}
		else if(contactInfo.equalsIgnoreCase("No"))
		{
			executeTestCase(reusableOwnerInfo, "OwnerContactInformation");			
		}
		} catch (UnCaughtException e) {
			e.printStackTrace();
		}		
	}
	
	public void multipleOwner(String multipleOwner)
	{
		if(multipleOwner.equalsIgnoreCase("Yes"))
		{
			ClickElement(radioMultipleOwners_Yes, "Multiple Owners - Yes");
			tenantsJointOrCommon();
			ClickElement(btnNext, "Next");
			enterMultipleOwnersDetails();
		}
		else if(multipleOwner.equalsIgnoreCase("No"))
		{
			ClickElement(radioMultipleOwners_No, "Multiple Owners - No");
			ClickElement(btnNext, "Next");
		}
	}
	
	public void tenantsJointOrCommon()
	{		
		/*String strJtTenants = userData.getData(dataTableName, "JointTenantswithRightofSurvivorship");
		String strCommonTenants = userData.getData(dataTableName, "TenantsInCommon");*/
		if(strJointTenants.trim().equalsIgnoreCase("Yes"))
		{
			ClickElement(radioJointTenants, "Joint Tenants with Right of Survivorship");
		}
		else if(strTenantsInCommon.trim().equalsIgnoreCase("Yes"))
		{
			ClickElement(radioCommonTenants, "Tenants in common");
		}
	}
	
	public void enterMultipleOwnersDetails()
	{
		try
		{
		switch (strOwnerType)
		{
		case "Individual":
		{
			readChoiceAloud();
			addMultipleOwnerInfo();			
			ClickElement(btnNext_MultipleOwners, "Next");
			break;
		}
		case "Business":
		{
			readChoiceAloud();
			executeTestCase(reusableOwnerInfo, "MultipleOwners_Business_QualifiedPlan");
			addMultipleOwnerInfo();	
			ClickElement(btnNext_MultipleOwners, "Next");
			break;
		}
		case "Established Trust":
		{
			readChoiceAloud();
			executeTestCase(reusableOwnerInfo, "MultipleOwners_EstablishedTrust");
			addMultipleOwnerInfo();	
			ClickElement(btnNext_MultipleOwners, "Next");
			break;
		}
		case "Qualified Plan":
		{
			readChoiceAloud();
			executeTestCase(reusableOwnerInfo, "MultipleOwners_Business_QualifiedPlan");
			addMultipleOwnerInfo();	
			ClickElement(btnNext_MultipleOwners, "Next");
			break;
		}
		case "UGMA/UTMA":
		{
			readChoiceAloud();
			addMultipleOwnerInfo();		
			ClickElement(btnNext_MultipleOwners, "Next");		
			break;
		}
		}
		} catch (UnCaughtException e) {
			e.printStackTrace();
		}
	}
	
	public void addMultipleOwnerInfo()
	{
		try{
		executeTestCase(reusableOwnerInfo, "MultipleOwner_Owner1");
		if(strTenantsInCommon.equalsIgnoreCase("Yes"))
		{	
			String strOwnershipPercent = userData.getData(dataTableName, "Ownership%");
			EnterText(txtOwnershipPercent, strOwnershipPercent, "Ownership");
		}			
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ClickElement(txtFirst, "First Name");
		ClickJSElement(btnSave, "Save");
		if(strNumberOfOwners.equalsIgnoreCase("2"))
		{
			//wait.waitForElementToDisplay(Driver, btnAdd);
			executeTestCase(reusableOwnerInfo, "MultipleOwner_Owner2");				
			if(strTenantsInCommon.equalsIgnoreCase("Yes"))
			{	
				String strOwnershipPercent = userData.getData(dataTableName, "Ownership%");
				EnterText(txtOwnershipPercent, strOwnershipPercent, "Ownership");
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			ClickElement(txtFirst, "First Name");
			ClickJSElement(btnSave, "Save");
		}		
		} catch (UnCaughtException e) {
			e.printStackTrace();
		}
		
	}
	
	public void readChoiceAloud()
	{
		
		wait.waitForPageLoad(Driver);
		if(strDesignatedRepresentative.equalsIgnoreCase("Yes"))
		{
			ClickJSElement(checkboxAnyRep, "Designated Representative");
			EnterText(txtRepresentativeName, strRepresentativeName, "Representative Name");
		}
				
		if(strAnyOwner.equalsIgnoreCase("Yes"))
		{
			ClickJSElement(checkboxAnyOwner, "Any Owner");
		}
		
		if(strAllOwner.equalsIgnoreCase("Yes"))
		{
			ClickJSElement(checkboxAllOwner, "All Owner");
		}
	}
	
}
