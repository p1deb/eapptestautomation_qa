package com.cognizant.BusinessComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cognizant.commands.Command;
import com.cognizant.core.CommandControl;
import com.cognizant.support.Status;
import com.cognizant.support.UnCaughtException;

public class USPAtrioticActOwnerIdentification extends Command{

	public USPAtrioticActOwnerIdentification(CommandControl cc) {
		super(cc);
		PageFactory.initElements(Driver, this);
	}
	
	WebElement element;
	
	@FindBy (xpath = "(//div[@id='div_70']/div/span/span/input)[1]")
	WebElement txtMonth;
	
	@FindBy (xpath = "//div[@id='div_70']/div/span/span/input[2]")
	WebElement txtDay;
	
	@FindBy (xpath = "//div[@id='div_70']/div/span/span/input[3]")
	WebElement txtYear;
	
	

	public void enterExpiryDate_OwnerIdentity(String dob)
	{
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		
		Driver.findElement(By.xpath("//span[@class='jq-dte-inner jq-dte-is-required']/input[1]")).sendKeys(strMonth);
		Driver.findElement(By.xpath("//span[@class='jq-dte-inner jq-dte-is-required']/input[2]")).sendKeys(strDay);
		Driver.findElement(By.xpath("//span[@class='jq-dte-inner jq-dte-is-required']/input[3]")).sendKeys(strYear);
		
		Report.updateTestLog("US Patriotic Act - Owner Identification", dob + " is entered in DOB", Status.DONE);		
	}
	
	
}
