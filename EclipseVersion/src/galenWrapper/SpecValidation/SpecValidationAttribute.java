/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package galenWrapper.SpecValidation;

import com.galenframework.page.Rect;
import com.galenframework.page.selenium.WebPageElement;
import com.galenframework.validation.PageValidation;
import com.galenframework.validation.ValidationErrorException;
import com.galenframework.validation.ValidationObject;
import com.galenframework.validation.ValidationResult;
import static java.util.Arrays.asList;

/**
 *
 * @author 394173
 */
public class SpecValidationAttribute extends SpecValidationTextWrapper<SpecAttribute> {

    @Override
    public ValidationResult check(PageValidation pageValidation, String objectName, SpecAttribute spec) throws ValidationErrorException {

        WebPageElement mainObject = (WebPageElement) pageValidation.findPageElement(objectName);

        checkAvailability(mainObject, objectName);

        Rect area = mainObject.getArea();
        String realText = mainObject.getWebElement().getAttribute(spec.getAtributeName());
        if (realText == null) {
            realText = "";
        }
        realText = applyOperationsTo(realText, spec.getOperations());
        checkValue(spec, objectName, realText, "Attribute \"" + spec.getAtributeName() + "\"", area);

        return new ValidationResult(spec,asList(new ValidationObject(area, objectName)));
    }

}
