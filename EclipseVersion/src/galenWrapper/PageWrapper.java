/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package galenWrapper;

import com.galenframework.page.PageElement;
import com.galenframework.page.selenium.SeleniumPage;
import com.galenframework.page.selenium.WebPageElement;
import com.galenframework.specs.page.Locator;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author 394173
 */
public class PageWrapper extends SeleniumPage {

    Map<String, WebElement> elementMap = new HashMap<>();
    private WebDriver driver;

    public PageWrapper(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public PageWrapper(WebDriver driver, Map<String, WebElement> elementMap) {
        this(driver);
        this.elementMap = elementMap;
    }

    public PageWrapper(WebDriver driver, String objectName, WebElement element) {
        super(driver);
        if (element != null) {
            this.elementMap.put(objectName, element);
        }
    }

    @Override
    public PageElement getObject(String objectName, Locator lctr) {
        return new WebPageElement(driver,objectName, elementMap.get(objectName), null);
    }

    public WebDriver getDriver() {
        return driver;
    }
}
