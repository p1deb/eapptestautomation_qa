/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package galenWrapper;

import com.galenframework.specs.page.Locator;
import com.galenframework.specs.page.PageSpec;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebElement;

/**
 *
 * @author 394173
 */
public class PageSpecWrapper extends PageSpec {

    Map<String, WebElement> elementMap = new HashMap<>();

    public void setObjectMap(Map<String, WebElement> elementMap) {
        this.elementMap = elementMap;
    }

    @Override
    public Locator getObjectLocator(String objectName) {
        return elementMap.containsKey(objectName) ? Locator.id("") : null;
    }

}
