/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package galenWrapper;

import com.galenframework.parser.ExpectRange;
import com.galenframework.parser.Expectations;
import com.galenframework.specs.Location;
import com.galenframework.specs.Range;
import com.galenframework.specs.colors.ColorRange;
import com.galenframework.parser.StringCharReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 394173
 */
public class Parser {

    public static Range parseRange(String Data) {
        return Data == null || Data.trim().isEmpty() ? Range.greaterThan(-1)
                : Expectations.range().read(new StringCharReader(Data));
    }

    public static Range parseRangePercent(String Data) {
        return Data == null || Data.trim().isEmpty() ? Range.greaterThan(-1)
                : getRange(Data);
    }

    private static Range getRange(String Data) {
        ExpectRange expectRange = new ExpectRange();
        expectRange.setEndingWord("%");
        return expectRange.read(new StringCharReader(Data));
    }

    public static List<Location> parseLocation(String Data) {
        return Data == null || Data.trim().isEmpty() ? new ArrayList<Location>() : Expectations.locations().read(new StringCharReader(Data));
    }

    public static List<ColorRange> parseColorRanges(String Data) {
        return Data == null || Data.trim().isEmpty() ? new ArrayList<ColorRange>() : Expectations.colorRanges().read(new StringCharReader(Data));
    }

    public static int parseInt(String Data) {
        return Data == null || Data.trim().isEmpty() ? 0 : parseInteger(Data);
    }

    public static int parseInt(Object Data) {
        return Data == null ? 0 : parseInt(Data.toString());
    }

    private static int parseInteger(String Data) {
        if (Data.matches("[0-9]+")) {
            return Integer.parseInt(Data);
        } else {
            return 0;
        }
    }

}
