/*
 * © Cognizant Technology Solutions 2014. All rights reserved. 
 *   Except for open source or proprietary third party software components
 *   embedded in this Cognizant proprietary software program ("Program"), this
 *   Program is protected by copyright laws, international treaties and other
 *   pending or existing intellectual property rights and statutes in India, the
 *   United States and other countries. Except as expressly permitted by Cognizant
 *   and its third party licensors, the Program may neither be used, reproduced,
 *   transmitted, distributed or modified, either in whole nor in part, in any
 *   manner or form whatsoever (including without limitation electronic,
 *   mechanical, printing, photocopying, recording or otherwise), without the
 *   prior, express, written consent and acknowledgment of Cognizant Technology
 *   Solutions. Any violation may result in severe civil and criminal penalties,
 */
package galenWrapper;

import com.galenframework.browser.SeleniumBrowser;
import com.galenframework.page.PageElement;
import com.galenframework.page.selenium.WebPageElement;
import com.galenframework.specs.Spec;
import com.galenframework.validation.PageValidation;
import com.galenframework.validation.SpecValidation;
import com.galenframework.validation.ValidationErrorException;
import com.galenframework.validation.ValidationFactory;
import com.galenframework.validation.ValidationResult;
import galenWrapper.SpecValidation.SpecAttribute;
import galenWrapper.SpecValidation.SpecTitle;
import galenWrapper.SpecValidation.SpecUrl;
import galenWrapper.SpecValidation.SpecValidationAttribute;
import galenWrapper.SpecValidation.SpecValidationTitle;
import galenWrapper.SpecValidation.SpecValidationUrl;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebElement;

/**
 *
 * @author 394173
 */
public class PageValidationWrapper extends PageValidation {

    Map<String, WebElement> elementMap = new HashMap<>();

    public PageValidationWrapper(PageWrapper page) {
        super(new SeleniumBrowser(page.getDriver()), page, new PageSpecWrapper(), null, null);
    }

    public PageValidationWrapper(PageWrapper page, Map<String, WebElement> elementMap) {
        this(page);
        this.elementMap = elementMap;
    }

    public PageValidationWrapper(PageWrapper page, String objectName, WebElement element) {
        this(page);
        if (element != null) {
            this.elementMap.put(objectName, element);
        }
    }

    @Override
    public ValidationResult check(String objectName, Spec spec) {
        ((PageSpecWrapper) this.getPageSpec()).setObjectMap(elementMap);
        SpecValidation<?> specValidation = ValidationFactoryWrapper.getValidation(spec, this);

        ValidationResult result = check(specValidation, objectName, spec);

        if (spec.isOnlyWarn()) {
            result.getError().setOnlyWarn(true);
        }
        return result;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private ValidationResult check(SpecValidation specValidation, String objectName, Spec spec) {
        try {
            return specValidation.check(this, objectName, spec);
        } catch (ValidationErrorException ex) {
            return ex.asValidationResult(spec);
        }
    }

    @Override
    public PageElement findPageElement(String objectName) {
        if (elementMap.get(objectName) != null) {
            return new WebPageElement(((PageWrapper) getPage()).getDriver(), objectName, elementMap.get(objectName), null);
        } else {
            return super.findPageElement(objectName);
        }
    }

}

class ValidationFactoryWrapper {

    public static SpecValidation<? extends Spec> getValidation(Spec spec, PageValidation pageValidation) {
        try {
            return ValidationFactory.getValidation(spec, pageValidation);
        } catch (Exception ex) {
            if (spec.getClass().equals(SpecUrl.class)) {
                return new SpecValidationUrl();
            } else if (spec.getClass().equals(SpecTitle.class)) {
                return new SpecValidationTitle();
            } else if (spec.getClass().equals(SpecAttribute.class)) {
                return new SpecValidationAttribute();
            }
            throw ex;
        }
    }
}
